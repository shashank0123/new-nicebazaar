<?php
use App\Product;
use App\OrderItem;
?>

@extends('front.app')

@section('title')
  @lang('registerHistory.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">@lang('breadcrumbs.front')</a></li>
    <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
    
    <li class="active">@lang('breadcrumbs.orderHistory')</li>
  </ul>
@stop

@section('content')
  <main>
    @include('front.include.sidebar')
    <div class="main-container">
      @include('front.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <input type="hidden" id="user_id" value="{{$member->id}}">
        <section class="tables-data">
          <div class="row">
            <div class="col-md-6">
              <h1><i class="fa fa-cart-arrow-down"></i> @lang('sidebar.purchaseHistoryTitle')</h1>
              {{--  <p class="lead">@lang('register.notice')</p> --}}
            </div>
            <div class="col-md-6">
              <form action="/en/searched-orders" method="GET" class="searchform" >
                <input type="text" class="form-set" name="product_keyword" style="font-size: 16px;" placeholder="Search for something">
                <button type="submit" class="btn btn-primary" style="padding: 1px 5px" >Search</button>
              </form>
            </div>
          </div>

          <div class="card">
            <div>
              <div class="datatables">
                <table class="table table-full" >
                  <thead>
                    <tr>
                      <th>@lang('orderhistory.serial')</th>
                      <th>@lang('orderhistory.products')</th>
                      <th>@lang('orderhistory.payment')</th>
                      <th>@lang('orderhistory.transactionId')</th>
                      <th>@lang('orderhistory.payableAmount')</th>
                      <th>@lang('orderhistory.status')</th>
                      <th>@lang('orderhistory.action')</th>
                      <th>@lang('orderhistory.date')</th>
                    </tr>
                  </thead>
                 <tbody id="showAll">
                @if($orders != null)
                @foreach($orders as $order)
                
                <?php $products = OrderItem::where('created_at',$order->created_at)->get(); ?>

                <tr>
                  <td><?php echo $count++.". " ?></td>
                  <td style="width: 20%">

                    @foreach($products as $product)
                    <?php $name=Product::where('id',$product->item_id)->first();

                    ?>
                    <?php echo $name->name.". " ?>
                    @endforeach 

                  </td>
                  <td>{{$order->payment_method}}</td>
                  <td>{{$order->transaction_id}}</td>
                  <td>Rs. {{$order->price}}</td>
                  <td>{{$order->status}}</td>
                  <td style="width: 20%">{{-- <form method="POST"> --}}<input type="button" name="search" onclick="returnOrder({{$order->id}},'{{$order->transaction_id}}','return');" class="btn btn-primary" style="padding: 1px 5px" value="Return">{{-- </form> --}}
                    {{-- <form method="POST"> --}}<input type="button" name="search" onclick="cancelOrder({{$order->id}},'{{$order->transaction_id}}','cancel');" class="btn btn-primary" style="padding: 1px 5px" value="Cancel">{{-- </form> --}}</td>
                    <td>{{$order->created_at}}</td>
                  </tr>
                  @endforeach

                  
                  @endif


                </tbody>

              </table>
                 @if($count==0)
                  <h3 style="text-align: center; padding-bottom: 20px">No orders currently.</h3>
                  @endif
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>



<script>
    function searchOrder(){
      alert('hii');
    }

    function returnOrder(id,txnID,action){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var userID = $('#user_id').val();
      
      var request = $.ajax({
        url: '/en/action-order',
        type: 'GET',
        dataType: 'JSON',
        data: {_token: CSRF_TOKEN, orderId: id, txnId: txnID, orderAction: action, userId: userID},    
       }); 

      request.done(function(data) {
        if (data.msg == 'Your request to return the order is sent'){          
          Swal(data.msg);
          setTimeout(function(){ window.location.href = "/en/member/my-orders"; }, 3000);
        }
        else
          Swal('failure');                  
      });

      request.fail(function(data) {
        Swal('Something Went Wrong');        
      });
    }

    function cancelOrder(id,txnID,action){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var userID = $('#user_id').val();
      var request = $.ajax({
        url: '/en/action-order',
        type: 'GET',
        dataType: 'JSON',
        data: {_token: CSRF_TOKEN, orderId: id, txnId: txnID, orderAction: action, userId: userID},    
       }); 

      request.done(function(data) {
        if (data.msg == 'Your request to cancel the order is sent'){          
          Swal(data.msg);
          setTimeout(function(){ window.location.href = "/en/member/my-orders"; }, 3000);
        }
        else
          Swal('failure');                  
      });

      request.fail(function(data) {
        Swal('Something Went Wrong');        
      });
    }
  </script>

@endsection