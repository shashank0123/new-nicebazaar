<?php 
use App\Models\Epoint;
use App\User;
$epoints = Epoint::where('member_id', $member->id)->first();
$user = User::where('id',$member->user_id)->first();

?>

<style>
  #close-nav { display: none; margin: 10px 30%;}
  @media screen and (max-width: 768px){
    #close-nav{ display: block; }
  }
</style>


<aside class="sidebar fixed" style="width:260px;left:0px;">

 <span onclick="hide_bar()"><i class="fa fa-close" id="close-nav"> &nbsp;&nbsp;Close</i></span>

 <div class="brand-logo" id="brand-logo" style="background-color: #ff0000">
  <div id="logo" style="padding: 0">
    <img src="{{ asset('/asset/images/menu/logo/logo.png') }}" width="100">
  </div>
</div>

<?php 
  use App\Models\MemberDetail;

  $profile = MemberDetail::where('member_id',$member->id)->first();
  ?>
  <style>
    #clickBro { display: none; }
    #file { display: none; }
  aside.sidebar { overflow-y: auto !important; overflow-x: hidden !important }
  </style>

<div class="user-logged-in">

  <div class="container">
    <form method="post" action="/upload-image" enctype="multipart/form-data" id="upload_image">
      <input type="hidden" name="member_id" id="uid" value="{{$member->id}}">
      @if($profile->profimage != null)
      <span id="uploaded_image">
        <img src="/assetsss/images/AdminProduct/{{$profile->profimage}}" width="150px" height="150px">
      </span>
      @else
      <span id="uploaded_image"></span>
      @endif
      <br>
      {{-- <img src="{{ asset('images/product/fashion-12.jpg') }}" style="width:100px;height:100px;border-radius:50%;margin: auto;display: block; margin-bottom:15px"> --}}
      <label for="file" style="cursor: pointer;margin-left: 15px; font-weight: bold;font-size: 18px;text-decoration: underline; ">U<span style="text-transform: lowercase !important;">pload</span> P<span style="text-transform: lowercase !important;">icture</span></label>
      <input type="file" name="file" id="file" onchange='submitMe()'/>
      <input type="submit" name="submit" id="clickBro" />
    </form>
  </div> 

  <h3 style="color: #fff; font-weight: bold; font-size: 20px">
        @if(!empty($user->first_name))
        {{strtoupper($user->first_name) }}
        @endif
        @if(!empty($user->last_name))
        {{strtoupper($user->last_name) }}
        @endif
      </h3>


  <div class="content">
    <input type="file" name="file" id="file" />
    <span id="uploaded_image"></span>
    {{-- <img src="{{ asset('images/product/fashion-12.jpg') }}" style="width:100px;height:100px;border-radius:50%;margin: auto;display: block; margin-bottom:15px"> --}}
    <div class="user-name">{{ $user->username }} <span class="text-muted f9" style="font-size:12px;color:#90caf9">(HL1{{sprintf("%05d", $member->user_id)}})</span></div>
    <div class="user-email">@lang('sidebar.package') - <span style="color:#fff;">Rs {{ number_format($member->package_amount, 0) }}</span></div>
    <div class="user-actions">
      <a class="m-r-5" href="{{ route('settings.account', ['lang' => \App::getLocale()]) }}">@lang('sidebar.settingsTopLabel')</a>
      <a href="{{ route('logout', ['lang' => \App::getLocale()]) }}">@lang('sidebar.logout')</a>
    </div>
  </div>
</div>

<ul class="menu-links">
  <li icon="md md-blur-on">
    <a href="/en/member"><i class="md md-blur-on"></i>&nbsp;<span>@lang('sidebar.home')</span></a>
  </li>

  <li>
    <a href="#" data-toggle="collapse" data-target="#MDMember" aria-expanded="false" aria-controls="MDMember" class="collapsible-header waves-effect"><i class="md md-accessibility"></i>&nbsp;@lang('sidebar.registerTitle')</a>
    <ul id="MDMember" class="collapse">
      <li>
        <a href="{{ route('member.register', ['lang' => \App::getLocale()]) }}">
          <span>@lang('sidebar.registerLink1')</span>
        </a>
      </li>
      <li>
        <a href="{{ route('member.registerHistory', ['lang' => \App::getLocale()]) }}">
          <span>@lang('sidebar.registerLink2')</span>
        </a>
      </li>
      <li>
        <a href="{{ route('member.upgrade', ['lang' => \App::getLocale()]) }}">
          <span>@lang('sidebar.registerLink3')</span>
        </a>
      </li>
    </ul>
  </li>

    {{-- <li>
      <a href="#" data-toggle="collapse" data-target="#Epin" aria-expanded="false" aria-controls="MDMember" class="collapsible-header waves-effect"><i class="md-lock-outline"></i>&nbsp;My Shoppy (Epin)</a>
      <ul id="Epin" class="collapse">
        
        <li>
          <a href="#">
            <span>Unused Epin</span>
          </a>
        </li>
        <li>
          <a href="#">
            <span>Used Epin</span>
          </a>
        </li>
      </ul>
    </li> --}}

 {{--    <li>
      <a href="#" data-toggle="collapse" data-target="#Epoint" aria-expanded="false" aria-controls="MDMember" class="collapsible-header waves-effect"><i class="md-control-point"></i>&nbsp;My Shoppy (Epoint)</a>
      <ul id="Epoint" class="collapse">
        <li>
          <a href="{{ route('member.epoint.allepoint') }}">
            <span>All Epoint</span>
          </a>
        </li>
      </ul>
    </li>
    --}}

    <li>
      <a href="#" data-toggle="collapse" data-target="#MDSettings" aria-expanded="false" aria-controls="MDSettings" class="collapsible-header waves-effect"><i class="md md-settings"></i>&nbsp;@lang('sidebar.settingsTitle')</a>
      <ul id="MDSettings" class="collapse">
        <li>
          <a href="{{ route('settings.account', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.settingsLink1')</span>
          </a>
        </li>
        <li>
          <a href="{{ route('settings.bank', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.settingsLink2')</span>
          </a>
        </li>

        <li>
          <a href="{{ route('settings.address', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.settingsLink3')</span>
          </a>
        </li>

      </ul>
    </li>

    <li>
      <a href="#" data-toggle="collapse" data-target="#MDGenealogy" aria-expanded="false" aria-controls="MDGenealogy" class="collapsible-header waves-effect"><i class="md md-swap-vert"></i>&nbsp;@lang('sidebar.networkTitle')</a>
      <ul id="MDGenealogy" class="collapse">
        <li>
          <a href="{{ route('network.binary', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.networkLink1')</span>
          </a>
        </li>
        {{-- <li>
          <a href="{{ route('network.unilevel', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.networkLink2')</span>
          </a>
        </li> --}}
      </ul>
    </li>

    <li>
      <a href="#" data-toggle="collapse" data-target="#income" aria-expanded="false" aria-controls="MDMember" class="collapsible-header waves-effect"><i class="md-receipt"></i>&nbsp;Income</a>
      <ul id="income" class="collapse">
        <li>
          <a href="{{ route('income.direct', ['lang' => \App::getLocale()]) }}">
            <span>Daily Payout Income</span>
          </a>
        </li>
        <li>
          <a href="#">
            <span>Repurchase Income</span>
          </a>
        </li><li>
          <a href="#">
            <span>Weekly Income</span>
          </a>
        </li>
        {{-- <li>
          <a href="#">
            <span>Allowance Income</span>
          </a>
        </li>
        <li>
          <a href="#">
            <span>Car Fund Income</span>
          </a>
        </li>
        <li>
          <a href="#">
            <span>House Fund Income</span>
          </a>
        </li>
        <li>
          <a href="#">
            <span>Reward Income</span>
          </a>
        </li> --}}
      </ul>
    </li>


    <li icon="md md-blur-on">
    <a href="/en"><i class="md md-add-shopping-cart"></i>&nbsp;<span>Shopping</span></a>
  </li>



    {{-- <li>
      <a href="#" data-toggle="collapse" data-target="#MDTrading" aria-expanded="false" aria-controls="MDTrading" class="collapsible-header waves-effect"><i class="md md-trending-up"></i>&nbsp;@lang('sidebar.sharesTitle')</a>
      <ul id="MDTrading" class="collapse">
        <li>
          <a href="{{ route('shares.market', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.sharesLink1')</span>
          </a>
        </li>
        <li>
          <a href="{{ route('shares.lock', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.sharesLink2')</span>
          </a>
        </li>
        <li>
          <a href="{{ route('shares.statement', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.sharesLink3')</span>
          </a>
        </li>
      </ul>
    </li> --}}

   {{--  <li>
      <a href="#" data-toggle="collapse" data-target="#MDTransaction" aria-expanded="false" aria-controls="MDTransaction" class="collapsible-header waves-effect"><i class="md md-shopping-cart"></i>&nbsp;@lang('sidebar.transactionTitle')</a>
      <ul id="MDTransaction" class="collapse">
        <li>
          <a href="{{ route('transaction.transfer', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.transactionLink1')</span>
          </a>
        </li>
        <li>
          <a href="{{ route('transaction.withdraw', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.transactionLink2')</span>
          </a>
        </li>
        <li>
          <a href="{{ route('transaction.statement', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.transactionLink3')</span>
          </a>
        </li>
      </ul>
    </li>
    --}}
   {{--  <li>
      <a href="#" data-toggle="collapse" data-target="#MDStatement" aria-expanded="false" aria-controls="MDStatement" class="collapsible-header waves-effect"><i class="md md-assessment"></i>&nbsp;@lang('sidebar.statementTitle')</a>
      <ul id="MDStatement" class="collapse">
        <li>
          <a href="{{ route('bonus.statement', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.statementLink1')</span>
          </a>
        </li>
        <li>
          <a href="{{ route('summary.statement', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.statementLink2')</span>
          </a>
        </li>
      </ul>
    </li> --}}

    {{-- <li>
      <a href="#" data-toggle="collapse" data-target="#MDCoin" aria-expanded="false" aria-controls="MDCoin" class="collapsible-header waves-effect"><i class="md md-album"></i>&nbsp;@lang('sidebar.coinTitle')</a>
      <ul id="MDCoin" class="collapse">
       <!-- <li>
          <a href="{{ route('coin.transaction', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.coinLink1')</span>
          </a>
        </li>-->
        <li>
          <a href="{{ route('coin.list', ['lang' => \App::getLocale()]) }}">
            <span>@lang('sidebar.coinLink2')</span>
          </a>
        </li>
      </ul>
    </li> --}}

    <li icon="md md-blur-on">
    <a href="{{ route('member.orders', ['lang' => \App::getLocale()]) }}"><i class="fa fa-cart-arrow-down"></i>&nbsp;<span>@lang('sidebar.orders')</span></a>
  </li>

    <?php
    $announcement = \Cache::remember('announcement.new', 60, function () {
      return \App\Models\Announcement::whereRaw('Date(created_at) = CURDATE()')->first();
    });
    ?>
    <li>
      <a href="{{ route('announcement.list', ['lang' => \App::getLocale()]) }}"><i class="md md-new-releases @if ($announcement) sidebar-has-notif @endif"></i>&nbsp;@lang('sidebar.announcementTitle')</a>
    </li>

    <li icon="md md-settings-power">
      <a href="{{ route('logout', ['lang' => \App::getLocale()]) }}"><i class="md md-settings-power"></i>&nbsp;<span>@lang('sidebar.logout')</span></a>
    </li>
  </ul>
</aside>

<aside class="sidebar-toggle hidden-xs hidden-sm">
  <button type="button" class="m-5" id="toggleNav" title="@lang('common.toggleNav')">
    <span class="sr-only">@lang('common.toggleNav')</span>
    <span class="md md-more-vert"></span>
  </button>
</aside>

@section('js-script')
<script>
  
  $(document).ready(function() {
    $('#upload_image').submit(function(e) {
      e.preventDefault();  
      $.ajax({  
        url: "/upload-image",  
        type: "POST",  
        dataType: 'JSON',
        data: new FormData(this),  
        contentType: false,  
        processData:false,  

        beforeSend:function(){
          $('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
        },

        success: function(data) {
              // alert('<img src="/assetsss/images/AdminProduct/'+data.image+'" width="120px" height="120px" />');
              var profile_image = data.image;
              // $('#uploaded_image').append(data.image);
              $('#uploaded_image').html('<img src="/assetsss/images/AdminProduct/'+data.image+'" width="150px" height="150px" />'); // show response from the php script.
            }
          });
    });
  });
  
  function submitMe() {
    $('#file').click();
    $("#clickBro").click();
  }

  function hide_bar() {
   $("#sidenav-overlay").click();
 }

</script>
@endsection