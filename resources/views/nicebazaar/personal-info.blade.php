<?php
use App\Address;
use App\User;
use App\State;
use App\Country;
use App\City;

$address=Address::where('user_id',$user->id)->first();

$cities = City::where('state_id',$address->state_id)->get();
?>
@extends('layouts.nicebazaar1')

<style type="text/css">
  .textpolicy{ background-color: white; margin-top: 20px; margin-bottom: 100px; text-align: justify; }
  .textpolicy p{font-weight: 100px; padding: 0px 10px  0px 10px ; font-size: 15px;
    line-height: 1.5; font-family: "Lato", sans-serif;     color: #595959; letter-spacing: 0.5px; text-align: justify;}
    .textpolicy h4{ text-align: center; margin-top: 20px; margin-bottom: 10px; font-size: 28px; font-weight:600 }
    .textpolicy h5{padding: 0px 30px  0px 30px ; font-size: 20px ; font-weight: 600; }
    ol {list-style-type:decimal; margin-left:  20px}
    ol li { line-height: 1.8; color: #595959; font-size: 15px}

    /*Nanhi kali CSS*/
    #donation,#password{ display: none; }
    #profile {  border : 1px solid #ccc; }
    #row-left{ border-right: 1px solid #ccc; }
    #row-left ul { background-color: #ff0000 ; color: #fff; cursor: pointer; font-size: 18px; margin: 15px 5px; }
    #row-left a { color: #fff; }
    #row-left li { padding: 20px; border-bottom: 1px solid #fff }
    #personal  { padding-bottom: 20px }
    #result-info { display: none; }
    #result-password { display: none; }
    
  </style>
  @section('content')

  <div class="container">
    <div class="row">       
      <div class="success-bottom "><br>
    
        <div class="container" id="table-cont">

          <br><br>
          <!-- Main Content - start -->
          <div class="container">
            <div class="row" id="profile">
              <div class="col-sm-3" id="row-left">
                <ul>
                  <li onclick="showProfile()">Profile</li>
                  <li onclick="showPassword()">Reset Password</li>
                  <li onclick="showOrders({{$user->id}})">Order History</li>
                  <li><a data-link-action="quickview" title="Quickview" data-toggle="modal" data-target="#joinNow" >Join Now</a></li>
                  <li><a href="{{ route('logout', ['lang' => \App::getLocale()]) }}">@lang('sidebar.logout')</a>

                  </li>
                </ul>
              </div>
              <div class="col-sm-9">
                <div class="container">

                  <div class="row" id='personal'>
                    <form class="container" id="update-account">

                      <h3><br>Personal Information</h3>
                      <p>(Your can update your profile from here.)</p>
                      <div class="col-sm-12" id="personal">
                        <div class="row">
                          <div class="col-sm-3">
                            <label>Name</label>
                          </div>
                          <div class="col-sm-9">
                            <input type="text" name="username" class="form-control" value="{{$user->first_name}}"/>
                          </div>
                        </div>              
                      </div>

                      <div class="col-sm-12" id="personal">
                        <div class="row">
                          <div class="col-sm-3">
                            <label>Email</label>
                          </div>
                          <div class="col-sm-9">
                            <input type="email" name="email"  class="form-control" value="{{$user->email}}" readonly />
                          </div>
                        </div>              
                      </div>

                      <div class="col-sm-12" id="personal">
                        <div class="row">
                          <div class="col-sm-3">
                            <label>Phone</label>
                          </div>
                          <div class="col-sm-9">
                            <input type="number" name="phone" id="phone" class="form-control" value="@if(!empty($address)){{$address->phone}}@endif"/>
                          </div>
                        </div>              
                      </div>

                      <div class="col-sm-12" id="personal">
                        <div class="row">
                          <div class="col-sm-3">
                            <label>Address</label>
                          </div>
                          <div class="col-sm-9">
                            <input type="text" name="address"  class="form-control" value="@if(!empty($address)){{$address->address}}@endif"/>
                          </div>
                        </div>              
                      </div>

                      <div class="col-sm-12" id="personal">
                        <div class="row">
                          <div class="col-sm-3">
                            <label>Country</label>                            
                          </div>

                          <?php


                          ?>
                          <div class="col-sm-9">
                            <select class="form-control" name="country_id" id="country_id">
                              @foreach($countries as $country)
                              <option value="{{$country->id}}" @if($address->country_id == $country->id)  {{'selected'}} @endif>{{$country->country}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>              
                      </div>

                      <div class="col-sm-12" id="personal">
                        <div class="row">
                          <div class="col-sm-3">
                            <label>State</label>                            
                          </div>

                          <?php


                          ?>
                          <div class="col-sm-9">
                            <select class="form-control" name="state_id" id="state_id">
                              @foreach($states as $state)
                              <option value="{{$state->id}}" @if($address->state_id == $state->id)  {{'selected'}} @endif>{{$state->state}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>              
                      </div>

                      <div class="col-sm-12" id="personal">
                        <div class="row">
                          <div class="col-sm-3">
                            <label>City</label>
                          </div>
                          <div class="col-sm-9">
                            <select class="form-control" name="city_id" id="city_id">
                              @foreach($cities as $city)
                              <option value="{{$city->id}}" @if($address->city_id == $city->id)  {{'selected'}} @endif>{{$city->city}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>              
                      </div>

                      <div class="col-sm-12" id="personal">
                        <div class="row">
                          <div class="col-sm-3">
                            <label>PIN Code</label>
                          </div>
                          <div class="col-sm-9">
                            <input type="text" name="pin_code"  class="form-control" value="@if(!empty($address)){{$address->pin_code}}@endif"/>
                          </div>
                        </div>              
                      </div>

                      <div class="col-sm-12" style="text-align: center;">
                        <button type="button" name="update" class="btn btn-danger" style="background-color: #ff0000" onclick="updateInfo()">Update</button>
                        <button type="button" name="cancel" class="btn btn-danger" style="background-color: #3f3f3f; border: none">Cancel</button>
                      </div>
                      <br><br>

                      <div style="text-align: center;" class="btn btn-success" id="result-info">

                      </div>

                    </form>
                  </div>

                  {{-- <div class="row" id="donation">
                    <h3>Donation History</h3>
                    <table class="table">
                      <thead>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Payment Method</th>
                        <th>Transaction ID</th>
                      </thead>
                      <tbody>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tbody>
                    </table>
                  </div> --}}

                  <div class="row" id="password">
                    <h3><br>Reset Your Password</h3>
                    <form id="reset-password">

                      <input type="hidden" name="user_id" value="">
                      <div clas="row">
                        <div class="col-sm-4">
                          <label>Current Password</label>
                        </div>
                        <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}" class="form-control">
                        <div class="col-sm-8">
                          <input type="text" name="old_password" class="form-control">
                        </div>
                      </div>
                      <br><br>
                      <div clas="row">
                        <div class="col-sm-4">
                          <label>New Password</label>
                        </div>
                        <div class="col-sm-8">                
                          <input type="password" name="new_password" class="form-control">
                        </div>
                      </div>
                      <br><br>

                      <div clas="row">
                        <div class="col-sm-4">
                          <label>Re-enter New Password </label>
                        </div>
                        <div class="col-sm-8">
                          <input type="password" name="re_password" class="form-control">              
                        </div>
                      </div>
                      <br>

                      <br><br>
                      <div class="row" style="text-align: center;">
                        <button type="button" name="reset" class="btn btn-danger" style="background-color: #9D6C34" onclick="updatePassword()">Submit</button>
                        <button type="button" name="cancel" class="btn btn-danger" style="background-color: #3f3f3f">Cancel</button>
                      </div>

                      <br><br>

                      <div style="text-align: center;" class="btn btn-success" id="result-password">

                      </div>

                    </form>
                  </div>

                </div>
              </div>
            </div>
          </div>
          <!-- Main Content - end -->
        </div>  
      </div>
    </div>
  </div>

  @endsection
  @section('script')

  <script>

    function showDonation(){
      $('#personal').hide();
      $('#password').hide();
      $('#donation').show();
    }

    function showPassword(){
      $('#personal').hide();
      $('#donation').hide();
      $('#password').show();
    }

    function showProfile(){
      $('#donation').hide();
      $('#password').hide();
      $('#personal').show();
    }

    function updateInfo(){
      $.ajax({              
        url: 'update-info',
        type: 'POST',             
        data: $('#update-account').serialize(),
        success: function (data) { 
          
          Swal(data.message);           
          
        },
        failure:function(data){
          alert(data.message);
        }
      }); 
    }

    function updatePassword(){
      $.ajax({              
        url: '/update-password',
        type: 'POST',             
        data: $('#reset-password').serialize(),
        success: function (data) { 
          Swal(data.message);        
          
        },
        failure:function(data){
          alert(data.message);
        }
      }); 
    }

    $('#state').on('change',function(){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var state = $('#state').val();

      $.ajax({
        type: 'POST',
        url: '/cities',
        data: {_token: CSRF_TOKEN, state_id : state },
        success:function(data){
          $('#city').html(data);
        }
      });
    });

    function showOrders(id){
      window.location.href = '/en/summary/user/'+id;
    }

  </script>

  @endsection

