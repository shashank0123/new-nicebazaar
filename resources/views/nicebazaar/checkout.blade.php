<?php
use App\Product;
use App\Order;
use App\Address;
use App\Country;
use App\State;
use App\City;
$countries = Country::all();
$add_cnt = 0;
$user_id = 0;
if(!empty($member->id)){
   //echo "Member : ".$member->user_id;
   $user_id = $member->user_id;
   $address = Address::where('user_id',$member->user_id)->first();
}
else{
   //echo "User : ".$user->id;
   $user_id = $user->id;
   $address = Address::where('user_id',$user->id)->first();
}

//$shipped = Order::where('user_id',$user_id)->where('status','SUCCESS')->get();
$shipped = Order::where('user_id',$user_id)->select('shipping_address')->distinct()->get();




if(!empty($addresses)){
   foreach($addresses as $a)
      $add_cnt++;
}
//echo $addresses;
//die;
$i=0;
$total = 0;
$tpv = 0;
?>
@extends('layouts.nicebazaar1')
<style>
   #submit-form { display: none; }
   .new-entry { display: none; }
   #shipped {display: none;}
</style>
@section('content')
<aside id="notifications">
   <div class="container">
   </div>
</aside>
<div class="innovatoryBreadcrumb">
   <div class="container">
      <nav data-depth="1" class="breadcrumb hidden-sm-down">
         <ol itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
               <a itemprop="item" href="/en">
                  <span itemprop="name">Home</span>
               </a>
               <meta itemprop="position" content="1">
            </li>
         </ol>
      </nav>
   </div>
</div>
<section id="wrapper">
   <div class="container">
      <section id="content">

         
         <form action="/payment" method="POST" id="shipping-form">
            <input type="hidden" value="<?php if(!empty($member->id)){echo 'member'; }else {echo 'user'; }?>" name="user_type">
            <input type="hidden" name="uid" id="uid" value="<?php if(!empty($member->id)) { echo $member->user_id; } else { echo $user->id ;} ?>">
            <input type="hidden" value="" name="getBill" id="getBill">
            <input type="hidden" value="" name="getShip" id="getShip">
            <div class="row">
               <div class="col-md-8">
                  <div class="col-sm-12" id="billed">
                     <h1>Billing Address</h1>
                     <div class="form-group">
                        <label>Billed To</label>
                        <select name="address_category1" id="address_category1" class="form-control">
                           <option value="Home" @if(!empty($address->address_category)) @if($address->address_category == 'Home'){{'selected'}} @endif @endif>Home</option>
                           <option value="Office" @if(!empty($address->address_category)) @if($address->address_category == 'Office'){{'selected'}} @endif @endif>Office</option>                       
                        </select>
                     </div>

                     <div class="form-group">
                        <label>Address</label>
                        <input type="text" name="address1" id="address1" class="form-control" value="@if(!empty($address->address)) {{$address->address}} @endif" >                           
                     </div>

                     <div class="form-group">
                        <label>Country</label>
                        <select name="country_id1" id="country_id1" class="form-control" onchange="getStates1()">
                           @foreach($countries as $country)
                           <option value="{{$country->id}}" @if(!empty($address->country_id)) @if($address->country_id == $country->id){{'selected'}} @endif @endif>{{$country->country}}</option>
                           @endforeach                                                  
                        </select>                         
                     </div>

                     <div class="form-group">
                        <label>State</label>
                        <select name="state_id1" id="state_id1" class="form-control">
                           <?php
                           if(!empty($address->state_id))
                           $state = State::where('id',$address->state_id)->first();
                           ?>
                           @if(!empty($state))
                           <option value="{{$state->id}}">{{$state->state}}</option>
                           @endif
                        </select>                          
                     </div>

                     <div class="form-group">
                        <label>City</label>
                        <select name="city_id1" id="city_id1" class="form-control">
                           <?php
                           if(!empty($address->city_id))
                           $city = City::where('id',$address->city_id)->first();
                           ?>
                           @if(!empty($city))
                           <option value="{{$city->id}}">{{$city->city}}</option>
                           @endif
                        </select>                          
                     </div>

                     <div class="form-group">
                        <label>PIN Code</label>
                        <input type="text" name="pin_code1" id="pin_code1" class="form-control" value="@if(!empty($address->pin_code)){{$address->pin_code}}@endif">                           
                     </div>

                     <div class="form-group">
                        <label>Phone</label>
                        <input type="text" name="phone1" id="phone1" class="form-control" value="@if(!empty($address->phone)){{$address->phone}}@endif">
                     </div>

                     <div class="form-group" style="font-size: 24px">
                        <input type="checkbox" name="shipping" id="shipping" onclick="showNewForm()" value="different"> Shipping to different address ?
                     </div>
                  </div>

                  <div class="col-sm-12" id="shipped">
                     <h1><br><br>Shipping Address</h1>

                        @if(!empty($shipped))
                        @foreach($shipped as $ship)
                        <?php $address = Address::where('id',$ship->shipping_address)->first();
                        $country = Country::where('id',$address->country_id)->first();
                        $state = State::where('id',$address->state_id)->first();
                        $city = City::where('id',$address->city_id)->first();
                         ?>
                         <br>
                     <div class="form-group" style="margin-left: 20px">
                        
                        <h3><input type="radio" name="shipping_prev" value="{{$ship->shipping_address}}">&nbsp;{{$address->address_category}}</h3>
                        <p>{{$address->address}}, {{$city->city}}, {{$state->state}}, {{$country->country}}- ({{$address->pin_code}}) , Contact No. - {{$address->phone}}</p>
                     </div>
                        @endforeach
                        @endif
                        <div style="text-align: center;">
                        <h3>OR</h3>
                        <h3>Add New Address</h3>
                     </div>
                     <div class="form-group">
                        <label>Shipped To</label>
                        <select name="address_category2" id="address_category2" class="form-control">
                           <option value="Home">Home</option>
                           <option value="Office">Office</option>                       
                        </select>
                     </div>

                     <div class="form-group">
                        <label>Address</label>
                        <input type="text" name="address2" id="address2" class="form-control">                           
                     </div>

                     <div class="form-group">
                        <label>Country</label>
                        <select name="country_id2" id="country_id2" class="form-control" onchange="getStates2()">
                           @foreach($countries as $country)
                           <option value="{{$country->id}}">{{$country->country}}</option>
                           @endforeach                                                  
                        </select>                         
                     </div>

                     <div class="form-group">
                        <label>State</label>
                        <select name="state_id2" id="state_id2" class="form-control">

                        </select>                          
                     </div>

                     <div class="form-group">
                        <label>City</label>
                        <select name="city_id2" id="city_id2" class="form-control">

                        </select>                          
                     </div>

                     <div class="form-group">
                        <label>PIN Code</label>
                        <input type="text" name="pin_code2" id="pin_code2" class="form-control">                           
                     </div>

                     <div class="form-group">
                        <label>Phone</label>
                        <input type="text" name="phone2" id="phone2" class="form-control">
                     </div>

                     
                  </div>
               </div>

               <div class="col-md-4">
                  <br><br>        

                  <section id="js-checkout-summary" class="card js-cart" data-refresh-url="">
                     @if(session()->get('cart') != null)

                     <?php

                     $ids = array();
                     $cat_id = array();               
                     $quantities = array();


                     foreach(session()->get('cart') as $data)
                     {
                        $ids[$i] = $data->product_id;                      
                        $quantities[$i] = $data->quantity;
                        $i++;
                     }                 
                     ?>
                     @for($j=0 ; $j<$i ; $j++ )
                     <?php
                     $product = Product::where('id',$ids[$j])->first(); 
                     $cost = $product->sell_price * $quantities[$j];
                     $total = $total + $cost;

                     $pv1 = $product->pv * $quantities[$j];
                     $tpv = $tpv + $pv1;

                     ?>
                     @endfor
                     @endif   
                     <div class="card-block">
                        <div class="cart-summary-line cart-summary-subtotals" id="cart-subtotal-products">
                           <span class="label">Subtotal</span>
                           <span class="value">Rs. {{$total}}</span>
                        </div>
                        <div class="cart-summary-line cart-summary-subtotals" id="cart-subtotal-shipping">
                           <span class="label">PV</span>
                           <span class="value">{{$tpv}}</span>
                        </div>
                     </div>
                     <hr>
                     <div class="card-block cart-summary-totals">
                        <div class="cart-summary-line cart-total">
                           <span class="label">Total (tax excl.)</span>
                           <span class="value">Rs. {{$total}}</span>
                        </div>
                     </div>
                     <div class="checkout cart-detailed-actions card-block">

                        <div class="text-xs-center">
                           <input type="hidden" name="total_bill" id="total_bill" value="{{$total}}">
                           <input type="hidden" name="total_pv" id="total_pv" value="{{$tpv}}">
                           <input type="hidden" name="id" value="<?php if(!empty($member->id)) { echo $member->user_id; } else { echo $user->id ;} ?>">
                           {{-- <input type="hidden" name="ammount" value="450"> --}} 
                           <button type="button" onclick="proceedToPay()" class="btn btn-primary" name="check"> Proceed to Checkout</button>

                           <button type="submit"  class="btn btn-primary" name="submit" id="submit-form"> Proceed to Checkout</button>

                        </div>

                     </div>
                  </section>

                  <div id="block-reassurance">
                     <ul>
                        <li>
                           <div class="block-reassurance-item">
                              <img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/blockreassurance/img/ic_verified_user_black_36dp_1x.png" alt="Security policy (edit with Customer reassurance module)">
                              <span class="h6">Security policy (edit with Customer reassurance module)</span>
                           </div>
                        </li>
                        <li>
                           <div class="block-reassurance-item">
                              <img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/blockreassurance/img/ic_local_shipping_black_36dp_1x.png" alt="Delivery policy (edit with Customer reassurance module)">
                              <span class="h6">Delivery policy (edit with Customer reassurance module)</span>
                           </div>
                        </li>
                        <li>
                           <div class="block-reassurance-item">
                              <img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/blockreassurance/img/ic_swap_horiz_black_36dp_1x.png" alt="Return policy (edit with Customer reassurance module)">
                              <span class="h6">Return policy (edit with Customer reassurance module)</span>
                           </div>
                        </li>
                     </ul>
                  </div>

               </div>
            </form>
         </div>
      </section>
   </div>
</section>

@endsection

@section('script')
<script>



   function getStates1(){
      var countryID = $('#country_id1').val();
      alert(countryID);
      if(countryID) {
         $.ajax({
            url: 'state/'+countryID,
            type: "GET",
            data : {"_token":"{{ csrf_token() }}",country_id:countryID},
            dataType: "json",
            success:function(data) {                       
               if(data){

                  $('#state_id1').empty();
                  $('#state_id1').focus;
                  $('#state_id1').append('<option value="">-- Select state_id --</option>'); 
                  $.each(data, function(key, value){
                     $('select[name="state_id1"]').append('<option value="'+ value.id +'">' + value.state+ '</option>');
                  });
               }else{
                  $('#state_id1').empty();
               }
            }
         });
      }else{
         $('#state_id1').empty();
      }
   }


   $(document).ready(function() {
      $('#state_id1').on('change', function() {
         var stateID = $(this).val();
         if(stateID) {
            $.ajax({
               url: 'city/'+stateID,
               type: "GET",
               data : {"_token":"{{ csrf_token() }}",state_id:stateID},
               dataType: "json",
               success:function(data) {                       
                  if(data){

                     $('#city_id1').empty();
                     $('#city_id1').focus;
                     $('#city_id1').append('<option value="">-- Select City --</option>'); 
                     $.each(data, function(key, value){
                        $('select[name="city_id1"]').append('<option value="'+ value.id +'">' + value.city+ '</option>');
                     });
                  }else{
                     $('#city_id1').empty();
                  }
               }
            });
         }else{
            $('#city_id1').empty();
         }
      });
   });



   function getStates2(){
      var countryID = $('#country_id2').val();
      alert(countryID);
      if(countryID) {
         $.ajax({
            url: 'state/'+countryID,
            type: "GET",
            data : {"_token":"{{ csrf_token() }}",country_id:countryID},
            dataType: "json",
            success:function(data) {                       
               if(data){

                  $('#state_id2').empty();
                  $('#state_id2').focus;
                  $('#state_id2').append('<option value="">-- Select state_id --</option>'); 
                  $.each(data, function(key, value){
                     $('select[name="state_id2"]').append('<option value="'+ value.id +'">' + value.state+ '</option>');
                  });
               }else{
                  $('#state_id2').empty();
               }
            }
         });
      }else{
         $('#state_id2').empty();
      }
   }


   $(document).ready(function() {
      $('#state_id2').on('change', function() {
         var stateID = $(this).val();
         if(stateID) {
            $.ajax({
               url: 'city/'+stateID,
               type: "GET",
               data : {"_token":"{{ csrf_token() }}",state_id:stateID},
               dataType: "json",
               success:function(data) {                       
                  if(data){

                     $('#city_id2').empty();
                     $('#city_id2').focus;
                     $('#city_id2').append('<option value="">-- Select City --</option>'); 
                     $.each(data, function(key, value){
                        $('select[name="city_id2"]').append('<option value="'+ value.id +'">' + value.city+ '</option>');
                     });
                  }else{
                     $('#city_id2').empty();
                  }
               }
            });
         }else{
            $('#city_id2').empty();
         }
      });
   });


   function proceedToPay(){
      var category1 = $('#address_category1').val();
      var user_id = $('#uid').val();
      var address1 = $('#address1').val();
      var country_id1 = $('#country_id1').val();
      var state_id1 = $('#state_id1').val();
      var city_id1 = $('#city_id1').val();
      var pin_code1 = $('#pin_code1').val();
      var phone1 = $('#phone1').val();
      var bill = $('#total_bill').val();
      var pvs = $('#total_pv').val();

      alert(category1+" / "+user_id+" / "+address1+" / "+country_id1+" / "+state_id1+" / "+city_id1+" / "+pin_code1+" / "+phone1);

      if(address1=="" || country_id1=="" || state_id1=="" || city_id1=="" || pin_code1=="" || phone1==""){
         Swal('All fields are mendatory');
      }

      else{
         var pinCode = pin_code1.replace(/[^\d]/g, '');
         if(pinCode.length == 6){
            var phoneNum = phone1.replace(/[^\d]/g, '');
            if(phoneNum.length >9 && phoneNum.length < 11) {

               if($("#shipping").prop('checked') == true){




                  var category2 = $('#address_category2').val();
                  var address2 = $('#address2').val();
                  var country_id2 = $('#country_id2').val();
                  var state_id2 = $('#state_id2').val();
                  var city_id2 = $('#city_id2').val();
                  var pin_code2 = $('#pin_code2').val();
                  var phone2 = $('#phone2').val();
                  alert(category2+" / "+user_id+" / "+address2+" / "+country_id2+" / "+state_id2+" / "+city_id2+" / "+pin_code2+" / "+phone2);

                  if(address2=="" || country_id2=="" || state_id2=="" || city_id2=="" || pin_code2=="" || phone2==""){
                     Swal('All fields are compulsary');
                  }

                  else{

                     $('#submit-form').click();
                     // $.ajax({
                     //    url: '/addresses',
                     //    type: "POST",
                     //    data : {"_token":"{{ csrf_token() }}",user_id : uid, category1: category1, address1: address1, country_id1: country_id1, state1: state1, city1: city1, pin_code1: pin_code1, phone1: phone1, category2: category2, country_id2: country_id2, state_id2: state_id2, city_id2: city_id2, pin_code2: pin_code2, phone2: phone2,bill: bill, pvs: pvs, key: 'different' },
                     //    dataType: "json",
                     //    success:function(data) {                       
                     //       if(data.message == 'Inserted'){
                     //          Swal('Done Both');
                     //       }
                     //    },
                     //    failure:function(data){
                     //       Swal(data.message);
                     //    } 
                     // });
                  }

    //do something

 }
 else{
   // $.ajax({
   //    url: '/addresses',
   //    type: "POST",
   //    data : {"_token":"{{ csrf_token() }}",user_id : uid, category1: category1, address1: address1, country_id1: country_id1, state_id1: state_id1, city_id1: city_id1, pin_code1: pin_code1, phone1: phone1, bill: bill, pvs: pvs, key: 'same' },
   //    dataType: "json",
   //    success:function(data) {     
         $('#submit-form').click();
         // if(data.message == 'Inserted'){
         //    Swal('Done Single');
         // }
   //    },
   //    failure:function(data){
   //       Swal(data.message);
   //    } 
   // });
}

}
else{
   Swal('Invalid phone number format');
}
}
else{
   Swal('Invalid PIN Code');
}
}     
}


   function showNewForm(){
      $('#shipped').toggle();
   }   

   
</script>
@endsection