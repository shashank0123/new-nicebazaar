@extends('layouts.nicebazaar1')
<?php
use App\MainCategory;
use App\Review;
use App\Product;
$count = Review::where('product_id',$getproduct->id)->count();
$bill = 0;
$i=0;

if($count>0)
{
   $rate = Review::where('product_id',$getproduct->id)->avg('rating');
   $final_rate = floor($rate);
}
else
   $final_rate = 0;
?>
<style>

   .success-div h4 { padding: 10px 0  }
</style>
@section('content')
<div class="innovatoryBreadcrumb">
   <div class="container">
      <nav data-depth="2" class="breadcrumb hidden-sm-down">
         <ol itemscope itemtype="">
            <li itemprop="itemListElement" itemscope itemtype="">
               <a itemprop="item" href="/en">
                  <span itemprop="name">Home</span>
               </a>
               <meta itemprop="position" content="1">
            </li>
            <li itemprop="itemListElement" itemscope itemtype="">
               <a itemprop="item" href="">
                  <span itemprop="name">Product</span>
               </a>
               <meta itemprop="position" content="2">
            </li>
         </ol>
      </nav>
   </div>
</div>
<section id="wrapper">
   <aside id="notifications">
      <div class="container">
      </div>
   </aside>
   <div class="container">
      <div class="row">
         <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
            <div class="it-special-products product-column-style" data-items="1" data-speed="1000" data-autoplay="0" data-time="3000"  data-arrow="0" data-pagination="0" data-move="1" data-pausehover="0"  data-md="1" data-sm="1" data-xs="1" data-xxs="1">
               <?php
               $featuredproducts = Product::where('trending','yes')->where('status','Active')->orderBy('created_at','desc')->limit(4)->get();
               ?>
               <div class="itProductList itcolumn">
                  <div class="title_block">
                     <h3><span>New Product</span></h3>
                  </div>
                  <div class="block-content">
                     <div class="itProductFilter row">
                        <div class="special-item owl-carousel">
                           <div class="item-product">
                              @if(isset($featuredproducts))
                              @foreach($featuredproducts as $col)
                              <?php
                              $featurecategory = Product::where('products.id',$col->id)->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();
                              
                              ?>
                              <?php $discount=(($col->mrp - $col->sell_price)*100)/$col->mrp;
                              $rate = Review::where('product_id',$col->id)->avg('rating');
                              $final_rate = floor($rate);
                              ?>
                              <div class="item">
                                 <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="46" data-id-product-attribute="289">
                                    <div class="innovatory-thumbnail-container">
                                       <div class="row no-margin">
                                          <div class="pull-left product_img">
                                             <a href="/productdetail/{{$col->id}}" class="thumbnail product-thumbnail">
                                                <img src = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $col->image1 }}" alt = "" data-full-size-image-url = "" width="100px" height="100px">
                                             </a>
                                          </div>
                                          <div class="innovatoryMedia-body">
                                             <div class="innovatory-product-description">
                                                <div class="comments_note">
                                                   <div class="star_content">
                                                      @for($i=0; $i<5 ; $i++)
                                                      <i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
                                                      @endfor
                                                   </div>
                                                   <span class="laberCountReview pull-left">Review</span>
                                                </div>
                                                <h2 class="h2 productName" itemprop="name"><a href="/productdetail/{{$col->id}}">{{$col->name}}</a></h2>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span itemprop="price" class="price">Rs.{{$col->sell_price}}</span>
                                                   &nbsp;&nbsp;
                                                   <span class="regular-price">Rs. {{$col->mrp}}</span>
                                                </div>
                                                <div>
                                                   <span class="bgsave">Save : {{floor($discount)}}%</span>
                                                </div>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span>PV : {{$col->pv}}</span>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              @endforeach
                              @endif
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="itleftbanners" class="block hidden-md-down">
               <ul class="banner-col">
                  <li class="itleftbanners-container">
                     <a href="#" title="Left Banner 1">
                        <img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itleftbanners/images/left-banner-1.jpg" alt="Left Banner 1" width="auto" height="auto"/>
                     </a>
                  </li>
               </ul>
            </div>
            <div class="it-special-products product-column-style" data-items="1" data-speed="1000" data-autoplay="0" data-time="3000"  data-arrow="0" data-pagination="0" data-move="1" data-pausehover="0"  data-md="1" data-sm="1" data-xs="1" data-xxs="1">
               <?php
               $trendproducts = Product::where('trending','yes')->where('status','Active')->orderBy('name','asc')->limit(4)->get();
               ?>
               <div class="itProductList itcolumn">
                  <div class="title_block">
                     <h3><span>Special Product</span></h3>
                  </div>
                  <div class="block-content">
                     <div class="itProductFilter row">
                        <div class="special-item owl-carousel">
                           <div class="item-product">
                              @if(isset($trendproducts))
                              @foreach($trendproducts as $set)
                              <?php
                              $trendcategory = Product::where('products.id',$set->id)->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();
                              
                              ?>
                              <?php $discount=(($set->mrp - $set->sell_price)*100)/$set->mrp;
                              $rate = Review::where('product_id',$set->id)->avg('rating');
                              $final_rate = floor($rate);
                              ?>
                              <div class="item">
                                 <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="46" data-id-product-attribute="289">
                                    <div class="innovatory-thumbnail-container">
                                       <div class="row no-margin">
                                          <div class="pull-left product_img">
                                             <a href="/productdetail/{{ $set->id }}" class="thumbnail product-thumbnail">
                                                <img src = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $set->image1 }}" alt = "" data-full-size-image-url = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $set->image1 }}" width="100px" height="100px">
                                             </a>
                                          </div>
                                          <div class="innovatoryMedia-body">
                                             <div class="innovatory-product-description">
                                                <div class="comments_note">
                                                   <div class="star_content">
                                                      @for($i=0; $i<5 ; $i++)
                                                      <i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
                                                      @endfor
                                                   </div>
                                                   <span class="laberCountReview pull-left">Review</span>
                                                </div>
                                                <h2 class="h2 productName" itemprop="name"><a href="/productdetail/{{ $set->id }}">{{$set->name}}</a></h2>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span itemprop="price" class="price">Rs. {{$set->sell_price}}</span>&nbsp;&nbsp;
                                                   <span class="regular-price">Rs. {{$set->mrp}}</span>
                                                </div>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span>PV : {{$set->pv}}</span>
                                                </div>
                                                <div>
                                                   <span class="bgsave">save : {{floor($discount)}}%</span>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              @endforeach
                              @endif
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
            <section id="main" itemscope itemtype="https://schema.org/Product">
               <meta itemprop="url" content="">
               <div class="innovatoryProduct">
                  <div class="row">
                     <div class="col-md-6 col-xl-5">
                        <section class="page-content" id="content">
                           <div class="images-container">
                              <div class="product-cover">
                                 <img id="itzoom-products" class="js-qv-product-cover img12" src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image1 }}" alt="" title="" style="width:100%;" itemprop="image">
                                 <div class="layer">
                                    <i class="fa fa-search-plus" onclick='$("#myemd").attr("src","{{ URL::to("/") }}/assetsss/images/AdminProduct/{{ $getproduct->image1 }}");' data-link-action="quickview" title="Quickview" data-toggle="modal" data-target="#myModal" style="font-size: 24px;"></i>
                                 </div>
                              </div>
                              <div class="js-qv-mask mask">
                                 <ul id="product-gellery" class="product-images owl-carousel js-qv-product-images">
                                    <li class="thumb-container">
                                       <a href="javascript:void(0)" data-image="" data-zoom-image="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image1 }}">
                                          <img
                                          class="thumb js-thumb  selected "
                                          data-image-medium-src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image1 }}"
                                          data-image-large-src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image1 }}"
                                          src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image1 }}"
                                          alt=""
                                          title=""
                                          itemprop="image"
                                          >
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </section>
                     </div>
                     <div class="col-md-6 col-xl-7">
                        <h1 class="h1" itemprop="name">{{$getproduct->name}}</h1>
                        <div class="comments_note">
                           <div class="star_content">
                              @for($i=0; $i<5 ; $i++)
                              <i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
                              @endfor     
                           </div>
                        </div>
                        <div class="product-prices">
                           <div class="product-price h5 has-discount" itemprop="offers" itemscope itemtype="">
                              <div class="current-price">
                                 <span itemprop="price" content="18.96">Rs.&nbsp;{{$getproduct->sell_price}}</span>
                              </div>
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <div class="current-price">
                                 <span itemprop="price" >Rs.&nbsp;<del>{{$getproduct->mrp}}</del></span>
                              </div>
                           </div>
                           <?php $discount=(($getproduct->mrp - $getproduct->sell_price)*100)/$getproduct->mrp;
                           $rate = Review::where('product_id',$getproduct->id)->avg('rating');
                           $final_rate = floor($rate);
                           ?>
                           <div class="product-price h5 has-discount" itemprop="offers" itemscope itemtype="">
                              <div class="current-price">
                                 <span itemprop="price" content="18.96">PV : {{$getproduct->pv}}</span>
                              </div>
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <div class="current-price">
                                 <span itemprop="price" class="bgsave">Save : {{floor($discount)}}%</span>
                              </div>
                           </div>
                        </div>
                        <div class="product-description-short" id="product-description-short-29" itemprop="description">
                           <p>{{$getproduct->short_descriptions}}</p>
                        </div>
                        <div class="product-information">
                           <div class="product-actions">
                              <form action="" method="POST" >
                                 <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                 <input type="hidden" name="product_id" value="" id="product_page_product_id">
                                 <input type="hidden" name="id_customization" value="0" id="product_customization_id">
                                 <div class="product-variants">
                                    <script>
                                       $(document).ready(function() {
                                        var owl = $(".quickview .product-images");
                                        owl.owlCarousel({
                                         nav:true,
                                         autoplay:false,
                                         dots:false,
                                         navText:["<i class='left'></i>","<i class='right'></i>"],
                                         responsive:{
                                          0:{
                                           items:2
                                        },
                                        480:{
                                           items:2
                                        },
                                        768:{
                                           items:3
                                        },
                                        1500:{
                                           items:3
                                        }
                                     }   
                                  });
                                        
                                     });
                                  </script>
                               </div>
                               <section class="product-discounts">
                               </section>
                               <div class="product-add-to-cart">
                                 <input type="hidden" name="id" id="cat_id" value="{{$getproduct->id}}" />
                                 <span class="control-label">Quantity</span>
                                 <div class="product-quantity">
                                    <div class="qty">
                                       <input
                                       type="text"
                                       name="quantity"
                                       id="quantity_wanted"
                                       value="1"
                                       class="input-group"
                                       min="1"
                                       >
                                    </div>
                                    <div class="add">
                                       <button
                                       class="btn btn-primary add-to-cart"
                                       data-button-action="add-to-cart"
                                       type="submit"
                                       onclick="addCart({{$getproduct->id}})">
                                       <!-- <i class="material-icons shopping-cart">&#xE547;</i> -->
                                       Add to cart
                                    </button>
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                           </div>
                           <input class="product-refresh ps-hidden-by-js" name="refresh" type="submit" value="Refresh">
                        </form>
                     </div>
                     <div id="block-reassurance">
                        <ul>
                           <li>
                              <div class="block-reassurance-item">
                                 <img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/blockreassurance/img/ic_verified_user_black_36dp_1x.png">
                                 <span class="h6">Security policy (edit with Customer reassurance module)</span>
                              </div>
                           </li>
                           <li>
                              <div class="block-reassurance-item">
                                 <img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/blockreassurance/img/ic_local_shipping_black_36dp_1x.png">
                                 <span class="h6">Delivery policy (edit with Customer reassurance module)</span>
                              </div>
                           </li>
                           <li>
                              <div class="block-reassurance-item">
                                 <img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/blockreassurance/img/ic_swap_horiz_black_36dp_1x.png">
                                 <span class="h6">Return policy (edit with Customer reassurance module)</span>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="tabs innovatoryTabs">
            <div class="nav nav-tabs">
               <ul>
                  <li class="nav-item">
                     <a class="nav-link active" data-toggle="tab" href="#description">Description</a>
                  </li>
                        <!--  <li class="nav-item">
                           <a class="nav-link" data-toggle="tab" href="#product-details">Product Details</a>
                           </li>
                           <li class="nav-item">
                           <a class="nav-link" data-toggle="tab" href="#productcomment">Reviews</a>
                        </li> -->
                     </ul>
                  </div>
                  <div class="tab-content" id="tab-content">
                     <div class="tab-pane fade in active" id="description">
                        <div class="product-description">
                           <p>{{$getproduct->short_descriptions}}</p>
                        </div>
                     </div>
                        <div class="tab-pane fade in" id="productcomment">
                           <script type="text/javascript">
                              var productcomments_controller_url = 'https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/module/productcomments/default';
                              var confirm_report_message = 'Are you sure that you want to report this comment?';
                              var secure_key = '81f12d57286d5d3ad1a99ef0737dbbae';
                              var productcomments_url_rewrite = '1';
                              var productcomment_added = 'Your comment has been added!';
                              var productcomment_added_moderation = 'Thanks for your Review. It will be available once approved by a moderator.';
                              var productcomment_title = 'New comment';
                              var productcomment_ok = 'OK';
                              var moderation_active = 1;
                           </script>
                           <div id="productCommentsBlock">
                              <div class="innovatorytabs">
                                 <div class="innovatoryButtonReviews clearfix">
                                    <a class="open-comment-form btn btn-primary" href="#new_comment_form">Write your review</a>
                                 </div>
                                 <div id="new_comment_form_ok" class="alert alert-success" style="display:none;padding:15px 25px"></div>
                                 <div id="product_comments_block_tab">
                                    <div class="comment clearfix">
                                       <div class="comment_author">
                                          <div class="star_content clearfix">
                                             <div class="star star_on"><i class="fa fa-star"></i></div>
                                             <div class="star star_on"><i class="fa fa-star"></i></div>
                                             <div class="star star_on"><i class="fa fa-star"></i></div>
                                             <div class="star"><i class="fa fa-star"></i></div>
                                             <div class="star"><i class="fa fa-star"></i></div>
                                          </div>
                                          <div class="comment_author_infos">| 
                                             <span>Brock Lee</span>
                                             <em>03/27/2019</em>
                                          </div>
                                       </div>
                                       <div class="comment_details">
                                          <h4 class="title_block">starsLiked the packaging</h4>
                                          <p>The quality of the product is good, nicely packed and delivered. Also looks like good quality plant to me. Well done</p>
                                          <ul>
                                             <li>Was this comment useful to you?
                                                <button class="usefulness_btn" data-is-usefull="1" data-id-product-comment="17">yes</button>
                                                <button class="usefulness_btn" data-is-usefull="0" data-id-product-comment="17">no</button>
                                             </li>
                                             <li><span class="report_btn" data-id-product-comment="17">Report abuse</span></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- Fancybox -->
                              <div style="display:none">
                                 <div id="new_comment_form">
                                    <form id="id_new_comment_form" action="#">
                                       <h2 class="title">Write your review</h2>
                                       <div class="product clearfix">
                                          <div class="product_desc">
                                             <p class="product_name"><strong>Diam Nonummy</strong></p>
                                             <p>Fusce vel tristique nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus. Ut vitae auctor turpis.</p>
                                          </div>
                                       </div>
                                       <div class="new_comment_form_content">
                                          <h2>Write your review</h2>
                                          <div id="new_comment_form_error" class="error" style="display:none;padding:15px 25px">
                                             <ul></ul>
                                          </div>
                                          <ul id="criterions_list">
                                             <li>
                                                <label>Quality</label>
                                                <div class="star_content">
                                                   <input class="star" type="radio" name="criterion[1]" value="1"/>
                                                   <input class="star" type="radio" name="criterion[1]" value="2"/>
                                                   <input class="star" type="radio" name="criterion[1]" value="3"/>
                                                   <input class="star" type="radio" name="criterion[1]" value="4"/>
                                                   <input class="star" type="radio" name="criterion[1]" value="5" checked="checked"/>
                                                </div>
                                                <div class="clearfix"></div>
                                             </li>
                                          </ul>
                                          <label for="comment_title">Title for your review<sup class="required">*</sup></label>
                                          <input id="comment_title" name="title" type="text" value=""/>
                                          <label for="content">Your review<sup class="required">*</sup></label>
                                          <textarea id="content" name="content"></textarea>
                                          <div id="new_comment_form_footer">
                                             <input id="id_product_comment_send" name="id_product" type="hidden" value='26'/>
                                             <p class="fl required"><sup>*</sup> Required fields</p>
                                             <p class="fr">
                                                <button class="btn btn-primary" id="submitNewMessage" name="submitMessage" type="submit">Send</button>&nbsp;
                                                or&nbsp;<a href="#" class="btn btn-primary" onclick="$.fancybox.close();">Cancel</a>
                                             </p>
                                             <div class="clearfix"></div>
                                          </div>
                                       </div>
                                    </form>
                                    <!-- /end new_comment_form_content -->
                                 </div>
                              </div>
                              <!-- End fancybox -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="tab-pane fade in" id="productcomment">
                     <script type="text/javascript">
                        var productcomments_controller_url = '../module/productcomments/default.html';
                        var confirm_report_message = 'Are you sure that you want to report this comment?';
                        var secure_key = '81f12d57286d5d3ad1a99ef0737dbbae';
                        var productcomments_url_rewrite = '1';
                        var productcomment_added = 'Your comment has been added!';
                        var productcomment_added_moderation = 'Thanks for your Review. It will be available once approved by a moderator.';
                        var productcomment_title = 'New comment';
                        var productcomment_ok = 'OK';
                        var moderation_active = 1;
                     </script>
                  </div>
               </div>
            </div>
            <section>
               <div class="Categoryproducts innovatoryProductGrid">
                  <div class="sec-heading text-center mb-30"><h3>other products in the same category:</h3></div>
                  <div class="innovatoryCate row">
                     <div class="innovatoryCategoryproducts owl-carousel">
                        @if($products != null)
                        @foreach($products as $relatedproduct)
                        <?php
                        $category = Maincategory::where('id',$relatedproduct->category_id)->first();
                        ?>
                        <?php $discount=(($relatedproduct->mrp - $relatedproduct->sell_price)*100)/$relatedproduct->mrp;
                        $rate = Review::where('product_id',$relatedproduct->id)->avg('rating');
                        $final_rate = floor($rate);
                        ?>
                        <div class="item-inner  ajax_block_product">
                           <div class="item">
                              <article class="product-miniature js-product-miniature" data-id-product="52" data-id-product-attribute="556" itemscope itemtype="http://schema.org/Product">
                                 <div class="innovatoryProduct-container">
                                    <div class="innovatoryProduct-image">
                                       <a href="/productdetail/{{$relatedproduct->id}}" class="thumbnail product-thumbnail">
                                          <span class="cover_image">
                                             <img src = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $relatedproduct->image1 }}" data-full-size-image-url = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $relatedproduct->image1 }}" alt="" width="250px" height="250px" />
                                          </span>
                                          <span class="hover_image">
                                             <img src = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $relatedproduct->image1 }}" data-full-size-image-url = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $relatedproduct->image1 }}" alt="" width="250px" height="auto" /> 
                                          </span>
                                       </a>
                                       <div class="innovatoryActions hidden-lg-up">
                                          <div class="innovatoryActions-i">
                                             <div class="innovatoryCart innovatoryItem">
                                                <form action="" method="post">
                                                   <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                   <input type="hidden" value="52" name="id_product">
                                                   <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart" onclick="addCart({{$relatedproduct->id}})">
                                                      <i class="ti-shopping-cart"></i>
                                                   </a>
                                                </form>
                                             </div> 
                                             <div class="innovatoryQuick innovatoryItem">
                                                <a  onclick='$("#myemd").attr("src","{{ URL::to("/") }}/assetsss/images/AdminProduct/{{ $relatedproduct->image1 }}");' class="quick-view" data-link-action="quickview" title="Quickview" data-toggle="modal" data-target="#myModal">
                                                   <i class="ti-eye"></i>
                                                </a>
                                             </div> 
                                             @if(!empty($member->id) || !empty($user->id))
                                             <div class="innovatoryItem innovatoryWish">
                                                <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="addWishList({{ $relatedproduct->id }},<?php if(!empty($member->id)) { echo $member->id; } else { echo $user->id ;} ?>)"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                             </div>
                                             @else
                                             <div class="innovatoryItem innovatoryWish">
                                                <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="Swal('Please Login First')"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                             </div>
                                             @endif    
                                          </div>
                                       </div>   
                                    </div>
                                    <div class="innovatory-product-description">
                                       <h2 class="h2 productName" itemprop="name"><a href="">{{$relatedproduct->name}}</a></h2>
                                       <div class="product-detail">
                                          <div class="innovatory-product-price-and-shipping">
                                             <span itemprop="price" class="price">Rs. {{$relatedproduct->sell_price}}</span>&nbsp;&nbsp;&nbsp;
                                          </div>
                                          <div class="innovatory-product-price-and-shipping">
                                             <span itemprop="price" class="price">Rs. <del>{{$relatedproduct->mrp}}</del></span>&nbsp;&nbsp;&nbsp;
                                          </div>
                                          <div class="innovatory-product-price-and-shipping">
                                             <span>PV : {{$relatedproduct->pv}}</span>
                                          </div>
                                          <div class="comments_note">
                                             @for($i=0; $i<5 ; $i++)
                                             <i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
                                             @endfor  
                                             <span class="laberCountReview pull-left">Review&nbsp</span>
                                          </div>
                                       </div>
                                       <div class="innovatoryActions hidden-md-down">
                                          <div class="innovatoryActions-i">
                                             <div class="innovatoryCart innovatoryItem">
                                                <form action="" method="post">
                                                   <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                   <input type="hidden" value="52" name="id_product">
                                                   <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart" onclick="addCart({{$relatedproduct->id}})">
                                                      <i class="ti-shopping-cart"></i>
                                                   </a>
                                                </form>
                                             </div> 
                                             <div class="innovatoryQuick innovatoryItem">
                                                <a  onclick='$("#myemd").attr("src","{{ URL::to("/") }}/assetsss/images/AdminProduct/{{ $relatedproduct->image1 }}");' class="quick-view" data-link-action="quickview" title="Quickview" data-toggle="modal" data-target="#myModal">
                                                   <i class="ti-eye"></i>
                                                </a>
                                             </div>
                                             @if(!empty($member->id) || !empty($user->id))
                                             <div class="innovatoryItem innovatoryWish">
                                                <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="addWishList({{ $relatedproduct->id }},<?php if(!empty($member->id)) { echo $member->id; } else { echo $user->id ;} ?>)"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                             </div>
                                             @else
                                             <div class="innovatoryItem innovatoryWish">
                                                <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="Swal('Please Login First')"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                             </div>
                                             @endif
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </article>
                           </div>
                        </div>
                        @endforeach
                        @endif          
                     </div>
                  </div>
               </div>
            </section>
            <script type="text/javascript">
               $(document).ready(function() {
                var owl = $(".innovatoryCategoryproducts");
                owl.owlCarousel({
                  loop:false,
                  nav:true,
                  dots:false,
                  navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
                  responsive:{
                   0:{
                    items:2
                 },
                 480:{
                    items:2
                 },
                 768:{
                    items:3
                 },
                 992:{
                    items:3
                 },
                 1200:{
                    items:3
                 }
              }             
           });
             });
          </script>
          <div class="modal fade js-product-images-modal" id="product-modal">
            <div class="modal-dialog" role="document">
               <div class="modal-content">
                  <div class="modal-body">
                     <figure>
                        <img class="js-modal-product-cover product-cover-modal" width="700" src="" alt="" title="" itemprop="image">
                     </figure>
                     <aside id="thumbnails" class="thumbnails js-thumbnails text-xs-center">
                        <div class="js-modal-mask mask  nomargin ">
                           <ul class="product-images js-modal-product-images">
                           </ul>
                        </div>
                     </aside>
                  </div>
               </div><!-- /.modal-content -->
            </div>
         </div> 
         <footer class="page-footer">
            <!-- Footer content -->
         </footer>
      </section>
   </div>
</div>
</div>
</section>
<div id="myModal" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
            <embed id="myemd" src=""
            frameborder="0" width="100%" height="450px">
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="success-div"> <h4 id="product-name"></h4 id="product-qty"><h4></h4></div>

@stop
@section('script')
<script>
   var countproduct=<?php if(session()->get('count') != null){echo session()->get('count'); } else { echo 0;} ?>;
   
   function addQuickCart(){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var pro_id = $('#hidden_id').val();
      var quant = $('#quickquantity').val();
      var cat_id = $('#cat_id').val();
      countproduct++;
      $.ajax({
         /* the route pointing to the post function */
         url: '/add-cart',
         type: 'POST',
         /* send the csrf-token and the input to the controller */
         data: {_token: CSRF_TOKEN, id: pro_id, quantity: quant},
         success: function (data) { 
            window.location.href = '/productdetail/'+cat_id;
            $("#show-total").html(countproduct);           
         }
      }); 
   }
   
   function openview(id,name,price,mrp,description,img1,img2,img3,rate){         
      $('#hidden_id').val(id);
      $('#viewname').text(name);
      $('#viewdescription').text(description);
      $('#viewprice').text(price);
      $('#rating').text(rate);
   //For main image
   $("#imggallery1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#imggallery2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   
   //for a tag (data-zoom attribute)    
   $("#imgdz2").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#imgdz3").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   $("#imgdz1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   
   //for a tag (data-images attribute)
   $("#imgdz1").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#imgdz2").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#imgdz3").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   
   //for image tag (src attribute)
   $("#img1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#img2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#img3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   $("#imgt1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#imgt2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#imgt3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   var discount = ((mrp-price)*100)/mrp;
   $('#viewdiscount').text(discount.toFixed(2));
   $('#viewrate').val(rate);
   
} 




function deleteProduct(id){
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');       
   
   $('#listhide'+id).hide();
   $.ajax({
      /* the route pointing to the post function */
      url: '/cart/delete-product/id',
      type: 'POST',
      /* send the csrf-token and the input to the controller */            
      data: {_token: CSRF_TOKEN, id: id},
      success: function (data) { 
         $(".show-total").html(data.showcount);       
         $("#bill").html(data.bill);         
      }
   }); 
}


   //adding items to cart
   function addCart(id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var pro_quant = $('#quantity_wanted').val();
      var cat_id = $('#cat_id').val();
      countproduct++;
      $.ajax({
         /* the route pointing to the post function */
         url: '/add-cart',
         type: 'POST',
         /* send the csrf-token and the input to the controller */
         dataType: 'JSON',
         data: {_token: CSRF_TOKEN, id: id, quantity: pro_quant},
         success: function (data) { 
           $(".show-total").html(data.count);
           $("#product-name").html(data.pro_name);
           $("#product-qty").html(data.pro_qty);
   // $("#ajaxdata").html(data);
}
}); 
   }
   
   
   function showCart(){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
         /* the route pointing to the post function */
         url: '/cart/show',
         type: 'POST',
         /* send the csrf-token and the input to the controller */               
         data: {_token: CSRF_TOKEN},
         success: function (data) {       
            $('#showminicart').html(data);
            $("#bill").text(data.bill);         
         }
      }); 
   }
   
   function increment(){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      $('#refresh-div').load(document.URL + ' #refresh-div');
      $('#total-bill').load(document.URL + ' #total-bill');
      $.ajax({
         /* the route pointing to the post function */
         url: '/cart/increase/id/quant',
         type: 'POST',
         /* send the csrf-token and the input to the controller */
         dataType: 'JSON',
         data: {_token: CSRF_TOKEN, id: id, quant: quant},
         success: function (data) { 
            
            $('#refresh-div').load(document.URL + ' #refresh-div');
         }
      });
   }
   
   function decrement(){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      
      $('#refresh-div').load(document.URL + ' #refresh-div');
      $('#total-bill').load(document.URL + ' #total-bill');
      $.ajax({
         /* the route pointing to the post function */
         url: '/cart/decrease/id/quant',
         type: 'POST',
         /* send the csrf-token and the input to the controller */
         dataType: 'JSON',
         data: {_token: CSRF_TOKEN, id: id, quant: quant},
         success: function (data) { 
            
            $('#refresh-div').load(document.URL + ' #refresh-div');
         }
      });
   }
   
   
   //adding items to Wishlist
   function addWishList(product_id,user_id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
         /* the route pointing to the post function */
         url: '/add-to-wishlist',
         type: 'POST',
         /* send the csrf-token and the input to the controller */
         data: {_token: CSRF_TOKEN, user_id: user_id,product_id:product_id},
         success: function (data) { 
            $(".ajaxdata").text(data.sel_wishlist);
         }
      }); 
   }
</script>
   @endsection