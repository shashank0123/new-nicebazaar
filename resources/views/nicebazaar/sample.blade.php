<?php
use App\Product;
use App\MainCategory;
use App\Testimonial;
use App\Review;
use App\Offer;
$offers = Offer::where('status','Active')->get();
$count_dot = 0;
?>

<style>
 .mySlides {display: none;}
 img {vertical-align: middle; 
   }

   #offer-image { height: 97%; width: 98%; margin: 1% }
 /* Slideshow container */
 .slideshow-container {
    max-width: 80%;
    height: 250px !important;
    position: relative;
    margin: auto;
    border: 1px solid #ccc;
 }

 /* Caption text */
 .text {
    color: #f2f2f2;
    font-size: 15px;
    padding: 8px 12px;
    position: absolute;
    bottom: 8px;
    width: 100%;
    text-align: center;
 }

 /* Number text (1/3 etc) */
 .numbertext {
    color: #f2f2f2;
    font-size: 12px;
    padding: 8px 12px;
    position: absolute;
    top: 0;
 }

 /* The dots/bullets/indicators */
 .dot {
    height: 15px;
    width: 15px;
    margin: 0 2px;
    background-color: #bbb;
    border-radius: 50%;
    display: inline-block;
    /*transition: background-color 0.6s ease;*/
 }

 .active {
    /*background-color: #717171;*/
 }

 /* Fading animation */
 .fade {
    -webkit-animation-name: fade;
    -webkit-animation-duration: 5.5s;
    animation-name: fade;
    animation-duration: 5.5s;
 }

 @-webkit-keyframes fade {
    from {opacity: .4} 
    to {opacity: 1}
 }

 @keyframes fade {
    from {opacity: .4} 
    to {opacity: 1}
 }

 /* On smaller screens, decrease text size */
 @media only screen and (max-width: 300px) {
    .text {font-size: 11px}
 }
 @media screen and (max-width: 543px){
.productslide1 { margin: auto;text-align: center; }

.productslide1 h2{ font-size:21px!important;  }
 .productslide1 h4{ font-size: 13px!important;  }
 .sliderdot { margin-top: 45px!important; }
 .mobile-view{ display: none; }
}

.productslide1 h2{ font-size: 35px;  }
 .productslide1 h4{ font-size: 16px;  }

</style>
@extends('layouts/nicebazaar2')
@section('content')
<section id="wrapper">
   <div class="ImageSlider">
      <!-- Module ImageSlider -->
      <div id="itimageslider" data-interval="5000" data-pause="true">
         <div class="itnivo-slider">
            <div id="spinner"></div>
            <div id="slider" class="">
               @foreach($banners as $banner)
               <a href="#" >
                  <img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/banner/{{ $banner->image_url }}" alt="itslider-1" width="auto" height="auto" />
               </a>
               @endforeach
            </div>
         </div>
      </div>

      <br>

      {{-- Offer SLider --}}
      @if(!empty($offers))
      <h1 style="text-align: center; font-size: 2.5em">Current Offers</h1>
      <div class="slideshow-container">
         
         @foreach($offers as $offer)
         <?php $count_dot++; ?>
         <div class="mySlides fade">
            <a href="/offers/{{$offer->id}}">
               <div class="row">
               <div class="col-sm-5" style="text-align: right;">
                  <img src="{{ URL::to('/') }}/offers/{{ $offer->image_url }} " id="offer-image" />
               </div>

               <div class="col-sm-7">
                  <br>
                   <div class="productslide1" ><h2 >{{strtoupper($offer->offer_title)}}</h2><h4 >{{strtoupper($offer->offer_sub_title)}}</h4></div><br>
                   <div class="mobile-view"><div><p>{{$offer->offer_short_description}}</p></div>
                   
                   <div style="text-align: center;"><a href="/offers/{{$offer->id}}"><u>View Details</u></a></div></div>
               </div>
            </div> 
            </a>       
       </div>
       @endforeach

       

    </div>
    <br>

    <div class="sliderdot" style="text-align:center">
      @for($i=0; $i<$count_dot ; $i++)
       <span class="dot"></span> 
       @endfor
      {{--  <span class="dot"></span> 
       <span class="dot"></span> --}} 
    </div>
    @endif

    <script>
      var slideIndex = 0;
      showSlides();

      function showSlides() {
       var i;
       var slides = document.getElementsByClassName("mySlides");
       var dots = document.getElementsByClassName("dot");
       for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";  
     }
     slideIndex++;
     if (slideIndex > slides.length) {slideIndex = 1}    
       for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
     }
     slides[slideIndex-1].style.display = "block";  
     dots[slideIndex-1].className += " active";
  setTimeout(showSlides, 5000); // Change image every 2 seconds
}
</script>
<br><br>
<!-- Module ImageSlider --><!-- Static Block module -->
<div class="slider_banner mt-30">
   @foreach($banners as $banner)
   <div class="slide-banner-info col-md-6">
      <div class="banner-col"><a href="#"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/banner/{{ $banner->image_url }}" alt="" class="img-responsive" width="auto" height="auto" /></a></div>
   </div>
   @endforeach
   <div class="clearfix"></div>
</div>
<!-- /Static block module -->
</div>
<aside id="notifications">
   <div class="container">
   </div>
</aside>
<div class="displayPosition displayPosition1">
   <div class="container">
      <div class="row">
         <!-- Static Block module -->
         <!-- /Static block module -->
         <div class="it_category_feature innovatorythemes">
            <div class="sec-heading text-center mb-30">
               <h3 class="page-heading">Top categories</h3>
            </div>
            <div class="category_feature row">
               <div class="itCategoryFeature owl-carousel">
                  @foreach($maincategory as $row)
                  <div class="item-inner first_item">
                     <div class="item">
                        <div class="innovatory-media-body" >
                           <div class="category-desc col-md-12">
                              <a href="/products/{{$row->id}}"><img class="img-icon" src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $row->image1 }}" width="100px" height="100px" alt=""/>
                                 <b><span>{{$row->Mcategory_name}}</span></b>
                                 <span class="icon-drop-mobile"></span></a>
                              </div>
                           </div>
                        </div>
                     </div>
                     @endforeach
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="displayPosition displayPosition2">
      <div class="container">
         <div class="row">
            <div class="displayPosition displayPosition3">
               <div class="container">
                  <div class="row">
                     <div class="It-Specials-prod innovatoryProductFilter innovatoryProductGrid clearfix">
                        <div class="sec-heading mb-30 text-center">
                           <h3><span>Specials</span> Products</h3>
                        </div>
                        <div class="prod-filter itContent row">
                           <div class="itProductFilter col-lg-8 padd-0">
                              <div class="owlProductFilter-It-Specials-prod  owl-carousel">
                                 <?php $allproducts = Product::where('status','Active')->orderBy('created_at','desc')->limit(12)->get(); ?>
                                 @if(isset($allproducts))
                                 @foreach($allproducts as $product)
                                 <?php
                                 $productcategory = Product::where('products.id',$product->id)->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();
                                 ?>
                                 <?php $discount=(($product->mrp - $product->sell_price)*100)/$product->mrp;
                                 $rate = Review::where('product_id',$product->id)->avg('rating');
                                 $final_rate = floor($rate);
                                 ?>
                                 <div class="item-inner  ajax_block_product wow fadeInUp">
                                    <div class="item product-box  ajax_block_product js-product-miniature" data-id-product="27" data-id-product-attribute="434">
                                       <div class="innovatoryProduct-container">
                                          <div class="innovatoryProduct-image">
                                             <a href="productdetail/{{ $product->id }}" class="thumbnail product-thumbnail"><span class="cover_image"><img src = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}"  data-full-size-image-url = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" alt="" width="200px" height="200px" /></span><span class="hover_image"><img src = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}"   data-full-size-image-url = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" alt="" width="200px" height="200px" /></span></a>          
                                             <div class="innovatoryActions hidden-lg-up">
                                                <div class="innovatoryActions-i">
                                                   <div class="innovatoryCart innovatoryItem" onclick="addCart({{$product->id}})">
                                                      <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea"><input type="hidden" value="27" name="id_product"><a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart"><i class="ti-shopping-cart"></i></a>
                                                   </div>
                                                   <div class="innovatoryQuick innovatoryItem"> <a  onclick='$("#myemd").attr("src","{{ URL::to("/") }}/assetsss/images/AdminProduct/{{ $product->image1 }}");' class="quick-view" data-link-action="quickview" title="Quickview" data-toggle="modal" data-target="#myModal">
                                                      <i class="ti-eye"></i>
                                                   </a>
                                                </div>
                                                @if(!empty($member->id) || !empty($user->id))
                                                <div class="innovatoryItem innovatoryWish">
                                                   <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="addWishList({{ $product->id }},<?php if(!empty($member->id)){ echo $member->id; } else { echo $user->id; }?>)"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                </div>
                                                @else
                                                <div class="innovatoryItem innovatoryWish">
                                                   <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="Swal('Please Login First')"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                </div>
                                                @endif
                                             }
                                          }
                                       </div>
                                    </div>
                                 </div>
                                 <div class="innovatory-product-description">
                                    <h2 class="h2 productName" itemprop="name"><a href="">{{$product->name}}</a></h2>
                                    <div class="product-detail">
                                       <div class="innovatory-product-price-and-shipping"><span itemprop="price" class="price">Rs. {{$product->sell_price}}</span>
                                          <span class="old-price regular-price">Rs. {{$product->mrp}}</span>
                                       </div>
                                       <div class="comments_note">
                                          <div class="star_content">
                                             @for($i=0; $i<5 ; $i++)
                                             <i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
                                             @endfor
                                          </div>
                                          <span class="laberCountReview pull-left">Review</span>
                                       </div>
                                    </div>
                                    <div class=" innovatory-product-price-and-shipping">
                                       <span class="bgsave">Save : <span class="savevalue">{{floor($discount)}}%</span></span>
                                    </div>
                                    <div class="innovatory-product-price-and-shipping">
                                       <span>PV 
                                          : {{$product->pv}}</span>
                                       </div>

                                       <div class="innovatoryActions hidden-md-down">
                                          <div class="innovatoryActions-i">
                                             <div class="innovatoryCart innovatoryItem" onclick="addCart({{$product->id}})">
                                                <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea"><input type="hidden" value="27" name="id_product"><a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart"><i class="ti-shopping-cart"></i></a>
                                             </div>
                                             <div class="innovatoryQuick innovatoryItem"><a  onclick='$("#myemd").attr("src","{{ URL::to("/") }}/assetsss/images/AdminProduct/{{ $product->image1 }}");' class="quick-view" data-link-action="quickview" title="Quickview" data-toggle="modal" data-target="#myModal">
                                                <i class="ti-eye"></i>
                                             </a>
                                          </div>
                                          @if(!empty($member->id) || !empty($user->id))
                                          <div class="innovatoryItem innovatoryWish">
                                             <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="addWishList({{ $product->id }},<?php if(!empty($member->id)){ echo $member->id; } else { echo $user->id; }?>)"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                          </div>
                                          @else
                                          <div class="innovatoryItem innovatoryWish">
                                             <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="Swal('Please Login First')"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                          </div>
                                          @endif
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        @endforeach          
                        @endif
                     </div>
                     {{-- <a href="#" class="btn-ajax-loading"><i class="fa fa-refresh"></i> <span>View more (2.450 products) ...</span></a> --}}
                  </div>
                  <div class="col-lg-4 hidden-md-down banner-sec">
                     <div class="banner-col-sm-12"><a href="#"><img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/themes/IT1005/assets/img/product-banner-2.jpg" alt="" class="img-responsive" width="250px" height="300px"></a></div>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <script type="text/javascript">$(document).ready(function() { var owl = $(".owlProductFilter-It-Specials-prod");
                  owl.owlCarousel({
                     rewind:true,nav:true,autoplay:false,dots:false,autoplayHoverPause:true,
                     navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
                     responsive:{
                        0:{
                           items:2
                        },
                        480:{
                           items:2
                        },
                        768:{
                           items:3
                        },
                        992:{
                           items:3
                        },
                        1200:{
                           items:3
                        }
                     }
                  });
               });
            </script>
         </div>
      </div>
   </div>
</div>
<div class="displayPosition displayPosition4">
   <div class="container">
      <div class="row">
         <!-- Static Block module -->
         <div class="offer_banner mt-60 mb-60">
            <div class="col-md-12">
               <div class="banner-col"><a href="#"><img src="https://innovatorythemes.com/prestashop/INNO01/INNO1026_Gomart/IT26/modules/itcustomhtml/views/img/offer-banner.png" alt="" class="img-responsive" width="auto" height="auto" /></a></div>
            </div>
            <div class="clearfix"></div>
         </div>
         <div class="col-md-12 col-lg-4 product-column-style">
            <div class="itProductList itcolumn clearfix">
               <div class="It-new-prod column">
                  <div class="title_block">
                     <h3><a href="">New Products</a></h3>
                  </div>
                  <div class="itProductFilter row">
                     <div class="owlProductFilter-It-new-prod-column innovatoryColumn owl-carousel">
                        <div class="item-inner ajax_block_product  wow fadeInUp animated">
                           @if(!empty($products))
                           @foreach($products as $row)
                           <?php
                           $category = Product::where('products.id',$row->id)->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();

                           ?>
                           <?php $discount=(($row->mrp - $row->sell_price)*100)/$row->mrp;
                           $rate = Review::where('product_id',$row->id)->avg('rating');
                           $final_rate = floor($rate);
                           ?>
                           <div class="item">
                              <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="62" data-id-product-attribute="681">
                                 <div class="innovatory-thumbnail-container">
                                    <div class="row no-margin">
                                       <div class="pull-left product_img">
                                          <a href="/productdetail/{{ $row->id }}" class="thumbnail product-thumbnail">
                                             <img src = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $row->image1 }}" alt = "" data-full-size-image-url = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $row->image1 }}" width="200px" height="150px">
                                          </a>
                                       </div>
                                       <div class="innovatoryMedia-body">
                                          <div class="innovatory-product-description">
                                             <div class="comments_note">
                                                <div class="star_content">
                                                   @for($i=0; $i<5 ; $i++)
                                                   <i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
                                                   @endfor
                                                </div>
                                                <span class="laberCountReview pull-left">Review&nbsp</span>
                                             </div>
                                             <h2 class="h2 productName" itemprop="name"><a href="">{{$row->name}}</a></h2>
                                             <div class="innovatory-product-price-and-shipping">
                                                <span itemprop="price" class="price">Rs. {{$row->sell_price}}</span>&nbsp;&nbsp;
                                                <span class="regular-price">Rs. {{$row->mrp}}</span>
                                             </div>
                                             <div class="innovatory-product-price-and-shipping">
                                                <span class="bgsave">Save :<span class="savevalue"> {{floor($discount)}}%</span></span>
                                             </div>
                                             <div class="innovatory-product-price-and-shipping">
                                                <span>PV : {{$row->pv}}</span>
                                             </div>

                                             <div class="innovatoryActions">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem" onclick="addCart({{$row->id}})">
                                                         <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                         <input type="hidden" value="24" name="id_product">
                                                         <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart">
                                                            <i class="ti-shopping-cart"></i>
                                                         </a>
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a  onclick='$("#myemd").attr("src","{{ URL::to("/") }}/assetsss/images/AdminProduct/{{ $row->image1 }}");' class="quick-view" data-link-action="quickview" title="Quickview" data-toggle="modal" data-target="#myModal">
                                                            <i class="ti-eye"></i>
                                                         </a>
                                                      </div>
                                                      @if(!empty($member->id) || !empty($user->id))
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="addWishList({{$col->id }},<?php if(!empty($member->id)){ echo $member->id; } else { echo $user->id; }?>)"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                      @else
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="Swal('Please Login First')"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                      @endif
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              @endforeach
                              @endif
                           </div>
                        </div>
                     </div>
                     <script type="text/javascript">
                        $(document).ready(function() {
                           var owl = $(".owlProductFilter-It-new-prod-column");
                           owl.owlCarousel({
                              dots:false,
                              loop:false,
                              autoplay:false,
                              autoplayTimeout:6000,
                              slideSpeed :6000,
                              autoplayHoverPause:true,
                              nav:true,
                              navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
                              responsive:{
                                 0:{
                                    items:1
                                 },
                                 480:{
                                    items:1
                                 },
                                 768:{
                                    items:1
                                 },
                                 991:{
                                    items:1
                                 },
                                 1200:{
                                    items:1
                                 }
                              }
                           });
                        });
                     </script>
                  </div>
               </div>
            </div>
            <?php
            $featuredproducts = Product::where('trending','yes')->where('status','Active')->orderBy('created_at','desc')->limit(4)->get();
            ?>
            <div class="col-md-12 col-lg-4 product-column-style">
               <div class="itProductList itcolumn clearfix">
                  <div class="It-bestseller-prod column">
                     <div class="title_block">
                        <h3><span>Best Seller</span></h3>
                     </div>
                     <div class="itProductFilter row">
                        <div class="owlProductFilter-It-bestseller-prod-column innovatoryColumn owl-carousel">
                           <div class="item-inner ajax_block_product  wow fadeInUp animated">
                              @if(isset($featuredproducts))
                              @foreach($featuredproducts as $col)
                              <?php
                              $featurecategory = Product::where('products.id',$col->id)->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();
                              
                              ?>
                              <?php $discount=(($col->mrp - $col->sell_price)*100)/$col->mrp;
                              $rate = Review::where('product_id',$col->id)->avg('rating');
                              $final_rate = floor($rate);
                              ?>
                              <div class="item">
                                 <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="24" data-id-product-attribute="480">
                                    <div class="innovatory-thumbnail-container">
                                       <div class="row no-margin">
                                          <div class="pull-left product_img">
                                             <a href="/productdetail/{{ $col->id }}" class="thumbnail product-thumbnail">
                                                <img src = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $col->image1 }}" alt = "" data-full-size-image-url = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $col->image1 }}" width="200px" height="150px">
                                             </a>
                                          </div>
                                          <div class="innovatoryMedia-body">
                                             <div class="innovatory-product-description">
                                                <div class="comments_note">
                                                   <div class="star_content">
                                                      @for($i=0; $i<5 ; $i++)
                                                      <i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
                                                      @endfor
                                                   </div>
                                                   <span class="laberCountReview pull-left">Review&nbsp</span>
                                                </div>
                                                <h2 class="h2 productName" itemprop="name"><a href="">{{$col->name}}</a></h2>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span itemprop="price" class="price">Rs. {{$col->mrp}}</span>&nbsp;&nbsp;
                                                   <span class="regular-price">Rs. {{$col->sell_price}}</span>
                                                </div>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span class="bgsave">Save :<span class="savevalue"> {{floor($discount)}}%</span></span>
                                                </div>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span>PV : {{$col->pv}}</span>
                                                </div>
                                                
                                                <div class="innovatoryActions">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem" onclick="addCart({{$col->id}})">
                                                         <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                         <input type="hidden" value="24" name="id_product">
                                                         <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart">
                                                            <i class="ti-shopping-cart"></i>
                                                         </a>
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a  onclick='$("#myemd").attr("src","{{ URL::to("/") }}/assetsss/images/AdminProduct/{{ $col->image1 }}");' class="quick-view" data-link-action="quickview" title="Quickview" data-toggle="modal" data-target="#myModal">
                                                            <i class="ti-eye"></i>
                                                         </a>
                                                      </div>
                                                      @if(!empty($member->id) || !empty($user->id))
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="addWishList({{$col->id }},<?php if(!empty($member->id)){ echo $member->id; } else { echo $user->id; }?>)"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                      @else
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="Swal('Please Login First')"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                      @endif
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              @endforeach
                              @endif
                           </div>
                        </div>
                     </div>
                     <script type="text/javascript">
                        $(document).ready(function() {
                           var owl = $(".owlProductFilter-It-bestseller-prod-column");
                           owl.owlCarousel({
                              dots:false,
                              loop:false,
                              autoplay:false,
                              autoplayTimeout:6000,
                              slideSpeed :6000,
                              autoplayHoverPause:true,
                              nav:true,
                              navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
                              responsive:{
                                 0:{
                                    items:1
                                 },
                                 480:{
                                    items:1
                                 },
                                 768:{
                                    items:1
                                 },
                                 991:{
                                    items:1
                                 },
                                 1200:{
                                    items:1
                                 }
                              }
                           });
                        });
                     </script>
                  </div>
               </div>
            </div>
            <?php
            $trendproducts = Product::where('trending','yes')->where('status','Active')->orderBy('name','asc')->limit(4)->get();
            ?>
            <div class="col-md-12 col-lg-4 product-column-style">
               <div class="itProductList itcolumn clearfix">
                  <div class="It-Specials-prod column">
                     <div class="title_block">
                        <h3><span>Specials</span></h3>
                     </div>
                     <div class="itProductFilter row">
                        <div class="owlProductFilter-It-Specials-prod-column innovatoryColumn owl-carousel">
                           <div class="item-inner ajax_block_product  wow fadeInUp animated">
                              @if(isset($trendproducts))
                              @foreach($trendproducts as $set)
                              <?php
                              $trendcategory = Product::where('products.id',$set->id)->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();
                              
                              ?>
                              <?php $discount=(($set->mrp - $set->sell_price)*100)/$set->mrp;
                              $rate = Review::where('product_id',$set->id)->avg('rating');
                              $final_rate = floor($rate);
                              ?>
                              <div class="item">
                                 <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="67" data-id-product-attribute="749">
                                    <div class="innovatory-thumbnail-container">
                                       <div class="row no-margin">
                                          <div class="pull-left product_img">
                                             <a href="/productdetail/{{ $set->id }}" class="thumbnail product-thumbnail">
                                                <img src = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $set->image1 }}" alt = "" data-full-size-image-url = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $set->image1 }}" width="200px" height="150px">
                                             </a>
                                          </div>
                                          <div class="innovatoryMedia-body">
                                             <div class="innovatory-product-description">
                                                <div class="comments_note">
                                                   <div class="star_content">
                                                      @for($i=0; $i<5 ; $i++)
                                                      <i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
                                                      @endfor
                                                   </div>
                                                   <span class="laberCountReview pull-left">Review&nbsp</span>
                                                </div>
                                                <h2 class="h2 productName" itemprop="name"><a href="">{{$set->name}}</a></h2>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span itemprop="price" class="price">Rs. {{$set->sell_price}}</span>&nbsp;&nbsp;
                                                   <span class="regular-price">Rs. {{$set->mrp}}</span>
                                                </div>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span class="bgsave">Save :  <span class="savevalue">{{floor($discount)}}

                                                   %</span> </span>
                                                </div>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span>PV : {{$set->pv}}</span>
                                                </div>
                                                
                                                <div class="innovatoryActions">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem" onclick="addCart({{$col->id}})">
                                                         <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                         <input type="hidden" value="67" name="id_product">
                                                         <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart">
                                                            <i class="ti-shopping-cart"></i>
                                                         </a>
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a  onclick='$("#myemd").attr("src","{{ URL::to("/") }}/assetsss/images/AdminProduct/{{ $set->image1 }}");' class="quick-view" data-link-action="quickview" title="Quickview" data-toggle="modal" data-target="#myModal">
                                                            <i class="ti-eye"></i>
                                                         </a>
                                                      </div>
                                                      @if(!empty($member->id) || !empty($user->id))
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="addWishList({{ $set->id }},<?php if(!empty($member->id)){ echo $member->id; } else { echo $user->id; }?>)"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                      @else
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="Swal('Please Login First')"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                      @endif
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              @endforeach
                              @endif
                           </div>
                        </div>
                     </div>
                     <script type="text/javascript">
                        $(document).ready(function() {
                           var owl = $(".owlProductFilter-It-Specials-prod-column");
                           owl.owlCarousel({
                              dots:false,
                              loop:false,
                              autoplay:false,
                              autoplayTimeout:6000,
                              slideSpeed :6000,
                              autoplayHoverPause:true,
                              nav:true,
                              navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
                              responsive:{
                                 0:{
                                    items:1
                                 },
                                 480:{
                                    items:1
                                 },
                                 768:{
                                    items:1
                                 },
                                 991:{
                                    items:1
                                 },
                                 1200:{
                                    items:1
                                 }
                              }
                           });
                        });
                     </script>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="displayPosition displayPosition6">
      <!-- Static Block module -->
      <!-- /Static block module -->
   </div>
</section>
<div id="myModal" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
            <embed id="myemd" src=""
            frameborder="0" width="100%" height="450px">
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
</div>
@stop
@section('script')
<script>
   var countproduct=<?php if(session()->get('count') != null){echo session()->get('count'); } else { echo 0;} ?>;
   
   function addToCart(){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var pro_id = $('#hidden_id').val();
      var quant = $('#quickquantity').val();
      countproduct++;
      $.ajax({
         /* the route pointing to the post function */
         url: '/add-cart',
         type: 'POST',
         /* send the csrf-token and the input to the controller */
         data: {_token: CSRF_TOKEN, id: pro_id, quantity: quant},
         success: function (data) { 
            $("#show-total").html(countproduct);           
         }
      }); 
   }
   
   //modal  box of quick view
   function openview(id,name,price,mrp,description,img1,img2,img3,rate){
      $('#hidden_id').val(id);    
      $('#viewname').text(name);
      $('#viewdescription').text(description);
      $('#viewprice').text(price);
   //For main image
   $("#imggallery1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#imggallery2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   
   //for a tag (data-zoom attribute)    
   $("#imgdz2").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#imgdz3").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   $("#imgdz1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   
   //for a tag (data-images attribute)
   $("#imgdz1").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#imgdz2").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#imgdz3").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   
   //for image tag (src attribute)
   $("#img1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#img2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#img3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   $("#imgt1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#imgt2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#imgt3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   var discount = ((mrp-price)*100)/mrp;
   $('#viewdiscount').text(discount.toFixed(2));
   $('#viewrate').val(rate);
   
}


function getOffers(){
   Swal('Hii');
}



   //adding items to cart
   function addCart(id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      countproduct++;

      $.ajax({
         /* the route pointing to the post function */
         url: '/add-to-cart',
         type: 'POST',
         /* send the csrf-token and the input to the controller */
         data: {_token: CSRF_TOKEN, id: id},
         success: function (data) { 
      // window.location.href = '/';
      $(".show-total").html(data.count);           
   }
}); 
   }
   
   //adding items to Wishlist
   function addWishList(product_id,user_id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
         /* the route pointing to the post function */
         url: '/add-to-wishlist',
         type: 'POST',
         /* send the csrf-token and the input to the controller */
         data: {_token: CSRF_TOKEN, user_id: user_id,product_id:product_id},
         success: function (data) { 
            $(".ajaxdata").text(data.sel_wishlist);
         }
      }); 
   }
</script>
@endsection