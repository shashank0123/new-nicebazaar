<?php
use App\Review;
$total = 0;
?>

<div class="cart-list-content" id="showminicart">
   <div class="cart-product-wrap">
   @if($cartproducts != null)
   <?php $i=0; $final_rate = 0; ?>

   
      @foreach($cartproducts as $product)
      <div class="products">
         <div class="img" id="listhide{{$product->id}}">

         <a class="thumb" href="/productdetail/{{$product->id}}"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }} " alt="" class="attachment-shop_thumbnail" style="width: 70px; height: 70px"></a>
      </div>
         <a href="#" class="title">{{$product->name}}</a>
      </br>
         <span class="quantity">
            <span class="amount">Rs. {{$product->sell_price}}</span> x {{$quantity[$i++]}}
         </span>
         <div class="rate">
         </div>
         <a title="Remove this item" class="mini-cart-remove" onclick="deleteProduct({{$product->id}})"><span class="fa fa-times" style="float: right;"></span></a>                  
      </div>
      @endforeach                      
  
</div><!--/.cart-list -->
   @endif

   <div class="cart-list-subtotal" style="margin:10px;">
      <strong class="txt fl">SubTotal:    </strong>
      <strong class="currency fr" id="bill">Rs. {{$bill}}</strong>
   </div><!--/.cart-list-subtotal --><br><br>
   <div class="cart-list-subtotal" style="margin:10px;">
      <strong class="txt fl">Total PV:    </strong>
      <strong class="currency fr" id="pv">{{$pv}}</strong>
   </div><!--/.cart-list-subtotal --><br><br>
    <div class="cart-buttons" onclick="goToCart()"><a href="<?php if(!empty($member->id) || !empty($user->id)){echo "/cart";}else{echo "/en/login";}?>">Check out <i class="ti-angle-right"></i></a></div>
</div>