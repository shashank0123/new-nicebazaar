<?php
use App\Product;
use App\MainCategory;
use App\Banner;
use App\Testimonial;
use App\Review;
$maincategory = MainCategory::where('category_id',0)->get();
$total = 0;

$final_rate=5;

?>

{{-- <div id="product-views" class="hidden-sm-down"> --}}
                        {{-- <div class="w3-bar w3-black" style="text-align: right;">
                         <button class="w3-bar-item w3-button" onclick="openCity('grid')">Grid</button>
                         <button class="w3-bar-item w3-button" onclick="openCity('list')">List</button>
                      </div> --}}
<br>
<div  id="product-views">
                      <div id="grid" class="w3-container city">
                        <div id="js-product-list">
                           <div  class="innovatoryProductGrid innovatoryProducts">
                              <div class="row" id="productCatList">
                                 @if($searched_products != null)
                                 @foreach($searched_products as $product)
                                 <input type="hidden" id="cat_id" value="{{$product->id}}">
                                 <div class="item-inner col-lg-4 col-xl-4 col-md-4 col-sm-6 col-xs-6  last-in-line   last-item-of-tablet-line    last-item-of-mobile-line " >
                                    <article class="product-miniature js-product-miniature" data-id-product="28" data-id-product-attribute="418" itemscope itemtype="http://schema.org/Product">
                                       <div class="innovatoryProduct-container item">
                                          <div class="innovatoryProduct-image" >
                                             <a href="/productdetail/{{$product->id}}">
                                                <img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" class="thumbnail product-thumbnail" width="250px" height="250px" style="text-align: center;">
                                             </a>
                                             <!-- <span class="innovatoryNew-label">New</span> -->
                                             <div class="innovatoryActions hidden-lg-up">
                                                <div class="innovatoryActions-i">
                                                   <div class="innovatoryCart innovatoryItem">
                                                      <!-- <form action="" method="post"> -->
                                                         <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                         <input type="hidden" value="28" name="id_product">
                                                         <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart" onclick="addCart({{$product->id}})">
                                                            <i class="ti-shopping-cart"></i>
                                                         </a>
                                                         <!-- </form> -->
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a  onclick='$("#myemd").attr("src","{{ URL::to("/") }}/assetsss/images/AdminProduct/{{ $product->image1 }}");' class="quick-view" data-link-action="quickview" title="Quickview" data-toggle="modal" data-target="#myModal">
                                                            <i class="ti-eye"></i>
                                                         </a>
                                                      </div>
                                                      @if(!empty($member->id) || !empty($user->id))
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="addWishList({{ $product->id }},<?php if(!empty($member->id)) { echo $member->id; } else { echo $user->id ;} ?>)"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                      @else
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="Swal('Please Login First')"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                      @endif
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="innovatory-product-description">
                                                <h2 class="h2 productName" itemprop="name"><a href="">{{$product->name}}</a></h2>
                                                <div class="product-detail">
                                                   <div class="innovatory-product-price-and-shipping">
                                                      <span itemprop="price" class="price">Rs. {{ $product->sell_price }}</span>
                                                      &nbsp;&nbsp;
                                                      <span class="regular-price">Rs. {{$product->mrp}}</span><br>

                                                      <span itemprop="price" class="price">PV : {{$product->pv}}</span>
                                                      
                                                   </div>
                                                   <div class="comments_note">
                                                      <div class="star_content">
                                                         @for($i=0; $i<5 ; $i++)
                                                         <i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
                                                         @endfor
                                                      </div>
                                                      <?php $discount=(($product->mrp - $product->sell_price)*100)/$product->mrp;
                                                      $rate = Review::where('product_id',$product->id)->avg('rating');
                                                      $final_rate = floor($rate);
                                                      ?>
                                                      <div>
                                                         <span class="bgsave">Save : {{floor($discount)}}%</span>
                                                      </div>
                                                      
                                                      <span class="laberCountReview pull-left">Review</span>
                                                   </div>
                                                </div>
                                                <div class="innovatoryActions hidden-md-down">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem">
                                                         <!-- <form action="" method="post"> -->
                                                            <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                            <input type="hidden" value="28" name="id_product">
                                                            <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart" onclick="addCart({{$product->id}})">
                                                               <i class="ti-shopping-cart"></i>
                                                            </a>
                                                            <!-- </form> -->
                                                         </div>
                                                         <div class="innovatoryQuick innovatoryItem">
                                                            <a  onclick='$("#myemd").attr("src","{{ URL::to("/") }}/assetsss/images/AdminProduct/{{ $product->image1 }}");' class="quick-view" data-link-action="quickview" title="Quickview" data-toggle="modal" data-target="#myModal">
                                                               <i class="ti-eye"></i>
                                                            </a>
                                                         </div>
                                                         @if(!empty($member->id) || !empty($user->id))
                                                         <div class="innovatoryItem innovatoryWish">
                                                            <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="addWishList({{ $product->id }},<?php if(!empty($member->id)) { echo $member->id; } else { echo $user->id ;} ?>)"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                         </div>
                                                         @else
                                                         <div class="innovatoryItem innovatoryWish">
                                                            <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="Swal('Please Login First')"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                         </div>
                                                         @endif
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </article>
                                       </div>
                                       @endforeach
                                       @endif

                                       @if($searched_products == null)
                                       <div class="section-common container">
                                          <div class="row" style="text-align: center; margin-top: 50px;">
                                             <h1 style="font-size: 3em">No products Available</h1>
                                          </div>
                                       </div>
                                       @endif   
                                    </div>
                                 </div>
                                 <div class="row no-margin">
                                 </div>
                              </div>
                           </div>



                           {{-- //List view --}}

                           <div id="list" class="w3-container city" style="display:none">
                             <div id="js-product-list">
                           <div  class="innovatoryProductGrid innovatoryProducts">
                              <div class="row" id="productCat">
                                 @if($searched_products != null)
                                 @foreach($searched_products as $product)
                                 <input type="hidden" id="cat_id" value="{{$product->id}}">
                                 <div class="item-inner col-lg-12 col-xl-12 col-md-12 col-sm-12 col-xs-12  last-in-line   last-item-of-tablet-line    last-item-of-mobile-line " >
                                    <article class="product-miniature js-product-miniature" data-id-product="28" data-id-product-attribute="418" itemscope itemtype="http://schema.org/Product">
                                       <div class="innovatoryProduct-container item">
                                          <div class="innovatoryProduct-image col-sm-4" >
                                             <a href="/productdetail/{{$product->id}}">
                                                <img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" class="thumbnail product-thumbnail" width="250px" height="250px" style="text-align: center;">
                                             </a>
                                             <!-- <span class="innovatoryNew-label">New</span> -->
                                             <div class="innovatoryActions hidden-lg-up">
                                                <div class="innovatoryActions-i">
                                                   <div class="innovatoryCart innovatoryItem">
                                                      <!-- <form action="" method="post"> -->
                                                         <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                         <input type="hidden" value="28" name="id_product">
                                                         <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart" onclick="addCart({{$product->id}})">
                                                            <i class="ti-shopping-cart"></i>
                                                         </a>
                                                         <!-- </form> -->
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a  onclick='$("#myemd").attr("src","{{ URL::to("/") }}/assetsss/images/AdminProduct/{{ $product->image1 }}");' class="quick-view" data-link-action="quickview" title="Quickview" data-toggle="modal" data-target="#myModal">
                                                            <i class="ti-eye"></i>
                                                         </a>
                                                      </div>
                                                      @if(!empty($member->id) || !empty($user->id))
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="addWishList({{ $product->id }},<?php if(!empty($member->id)) { echo $member->id; } else { echo $user->id ;} ?>)"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                      @else
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="Swal('Please Login First')"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                      @endif
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="innovatory-product-description col-sm-8">
                                                <br>

                                                <h2 class="h2 productName" itemprop="name"><a href=""><b>{{$product->name}}</b></a></h2><br>
                                                <div class="product-detail">


                                                    <div class="row">
                                                   <div class="col-sm-8">
                                                       <div class="innovatory-product-price-and-shipping" style="text-align: left;">
                                                      <span itemprop="price" class="price">Rs. {{ $product->sell_price }}</span>
                                                      &nbsp;&nbsp;
                                                      <span class="regular-price">Rs. {{$product->mrp}}</span><br>

                                                      <span itemprop="price" class="price">PV : {{$product->pv}}</span>
                                                      <br><br>
                                                         <h2 class="h2 productName" itemprop="name"><a href="">{{$product->short_descriptions}}</a></h2>


                                                   </div>
                                                   </div>

                                                   <div class="col-sm-4">
                                                       <div class="comments_note">
                                                      <div class="star_content">
                                                         @for($i=0; $i<5 ; $i++)
                                                         <i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
                                                         @endfor
                                                      </div>
                                                      <?php $discount=(($product->mrp - $product->sell_price)*100)/$product->mrp;
                                                      $rate = Review::where('product_id',$product->id)->avg('rating');
                                                      $final_rate = floor($rate);
                                                      ?>
                                                      <div>
                                                         <span class="bgsave">Save : {{floor($discount)}}%</span>
                                                      </div>
                                                      
                                                      <span class="laberCountReview pull-left">Review</span>
                                                   </div>
                                                   </div>
                                                </div>


                                                  
                                                  

                                                </div>
                                                <div class="innovatoryActions hidden-md-down">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem">
                                                         <!-- <form action="" method="post"> -->
                                                            <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                            <input type="hidden" value="28" name="id_product">
                                                            <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart" onclick="addCart({{$product->id}})">
                                                               <i class="ti-shopping-cart"></i>
                                                            </a>
                                                            <!-- </form> -->
                                                         </div>
                                                         <div class="innovatoryQuick innovatoryItem">
                                                            <a  onclick='$("#myemd").attr("src","{{ URL::to("/") }}/assetsss/images/AdminProduct/{{ $product->image1 }}");' class="quick-view" data-link-action="quickview" title="Quickview" data-toggle="modal" data-target="#myModal">
                                                               <i class="ti-eye"></i>
                                                            </a>
                                                         </div>
                                                         @if(!empty($member->id) || !empty($user->id))
                                                         <div class="innovatoryItem innovatoryWish">
                                                            <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="addWishList({{ $product->id }},<?php if(!empty($member->id)) { echo $member->id; } else { echo $user->id ;} ?>)"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                         </div>
                                                         @else
                                                         <div class="innovatoryItem innovatoryWish">
                                                            <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="Swal('Please Login First')"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                         </div>
                                                         @endif
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </article>
                                       </div>
                                       @endforeach
                                       @endif

                                       @if($searched_products == null)
                                       <div class="section-common container">
                                          <div class="row" style="text-align: center; margin-top: 50px;">
                                             <h1 style="font-size: 3em">No products Available</h1>
                                          </div>
                                       </div>
                                       @endif   
                                    </div>
                                 </div>
                                 <div class="row no-margin">
                                 </div>
                              </div>
                         </div>



                         <script>
                           function openCity(cityName) {
                            var i;
                            var x = document.getElementsByClassName("city");
                            for (i = 0; i < x.length; i++) {
                             x[i].style.display = "none";  
                          }
                          document.getElementById(cityName).style.display = "block";  
                       }
                    </script>
                 </div>
