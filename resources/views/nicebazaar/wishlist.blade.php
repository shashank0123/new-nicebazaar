<?php
use App\Product;
use App\Review;
use App\Wishlist;
$bill = 0;
$i=0;
if(!empty($member->id)){
   $wishlistItems = Wishlist::where('user_id',$member->id)->get();
}
elseif(!empty($user->id)){
   $wishlistItems = Wishlist::where('user_id',$user->id)->get();
}
$count = 0;
?>
@extends('layouts/nicebazaar1')
<style>
   .block_newsletter .news-icon { line-height: 0 !important }
</style>
@section('content')
<div class="innovatoryBreadcrumb">
   <div class="container">
      <nav data-depth="1" class="breadcrumb hidden-sm-down">
         <ol itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
               <a itemprop="item" href="">
                  <span itemprop="name">Wishlist</span>
               </a>
               <meta itemprop="position" content="1">
            </li>
         </ol>
      </nav>
   </div>
</div>
<section id="wrapper">
   <aside id="notifications">
      <div class="container">
      </div>
   </aside>
   <div class="container">
      <div class="row">
         <div id="content-wrapper">
            <section id="main">
               <div class="cart-grid row">
                  <!-- Left Block: cart product informations & shpping -->
                  <div class="cart-grid-body col-xs-12 col-lg-12">
                     <!-- cart products detailed -->
                     <div class="card cart-container">
                        <div class="card-block">
                           <h1 class="h1">Wishlist</h1>
                        </div>
                        <hr>
                        <div class="cart-overview js-cart" data-refresh-url="">
                           <ul class="cart-items">
                              <li class="cart-item">
                                 <div class="product-line-grid table-responsive">
                                    @if(!empty($wishlistItems))

                                    <table class="table">
                                       <thead>
                                          <tr>
                                             <th></th>
                                             <th>Poduct</th>
                                             <th>Price</th>
                                             <th>Action</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          @foreach($wishlistItems as $item)

                                          <?php $product = Product::where('id',$item->product_id)->first(); $count++;?>
                                          <tr id="wishlist{{$product->id}}">
                                             <td> <span class="product-image media-middle" style="width: auto;">
                                                <img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" alt="Accumsan Fusce" width="75px" height="75px">
                                             </span>
                                          </td>
                                          <td><a class="label" href="" data-id_customization="0">{{$product->name}}</a></td>
                                          <td><span class="value">Rs. {{ $product->sell_price }}</span></td>
                                          <td><a onclick="deleteWishlistProduct({{$product->id}},<?php if(!empty($member->id)){ echo $member->id; } else{echo $user->id;} ?>)">
                                             <span class="fa fa-trash" style="font-size: 20px;"></span>
                                          </a>
                                          <a onclick="addCart({{$product->id}},<?php if(!empty($member->id)){ echo $member->id; } else{echo $user->id;} ?>)">
                                             <span class="fa fa-shopping-cart" style="font-size: 20px;"></span>
                                          </a>
                                       </td>
                                    </tr>
                                    @endforeach
                              </tbody>
                              </table>
                             
                              
                              @endif

                              <div class="clearfix"></div>
                           </div>
                        </li>
                     </ul>

                     @if($count==0)

                     <div class="sattement" style="text-align: center;">
                                 <h1>Nothing in wishlist</h1>
                              </div>

                     @endif
                  </div>
               </div>
               <a class="label" href="/en">
                  <i class="material-icons"></i>Continue shopping
               </a>
            </div>
         </div>
      </section>
   </div>
</div>
</div>
<div class="displayPosition displayPosition6">
   <!-- Static Block module -->
   <!-- /Static block module -->
</div>
</section>
@stop
@section('script')
<script>
   var countproduct=<?php if(session()->get('count') != null){echo session()->get('count'); } else { echo 0;} ?>;
   
   function deleteWishlistProduct(pid,uid){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var pro_id = $('#hidden_id').val();
      var quant = $('#quickquantity').val();
      countproduct++;
      $.ajax({
         /* the route pointing to the post function */
         url: '/wishlist/delete-product/id',
         type: 'POST',
         datatype: 'JSON',
         /* send the csrf-token and the input to the controller */
         data: {_token: CSRF_TOKEN, pid: pid, uid: uid},
         success: function (data) { 
           window.location.href = '/wishlist';  
        }
     }); 
   }
   
   //modal  box of quick view
   function openview(id,name,price,mrp,description,img1,img2,img3,rate){
      $('#hidden_id').val(id);    
      $('#viewname').text(name);
      $('#viewdescription').text(description);
      $('#viewprice').text(price);
   //For main image
   $("#imggallery1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#imggallery2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   
   //for a tag (data-zoom attribute)    
   $("#imgdz2").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#imgdz3").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   $("#imgdz1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   
   //for a tag (data-images attribute)
   $("#imgdz1").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#imgdz2").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#imgdz3").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   
   //for image tag (src attribute)
   $("#img1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#img2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#img3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   $("#imgt1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#imgt2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#imgt3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   var discount = ((mrp-price)*100)/mrp;
   $('#viewdiscount').text(discount.toFixed(2));
   $('#viewrate').val(rate);
   
}



   //adding items to cart
   function addCart(id,user_id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var  quantity=1;
     
      $.ajax({
         /* the route pointing to the post function */
         url: '/add-cart',
         type: 'POST',
         /* send the csrf-token and the input to the controller */
         data: {_token: CSRF_TOKEN, id: id,uid:user_id,quantity:quantity},
         success: function (data) { 
            window.location.href = '/wishlist';     
         }
      }); 
   }
   
   //adding items to Wishlist
   function addWishList(product_id,user_id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
         /* the route pointing to the post function */
         url: '/add-to-wishlist',
         type: 'POST',
         /* send the csrf-token and the input to the controller */
         data: {_token: CSRF_TOKEN, user_id: user_id,product_id:product_id},
         success: function (data) { 
            window.location.href = "/";
            $("#ajaxdata").text(data.sel_wishlist);iu
         }
      }); 
   }
</script>
@endsection