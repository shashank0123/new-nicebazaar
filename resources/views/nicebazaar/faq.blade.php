@extends('layouts.nicebazaar1')

<style type="text/css">
	.faq_section{
		margin:40px auto;
	}
	.FaQ_Each{
		padding-bottom: 10px;
	}
	.box  {
		/*background: rgba(246, 246, 246, 1);*/
		color: #ffffff;
		padding-top: 15px;
		padding-bottom:15px;
		padding-left: 20px;
		font-size: 13px;
		text-transform: uppercase;
		background-color: #ff0000;
		cursor:pointer;
		border: 1px solid #d9d9d9;
	}

	.box span:hover { color: #37bc9b; }
	.draw {
		display: none;
		background: #ffffff;
		padding: 20px;
		border-bottom: 1px solid #d9d9d9;
		border-left: 1px solid #d9d9d9;
		border-right: 1px solid #d9d9d9;
		color: #000000;
		padding-left:30px;
	}
	.pull-right { margin-right: 20px }
	.faq_section{ text-align: center; }
	#other{
		display: none;
	}

	.frequently-desc { padding: 50px 50px 10px; text-align: center; line-height: 1.5; font-size: 16px }
</style>
@section('content')

<div class="container">
	<div class="row">				
		<div class="success-bottom "><br>
			{{-- <h4>Terms & Conditions</h4><br> --}}
			<div class="container" id="table-cont">
				<!-- Main Content - start -->
				<main>
					<section class="container">
						<div class="container-fluid" style="background-color:#ff00000a;">
							<div class="row">
								<div class="col-sm-2"></div> 
								<div  class="col-sm-8 textpolicy">
									<div class="container-fluid faq">
										<div class="frequently-content">
											<div class="frequently-desc">
												<h4>Below are frequently asked questions, you may find the answer for yourself</h4>
											</div>
										</div>
										<div class="container faq_section">
											@if($faqs != null)
											{{-- Row1 --}}
											@foreach($faqs as $faq)
											<div class="FaQ_Each">
												<section class="box">
													<h4>
														<span>{{$faq->questions}}</span>
														<span class="pull-right">
															<i class="fa fa-plus" aria-hidden="true"></i>
															<i class="fa fa-minus" id="other" aria-hidden="true"></i>
														</span></h4>
													</section>
													<section class="draw">
														{{$faq->answers}}
													</section>
												</div>
												@endforeach
												@endif
												{{-- Row 2 --}}
											</div>
										</div>
									</div> 
									<div class="col-sm-2"></div>      
								</div>
							</div>							
						</section>
					</main>
					<!-- Main Content - end -->
				</div>	
			</div>
		</div>
	</div>
	@endsection 
	@section('script')
	
	<script>
		$(document).ready(function(){
			$(".box").click(function(){
				$(this).next().slideToggle("fast");
				$(this).find('i').toggle();
			});  

		});
	</script>

	@endsection

