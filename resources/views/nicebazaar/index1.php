<?php
   use App\Product;
   use App\MainCategory;
   use App\Testimonial;
   use App\Review;
   
   $count=0;
   ?>
@extends('layouts/nicebazaar');
<section id="wrapper">
   <div class="ImageSlider">
      <!-- Module ImageSlider -->
      <div id="itimageslider" data-interval="5000" data-pause="true">
         <div class="itnivo-slider">
            <div id="spinner"></div>
            <div id="slider" class=""><a href="#" title="itslider-1"><img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itimageslider/images/itslider-1.jpg" alt="itslider-1" width="auto" height="auto" /></a><a href="#" title="itslider-2"><img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itimageslider/images/itslider-2.jpg" alt="itslider-2" width="auto" height="auto" /></a></div>
         </div>
      </div>
      <!-- Module ImageSlider --><!-- Static Block module -->
      <div class="slider_banner mt-30">
         <div class="slide-banner-info col-md-6">
            <div class="banner-col"><a href="#"><img src="https://innovatorythemes.com/prestashop/INNO01/INNO1026_Gomart/IT26/modules/itcustomhtml/views/img/two-banner-1.jpg" alt="" class="img-responsive" width="auto" height="auto" /></a></div>
         </div>
         <div class="slide-banner-info col-md-6">
            <div class="banner-col"><a href="#"><img src="https://innovatorythemes.com/prestashop/INNO01/INNO1026_Gomart/IT26/modules/itcustomhtml/views/img/two-banner-2.jpg" alt="" class="img-responsive" width="auto" height="auto" /></a></div>
         </div>
         <div class="clearfix"></div>
      </div>
      <!-- /Static block module -->
   </div>
   <aside id="notifications">
      <div class="container">
      </div>
   </aside>
   <div class="displayPosition displayPosition1">
      <div class="container">
         <div class="row">
            <!-- Static Block module -->
            <!-- /Static block module -->
            <div class="it_category_feature innovatorythemes">
               <div class="sec-heading text-center mb-30">
                  <h3 class="page-heading">Top categories</h3>
               </div>
               <div class="category_feature row">
                  <div class="itCategoryFeature owl-carousel">
                     @foreach($maincategory as $row)
                     <div class="item-inner first_item">
                        <div class="item">
                           <div class="innovatory-media-body">
                              <div class="cat-img col-md-6 padd-0">
                                 <a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/3-frozen-item" title="Frozen  Item"><img  class="replace-2x" alt="Frozen  Item" src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/img/c/3_thumb.jpg" width="auto" height="auto" /></a>
                              </div>
                              <div class="category-desc col-md-6">
                                 <h2 class="categoryName"><a href="/products/{{$row->id}}">{{$row->Mcategory_name}}</a></h2>
                              </div>
                           </div>
                        </div>
                     </div>
                     @endforeach
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="It-Specials-prod innovatoryProductFilter innovatoryProductGrid clearfix">
      <div class="sec-heading mb-30 text-center">
         <h3><span>Specials</span> Products</h3>
      </div>
      <div class="prod-filter itContent row">
         <div class="itProductFilter col-lg-8 padd-0">
            <div class="owlProductFilter-It-Specials-prod  owl-carousel">
               <?php $allproducts = Product::where('status','Active')->orderBy('created_at','desc')->limit(12)->get(); ?>
            @if(isset($allproducts))
            @foreach($allproducts as $product)
            <?php
            $productcategory = Product::where('products.id',$product->id)->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();
            ?>
            <?php $discount=(($product->mrp - $product->sell_price)*100)/$product->mrp;

            $rate = Review::where('product_id',$product->id)->avg('rating');
            $final_rate = floor($rate);
            ?>
               <div class="item-inner  ajax_block_product wow fadeInUp">
                  <div class="item product-box  ajax_block_product js-product-miniature" data-id-product="29" data-id-product-attribute="403">
                     <div class="innovatoryProduct-container">
                        <div class="innovatoryProduct-image">
                           <a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/29-403-swirl-home-town.html#/5-color-grey/29-kg-05_kg" class="thumbnail product-thumbnail"><span class="cover_image"><img src = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}"  data-full-size-image-url = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" alt="" width="auto" height="auto" /></span><span class="hover_image"><img src = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" data-full-size-image-url = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" alt="" width="auto" height="auto" /></span></a><span class="innovatoryNew-label">New</span><span class="innovatorySale-label">-15%</span>       
                           <div class="innovatoryActions hidden-lg-up">
                              <div class="innovatoryActions-i">
                                 <div class="innovatoryCart innovatoryItem">
                                    <form action="" method="post"><input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea"><input type="hidden" value="29" name="id_product"><a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart"><i class="ti-shopping-cart"></i></a></form>
                                 </div>
                                 <div class="innovatoryQuick innovatoryItem"><a href="#" class="quick-view" data-link-action="quickview" title="Quickview"><i class="ti-eye"></i><span>Quickview</span></a></div>
                                 <div class="innovatoryItem innovatoryWish">
                                    <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_29" title="Add to wishlist" href="#" rel="29" onclick="WishlistCart('wishlist_block_list', 'add', '29', false, 1); return false;"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="innovatory-product-description">
                           <h2 class="h2 productName" itemprop="name"><a href="">{{$product->name}}</a></h2>
                           <div class="product-detail">
                              <div class="innovatory-product-price-and-shipping"><span itemprop="price" class="price">$21.25</span><span class="old-price regular-price">$25.00</span></div>
                              <div class="comments_note">
                                 <div class="star_content">
                                    <div class="star star_on"><i class="fa fa-star"></i></div>
                                    <div class="star star_on"><i class="fa fa-star"></i></div>
                                    <div class="star star_on"><i class="fa fa-star"></i></div>
                                    <div class="star star_on"><i class="fa fa-star"></i></div>
                                    <div class="star"><i class="fa fa-star"></i></div>
                                 </div>
                                 <span class="laberCountReview pull-left">Review</span>
                              </div>
                           </div>
                           <div class="innovatoryActions hidden-md-down">
                              <div class="innovatoryActions-i">
                                 <div class="innovatoryCart innovatoryItem">
                                    <form action="" method="post"><input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea"><input type="hidden" value="29" name="id_product"><a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart"><i class="ti-shopping-cart"></i></a></form>
                                 </div>
                                 <div class="innovatoryQuick innovatoryItem"><a href="#" class="quick-view" data-link-action="quickview" title="Quickview"><i class="ti-eye"></i><span>Quickview</span></a></div>
                                 <div class="innovatoryItem innovatoryWish">
                                    <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_29" title="Add to wishlist" href="#" rel="29" onclick="WishlistCart('wishlist_block_list', 'add', '29', false, 1); return false;"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               @endforeach          
            @endif
            </div>
            {{-- <a href="#" class="btn-ajax-loading"><i class="fa fa-refresh"></i> <span>View more (2.450 products) ...</span></a> --}}
         </div>
         <div class="col-lg-4 hidden-md-down banner-sec">
            <div class="banner-col"><a href="#"><img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/themes/IT1005/assets/img/product-banner-2.jpg" alt="" class="img-responsive" width="auto" height="auto"></a></div>
         </div>
         <div class="clearfix"></div>
      </div>
      <script type="text/javascript">$(document).ready(function() { var owl = $(".owlProductFilter-It-Specials-prod");
         owl.owlCarousel({
          rewind:true,nav:true,autoplay:false,dots:false,autoplayHoverPause:true,
             navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
             responsive:{
              0:{
                items:2
              },
                 480:{
                     items:2
                 },
                 768:{
                     items:3
                 },
                 992:{
                     items:3
                 },
                 1200:{
                     items:3
                 }
             }
         });
         });
      </script>
   </div>
   </div>
   </div>
   </div>
   <div class="container">
      <div class="row">
         <div id="content-wrapper">
            <div id="main">
               <div id="content" class="page-home">
                  <!-- Static Block module -->
                  <!-- /Static block module -->
               </div>
               <footer class="page-footer">
                  <!-- Footer content -->
               </footer>
            </div>
         </div>
      </div>
   </div>
   <div class="displayPosition displayPosition4">
      <div class="container">
         <div class="row">
            <!-- Static Block module -->
            <div class="offer_banner mt-60 mb-60">
               <div class="col-md-12">
                  <div class="banner-col"><a href="#"><img src="https://innovatorythemes.com/prestashop/INNO01/INNO1026_Gomart/IT26/modules/itcustomhtml/views/img/offer-banner.png" alt="" class="img-responsive" width="auto" height="auto" /></a></div>
               </div>
               <div class="clearfix"></div>
            </div>
            <!-- /Static block module --><!-- <script type="text/javascript" src="/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itproductfilter/views/js/jquery-1.7.1.min.js"></script> -->
            <div class="col-md-12 col-lg-4 product-column-style">
               <div class="itProductList itcolumn clearfix">
                  <div class="It-new-prod column">
                     <div class="title_block">
                        <h3><a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/new-products">New Products</a></h3>
                     </div>
                     <div class="itProductFilter row">
                        <div class="owlProductFilter-It-new-prod-column innovatoryColumn owl-carousel">
                           <div class="item-inner ajax_block_product  wow fadeInUp animated">
                              @if(isset($products))
                     @foreach($products as $row)
                     <?php

                     $category = Product::where('products.id',$row->id)->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();
                     
                     ?>

                     <?php $discount=(($row->mrp - $row->sell_price)*100)/$row->mrp;

                     $rate = Review::where('product_id',$row->id)->avg('rating');
                     $final_rate = floor($rate);
                     ?>
                              <div class="item">
                                 <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="29" data-id-product-attribute="403">
                                    <div class="innovatory-thumbnail-container">
                                       <div class="row no-margin">
                                          <div class="pull-left product_img">
                                             <a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/29-403-swirl-home-town.html#/5-color-grey/29-kg-05_kg" class="thumbnail product-thumbnail">
                                             <img src = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $row->image1 }}" alt = "" data-full-size-image-url = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $row->image1 }}" width="auto" height="auto">
                                             </a>
                                          </div>
                                          <div class="innovatoryMedia-body">
                                             <div class="innovatory-product-description">
                                                <div class="comments_note">
                                                   <div class="star_content">
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                   </div>
                                                   <span class="laberCountReview pull-left">Review</span>
                                                </div>
                                                <h2 class="h2 productName" itemprop="name"><a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/29-403-swirl-home-town.html#/5-color-grey/29-kg-05_kg">{{$row->name}}</a></h2>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span itemprop="price" class="price">$21.25</span>
                                                   <span class="reduction_percent_display">              
                                                   -15%  
                                                   </span>
                                                   <span class="regular-price">$25.00</span>
                                                </div>
                                                <div class="innovatoryActions">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem">
                                                         <form action="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/cart" method="post">
                                                            <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                            <input type="hidden" value="29" name="id_product">
                                                            <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart">
                                                            <i class="ti-shopping-cart"></i>
                                                            </a>
                                                         </form>
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a href="#" class="quick-view" data-link-action="quickview" title="Quickview">
                                                         <i class="ti-eye"></i><span></span>
                                                         </a>
                                                      </div>
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_29" title="Add to wishlist" href="#" rel="29" onclick="WishlistCart('wishlist_block_list', 'add', '29', false, 1); return false;"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              @endforeach
                     @endif
                           </div>
                        </div>
                     </div>
                     <script type="text/javascript">
                        $(document).ready(function() {
                         var owl = $(".owlProductFilter-It-new-prod-column");
                         owl.owlCarousel({
                           dots:false,
                           loop:false,
                           autoplay:false,
                             autoplayTimeout:6000,
                             slideSpeed :6000,
                                 autoplayHoverPause:true,
                             nav:true,
                             navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
                             responsive:{
                               0:{
                                 items:1
                               },
                                 480:{
                                     items:1
                                 },
                                 768:{
                                     items:1
                                 },
                                 991:{
                                     items:1
                                 },
                                 1200:{
                                     items:1
                                 }
                             }
                         });
                        });
                     </script>
                  </div>
               </div>
            </div>
            <div class="col-md-12 col-lg-4 product-column-style">
               <div class="itProductList itcolumn clearfix">
                  <div class="It-bestseller-prod column">
                     <div class="title_block">
                        <h3><span>Best Seller</span></h3>
                     </div>
                     <div class="itProductFilter row">
                        <div class="owlProductFilter-It-bestseller-prod-column innovatoryColumn owl-carousel">
                           <div class="item-inner ajax_block_product  wow fadeInUp animated">
                              <div class="item">
                                 <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="25" data-id-product-attribute="463">
                                    <div class="innovatory-thumbnail-container">
                                       <div class="row no-margin">
                                          <div class="pull-left product_img">
                                             <a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/25-463-chequered-set.html#/8-color-white/29-kg-05_kg" class="thumbnail product-thumbnail">
                                             <img src = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/293-cart_default/chequered-set.jpg" alt = "" data-full-size-image-url = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/293-large_default/chequered-set.jpg" width="auto" height="auto">
                                             </a>
                                          </div>
                                          <div class="innovatoryMedia-body">
                                             <div class="innovatory-product-description">
                                                <div class="comments_note">
                                                   <div class="star_content">
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                   </div>
                                                   <span class="laberCountReview pull-left">Review</span>
                                                </div>
                                                <h2 class="h2 productName" itemprop="name"><a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/25-463-chequered-set.html#/8-color-white/29-kg-05_kg">Tincidunt ut Laoreet</a></h2>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span itemprop="price" class="price">$27.00</span>
                                                   <span class="reduction_percent_display">              
                                                   -10%  
                                                   </span>
                                                   <span class="regular-price">$30.00</span>
                                                </div>
                                                <div class="innovatoryActions">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem">
                                                         <form action="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/cart" method="post">
                                                            <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                            <input type="hidden" value="25" name="id_product">
                                                            <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart">
                                                            <i class="ti-shopping-cart"></i>
                                                            </a>
                                                         </form>
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a href="#" class="quick-view" data-link-action="quickview" title="Quickview">
                                                         <i class="ti-eye"></i><span></span>
                                                         </a>
                                                      </div>
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_25" title="Add to wishlist" href="#" rel="25" onclick="WishlistCart('wishlist_block_list', 'add', '25', false, 1); return false;"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                 <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="22" data-id-product-attribute="509">
                                    <div class="innovatory-thumbnail-container">
                                       <div class="row no-margin">
                                          <div class="pull-left product_img">
                                             <a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/22-509-slotted-spoon-stainless-silver.html#/8-color-white/29-kg-05_kg" class="thumbnail product-thumbnail">
                                             <img src = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/314-cart_default/slotted-spoon-stainless-silver.jpg" alt = "" data-full-size-image-url = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/314-large_default/slotted-spoon-stainless-silver.jpg" width="auto" height="auto">
                                             </a>
                                          </div>
                                          <div class="innovatoryMedia-body">
                                             <div class="innovatory-product-description">
                                                <div class="comments_note">
                                                   <div class="star_content">
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                   </div>
                                                   <span class="laberCountReview pull-left">Review</span>
                                                </div>
                                                <h2 class="h2 productName" itemprop="name"><a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/22-509-slotted-spoon-stainless-silver.html#/8-color-white/29-kg-05_kg">Tation Ullamcorpe</a></h2>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span itemprop="price" class="price">$36.00</span>
                                                   <span class="reduction_percent_display">              
                                                   -10%  
                                                   </span>
                                                   <span class="regular-price">$40.00</span>
                                                </div>
                                                <div class="innovatoryActions">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem">
                                                         <form action="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/cart" method="post">
                                                            <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                            <input type="hidden" value="22" name="id_product">
                                                            <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart">
                                                            <i class="ti-shopping-cart"></i>
                                                            </a>
                                                         </form>
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a href="#" class="quick-view" data-link-action="quickview" title="Quickview">
                                                         <i class="ti-eye"></i><span></span>
                                                         </a>
                                                      </div>
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" href="#" rel="22" onclick="WishlistCart('wishlist_block_list', 'add', '22', false, 1); return false;"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                 <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="24" data-id-product-attribute="480">
                                    <div class="innovatory-thumbnail-container">
                                       <div class="row no-margin">
                                          <div class="pull-left product_img">
                                             <a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/24-480-two-tier-fruit-basket.html#/5-color-grey/30-kg-10_kg" class="thumbnail product-thumbnail">
                                             <img src = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/300-cart_default/two-tier-fruit-basket.jpg" alt = "" data-full-size-image-url = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/300-large_default/two-tier-fruit-basket.jpg" width="auto" height="auto">
                                             </a>
                                          </div>
                                          <div class="innovatoryMedia-body">
                                             <div class="innovatory-product-description">
                                                <div class="comments_note">
                                                   <div class="star_content">
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                   </div>
                                                   <span class="laberCountReview pull-left">Review&nbsp</span>
                                                </div>
                                                <h2 class="h2 productName" itemprop="name"><a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/24-480-two-tier-fruit-basket.html#/5-color-grey/30-kg-10_kg">Finibus Bonorum</a></h2>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span itemprop="price" class="price">$48.00</span>
                                                   <span class="reduction_percent_display">              
                                                   -20%  
                                                   </span>
                                                   <span class="regular-price">$60.00</span>
                                                </div>
                                                <div class="innovatoryActions">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem">
                                                         <form action="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/cart" method="post">
                                                            <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                            <input type="hidden" value="24" name="id_product">
                                                            <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart">
                                                            <i class="ti-shopping-cart"></i>
                                                            </a>
                                                         </form>
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a href="#" class="quick-view" data-link-action="quickview" title="Quickview">
                                                         <i class="ti-eye"></i><span></span>
                                                         </a>
                                                      </div>
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_24" title="Add to wishlist" href="#" rel="24" onclick="WishlistCart('wishlist_block_list', 'add', '24', false, 1); return false;"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="item-inner ajax_block_product  wow fadeInUp animated">
                              <div class="item">
                                 <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="27" data-id-product-attribute="434">
                                    <div class="innovatory-thumbnail-container">
                                       <div class="row no-margin">
                                          <div class="pull-left product_img">
                                             <a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/27-434-willy-wardrobe.html#/5-color-grey/30-kg-10_kg" class="thumbnail product-thumbnail">
                                             <img src = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/205-cart_default/willy-wardrobe.jpg" alt = "" data-full-size-image-url = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/205-large_default/willy-wardrobe.jpg" width="auto" height="auto">
                                             </a>
                                          </div>
                                          <div class="innovatoryMedia-body">
                                             <div class="innovatory-product-description">
                                                <div class="comments_note">
                                                   <div class="star_content">
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                   </div>
                                                   <span class="laberCountReview pull-left">Review</span>
                                                </div>
                                                <h2 class="h2 productName" itemprop="name"><a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/27-434-willy-wardrobe.html#/5-color-grey/30-kg-10_kg">Amet Consectetuer</a></h2>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span itemprop="price" class="price">$41.80</span>
                                                   <span class="reduction_percent_display">              
                                                   -5% 
                                                   </span>
                                                   <span class="regular-price">$44.00</span>
                                                </div>
                                                <div class="innovatoryActions">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem">
                                                         <form action="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/cart" method="post">
                                                            <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                            <input type="hidden" value="27" name="id_product">
                                                            <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart">
                                                            <i class="ti-shopping-cart"></i>
                                                            </a>
                                                         </form>
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a href="#" class="quick-view" data-link-action="quickview" title="Quickview">
                                                         <i class="ti-eye"></i><span></span>
                                                         </a>
                                                      </div>
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_27" title="Add to wishlist" href="#" rel="27" onclick="WishlistCart('wishlist_block_list', 'add', '27', false, 1); return false;"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                 <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="28" data-id-product-attribute="418">
                                    <div class="innovatory-thumbnail-container">
                                       <div class="row no-margin">
                                          <div class="pull-left product_img">
                                             <a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/28-418-kaira-wood-shoe.html#/5-color-grey/30-kg-10_kg" class="thumbnail product-thumbnail">
                                             <img src = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/280-cart_default/kaira-wood-shoe.jpg" alt = "" data-full-size-image-url = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/280-large_default/kaira-wood-shoe.jpg" width="auto" height="auto">
                                             </a>
                                          </div>
                                          <div class="innovatoryMedia-body">
                                             <div class="innovatory-product-description">
                                                <div class="comments_note">
                                                   <div class="star_content">
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                   </div>
                                                   <span class="laberCountReview pull-left">Review</span>
                                                </div>
                                                <h2 class="h2 productName" itemprop="name"><a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/28-418-kaira-wood-shoe.html#/5-color-grey/30-kg-10_kg">Osccaecati Primis Mad</a></h2>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span itemprop="price" class="price">$35.00</span>
                                                </div>
                                                <div class="innovatoryActions">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem">
                                                         <form action="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/cart" method="post">
                                                            <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                            <input type="hidden" value="28" name="id_product">
                                                            <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart">
                                                            <i class="ti-shopping-cart"></i>
                                                            </a>
                                                         </form>
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a href="#" class="quick-view" data-link-action="quickview" title="Quickview">
                                                         <i class="ti-eye"></i><span></span>
                                                         </a>
                                                      </div>
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_28" title="Add to wishlist" href="#" rel="28" onclick="WishlistCart('wishlist_block_list', 'add', '28', false, 1); return false;"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                 <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="37" data-id-product-attribute="387">
                                    <div class="innovatory-thumbnail-container">
                                       <div class="row no-margin">
                                          <div class="pull-left product_img">
                                             <a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/37-387-stoves-tree.html#/5-color-grey/29-kg-05_kg" class="thumbnail product-thumbnail">
                                             <img src = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/274-cart_default/stoves-tree.jpg" alt = "" data-full-size-image-url = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/274-large_default/stoves-tree.jpg" width="auto" height="auto">
                                             </a>
                                          </div>
                                          <div class="innovatoryMedia-body">
                                             <div class="innovatory-product-description">
                                                <div class="comments_note">
                                                   <div class="star_content">
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                   </div>
                                                   <span class="laberCountReview pull-left">Review</span>
                                                </div>
                                                <h2 class="h2 productName" itemprop="name"><a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/37-387-stoves-tree.html#/5-color-grey/29-kg-05_kg">Quisque in arcu</a></h2>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span itemprop="price" class="price">$34.20</span>
                                                   <span class="reduction_percent_display">              
                                                   -10%  
                                                   </span>
                                                   <span class="regular-price">$38.00</span>
                                                </div>
                                                <div class="innovatoryActions">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem">
                                                         <form action="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/cart" method="post">
                                                            <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                            <input type="hidden" value="37" name="id_product">
                                                            <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart">
                                                            <i class="ti-shopping-cart"></i>
                                                            </a>
                                                         </form>
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a href="#" class="quick-view" data-link-action="quickview" title="Quickview">
                                                         <i class="ti-eye"></i><span></span>
                                                         </a>
                                                      </div>
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_37" title="Add to wishlist" href="#" rel="37" onclick="WishlistCart('wishlist_block_list', 'add', '37', false, 1); return false;"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <script type="text/javascript">
                        $(document).ready(function() {
                         var owl = $(".owlProductFilter-It-bestseller-prod-column");
                         owl.owlCarousel({
                           dots:false,
                           loop:false,
                           autoplay:false,
                             autoplayTimeout:6000,
                             slideSpeed :6000,
                                 autoplayHoverPause:true,
                             nav:true,
                             navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
                             responsive:{
                               0:{
                                 items:1
                               },
                                 480:{
                                     items:1
                                 },
                                 768:{
                                     items:1
                                 },
                                 991:{
                                     items:1
                                 },
                                 1200:{
                                     items:1
                                 }
                             }
                         });
                        });
                     </script>
                  </div>
               </div>
            </div>
            <div class="col-md-12 col-lg-4 product-column-style">
               <div class="itProductList itcolumn clearfix">
                  <div class="It-Specials-prod column">
                     <div class="title_block">
                        <h3><span>Specials</span></h3>
                     </div>
                     <div class="itProductFilter row">
                        <div class="owlProductFilter-It-Specials-prod-column innovatoryColumn owl-carousel">
                           <div class="item-inner ajax_block_product  wow fadeInUp animated">
                              <div class="item">
                                 <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="29" data-id-product-attribute="403">
                                    <div class="innovatory-thumbnail-container">
                                       <div class="row no-margin">
                                          <div class="pull-left product_img">
                                             <a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/29-403-swirl-home-town.html#/5-color-grey/29-kg-05_kg" class="thumbnail product-thumbnail">
                                             <img src = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/204-cart_default/swirl-home-town.jpg" alt = "" data-full-size-image-url = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/204-large_default/swirl-home-town.jpg" width="auto" height="auto">
                                             </a>
                                          </div>
                                          <div class="innovatoryMedia-body">
                                             <div class="innovatory-product-description">
                                                <div class="comments_note">
                                                   <div class="star_content">
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                   </div>
                                                   <span class="laberCountReview pull-left">Review</span>
                                                </div>
                                                <h2 class="h2 productName" itemprop="name"><a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/29-403-swirl-home-town.html#/5-color-grey/29-kg-05_kg">Accumsan Fusce</a></h2>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span itemprop="price" class="price">$21.25</span>
                                                   <span class="reduction_percent_display">              
                                                   -15%  
                                                   </span>
                                                   <span class="regular-price">$25.00</span>
                                                </div>
                                                <div class="innovatoryActions">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem">
                                                         <form action="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/cart" method="post">
                                                            <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                            <input type="hidden" value="29" name="id_product">
                                                            <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart">
                                                            <i class="ti-shopping-cart"></i>
                                                            </a>
                                                         </form>
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a href="#" class="quick-view" data-link-action="quickview" title="Quickview">
                                                         <i class="ti-eye"></i><span></span>
                                                         </a>
                                                      </div>
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_29" title="Add to wishlist" href="#" rel="29" onclick="WishlistCart('wishlist_block_list', 'add', '29', false, 1); return false;"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                 <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="27" data-id-product-attribute="434">
                                    <div class="innovatory-thumbnail-container">
                                       <div class="row no-margin">
                                          <div class="pull-left product_img">
                                             <a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/27-434-willy-wardrobe.html#/5-color-grey/30-kg-10_kg" class="thumbnail product-thumbnail">
                                             <img src = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/205-cart_default/willy-wardrobe.jpg" alt = "" data-full-size-image-url = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/205-large_default/willy-wardrobe.jpg" width="auto" height="auto">
                                             </a>
                                          </div>
                                          <div class="innovatoryMedia-body">
                                             <div class="innovatory-product-description">
                                                <div class="comments_note">
                                                   <div class="star_content">
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                   </div>
                                                   <span class="laberCountReview pull-left">Review</span>
                                                </div>
                                                <h2 class="h2 productName" itemprop="name"><a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/27-434-willy-wardrobe.html#/5-color-grey/30-kg-10_kg">Amet Consectetuer</a></h2>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span itemprop="price" class="price">$41.80</span>
                                                   <span class="reduction_percent_display">              
                                                   -5% 
                                                   </span>
                                                   <span class="regular-price">$44.00</span>
                                                </div>
                                                <div class="innovatoryActions">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem">
                                                         <form action="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/cart" method="post">
                                                            <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                            <input type="hidden" value="27" name="id_product">
                                                            <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart">
                                                            <i class="ti-shopping-cart"></i>
                                                            </a>
                                                         </form>
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a href="#" class="quick-view" data-link-action="quickview" title="Quickview">
                                                         <i class="ti-eye"></i><span></span>
                                                         </a>
                                                      </div>
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_27" title="Add to wishlist" href="#" rel="27" onclick="WishlistCart('wishlist_block_list', 'add', '27', false, 1); return false;"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                 <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="67" data-id-product-attribute="749">
                                    <div class="innovatory-thumbnail-container">
                                       <div class="row no-margin">
                                          <div class="pull-left product_img">
                                             <a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/67-749-slotted-spoon-stainless-silver.html#/8-color-white/29-kg-05_kg" class="thumbnail product-thumbnail">
                                             <img src = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/494-cart_default/slotted-spoon-stainless-silver.jpg" alt = "" data-full-size-image-url = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/494-large_default/slotted-spoon-stainless-silver.jpg" width="auto" height="auto">
                                             </a>
                                          </div>
                                          <div class="innovatoryMedia-body">
                                             <div class="innovatory-product-description">
                                                <div class="comments_note">
                                                   <div class="star_content">
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                   </div>
                                                   <span class="laberCountReview pull-left">Review&nbsp</span>
                                                </div>
                                                <h2 class="h2 productName" itemprop="name"><a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/67-749-slotted-spoon-stainless-silver.html#/8-color-white/29-kg-05_kg">Cras varius libero</a></h2>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span itemprop="price" class="price">$36.00</span>
                                                   <span class="reduction_percent_display">              
                                                   -10%  
                                                   </span>
                                                   <span class="regular-price">$40.00</span>
                                                </div>
                                                <div class="innovatoryActions">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem">
                                                         <form action="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/cart" method="post">
                                                            <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                            <input type="hidden" value="67" name="id_product">
                                                            <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart">
                                                            <i class="ti-shopping-cart"></i>
                                                            </a>
                                                         </form>
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a href="#" class="quick-view" data-link-action="quickview" title="Quickview">
                                                         <i class="ti-eye"></i><span></span>
                                                         </a>
                                                      </div>
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_67" title="Add to wishlist" href="#" rel="67" onclick="WishlistCart('wishlist_block_list', 'add', '67', false, 1); return false;"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="item-inner ajax_block_product  wow fadeInUp animated">
                              <div class="item">
                                 <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="58" data-id-product-attribute="633">
                                    <div class="innovatory-thumbnail-container">
                                       <div class="row no-margin">
                                          <div class="pull-left product_img">
                                             <a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/58-633-swirl-home-town.html#/5-color-grey/29-kg-05_kg" class="thumbnail product-thumbnail">
                                             <img src = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/406-cart_default/swirl-home-town.jpg" alt = "" data-full-size-image-url = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/406-large_default/swirl-home-town.jpg" width="auto" height="auto">
                                             </a>
                                          </div>
                                          <div class="innovatoryMedia-body">
                                             <div class="innovatory-product-description">
                                                <div class="comments_note">
                                                   <div class="star_content">
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                   </div>
                                                   <span class="laberCountReview pull-left">Review&nbsp</span>
                                                </div>
                                                <h2 class="h2 productName" itemprop="name"><a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/58-633-swirl-home-town.html#/5-color-grey/29-kg-05_kg">Desires to obtain</a></h2>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span itemprop="price" class="price">$21.25</span>
                                                   <span class="reduction_percent_display">              
                                                   -15%  
                                                   </span>
                                                   <span class="regular-price">$25.00</span>
                                                </div>
                                                <div class="innovatoryActions">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem">
                                                         <form action="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/cart" method="post">
                                                            <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                            <input type="hidden" value="58" name="id_product">
                                                            <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart">
                                                            <i class="ti-shopping-cart"></i>
                                                            </a>
                                                         </form>
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a href="#" class="quick-view" data-link-action="quickview" title="Quickview">
                                                         <i class="ti-eye"></i><span></span>
                                                         </a>
                                                      </div>
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_58" title="Add to wishlist" href="#" rel="58" onclick="WishlistCart('wishlist_block_list', 'add', '58', false, 1); return false;"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                 <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="26" data-id-product-attribute="442">
                                    <div class="innovatory-thumbnail-container">
                                       <div class="row no-margin">
                                          <div class="pull-left product_img">
                                             <a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/26-442-travel-tea-ceramic.html#/5-color-grey/29-kg-05_kg" class="thumbnail product-thumbnail">
                                             <img src = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/288-cart_default/travel-tea-ceramic.jpg" alt = "" data-full-size-image-url = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/288-large_default/travel-tea-ceramic.jpg" width="auto" height="auto">
                                             </a>
                                          </div>
                                          <div class="innovatoryMedia-body">
                                             <div class="innovatory-product-description">
                                                <div class="comments_note">
                                                   <div class="star_content">
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star star_on"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                   </div>
                                                   <span class="laberCountReview pull-left">Review</span>
                                                </div>
                                                <h2 class="h2 productName" itemprop="name"><a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/26-442-travel-tea-ceramic.html#/5-color-grey/29-kg-05_kg">Diam Nonummy</a></h2>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span itemprop="price" class="price">$51.00</span>
                                                   <span class="reduction_percent_display">              
                                                   -15%  
                                                   </span>
                                                   <span class="regular-price">$60.00</span>
                                                </div>
                                                <div class="innovatoryActions">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem">
                                                         <form action="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/cart" method="post">
                                                            <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                            <input type="hidden" value="26" name="id_product">
                                                            <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart">
                                                            <i class="ti-shopping-cart"></i>
                                                            </a>
                                                         </form>
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a href="#" class="quick-view" data-link-action="quickview" title="Quickview">
                                                         <i class="ti-eye"></i><span></span>
                                                         </a>
                                                      </div>
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_26" title="Add to wishlist" href="#" rel="26" onclick="WishlistCart('wishlist_block_list', 'add', '26', false, 1); return false;"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                 <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="64" data-id-product-attribute="713">
                                    <div class="innovatory-thumbnail-container">
                                       <div class="row no-margin">
                                          <div class="pull-left product_img">
                                             <a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/64-713-swirl-home-town.html#/5-color-grey/29-kg-05_kg" class="thumbnail product-thumbnail">
                                             <img src = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/474-cart_default/swirl-home-town.jpg" alt = "" data-full-size-image-url = "https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/474-large_default/swirl-home-town.jpg" width="auto" height="auto">
                                             </a>
                                          </div>
                                          <div class="innovatoryMedia-body">
                                             <div class="innovatory-product-description">
                                                <div class="comments_note">
                                                   <div class="star_content">
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                      <div class="star"><i class="fa fa-star"></i></div>
                                                   </div>
                                                   <span class="laberCountReview pull-left">Review&nbsp</span>
                                                </div>
                                                <h2 class="h2 productName" itemprop="name"><a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/home/64-713-swirl-home-town.html#/5-color-grey/29-kg-05_kg">Fermentum urna</a></h2>
                                                <div class="innovatory-product-price-and-shipping">
                                                   <span itemprop="price" class="price">$21.25</span>
                                                   <span class="reduction_percent_display">              
                                                   -15%  
                                                   </span>
                                                   <span class="regular-price">$25.00</span>
                                                </div>
                                                <div class="innovatoryActions">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem">
                                                         <form action="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/cart" method="post">
                                                            <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                            <input type="hidden" value="64" name="id_product">
                                                            <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart">
                                                            <i class="ti-shopping-cart"></i>
                                                            </a>
                                                         </form>
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a href="#" class="quick-view" data-link-action="quickview" title="Quickview">
                                                         <i class="ti-eye"></i><span></span>
                                                         </a>
                                                      </div>
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_64" title="Add to wishlist" href="#" rel="64" onclick="WishlistCart('wishlist_block_list', 'add', '64', false, 1); return false;"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <script type="text/javascript">
                        $(document).ready(function() {
                         var owl = $(".owlProductFilter-It-Specials-prod-column");
                         owl.owlCarousel({
                           dots:false,
                           loop:false,
                           autoplay:false,
                             autoplayTimeout:6000,
                             slideSpeed :6000,
                                 autoplayHoverPause:true,
                             nav:true,
                             navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
                             responsive:{
                               0:{
                                 items:1
                               },
                                 480:{
                                     items:1
                                 },
                                 768:{
                                     items:1
                                 },
                                 991:{
                                     items:1
                                 },
                                 1200:{
                                     items:1
                                 }
                             }
                         });
                        });
                     </script>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="displayPosition displayPosition6">
   </div>
</section>