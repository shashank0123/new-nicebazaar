<?php
use App\Product;
use App\Review;
?>

@extends('layouts/nicebazaar1')
@section('content')

<div class="container">
<h1 style="text-align: center; font-size: 40px"><u>Offers List</u></h1><br>
	<div class="row">
		<div class="col-sm-3">
			<div class="it-special-products product-column-style" data-items="1" data-speed="1000" data-autoplay="0" data-time="3000"  data-arrow="0" data-pagination="0" data-move="1" data-pausehover="0"  data-md="1" data-sm="1" data-xs="1" data-xxs="1">
				<?php
				$featuredproducts = Product::where('trending','yes')->where('status','Active')->orderBy('created_at','desc')->limit(4)->get();
				?>
				<div class="itProductList itcolumn">
					<div class="title_block">
						<h3><span>New Product</span></h3>
					</div>
					<div class="block-content">
						<div class="itProductFilter row">
							<div class="special-item owl-carousel">
								<div class="item-product">
									@if(isset($featuredproducts))
									@foreach($featuredproducts as $col)
									<?php
									$featurecategory = Product::where('products.id',$col->id)->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();

									?>
									<?php $discount=(($col->mrp - $col->sell_price)*100)/$col->mrp;
									$rate = Review::where('product_id',$col->id)->avg('rating');
									$final_rate = floor($rate);
									?>
									<div class="item">
										<div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="46" data-id-product-attribute="289">
											<div class="innovatory-thumbnail-container">
												<div class="row no-margin">
													<div class="pull-left product_img">
														<a href="/productdetail/{{$col->id}}" class="thumbnail product-thumbnail">
															<img src = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $col->image1 }}" alt = "" data-full-size-image-url = "" width="100px" height="100px">
														</a>
													</div>
													<div class="innovatoryMedia-body">
														<div class="innovatory-product-description">
															<div class="comments_note">
																<div class="star_content">
																	@for($i=0; $i<5 ; $i++)
																	<i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
																	@endfor
																</div>
																<span class="laberCountReview pull-left">Review</span>
															</div>
															<h2 class="h2 productName" itemprop="name"><a href="/productdetail/{{$col->id}}">{{$col->name}}</a></h2>
															<div class="innovatory-product-price-and-shipping">
																<span itemprop="price" class="price">Rs.{{$col->sell_price}}</span>
																&nbsp;&nbsp;
																<span class="regular-price">Rs. {{$col->mrp}}</span>
															</div>
															<div>
																<span class="bgsave">Save : {{floor($discount)}}%</span>
															</div>
															<div class="innovatory-product-price-and-shipping">
																<span>PV : {{$col->pv}}</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									@endforeach
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		
<div id="itleftbanners" class="block hidden-md-down">
               <ul class="banner-col">
                  <li class="itleftbanners-container">
                     <a href="#" title="Left Banner 1">
                        <img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itleftbanners/images/left-banner-1.jpg" alt="Left Banner 1" width="auto" height="auto"/>
                     </a>
                  </li>
               </ul>
            </div>
		
		<div class="it-special-products product-column-style" data-items="1" data-speed="1000" data-autoplay="0" data-time="3000"  data-arrow="0" data-pagination="0" data-move="1" data-pausehover="0"  data-md="1" data-sm="1" data-xs="1" data-xxs="1">
			<?php
			$trendproducts = Product::where('trending','yes')->where('status','Active')->orderBy('name','asc')->limit(4)->get();
			?>
			<div class="itProductList itcolumn">
				<div class="title_block">
					<h3><span>Special Product</span></h3>
				</div>
				<div class="block-content">
					<div class="itProductFilter row">
						<div class="special-item owl-carousel">
							<div class="item-product">
								@if(isset($trendproducts))
								@foreach($trendproducts as $set)
								<?php
								$trendcategory = Product::where('products.id',$set->id)->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();

								?>
								<?php $discount=(($set->mrp - $set->sell_price)*100)/$set->mrp;
								$rate = Review::where('product_id',$set->id)->avg('rating');
								$final_rate = floor($rate);
								?>
								<div class="item">
									<div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="46" data-id-product-attribute="289">
										<div class="innovatory-thumbnail-container">
											<div class="row no-margin">
												<div class="pull-left product_img">
													<a href="/productdetail/{{ $set->id }}" class="thumbnail product-thumbnail">
														<img src = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $set->image1 }}" alt = "" data-full-size-image-url = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $set->image1 }}" width="100px" height="100px">
													</a>
												</div>
												<div class="innovatoryMedia-body">
													<div class="innovatory-product-description">
														<div class="comments_note">
															<div class="star_content">
																@for($i=0; $i<5 ; $i++)
																<i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
																@endfor
															</div>
															<span class="laberCountReview pull-left">Review</span>
														</div>
														<h2 class="h2 productName" itemprop="name"><a href="/productdetail/{{ $set->id }}">{{$set->name}}</a></h2>
														<div class="innovatory-product-price-and-shipping">
															<span itemprop="price" class="price">Rs. {{$set->sell_price}}</span>&nbsp;&nbsp;
															<span class="regular-price">Rs. {{$set->mrp}}</span>
														</div>
														<div class="innovatory-product-price-and-shipping">
															<span>PV : {{$set->pv}}</span>
														</div>
														<div>
															<span class="bgsave">save : {{floor($discount)}}%</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="col-sm-1"></div>

		<div class="col-sm-8">

			<div class="row">
				@if(!empty($offer))
				<br><br>
				<div class="col-sm-12" style="border-bottom: 1px solid #ddd;">
					<div class="col-sm-4" style="text-align: center;">
						<img src="{{ URL::to('/') }}/offers/{{ $offer->image_url }} " style="margin-top: 10%; margin-bottom: 20px; max-height: 200px; widows: auto">

					</div>
					<div class="col-sm-8">
						<h1>{{$offer->offer_title}}</h1>
						<h3>{{$offer->offer_sub_title}}</h3><br>
						<p><?php echo $offer->offer_long_description; ?></p>				
					</div>
					<br>
				</div>
				@endif
			</div>
			<br><br>
			@if(!empty($offers))
			<h1 style="text-align: center;"><u>Other Offers</u></h1>
			<br><br>
			@foreach($offers as $offer)
			<div class="row">
				<div class="col-sm-12" style="border-bottom: 1px solid #ddd;">
					<div class="col-sm-4" style="text-align: center;">
						<img src="{{ URL::to('/') }}/offers/{{ $offer->image_url }} " style="margin-top: 0%; max-height: 200px; margin-bottom: 20px; widows: auto">
						<br>
					</div>
					<div class="col-sm-8"><br><br>
						<h1>{{$offer->offer_title}}</h1>
						<h3>{{$offer->offer_sub_title}}</h3><br>
						<p><?php echo $offer->offer_long_description;?></p>				
					</div>
					<br>
				</div>

			</div>
			@endforeach
			@endif


		</div>
		<br><br>
	</div>
</div>
<br>






@endsection