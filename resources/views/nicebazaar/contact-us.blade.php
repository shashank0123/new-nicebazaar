
@extends('layouts.nicebazaar1')

<style type="text/css">
	.textpolicy{ background-color: white; margin-top: 20px; margin-bottom: 100px; text-align: justify; }
	.textpolicy p{font-weight: 100px; padding: 0px 10px  0px 10px ; font-size: 15px;
		line-height: 1.5; font-family: "Lato", sans-serif;     color: #595959; letter-spacing: 0.5px; text-align: justify;}
		.textpolicy h4{ text-align: center; margin-top: 20px; margin-bottom: 10px; font-size: 28px; font-weight:600 }
		.textpolicy h5{padding: 0px 30px  0px 30px ; font-size: 20px ; font-weight: 600; }
		ol {list-style-type:decimal; margin-left:  20px}
		ol li { line-height: 1.8; color: #595959; font-size: 15px}
		.material-icons { font-size: 37.5px !important; color: #ffffff!important; }
		.card { text-align: center;  background-color: #ff0000}
		.card p {color: #fff}
		#contact-form label { margin-top: 10px }
		#send-button { margin-top: 15px; background-color: #ff0000 }
		
	</style>
	@section('content')

	<div class="container">
		<div class="row">				
			<div class="success-bottom "><br>
				{{-- <h4>Terms & Conditions</h4><br> --}}
				<div class="container" id="table-cont">
					
					<!-- Main Content - start -->
					<main>
						<section class="container">

							
							<div class="container-fluid" style="background-color:#eeeeeec7;">
								<br><br>
								<h1>Contact Details</h1>
								<div class="row">
									<div class="col-sm-4">
										<div class="card">
											<div class="row">
												<br>
												<i class="material-icons">location_on</i>
												<br>
												<br>
											</div>
											<div class="row">
												<p>Jivan park uttam nagar east</p>


											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="card">
											<div class="row">
												<br>
												<i class="material-icons">email</i>
												<br>
												<br>
											</div>
											<div class="row">
												<p>
													info@nicebazaar.com
												</p>
											</div>
										</div>
									</div>
									<div class="col-sm-4">							
										<div class="card">
											<div class="row">
												<br>
												<i class="material-icons">local_phone</i>
												<br>
												<br>
											</div>
											<div class="row">
												<p>
													(+91) 8743028531						
												</p>
											</div>
										</div>							
									</div>
								</div>
								<br><br>
								<h1>Get in touch</h1>
								<div class="row">
									<form id="contact-form">
										<div class="col-sm-6">
											<label for="name">Full Name</label>
											<input class="form-control" type="text" name="name" id="name" placeholder="Enter name here">
										</div>
										<div class="col-sm-6">
											<label for="email">Email</label>
											<input class="form-control" type="email" name="email" id="email" placeholder="Enter email here ">
										</div>
										<div class="col-sm-6">
											<label for="phone">Contact Number</label>
											<input class="form-control" type="text" name="phone" id="phone" placeholder="Enter phone number here">
										</div>
										<div class="col-sm-6">
											<label for="subject">Subject</label>
											<input class="form-control" type="text" name="subject" id="subject" placeholder="Enter subject here">
										</div>
										<div class="col-sm-12">
											<label for="message">Message</label>
											<textarea class="form-control" type="text" name="message" id="message" rows="5" placeholder="Your message here"></textarea>
										</div>
										<div class="col-sm-12">
											<button type="button" id="send-button" class="btn btn-danger" onclick="sendMessage()">Send</button> 
										</div>
									</form>
								</div>
								<br><br>

								<div class="row">
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3502.3978085308586!2d77.07341131455931!3d28.617837141497837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d04c8de8d7595%3A0xd70f6083653bb727!2sFriday+Market!5e0!3m2!1sen!2sin!4v1558335636767!5m2!1sen!2sin" width="100%" height="200px"></iframe>
								</div>
								<br><br>
							</div>
							
						</section>
					</main>
					<!-- Main Content - end -->
				</div>	
			</div>
		</div>
	</div>

	<script>
		function sendMessage(){


			var email = $('#email').val();
			var name = $('#name').val();
			var phone = $('#phone').val();
			var subject = $('#subject').val();
			var message = $('#message').val();
			alert(name+"/"+email+"/"+phone+"/"+subject+"/"+message);
			if(email=="" || name=="" || phone=="" || subject=="" || message==""){
				Swal('All fields are required');
			}
			else{

				var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;     
				if (reg.test(email) == false) {
					Swal('Invalid Email Address'); 
				}
				else{

					var phoneNum = phone.replace(/[^\d]/g, '');
					if(phoneNum.length >9 && phoneNum.length < 11) {

						$.ajax({
							url: '/contact-us/form',
							type: 'post',
							data: $('#contact-form').serialize(),
							success: function (data) { 
								if(data.msg == "Thank you .We will reply you soon"){
									Swal(data.msg);     
								}
							
							else{
								Swal(data.msg);
							}
						}
						});

					}
					else{
						Swal('Invalid phone number format');
					}      
				}
			}
		}
	</script>

	@endsection

