
<?php
use App\Product;
use App\MainCategory;
use App\Banner;
use App\Testimonial;
use App\Review;
$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
$min_price = Product::min('sell_price');
$max_price = Product::max('sell_price');
$flag=0;
?>

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: {{$max_price}},
      values: [ 75, 10000 ],
      slide: function( event, ui ) {
        var min = $( "#amount" ).val( "Rs. " + ui.values[ 0 ] + " - Rs. " + ui.values[ 1 ] );
        $('#min_amt').val(ui.values[ 0 ]);
        $('#max_amt').val(ui.values[ 1 ]);
     }
  });
    $( "#amount" ).val( "Rs. " + $( "#slider-range" ).slider( "values", 0 ) +
      " - Rs. " + $( "#slider-range" ).slider( "values", 1 ) );
 } );
</script>
@extends('layouts/nicebazaar1')


@section('content')

<div class="innovatoryBreadcrumb">
   <div class="container">
      <nav data-depth="2" class="breadcrumb hidden-sm-down">
         <ol itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
               <a itemprop="item" href="index.php">
                  <input type="hidden" name="hid_id" value="{{$keyword}}" id="hid_id">
                  <span itemprop="name">Home</span>
               </a>
               <meta itemprop="position" content="1">
            </li>
         </ol>
      </nav>
   </div>
</div>
<section id="wrapper">
   <aside id="notifications">
      <div class="container">
      </div>
   </aside>
   <div class="container">
      <div class="row">
         <div class="row">
            <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">

               <div id="search_filters_wrapper" class="hidden-sm-down">
                  <div id="search_filter_controls" class="hidden-md-up">
                     <span id="_mobile_search_filters_clear_all"></span>
                     <button class="btn btn-secondary ok">
                        <i class="material-icons">&#xE876;</i>
                        OK
                     </button>
                  </div>
                  <div id="search_filters">
                     <?php $subcategory = MainCategory::where('category_id',0)->where('status','Active')->get(); ?>
                     @if(!empty($subcategory))
                     <section class="facet clearfix"  style="max-height: 300px; overflow: auto;">
                        <h1 class="h6 facet-title hidden-sm-down">Sub-Categories</h1>
                        <div class="title hidden-md-up" data-target="#facet_8252" data-toggle="collapse">
                           <h1 class="h6 facet-title">Categories</h1>
                           <span class="pull-xs-right">
                              <span class="navbar-toggler collapse-icons">
                                 <i class="material-icons add">&#xE313;</i>
                                 <i class="material-icons remove">&#xE316;</i>
                              </span>
                           </span>
                        </div>
                        <ul id="facet_8252" class="collapse">
                           <?php $i=1; ?>
                           @foreach($subcategory as $row)
                           <li>
                              <?php $count=Product::count(); ?>
                              <a href="/products/{{$row->id}}">
                                 {{$row->Mcategory_name}}
                                 <span>{{-- ({{$i++}}) --}}</span></a>
                              </li>
                              @endforeach
                           </ul>
                        </section>
                        @endif
                        
                        <section class="facet clearfix"  style="max-height: 300px; overflow: auto; text-align: left;">
                           <h1 class="h6 facet-title hidden-sm-down">Discount</h1>
                           <div class="title hidden-md-up">
                              <h1 class="h6 facet-title">Discount</h1>
                              <span class="pull-xs-right">
                                 <span class="navbar-toggler collapse-icons">
                                 <!-- <i class="material-icons add">&#xE313;</i>
                                    <i class="material-icons remove">&#xE316;</i> -->
                                 </span>
                              </span>
                           </div>
                           <ul style="text-align: left; margin: 10px 20px;">
                              <li>
                                 <input type="radio" name="discount" id="dc1" value="0,10" onclick="discount1()">
                                 upto 10%
                                 <span></span>
                              </li>
                              <li>
                                 <input type="radio" name="discount" id="dc2" value="10,20" onclick="discount2()">
                                 10% - 20%
                                 <span></span>
                              </li>
                              <li>
                                 <input type="radio" name="discount" id="dc3" value="20,30" onclick="discount3()">
                                 20% - 30%
                                 <span></span>
                              </li>
                              <li>
                                 <input type="radio" name="discount" id="dc4" value="30,40" onclick="discount4()">
                                 30% - 40%
                                 <span></span>
                              </li>
                              <li>
                                 <input type="radio" name="discount" id="dc5" value="40,50" onclick="discount5()">
                                 40% - 50%
                                 <span></span>
                              </li>
                              <li>
                                 <input type="radio" name="discount" id="dc6" value="50,100" onclick="discount6()">
                                 more than 50%
                                 <span></span>
                              </li>
                           </ul>
                        </section>
                        <section class="facet clearfix"  style="max-height: 250px; overflow: auto">
                           <h1 class="h6 facet-title hidden-sm-down">Brands</h1>
                           <div class="title hidden-md-up" >
                              
                              <h1 class="h6 facet-title">Brands</h1>
                              <span class="pull-xs-right">
                                 <span class="navbar-toggler collapse-icons">
                                <!--  <i class="material-icons add">&#xE313;</i>
                                 <i class="material-icons remove">&#xE316;</i> -->
                              </span>
                           </span>
                        </div>
                        <ul id="brandid" style="margin: 10px 20px">
                           <?php $brand_cnt = 0; ?>
                           <input type="hidden" value="" id="get-discount">
                           <input type="hidden" value="" id="brandId">
                           @foreach($sel_brands as $row)
                           
                           <li onclick="brandFilter('{{$row->brand}}')">
                             <input type="hidden"  id="getBrand{{$row->brand}}" value="{{$row->brand}}">
                              <a style="cursor: pointer;">
                                 {{$row->brand}}
                                 <span></span></a>
                              </li>
                              @endforeach
                           </ul>
                        </section>


                        <section class="facet clearfix"  style="max-height: 350px;">
                           <p>
                             <label for="amount">Price Filter:</label>
                             <input type="text" id="amount" readonly style="border:0; color:#333; font-weight:bold;">
                             <input type="hidden" name="min_amt" id="min_amt" value="{{$min_price}}">
                             <input type="hidden" name="max_amt" id="max_amt" value="{{$max_price}}">
                          </p>

                          <div id="slider-range"  style="margin: 0 10px"></div><br>

                          <input type="button" class="btn btn-danger" value="Filter"onclick="priceFilter()">
                          <br>
                       </section>


                        {{-- <section class="facet clearfix"  style="max-height: 350px;">
                           <h1 class="h6 facet-title hidden-sm-down">filter by range</h1>
                           <div id="price"></div>
                           <div class="slidecontainer" style=" padding: 20px !important 10px !important 20px !important 10px !important;">
                              <input type="range" min="1" max="10000" value="100" class="slider" id="myRange" onchange="priceFilter()" style="width:100%;">
                           </div>
                        </section> --}}
                        <aside class="widget widget-size-filter">
                           <h2 class="widget-title section-title">Weight Options</h2>
                           <div class="clear"></div>
                           {{-- <form action="#" class="clearfix"> --}}
                              <div class="size-option">
                                 <select name="size" id="size" class="orderby js__select2" style="width: 90%">
                                    <option>Select Weight</option>
                                    <option value="100gm">100gm</option>
                                    <option value="200gm">200gm</option>
                                    <option value="500gm">500gm</option>
                                    <option value="1kg">1kg</option>
                                 </select>
                                 <input type="hidden" id="weight-id" value="">
                                 {{-- <label class="lbl-variant"><input type="radio" name="radio-size-widget"  id="size" value="S" class="lbl-radio"><span class="lbl-text">S</span></label>
                                 <label class="lbl-variant" id="sizeM"><input type="radio" name="radio-size-widget" id="size"  value="M" class="lbl-radio"><span class="lbl-text">M</span></label>
                                 <label class="lbl-variant" id="sizeL"><input type="radio" name="radio-size-widget" id="size"  value="L" class="lbl-radio"><span class="lbl-text">L</span></label>
                                 <label class="lbl-variant" id="sizeXL"><input type="radio" name="radio-size-widget" id="size"  value="XL" class="lbl-radio"><span class="lbl-text">XL</span></label>
                                 <label class="lbl-variant" id="sizeXXL"><input type="radio" name="radio-size-widget" id="size"  value="XXL" class="lbl-radio"><span class="lbl-text">XXL</span></label> --}}
                              </div>
                           {{-- </form> --}}
                        </aside><!-- .widget-size-filter -->
                     </div>
                  </div>


               <div class="it-special-products product-column-style" data-items="1" data-speed="1000" data-autoplay="0" data-time="3000"  data-arrow="0" data-pagination="0" data-move="1" data-pausehover="0"  data-md="1" data-sm="1" data-xs="1" data-xxs="1">
                  <?php
                  $featuredproducts = Product::where('trending','yes')->where('status','Active')->orderBy('created_at','desc')->limit(4)->get();
                  ?>
                  <div class="itProductList itcolumn">
                     <div class="title_block">
                        <h3>New Product</span></h3>
                     </div>
                     <div class="block-content">
                        <div class="itProductFilter row">
                           <div class="special-item owl-carousel">
                              <div class="item-product">
                                 @if(isset($featuredproducts))
                                 @foreach($featuredproducts as $col)
                                 <?php
                                 $featurecategory = Product::where('products.id',$col->id)->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();

                                 ?>
                                 <?php $discount=(($col->mrp - $col->sell_price)*100)/$col->mrp;
                                 $rate = Review::where('product_id',$col->id)->avg('rating');
                                 $final_rate = floor($rate);
                                 ?>
                                 <div class="item">
                                    <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="46" data-id-product-attribute="289">
                                       <div class="innovatory-thumbnail-container">
                                          <div class="row no-margin">
                                             <div class="pull-left product_img">
                                                <a href="/productdetail/{{$col->id}}" class="thumbnail product-thumbnail">
                                                   <img src = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $col->image1 }}" alt = "" data-full-size-image-url = "" width="100px" height="100px">
                                                </a>
                                             </div>
                                             <div class="innovatoryMedia-body">
                                                <div class="innovatory-product-description">
                                                   <div class="comments_note">
                                                      <div class="star_content">
                                                         @for($i=0; $i<5 ; $i++)
                                                         <i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
                                                         @endfor
                                                      </div>
                                                      <span class="laberCountReview pull-left">Review</span>
                                                   </div>
                                                   <h2 class="h2 productName" itemprop="name"><a href="/productdetail/{{$col->id}}">{{$col->name}}</a></h2>
                                                   <div class="innovatory-product-price-and-shipping">
                                                      <span itemprop="price" class="price">Rs. {{$col->mrp}}</span>&nbsp;&nbsp;
                                                      <span class="regular-price">Rs. {{$col->sell_price}}</span>
                                                   </div>
                                                   <div class=" innovatory-product-price-and-shipping">
                                                      <span class="bgsave">Save :<span class="savevalue"> {{floor($discount)}}%</span></span>
                                                   </div>
                                                   <div class="innovatory-product-price-and-shipping">
                                                      <span>PV : {{$col->pv}}</span>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 @endforeach
                                 @endif
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div id="itleftbanners" class="block hidden-md-down">
                  <ul class="banner-col">
                     <li class="itleftbanners-container">
                        <a href="#" title="Left Banner 1">
                           <img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itleftbanners/images/left-banner-1.jpg" alt="Left Banner 1" width="auto" height="auto"/>
                        </a>
                     </li>
                  </ul>
               </div>
               <div class="it-special-products product-column-style" data-items="1" data-speed="1000" data-autoplay="0" data-time="3000"  data-arrow="0" data-pagination="0" data-move="1" data-pausehover="0"  data-md="1" data-sm="1" data-xs="1" data-xxs="1">
                  <div class="itProductList itcolumn">
                     <div class="title_block">
                        <h3>Special Product</span></h3>
                     </div>
                     <?php
                     $trendproducts = Product::where('trending','yes')->where('status','Active')->orderBy('name','asc')->limit(4)->get();
                     ?>
                     <div class="block-content">
                        <div class="itProductFilter row">
                           <div class="special-item owl-carousel">
                              <div class="item-product">
                                 @if(isset($trendproducts))
                                 @foreach($trendproducts as $set)
                                 <?php
                                 $trendcategory = Product::where('products.id',$set->id)->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();

                                 ?>
                                 <?php $discount=(($set->mrp - $set->sell_price)*100)/$set->mrp;
                                 $rate = Review::where('product_id',$set->id)->avg('rating');
                                 $final_rate = floor($rate);
                                 ?>
                                 <div class="item">
                                    <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="46" data-id-product-attribute="289">
                                       <div class="innovatory-thumbnail-container">
                                          <div class="row no-margin">
                                             <div class="pull-left product_img">
                                                <a href="productdetail/{{ $set->id }}" class="thumbnail product-thumbnail">
                                                   <img src = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $set->image1 }}" alt = "" data-full-size-image-url = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $set->image1 }}" width="100px" height="100px">
                                                </a>
                                             </div>
                                             <div class="innovatoryMedia-body">
                                                <div class="innovatory-product-description">
                                                   <div class="comments_note">
                                                      <div class="star_content">
                                                         @for($i=0; $i<5 ; $i++)
                                                         <i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
                                                         @endfor
                                                      </div>
                                                      <span class="laberCountReview pull-left">Review</span>
                                                   </div>
                                                   <h2 class="h2 productName" itemprop="name"><a href="/productdetail/{{$set->id}}">{{$set->name}}</a></h2>
                                                   <div class="innovatory-product-price-and-shipping">
                                                      <span itemprop="price" class="price">Rs. {{$set->sell_price}}</span>
                                                      &nbsp;&nbsp;
                                                      <span class="regular-price">Rs. {{$set->mrp}}</span>
                                                   </div>
                                                   <div>
                                                      <span class="bgsave">Save :<span class="savevalue"> {{floor($discount)}}%</span></span>
                                                   </div>
                                                   <div class="innovatory-product-price-and-shipping">
                                                      <span>PV : {{$set->pv}}</span>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 @endforeach
                                 @endif
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
               <section id="main">
                  <section id="products" class="active_grid">
                     <div id="">
                        <div id="js-product-list-top" class="products-selection row">
                           <div class="col-lg-5 total-products">
                              <h2>Searched Products</h2>
                           </div>
                           <div class="col-lg-7">
                              <div class="products-sort-order dropdown pull-right">
                                 <form action="" method="POST"  >
                                    <select class="form-control" id="sort">
                                       <option value="name" style="font-size: 16px;">Name</option>
                                       <option value="low-high" style="font-size: 16px;">Price: low to high</option>
                                       <option value="high-low" >Price: high to low</option>
                                    </select>
                                 </form>
                                 <!-- </div> -->
                              </div>
                              <span class=" hidden-sm-down sort-by pull-right">Sort by:</span>      
                              <div class="col-sm-3 col-xs-4 hidden-md-up filter-button">
                                 <button id="search_filter_toggler" class="btn btn-secondary">
                                    Filter
                                 </button>
                              </div>
                              <div class="clearfix"></div>
                           </div>
                           <div class="clearfix"></div>
                        </div>
                     </div>
                     <div  class="hidden-sm-down">
                        <div class="w3-bar w3-black" style="text-align: right;">
                         <button class="w3-bar-item w3-button" style="text-align: right;" onclick="openCity('grid')">Grid</button>
                         <button class="w3-bar-item w3-button"  style="text-align: right;" onclick="openCity('list')">List</button>
                      </div>

                      <div id="product-views">
<br>
                      <div id="grid" class="w3-container city">
                        <div id="js-product-list">
                           <div  class="innovatoryProductGrid innovatoryProducts">
                              <div class="row" id="productCatList">
                                 @if($searched_products != null)
                                  @foreach($searched_products as $product)
                                  <?php $flag++; ?>
                                 <input type="hidden" id="cat_id" value="{{$product->id}}">
                                 <div class="item-inner col-lg-4 col-xl-4 col-md-4 col-sm-6 col-xs-6  last-in-line   last-item-of-tablet-line    last-item-of-mobile-line " >
                                    <article class="product-miniature js-product-miniature" data-id-product="28" data-id-product-attribute="418" itemscope itemtype="http://schema.org/Product">
                                       <div class="innovatoryProduct-container item">
                                          <div class="innovatoryProduct-image" >
                                             <a href="/productdetail/{{$product->id}}">
                                                <img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" class="thumbnail product-thumbnail" width="250px" height="250px" style="text-align: center;">
                                             </a>
                                             <!-- <span class="innovatoryNew-label">New</span> -->
                                             <div class="innovatoryActions hidden-lg-up">
                                                <div class="innovatoryActions-i">
                                                   <div class="innovatoryCart innovatoryItem">
                                                      <!-- <form action="" method="post"> -->
                                                         <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                         <input type="hidden" value="28" name="id_product">
                                                         <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart" onclick="addCart({{$product->id}})">
                                                            <i class="ti-shopping-cart"></i>
                                                         </a>
                                                         <!-- </form> -->
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a  onclick='$("#myemd").attr("src","{{ URL::to("/") }}/assetsss/images/AdminProduct/{{ $product->image1 }}");' class="quick-view" data-link-action="quickview" title="Quickview" data-toggle="modal" data-target="#myModal">
                                                            <i class="ti-eye"></i>
                                                         </a>
                                                      </div>
                                                      @if(!empty($member->id) || !empty($user->id))
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="addWishList({{ $product->id }},<?php if(!empty($member->id)) { echo $member->id; } else { echo $user->id ;} ?>)"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                      @else
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="Swal('Please Login First')"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                      @endif
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="innovatory-product-description">
                                                <h2 class="h2 productName" itemprop="name"><a href="">{{$product->name}}</a></h2>
                                                <div class="product-detail">
                                                   <div class="innovatory-product-price-and-shipping">
                                                      <span itemprop="price" class="price">Rs. {{ $product->sell_price }}</span>
                                                      &nbsp;&nbsp;
                                                      <span class="regular-price">Rs. {{$product->mrp}}</span><br>

                                                      <span itemprop="price" class="price">PV : {{$product->pv}}</span>
                                                      
                                                   </div>
                                                   <div class="comments_note">
                                                      <div class="star_content">
                                                         @for($i=0; $i<5 ; $i++)
                                                         <i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
                                                         @endfor
                                                      </div>
                                                      <?php $discount=(($product->mrp - $product->sell_price)*100)/$product->mrp;
                                                      $rate = Review::where('product_id',$product->id)->avg('rating');
                                                      $final_rate = floor($rate);
                                                      ?>
                                                      <div>
                                                         <span class="bgsave">Save : {{floor($discount)}}%</span>
                                                      </div>
                                                      
                                                      <span class="laberCountReview pull-left">Review</span>
                                                   </div>
                                                </div>
                                                <div class="innovatoryActions hidden-md-down">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem">
                                                         <!-- <form action="" method="post"> -->
                                                            <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                            <input type="hidden" value="28" name="id_product">
                                                            <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart" onclick="addCart({{$product->id}})">
                                                               <i class="ti-shopping-cart"></i>
                                                            </a>
                                                            <!-- </form> -->
                                                         </div>
                                                         <div class="innovatoryQuick innovatoryItem">
                                                            <a  onclick='$("#myemd").attr("src","{{ URL::to("/") }}/assetsss/images/AdminProduct/{{ $product->image1 }}");' class="quick-view" data-link-action="quickview" title="Quickview" data-toggle="modal" data-target="#myModal">
                                                               <i class="ti-eye"></i>
                                                            </a>
                                                         </div>
                                                         @if(!empty($member->id) || !empty($user->id))
                                                         <div class="innovatoryItem innovatoryWish">
                                                            <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="addWishList({{ $product->id }},<?php if(!empty($member->id)) { echo $member->id; } else { echo $user->id ;} ?>)"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                         </div>
                                                         @else
                                                         <div class="innovatoryItem innovatoryWish">
                                                            <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="Swal('Please Login First')"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                         </div>
                                                         @endif
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </article>
                                       </div>
                                       @endforeach
                                       @endif

                                       @if($searched_products == null)
                                       <div class="section-common container">
                                          <div class="row" style="text-align: center; margin-top: 50px;">
                                             <h1 style="font-size: 3em">No products Available</h1>
                                          </div>
                                       </div>
                                       @endif   
                                    </div>
                                 </div>
                                 <div class="row no-margin">
                                 </div>
                              </div>
                           </div>



                           {{-- //List view --}}

                           <div id="list" class="w3-container city" style="display:none">
                             <div id="js-product-list">
                           <div  class="innovatoryProductGrid innovatoryProducts">
                              <div class="row" id="productCat">
                                 @if($searched_products != null)
                                 @foreach($searched_products as $product)
                                 <input type="hidden" id="cat_id" value="{{$product->id}}">
                                 <div class="item-inner col-lg-12 col-xl-12 col-md-12 col-sm-12 col-xs-12  last-in-line   last-item-of-tablet-line    last-item-of-mobile-line " >
                                    <article class="product-miniature js-product-miniature" data-id-product="28" data-id-product-attribute="418" itemscope itemtype="http://schema.org/Product">
                                       <div class="innovatoryProduct-container item">
                                          <div class="innovatoryProduct-image col-sm-4" >
                                             <a href="/productdetail/{{$product->id}}">
                                                <img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" class="thumbnail product-thumbnail" width="250px" height="250px" style="text-align: center;">
                                             </a>
                                             <!-- <span class="innovatoryNew-label">New</span> -->
                                             <div class="innovatoryActions hidden-lg-up">
                                                <div class="innovatoryActions-i">
                                                   <div class="innovatoryCart innovatoryItem">
                                                      <!-- <form action="" method="post"> -->
                                                         <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                         <input type="hidden" value="28" name="id_product">
                                                         <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart" onclick="addCart({{$product->id}})">
                                                            <i class="ti-shopping-cart"></i>
                                                         </a>
                                                         <!-- </form> -->
                                                      </div>
                                                      <div class="innovatoryQuick innovatoryItem">
                                                         <a  onclick='$("#myemd").attr("src","{{ URL::to("/") }}/assetsss/images/AdminProduct/{{ $product->image1 }}");' class="quick-view" data-link-action="quickview" title="Quickview" data-toggle="modal" data-target="#myModal">
                                                            <i class="ti-eye"></i>
                                                         </a>
                                                      </div>
                                                      @if(!empty($member->id) || !empty($user->id))
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="addWishList({{ $product->id }},<?php if(!empty($member->id)) { echo $member->id; } else { echo $user->id ;} ?>)"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                      @else
                                                      <div class="innovatoryItem innovatoryWish">
                                                         <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="Swal('Please Login First')"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                      </div>
                                                      @endif
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="innovatory-product-description col-sm-8">
                                                <br>

                                                <h2 class="h2 productName" itemprop="name"><a href=""><b>{{$product->name}}</b></a></h2><br>
                                                <div class="product-detail">


                                                    <div class="row">
                                                   <div class="col-sm-8">
                                                       <div class="innovatory-product-price-and-shipping" style="text-align: left;">
                                                      <span itemprop="price" class="price">Rs. {{ $product->sell_price }}</span>
                                                      &nbsp;&nbsp;
                                                      <span class="regular-price">Rs. {{$product->mrp}}</span><br>

                                                      <span itemprop="price" class="price">PV : {{$product->pv}}</span>
                                                      <br><br>
                                                         <h2 class="h2 productName" itemprop="name"><a href="">{{$product->short_descriptions}}</a></h2>


                                                   </div>
                                                   </div>

                                                   <div class="col-sm-4">
                                                       <div class="comments_note">
                                                      <div class="star_content">
                                                         @for($i=0; $i<5 ; $i++)
                                                         <i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
                                                         @endfor
                                                      </div>
                                                      <?php $discount=(($product->mrp - $product->sell_price)*100)/$product->mrp;
                                                      $rate = Review::where('product_id',$product->id)->avg('rating');
                                                      $final_rate = floor($rate);
                                                      ?>
                                                      <div>
                                                         <span class="bgsave">Save : {{floor($discount)}}%</span>
                                                      </div>
                                                      
                                                      <span class="laberCountReview pull-left">Review</span>
                                                   </div>
                                                   </div>
                                                </div>


                                                  
                                                  

                                                </div>
                                                <div class="innovatoryActions hidden-md-down">
                                                   <div class="innovatoryActions-i">
                                                      <div class="innovatoryCart innovatoryItem">
                                                         <!-- <form action="" method="post"> -->
                                                            <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                            <input type="hidden" value="28" name="id_product">
                                                            <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart" onclick="addCart({{$product->id}})">
                                                               <i class="ti-shopping-cart"></i>
                                                            </a>
                                                            <!-- </form> -->
                                                         </div>
                                                         <div class="innovatoryQuick innovatoryItem">
                                                            <a  onclick='$("#myemd").attr("src","{{ URL::to("/") }}/assetsss/images/AdminProduct/{{ $product->image1 }}");' class="quick-view" data-link-action="quickview" title="Quickview" data-toggle="modal" data-target="#myModal">
                                                               <i class="ti-eye"></i>
                                                            </a>
                                                         </div>
                                                         @if(!empty($member->id) || !empty($user->id))
                                                         <div class="innovatoryItem innovatoryWish">
                                                            <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="addWishList({{ $product->id }},<?php if(!empty($member->id)) { echo $member->id; } else { echo $user->id ;} ?>)"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                         </div>
                                                         @else
                                                         <div class="innovatoryItem innovatoryWish">
                                                            <div class="innovatorywishlist product-item-wishlist"><a class="addToWishlist wishlistProd_22" title="Add to wishlist" onclick="Swal('Please Login First')"><i class="ti-heart"></i> <span>Add to wishlist</span></a></div>
                                                         </div>
                                                         @endif
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </article>
                                       </div>
                                       @endforeach
                                       @endif

                                       @if($searched_products == null)
                                       <div class="section-common container">
                                          <div class="row" style="text-align: center; margin-top: 50px;">
                                             <h1 style="font-size: 3em">No products Available</h1>
                                          </div>
                                       </div>
                                       @endif   
                                    </div>
                                 </div>
                                 <div class="row no-margin">
                                 </div>
                              </div>
                         </div>



                         <script>
                           function openCity(cityName) {
                            var i;
                            var x = document.getElementsByClassName("city");
                            for (i = 0; i < x.length; i++) {
                             x[i].style.display = "none";  
                          }
                          document.getElementById(cityName).style.display = "block";  
                       }
                    </script>
                 </div>
                 </div>
                           {{-- {{$flag}} --}}
                           @if($flag==0)
                           <div class="section-common container">
                              <div class="row" style="text-align: center;">
                                 <h1 style="font-size: 3em">{{$message}}</h1>
                              </div>
                           </div>
                           @endif
                           

                           <div class="hidden-md-up text-xs-right up">
                              <a href="#header" class="btn btn-secondary">
                                 Back to top
                                 <i class="material-icons">&#xE316;</i>
                              </a>
                           </div>
                        </div>
                     </div>
                     <div id="js-product-list-bottom">
                        <div id="js-product-list-bottom"></div>
                     </div>
                  </section>
               </section>
            </div>
         </div>
      </div>
      <div class="displayPosition displayPosition6">
         <!-- Static Block module -->
         <!-- /Static block module -->
      </div>
   </section>
   <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
               <embed id="myemd" src=""
               frameborder="0" width="100%" height="450px">
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
               <embed id="myemd" src=""
               frameborder="0" width="100%" height="450px">
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
   </div>
   <script>
      var countproduct=<?php if(session()->get('count') != null){echo session()->get('count'); } else { echo 0;} ?>;

      function addToCart(){
         var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
         var pro_id = $('#hidden_id').val();
         var quant = $('#quickquantity').val();
         countproduct++;
         $.ajax({
            /* the route pointing to the post function */
            url: '/add-cart',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, id: pro_id, quantity: quant},
            success: function (data) { 
               $("#show-total").html(countproduct);           
            }
         }); 
      }

      function openview(id,name,price,mrp,description,img1,img2,img3,rate){
         $('#hidden_id').val(id);
         $('#viewname').text(name);

         $('#viewdescription').text(description);
         $('#viewprice').text(price);
   //For main image
   $("#imggallery1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#imggallery2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   
   //for a tag (data-zoom attribute)    
   $("#imgdz2").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#imgdz3").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   $("#imgdz1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   
   //for a tag (data-images attribute)
   $("#imgdz1").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#imgdz2").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#imgdz3").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   
   //for image tag (src attribute)
   $("#img1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#img2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#img3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   $("#imgt1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#imgt2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#imgt3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   var discount = ((mrp-price)*100)/mrp;
   $('#viewdiscount').text(discount.toFixed(2));
   $('#viewrate').val(rate);
   
}

function searchFilter(id){ 
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   
   $.ajax({
      /* the route pointing to the post function */
      url: '/wishlist/delete-all',
      type: 'POST',
      /* send the csrf-token and the input to the controller */
      dataType: 'JSON',

      success: function (data) { 
         $('#no-result').show();
         $('#refresh-wishlist').load(document.URL + ' #refresh-wishlist'); 
      }
   });
}

function searchAll(){      
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   
   $.ajax({
      /* the route pointing to the post function */
      url: '/search-all',
      type: 'POST',
      /* send the csrf-token and the input to the controller */
      dataType: 'JSON',
      success: function (data) { 
         $('#refresh-wishlist').load(document.URL + ' #refresh-wishlist'); 
      }
   });
} 

   //adding items to cart
   function addCart(id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      countproduct++;

      $.ajax({
         /* the route pointing to the post function */
         url: '/add-to-cart',
         type: 'POST',
         /* send the csrf-token and the input to the controller */
         data: {_token: CSRF_TOKEN, id: id},
         success: function (data) { 
      // window.location.href = '/';
      $(".show-total").html(data.count);           
   }
}); 
   }
   
   //adding items to Wishlist
   function addWishList(product_id,user_id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
         /* the route pointing to the post function */
         url: '/add-to-wishlist',
         type: 'POST',
         /* send the csrf-token and the input to the controller */
         data: {_token: CSRF_TOKEN, user_id: user_id,product_id:product_id},
         success: function (data) { 
            $(".ajaxdata").text(data.sel_wishlist);
         }
      }); 
   }
   
   $('#cat0').click(function(){   
      var cat = $('#cat0').val();
      $.ajax({
         type: 'get',
         data_type: 'html',
         url: '/search-product/productsCat',
         data:{ cat_id:cat},

         success:function(response){
            $('#productCat').html(response);
            console.log(response);
         }
      });
   });   
   
   @foreach($maincategory as $row)
   $('#cat{{$row->id}}').click(function(){

      var cat = $('#cat{{$row->id}}').val();
      var brand = $('#brandId').val(brand);
      var disc = $('#get-discount').val();   
      var min = $('#min_amt').val();
      var max = $('#max_amt').val();
      var weight = $('#weight-id').val();

      $.ajax({
         type: 'get',
         data_type: 'html',
         url: '/search-product/productsCat',
         data:{ cat_id:cat},
         success:function(response){            
            $('#productCat').html(response);
            $('#hid_id').val(cat);
            console.log(response);
         }
      });
   });
   @endforeach
   
   $('#sort').on('change',(function(){
      var sort = $('#sort').val();
      var category_id = $('#hid_id').val();
      var brand = $('#brandId').val();
      var disc = $('#get-discount').val();      
      var min = $('#min_amt').val();
      var max = $('#max_amt').val();
      var weight = $('#weight-id').val();


      $.ajax({
         type: 'get',
         data_type: 'html',
         url: '/searched/sort',
         data:{ sort_type:sort , category_id:category_id , brand:brand , disc:disc , min:min , max:max , weight:weight },

         success:function(response){            
            $('#productCat').html(response);
            $('#brandId').val(brand);
            $('#get-discount').val(disc);
            $('#hid_id').val(category_id);
            $('#min_amt').val(min);
            $('#max_amt').val(max);
            $('#weight-id').val(weight);
            console.log(response);
         }
      });
   }));
   
   function priceFilter(){
      var brand = $('#brandId').val();
      var disc = $('#get-discount').val();
      var id = $('#hid_id').val();
      var min = $('#min_amt').val();
      var max = $('#max_amt').val();
      var weight = $('#weight-id').val();
      
      // var price = $('#myRange').val();
      // $('#price').text(price);
      
      $.ajax({
         type: 'get',
         data_type: 'html',
         url: '/searched/price-filter',
         data:{  cat_id: id , min: min, max: max, weight: weight, discount: disc , brand: brand },

         success:function(response){            
            $('#productCat').html(response);
            $('#brandId').val(brand);
            $('#get-discount').val(disc);
            $('#hid_id').val(id);
            $('#min_amt').val(min);
            $('#max_amt').val(max);
            $('#weight-id').val(weight);
            console.log(response);
         }
      });
   }

// function priceFilter(){
//       // var min = $('#min-price').val();
//       // var max = $('#max-price').val();
//       var price=$('#myRange').val();
//       $.ajax({
//          type: 'get',
//          data_type: 'html',
//          url: '/searched/price-filter',
//          data:{ max_value:max , min_value:min },

//          success:function(response){            
//             $('#productCat').html(response);
//             console.log(response);
//          }
//       });
//    }

$('#size').on('change',(function(){
   var size_code = $('#size').val();
   var min = $('#min_amt').val();
   var max = $('#max_amt').val();
   var category_id = $('#hid_id').val();
   var disc = $('#get-discount').val();
   var brand = $('#brandId').val();
   $('#weight-id').val(size_code);
   // var color_code = $('#color').val();
   

   $.ajax({
      type: 'get',
      data_type: 'html',
      url: '/searched/size',
      data:{ size: size_code, cat_id: category_id, color: color_code, min_cost: min, max_cost: max},
      success:function(response){            
         $('#productCat').html(response);
         
         console.log(response);
      }
   });
}));

function discount1(){

   var brand = $('#brandId').val();
   var disc = $('#dc1').val();
   var id = $('#hid_id').val();
   var min = $('#min_amt').val();
   var max = $('#max_amt').val();
   var weight = $('#weight-id').val();
   $('#get-discount').val(disc);

   $.ajax({
      type: 'get',
      data_type: 'html',
      url: '/searched/discount',
      data:{discount:disc , cat_id:id , min : min , max: max , brand: brand , weight: weight},
      success:function(response){            
         $('#productCat').html(response);
         $('#hid_id').val(id);
         $('#brandId').val(brand);
         $('#get-discount').val(disc);         
         $('#min_amt').val(min);
         $('#max_amt').val(max);
         $('#weight-id').val(weight);
         console.log(response);
      }
   });
}

function discount2(){
 var brand = $('#brandId').val();
 var disc = $('#dc2').val();
 var id = $('#hid_id').val();
 var min = $('#min_amt').val();
 var max = $('#max_amt').val();
 var weight = $('#weight-id').val();
 $('#get-discount').val(disc);

 $.ajax({
   type: 'get',
   data_type: 'html',
   url: '/searched/discount',
   data:{discount:disc , cat_id:id , min : min , max: max , brand: brand , weight: weight},
   success:function(response){            
      $('#productCat').html(response);
      $('#hid_id').val(id);
      $('#brandId').val(brand);
      $('#get-discount').val(disc);         
      $('#min_amt').val(min);
      $('#max_amt').val(max);
      $('#weight-id').val(weight);
      console.log(response);
   }
});
}

function discount3(){
   var brand = $('#brandId').val();
   var disc = $('#dc3').val();
   var id = $('#hid_id').val();
   var min = $('#min_amt').val();
   var max = $('#max_amt').val();
   var weight = $('#weight-id').val();
   $('#get-discount').val(disc);

   $.ajax({
      type: 'get',
      data_type: 'html',
      url: '/searched/discount',
      data:{discount:disc , cat_id:id , min : min , max: max , brand: brand , weight: weight},
      success:function(response){            
         $('#productCat').html(response);
         $('#hid_id').val(id);
         $('#brandId').val(brand);
         $('#get-discount').val(disc);         
         $('#min_amt').val(min);
         $('#max_amt').val(max);
         $('#weight-id').val(weight);
         console.log(response);
      }
   });
}

function discount4(){
 var brand = $('#brandId').val();
 var disc = $('#dc4').val();
 var id = $('#hid_id').val();
 var min = $('#min_amt').val();
 var max = $('#max_amt').val();
 var weight = $('#weight-id').val();
 $('#get-discount').val(disc);

 $.ajax({
   type: 'get',
   data_type: 'html',
   url: '/searched/discount',
   data:{discount:disc , cat_id:id , min : min , max: max , brand: brand , weight: weight},
   success:function(response){            
      $('#productCat').html(response);
      $('#hid_id').val(id);
      $('#brandId').val(brand);
      $('#get-discount').val(disc);         
      $('#min_amt').val(min);
      $('#max_amt').val(max);
      $('#weight-id').val(weight);
      console.log(response);
   }
});
}

function discount5(){
  var brand = $('#brandId').val();
  var disc = $('#dc5').val();
  var id = $('#hid_id').val();
  var min = $('#min_amt').val();
  var max = $('#max_amt').val();
  var weight = $('#weight-id').val();
  $('#get-discount').val(disc);

  $.ajax({
   type: 'get',
   data_type: 'html',
   url: '/searched/discount',
   data:{discount:disc , cat_id:id , min : min , max: max , brand: brand , weight: weight},
   success:function(response){            
      $('#productCat').html(response);
      $('#hid_id').val(id);
      $('#brandId').val(brand);
      $('#get-discount').val(disc);         
      $('#min_amt').val(min);
      $('#max_amt').val(max);
      $('#weight-id').val(weight);
      console.log(response);
   }
});
}

function discount6(){
  var brand = $('#brandId').val();
  var disc = $('#dc6').val();
  var id = $('#hid_id').val();
  var min = $('#min_amt').val();
  var max = $('#max_amt').val();
  var weight = $('#weight-id').val();
  $('#get-discount').val(disc);

  $.ajax({
   type: 'get',
   data_type: 'html',
   url: '/searched/discount',
   data:{discount:disc , cat_id:id , min : min , max: max , brand: brand , weight: weight},
   success:function(response){            
      $('#productCat').html(response);
      $('#hid_id').val(id);
      $('#brandId').val(brand);
      $('#get-discount').val(disc);         
      $('#min_amt').val(min);
      $('#max_amt').val(max);
      $('#weight-id').val(weight);
      console.log(response);
   }
});
}

function brandFilter(getbrand){
  
   var brand = getbrand;
   var disc = $('#get-discount').val();
   var category_id = $('#hid_id').val();
   var min = $('#min_amt').val();
   var max = $('#max_amt').val();
   var weight = $('#weight-id').val();
   
   $.ajax({
      type: 'get',
      data_type: 'html',
      url: '/searched/brand',
      data:{discount: disc,cat_id: category_id, min: min , max: max , weight: weight , brand : brand},
      success:function(response){            
         $('#productCat').html(response);
         $('#hid_id').val(category_id);
         $('#brandId').val(brand);
         $('#get-discount').val(disc);         
         $('#min_amt').val(min);
         $('#max_amt').val(max);
         $('#weight-id').val(weight);

         console.log(response);
      }
   });
}

   
</script>
@stop