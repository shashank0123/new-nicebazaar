<?php
$postdata =$_POST;
use App\MainCategory;
use App\Product;
use App\OrderItem;

$maincategory = MainCategory::where('category_id',0)->get();

$cartproducts = array();
$quantity = array();
$i = 0;
if(session()->get('cart') != null){
	foreach(session()->get('cart') as $cart){
		$cartproducts[$i] = Product::where('id',$cart->product_id)->first();
		$quantity[$i] = $cart->quantity;
		$i++;
	}
}
$set = 0;
$count=1;
?>
@extends('layouts.nicebazaar1')

@section('content')
<style>

	.block_newsletter .news-icon { line-height: 0 }
	.innovatorySocial ul li a { line-height: 20px !important }
	.success-top{ text-align: center;margin: 50px 50px; }
	.success-top h1 { text-align: center; color: #37BC9B; font-weight: bold;font-size:  72px;}
	.success-top p { text-align: center; font-weight: bold;font-size:  18px;}
	.success-bottom { border: 1px solid #ddd; background-color: #efe; margin-bottom: 50px }
	.success-bottom .table { margin: 20px 0px; border: none; }
	#table-cont { width: 90% }
	.table>tbody>tr>td { padding: 20px 10px; }
	.table tr td{ padding: 20px 10px; }
	.success-bottom h4 { margin-left: 50px; text-align: center; font-weight: bold; color: #aaa; }
	
</style>

<div class="container">
	<div class="row">		
		<br><br><br><br><br><br><br><br><br>
		<div class="success-bottom "><br><br>

			@if(!empty($transaction_details))
			<h4>Payment History</h4><br>
			<div class="container" id="table-cont">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Order Detail </th>
							<th>Status</th>
							<th>Payment Method</th>
							<th>Transaction Id</th>
							<th>Total Payable Amount</th>
							<th>Date</th>

						</tr>
					</thead>

					<tbody>
						
						@foreach($transaction_details as $detail)
						<?php $set++; ?>

						<?php $products = OrderItem::where('created_at',$detail->created_at)->get(); ?>
						<tr>
							<td><?php echo $count++.". " ?>
								<!-- {{$products}} -->
								@foreach($products as $product)
								<?php $name=Product::where('id',$product->item_id)->first();
								?>
								<?php echo $name->name.". " ?>
								@endforeach 		

							</td>
							<td>{{$detail->status}}</td>
							<td><?php echo $detail->payment_method;?></td>
							<td>{{$detail->transaction_id}}</td>
							<td>Rs. {{$detail->price}}</td>
							<td>Rs. {{$detail->created_at}}</td>
						</tr>
						@endforeach
						
					</tbody>
				</table>		
			</div>	
			@endif

			@if($set == 0)
			<div style="text-align: center;margin-top: 50px">
				<h1>No Orders Yet</h1>
				<br><br>
			</div>
			@endif
		</div>
	</div>
</div>

@endsection

