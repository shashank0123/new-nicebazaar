@extends('layouts.nicebazaar1')

<style type="text/css">
	.textpolicy{ background-color: white; margin-top: 20px; margin-bottom: 100px; text-align: justify; }
	.textpolicy p{font-weight: 100px; padding: 0px 30px  0px 30px ; font-size: 15px;
		line-height: 24px; font-family: "Lato", sans-serif;     color: #595959; letter-spacing: 0.5px;}
		.textpolicy h3{ text-align: center; margin-top: 20px; margin-bottom: 10px; font-size: 28px; font-weight:600 }
		.textpolicy h5{padding: 0px 30px  0px 30px ; font-size: 20px ; font-weight: 600; }
		.textpolicy ol {list-style-type:decimal; margin-left:  10%; margin-right: 10%;}
		.textpolicy ul {list-style-type:decimal; margin-left:  10%; margin-right: 10%}
		.table tr th , .table tr td{ text-align: center;color: #595959; padding: 15px 0 !important }
		.textpolicy ol li { line-height: 1.8; color: #595959; font-size: 15px;}
		.textpolicy ul li { line-height: 1.8; color: #595959; font-size: 15px;}

		.about-us-area .overview-content { margin-top: 15%; text-align: center; }
		.about-us-area .overview-content span { color: #37bc9b; }
		.about-us_btn { display: block;padding: 10px; background-color:  #37bc9b; color: #fff; width: 20%; }
		.hiraola-about-us_btn-area a{ margin-left: 35% }
		.count { font-size: 48px; color:  #ff0000; }
		.team-area { text-align: center; }
		.team-area h4 { font-size: 28px; font-weight: 600; margin-bottom: 20px }
		.count-title span { font-weight: bold;color: #777; font-size: 20px }
		a { color: #555; }		
	</style>
	@section('content')

	<div class="container">
		<div class="row">				
			<div class="success-bottom "><br>
				{{-- <h4>Terms & Conditions</h4><br> --}}
				<div class="container" id="table-cont">					
					<!-- Main Content - start -->
					<main>
						<section class="container">							
							<div class="container-fluid" style="background-color:#ff00000a;">
								<div class="row">									
									<div class="col-sm-2"></div> 
									<div  class="col-sm-8 textpolicy">						
										<h3>About Us</h3>
										<hr>
										<p>www.wealthindia.global is a portal hosted by Wealth India Trading Pvt. Ltd. We the best prices and exclusive offers on a huge range of technology & lifestyle products like apparels for men's and women's, home & kitchen appliances, personal care & grooming equipment's. Our direct association with all the major brands means our customers will always find special deals on great products. To help our customers choose right products, we make it easy to find availability, pricing, reviews and ratings.
											<br><br>

											<h3>Our Mission</h3>

											<p>At Wealth India , our focus has always been on helping customers save time and money. This is why we've invested in building a website that allows them to securely manage their accounts without intervention. It's the empowering, 24/7 self service approach that ensures we keep our customers satisfied, and our prices competitive. </p>
											<br><br>

											<h3>Our Vision</h3>
											<p>Our vision is to be earth's most customer centric company; to build a place where people can come to find and discover anything they might want to buy online. </p>
										</div> 
										<div class="col-sm-2"></div>      
									</div>
								</div>

								<!-- Begin Hiraola's Project Countdown Area -->
								<div class="project-count-area">
									<div class="container-fluid"> <br><br>
										<div class="row">
											<div class="col-lg-3 col-md-6 col-sm-6">
												<div class="single-count text-center">
													<div class="count-icon">
														<span class="ion-ios-briefcase-outline"><i class='fa fa-briefcase' style='font-size:36px'></i></span><br><br>
													</div>
													<div class="count-title">
														<h2 class="count">3000</h2>
														<span>Product Sold</span>
													</div>
												</div>
											</div>
											<div class="col-lg-3 col-md-6 col-sm-6">
												<div class="single-count text-center">
													<div class="count-icon">
														<span class="ion-ios-wineglass-outline"><i class='fa fa-glass' style='font-size:36px'></i></span><br><br>
													</div>
													<div class="count-title">
														<h2 class="count">300</h2>
														<span>Customers Served</span>
													</div>
												</div>
											</div>
											<div class="col-lg-3 col-md-6 col-sm-6">
												<div class="single-count text-center">
													<div class="count-icon">
														<span class="ion-ios-lightbulb-outline"><i class='fa fa-lightbulb-o' style='font-size:36px'></i></span><br><br>
													</div>
													<div class="count-title">
														<h2 class="count">768</h2>
														<span>Days Worked</span>
													</div>
												</div>
											</div>
											<div class="col-lg-3 col-md-6 col-sm-6">
												<div class="single-count text-center">
													<div class="count-icon">
														<span class="ion-happy-outline"><i class='fa fa-smile-o' style='font-size:36px'></i></span><br><br>
													</div>
													<div class="count-title">
														<h2 class="count">450</h2>
														<span>Product Served</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div><br><br>
								<!-- Hiraola's Project Countdown Area End Here -->


							</section>





						</main>
						<!-- Main Content - end -->
					</div>	
				</div>
			</div>
		</div>


		@endsection

		@section('script')
		<script>
			$('.count').each(function() {
				$(this).prop('Counter', 0).animate({
					Counter: $(this).text()
				}, {
					duration: 5000,
					easing: 'swing',
					step: function(now) {
						$(this).text(Math.ceil(now));
					}
				});
			});
		</script>


		@endsection

