<?php
   use App\Product;
   use App\Review;
   $bill = 0;
   $i=0;
   ?>
@extends('layouts/nicebazaar1')
@section('content')
<div class="innovatoryBreadcrumb">
   <div class="container">
      <nav data-depth="1" class="breadcrumb hidden-sm-down">
         <ol itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
               <a itemprop="item" href="">
               <span itemprop="name">Home</span>
               </a>
               <meta itemprop="position" content="1">
            </li>
         </ol>
      </nav>
   </div>
</div>
<section id="wrapper">
   <aside id="notifications">
      <div class="container">
      </div>
   </aside>
   <div class="container">
      <div class="row">
         <div id="content-wrapper">
            <section id="main">
               <div class="cart-grid row">
                  <!-- Left Block: cart product informations & shpping -->
                  <div class="cart-grid-body col-xs-12 col-lg-8">
                     <!-- cart products detailed -->
                     <div class="card cart-container">
                        <div class="card-block">
                           <h1 class="h1">Shopping Cart</h1>
                        </div>
                        <hr>
                        <div class="cart-overview js-cart" data-refresh-url="">
                           <ul class="cart-items">
                              <li class="cart-item">
                                 <div class="product-line-grid table-responsive">
                                    @if(session()->get('cart') != null)
                                    <?php
                                       $ids = array();
                                       $cat_id = array();               
                                       $quantities = array();
                                       
                                       
                                       foreach(session()->get('cart') as $data)
                                       {
                                          $ids[$i] = $data->product_id;                      
                                          $quantities[$i] = $data->quantity;
                                          $i++;
                                       }                 
                                       ?>
                                    <table class="table">
                                       <thead>
                                          <tr>
                                             <th></th>
                                             <th>Poduct</th>
                                             <th>Price</th>
                                             <th>PV</th>
                                             <th style="width: 100px">Quantity</th>
                                             <th style="width: 100px">Total</th>
                                             <th>Action</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          @for($j=0 ; $j<$i ; $j++ )
                                          <?php
                                             $product = Product::where('id',$ids[$j])->first(); 
                                             if($product != null)
                                             {
                                                $cat_id[$j] = $product->category_id;
                                             }
                                             else
                                             {
                                                $cat_id[$j] = 0;
                                             }
                                             ?>
                                          <tr id="cart{{$product->id}}">
                                             <td> <span class="product-image media-middle" style="width: auto;">
                                                <img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" alt="Accumsan Fusce" width="75px" height="75px">
                                                </span>
                                             </td>
                                             <td><a class="label" href="" data-id_customization="0">{{$product->name}}</a></td>
                                             <td><span class="value">Rs. {{ $product->sell_price }}</span></td>
                                             <td>{{ $product->pv }}</td>
                                             <td>
                                                <div id="incdec">
                                                    <i class="fa fa-minus" id="down" onclick="decrement({{ $product->id }})"></i>

                                                   <input type="number" value="{{$quantities[$j]}}" min="1" style="width: 40px;text-align: center;" readonly="readonly" id="qty{{ $product->id}}" />

                                                <i class="fa fa-plus" id="up" onclick="increment({{ $product->id }})"></i>
                                                  
                                                   <!--   <img src="up_arrow.jpeg" id="up" />
                                                      <img src="down_arrow.jpeg" id="down" /> -->
                                                </div>
                                             </td>
                                             <?php
                                                $total = $quantities[$j]*$product->sell_price;
                                                 $total_pv = $quantities[$j]*$product->pv;
                                                $bill =$bill +  $total;
                                                 $pv =$pv +  $total_pv;
                                                ?>
                                             <td> Rs. <span id="sub{{$product->id}}">{{$total}}</span></td>
                                             <td><a onclick="deleteCartProduct({{$ids[$j]}})">
                                                <span class="fa fa-times"></span>
                                                </a>
                                             </td>
                                          </tr>
                                          @endfor
                                       </tbody>
                                    </table>
                                    @else
                                    <div class="sattement">
                                       <h2>Nothing in Cart</h2>
                                    </div>
                                    @endif
                                    {{--   --}}
                                    <div class="clearfix"></div>
                                 </div>
                              </li>
                           </ul>
                        </div>
                     </div>
                     <a class="label" href="/en">
                     <i class="material-icons"></i>Continue shopping
                     </a>
                  </div>
                  <div class="cart-grid-right col-xs-12 col-lg-4">
                     <div class="card cart-summary">
                        <div class="cart-detailed-totals">
                           <div class="card-block">
                              <div class="cart-summary-line" id="cart-subtotal-products">
                                 <span class="label js-subtotal">
                                 Sub-Total
                                 </span>
                                 <span class="value" id="total_price">Rs. {{$bill}}</span>
                              </div>
                              <div class="cart-summary-line" id="cart-subtotal-products">
                                 <span class="label js-subtotal">
                                 PV
                                 </span>
                                 <span class="value" id="pv">{{$pv}}</span>
                              </div>
                              <!-- <div class="cart-summary-line" id="cart-subtotal-shipping">
                                 <span class="label">
                                 Shipping
                                 </span>
                                 <span class="value">0.00</span>
                                 <div><small class="value"></small></div>
                                 </div> -->
                           </div>
                           <hr>
                           <div class="card-block">
                              <div class="cart-summary-line cart-total">
                                 <span class="label">Total (tax excl.)</span>
                                 <span class="value" id="grand_total">Rs. {{$bill}}</span>
                              </div>
                           </div>
                           <hr>
                        </div>
                        <div class="checkout cart-detailed-actions card-block">
                           <form action="" method="POST">
                              <div class="text-xs-center">
                                 <input type="hidden" name="ammount" value="450">
                                 <a href="/checkout" class="btn btn-primary" name="submit" type="submit"> Proceed to Checkout</a>
                              </div>
                           </form>
                        </div>
                     </div>
                     <div id="block-reassurance">
                        <ul>
                           <li>
                              <div class="block-reassurance-item">
                                 <img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/blockreassurance/img/ic_verified_user_black_36dp_1x.png" alt="Security policy (edit with Customer reassurance module)">
                                 <span class="h6">Security policy (edit with Customer reassurance module)</span>
                              </div>
                           </li>
                           <li>
                              <div class="block-reassurance-item">
                                 <img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/blockreassurance/img/ic_local_shipping_black_36dp_1x.png" alt="Delivery policy (edit with Customer reassurance module)">
                                 <span class="h6">Delivery policy (edit with Customer reassurance module)</span>
                              </div>
                           </li>
                           <li>
                              <div class="block-reassurance-item">
                                 <img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/blockreassurance/img/ic_swap_horiz_black_36dp_1x.png" alt="Return policy (edit with Customer reassurance module)">
                                 <span class="h6">Return policy (edit with Customer reassurance module)</span>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
   <div class="displayPosition displayPosition6">
      <!-- Static Block module -->
      <!-- /Static block module -->
   </div>
</section>
<script>
   //modal  box of quick view
   function openview(id,name,price,mrp,description,img1,img2,img3,rate){
      $('#hidden_id').val(id);    
      $('#viewname').text(name);
      $('#viewdescription').text(description);
      $('#viewprice').text(price);
   //For main image
   $("#imggallery1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#imggallery2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   
   //for a tag (data-zoom attribute)    
   $("#imgdz2").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#imgdz3").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   $("#imgdz1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   
   //for a tag (data-images attribute)
   $("#imgdz1").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#imgdz2").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#imgdz3").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   
   //for image tag (src attribute)
   $("#img1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#img2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#img3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   $("#imgt1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
   $("#imgt2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
   $("#imgt3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
   var discount = ((mrp-price)*100)/mrp;
   $('#viewdiscount').text(discount.toFixed(2));
   $('#viewrate').val(rate);
   
   }
   
   
   var countproduct=<?php if(session()->get('count') != null){echo session()->get('count'); } else { echo 0;} ?>;
   
   function addToCart(){
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   var pro_id = $('#hidden_id').val();
   var quant = $('#quickquantity').val();
   countproduct++;
   $.ajax({
   /* the route pointing to the post function */
   url: '/add-cart',
   type: 'POST',
   /* send the csrf-token and the input to the controller */
   data: {_token: CSRF_TOKEN, id: pro_id, quantity: quant},
   success: function (data) { 
      window.location.href = '/cart'; 
      $("#show-total").html(countproduct);           
   }
   }); 
   }
   
   
   
   var count=<?php echo session()->get('count'); ?>;
   function deleteCartProduct(id){
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   count--;
   $('#cart'+id).hide();
   
   $.ajax({
   /* the route pointing to the post function */
   url: '/cart/delete-product/id',
   type: 'POST',
   datatype: 'JSON',
   /* send the csrf-token and the input to the controller */
   data: {_token: CSRF_TOKEN, id: id},
   success: function (data) { 
      
      $("#total_price").empty();    
      $("#total_price").html(data.bill);     
      $("#grand_total").html(data.bill);
      $("#pv").html(data.pv);   
       window.location.href = '/cart';  
   
         }
      }); 
   }
   
   function increment(id,quant){
   var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   
      var quantity = $('#qty'+id).val();
      // alert(quantity);
      
      $.ajax({
         /* the route pointing to the post function */
         url: '/cart/increase/id/quant',
         type: 'POST',
         datatype: 'JSON',
         /* send the csrf-token and the input to the controller */
         data: {_token: CSRF_TOKEN, id: id, quant: quantity},
         success: function (data) { 
            // alert(data.quant);
             // window.location.href = '/cart';
            $('#qty'+id).val(data.quant);
            // $('#quantity'+id).html(data.quant);
            // $('#refresh-div').load(document.URL + ' #refresh-div');
            $('#sub'+id).html(data.sub);

            $("#total_price").text('Rs. '+data.bill);
                $("#grand_total").text('Rs. '+data.bill);
                $('#pv').html(data.pv);
           
         }
      });
   }
   
   function decrement(id,quant){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var quantity = $('#qty'+id).val();
      
      $.ajax({
         /* the route pointing to the post function */
         url: '/cart/decrease/id/quant',
         type: 'POST',
         
         data: {_token: CSRF_TOKEN, id: id, quant: quant},
         success: function (data) { 
            $('#qty'+id).val(data.quant);
          // window.location.href = '/cart';
          // $('#refresh-div').load(document.URL + ' #refresh-div');
          $('#sub'+id).html(data.sub);

          $("#total_price").text('Rs. '+data.bill);
                $("#grand_total").text('Rs. '+data.bill);
                $('#pv').html(data.pv);
           
         }
      });
   }
   
   //adding items to Wishlist
   function addWishList(id,count){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   
      $.ajax({
         /* the route pointing to the post function */
         url: '/add-to-wishlist',
         type: 'POST',
         /* send the csrf-token and the input to the controller */
         data: {_token: CSRF_TOKEN, id: id},
         success: function (data) { 
            window.location.href = '/cart';
            $("#ajaxdata").text(count);
         }
      }); 
   }
   
   
   function update_cart(id,count){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var quantity = $('#qty'+id).val();
     
      $.ajax({
         /* the route pointing to the post function */
         url: '/add-to-session',
         type: 'POST',
         /* send the csrf-token and the input to the controller */
         data: {_token: CSRF_TOKEN, id: id,quantity:quantity,},
         success: function (data) {
            window.location.href = '/cart';
            $("#ajaxdata").text(count);
         }
      }); 
   }
   
   // $(document).ready(function(){
   //  $("#up").on('click',function(){
   //      $("#incdec input").val(parseInt($("#incdec input").val())+1);
   //  });
   
   //  $("#down").on('click',function(){
   //      $("#incdec input").val(parseInt($("#incdec input").val())-1);
   //  });
   
   // });
</script>
@endsection