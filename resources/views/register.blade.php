@extends('front.app')


@section('title')
Register - {{ config('app.name') }}
@stop

<style>
  body{
    background:url("../assets/img/login.png") no-repeat center center fixed !important;
    background-size: 100% 100% !important;
  }
  .pink-text {  font-size: 24px;  }

  .card{margin-top:0% auto; max-width:40%;margin-left: 28%;margin-top: 10%; background-color: #fff;border-radius: 4%;}


  .pull-right button { margin-bottom: 20px !important }
  @media screen and (max-width: 991px) {
    .card { max-width:70%;margin-left: 15%;margin-top: 20% !important }
  }

  @media screen and (max-width: 580px) {
    .card { max-width:90%;margin-left: 5%; }
  }

  #login-head { width: 100% !important; background-color: red }
  #otpcontainer { display: none; }


  #member-detail { display: none; }
  #member-detail .row{ width: 38%; background-color: #eee }
  @media screen and (max-width: 768px){
    #member-detail .row{ width: 90% }
  }
</style>

@section('content')
<div class="clearfix"></div>
<div class="center">
  <div  class="col-lg-12 col-md-12 col-sm-12" style="background: red; position: absolute; top: 0;">
    <div style="background:red;padding-left: 10px; padding-right: 10px; padding-top: 5px;
    padding-bottom: 0;" class="pull-right" >
    <ul>
      <li style="list-style: none">
        <a href="#" style="color: #fff; font-size: 14px; font-weight: bold;">Phone Number:  +91-8743028531</a><br>
      </li>
    </ul>
  </div>
</div>

<div class="card bordered z-depth-2" style="margin-top: 70px">
  <div class="card-header">
    <div class="brand-logo" style="text-align: center;">
     <a href="/">
      <img src="{{ asset('/asset/images/menu/logo/logo.png') }}" width="100">
    </a>
  </div>
</div>
<div id="formcontainer" class="container m-b-30" style="max-width: 100%;">
  <form class="form-floating action-form" id="register" http-type="post" data-url="{{ route('registerforuse', ['lang' => \App::getLocale()]) }}">
    <div class="card-content">
      <div class="m-b-30">
        <div class="card-title strong pink-text">Registeration Form - Become a member</div>
      </div>
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" class="form-control" id="name" required="required">
      </div>

      <div class="form-group">
        <label for="email">Email</label>
        <input type="text" name="email" class="form-control" id="email" required="required">
      </div>

      <div class="form-group">
        <label for="username">User Name</label>
        <input type="text" name="username" class="form-control" id="username" required="required" onchange = "checkExistance()">
      </div>        

      <p id="check-availability" style="color: #800000; font-weight: bold;"></p>

      <div class="form-group">
        <label for="mobile">Mobile</label>
        <input type="phone" name="mobile" class="form-control" min="10" maxlength="10" id="mobile" required="required">
      </div>

      <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="password" class="form-control" min="10" maxlength="10" id="password" required="required">
      </div>

      <div class="form-group">
        <label for="repass">Re-type Password</label>
        <input type="password" name="repass" class="form-control" min="10" maxlength="10" id="repass" required="required" id="repass">
      </div>
    </div>

    <div class="card-action clearfix">
      <div class="pull-left">
        <a href="/en/login" class="btn btn-link black-text">              
          <span style="color:red">Login</span>
        </a>
      </div>

      <div class="pull-right">
        <span id="submit" onclick="showDiv()" class="btn btn-link black-text">
          <span class="btn-preloader">
            <i class="md md-cached md-spin"></i>
          </span>
          <span>@lang('login.savetitle')</span>
        </span>
      </div>
    </div>
  </form>
  <br>
</div>
</div>
</div>

@stop

<script>

  function checkExistance(){
    var name = $('#username').val();
    alert(name);
    $.ajax({
      type: "POST",
      url: "/user-availability",
      data:{ uname: name },
      success:function(msg){
        if(msg.message == 'Username already exists. Try another'){

          $('#check-availability').show();
          $('#check-availability').html(msg.message);
        }
        else{
          $('#check-availability').hide();
        }
      }
    });
  }    


  function showDiv(){    
    var name  = $('#name').val();
    var email  = $('#email').val();
    var username  = $('#username').val();
    var mobile  = $('#mobile').val();
    var password  = $('#password').val();
    var repass  = $('#repass').val();


    if(name==""||email==""||username==""||mobile==""||password==""||repass==""){
      Swal('All Fields Are Mendatory');
    }

    else{


       var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;     
      if (reg.test(email) == false) {
        Swal('Invalid Email Address'); 
      }

      else{

        var phoneNum = mobile.replace(/[^\d]/g, '');
        if(phoneNum.length >9 && phoneNum.length < 11) {


     var request = $.ajax({
      type: "POST",
      url: "/register",
      data: $('#register').serialize(),
    });
     request.done(function(msg) {
      if (msg.message == 'Registered Successfully'){
        Swal(msg.message);
        console.log(msg);
        setTimeout(function(){ window.location.href = "/en/login"; }, 3000);
      }
      else
        Swal(msg.message);                  
    });

     request.fail(function(msg) { 
      data = msg;
    // console.log(msg.responseJSON.username[0]);
    if(msg.responseJSON.email[0]){
      Swal(msg.responseJSON.email[0]);
    }
    else if(msg.responseJSON.mobile[0]){
      Swal(msg.responseJSON.mobile[0]);
    }
    else if(msg.responseJSON.password[0]){
      Swal(msg.responseJSON.password[0]);
    }
    else
      Swal(msg.message);
  });
   }

   else{
    Swal('Invalid Phone Number Format');
   }
 }

   }
 }


 function verifyOTP(){    
  var request = $.ajax({
    type: "POST",
    url: "verifyotp",
    data: $('#otpform').serialize(),      
  });
  request.done(function(msg) {
    if (msg.message == 'Registered Successfully. Please Wait we will call you shortly.'){
      console.log(msg);
      window.location.href = "/register/membership-fee/"+msg.user_id;      
    }
    else{
      Swal(msg.message);
    }
  });

  request.fail(function(msg) {
    Swal(msg.message);
  });
}  
</script>