@extends('back.app')

@section('title')
  @lang('settings.title1') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">@lang('breadcrumbs.front')</a></li>
    <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
    <li class="active">@lang('breadcrumbs.settingsPersonal')</li>
  </ul>
@stop
<style>
  li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,td,th { text-transform: uppercase !important; }
  ul li , ul li a { color: #fff !important; }
</style>
@section('content')
  <main style="text-transform: uppercase !important;">
    @include('back.include.sidebar')
    <div class="main-container">
      @include('back.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style=" width: 90%; margin-left: 5%">
        <section>
          <div class="page-header">
            <br>
            <h1><i class="md md-settings"></i> @lang('settings.reset1')</h1>
            {{-- <p class="lead">@lang('settings.reset2')</p> --}}
          </div>

          <div class="row m-b-40">
            <div class="col-md-6">
              <div class="well">
                <form data-parsley-validate="" role="form" class="action-form" id="accountBasicForm" http-type="post" data-url="{{ route('account.passwordUpdate') }}" data-nationality="true" enctype="multipart/form-data">
                  <fieldset>               

                    <div class="form-group">
                      <label class="control-label" for="username">USERNAME</label>
                      <input type="text" class="form-control" name="username" id="username" required="" onkeyup="getUserDetail()">
                    </div>

                   <p id="get-detail"></p>

                   <div class="form-group">
                      <label class="control-label" for="old-password">@lang('settings.old')</label>
                      <input type="password" name="old-password" class="form-control" id="old-password" minlength="5">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputPassword">@lang('settings.password')</label>
                      <input type="password" name="password" class="form-control" id="password" minlength="5">
                    </div>

                    <div class="form-group">
                      <label class="control-label">@lang('settings.repassword')</label>
                      <input type="password" name="confirm-password" class="form-control" data-parsley-equalto="#password" minlength="5">
                    </div>
                    
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">
                        <span class="btn-preloader">
                          <i class="md md-cached md-spin"></i>
                        </span>
                        <span>@lang('common.submit')</span>
                      </button>
                      <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>

  <script>
    function getUserDetail(){
      var name = $('#username').val();
      // alert(name);
      $.ajax({
        type: "POST",
        url: "/user-name",
        data:{ uname: name },
        success:function(msg){
          if(msg.message == 'SUCCESS'){

          $('#get-detail').empty();
          $('#get-detail').html(msg.name);
          }
          else{

          $('#get-detail').empty();
            $('#get-detail').html(msg.message);
          }
        }
      });
    }

  </script>
@stop
