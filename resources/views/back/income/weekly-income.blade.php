@extends('back.app')

@section('title')
  @lang('incomedirect.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">@lang('breadcrumbs.front')</a></li>
    <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
  <li class="active">@lang('breadcrumbs.weeklyincome')</li>
  </ul>
@stop

@section('content')
  <main>
    @include('back.include.sidebar')
    <div class="main-container">
      @include('back.include.header')
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section class="tables-data" style="padding:1% 5%">
          <div class="page-header">
            <h1><i class="md md-group-add"></i> @lang('incomeweekly.title')</h1>
            <p class="lead">@lang('incomeweekly.subtitleback')</p>
          </div>

          <div class="card">
            <div>
              <div class="datatables">
                 <table class="table table-full" >
                    <thead>
                      <tr>
                        <th>Username</th>
                        <th>Cash Amount</th>
                        {{-- <th data-id="admin_charge">@lang('misc.admin')</th> --}}
                        <th>TDS Charges</th>
                        {{-- <th data-id="repurchase">@lang('misc.repurchase')</th> --}}
                        {{-- <th data-id="amount_promotion">@lang('misc.promotion')</th> --}}
                        <th>Total</th>
                        <th>Date</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if(!empty($bonus_pairing))
                      @foreach($bonus_pairing as $pairing)
                      <tr>
                        <th>{{$pairing->username}}</th>
                        <th>{{$pairing->amount_cash}}</th>
                        {{-- <th data-id="admin_charge">@lang('misc.admin')</th> --}}
                        <th>{{$pairing->amount_promotion}}</th>
                        {{-- <th data-id="repurchase">@lang('misc.repurchase')</th> --}}
                        {{-- <th data-id="amount_promotion">@lang('misc.promotion')</th> --}}
                        <th>{{$pairing->total}}</th>
                        <th>{{$pairing->created_at}}</th>
                      </tr>
                      @endforeach
                      @endif
                    </tbody>
                  </table>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>
@stop
