<?php
  use App\Models\Member;
  $members = Member::all();

  use App\Models\Package;
  $packages = Package::all();

  use App\Models\TransferEpoint;
  $epoints = TransferEpoint::all();
?>

@extends('back.app')

@section('title')
  All Epoints | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">All Epoints</li>
</ul>
@stop

@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container">
    @include('back.include.header')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section>
        <div class="page-header">
          <h1><!-- <i class="md md-person-add"></i> --> All Epoints</h1>
        </div>
     </section>
     <div class="well white" style="margin: 20px">
    <table class="table table-hover">
    <thead>
      <tr>
        <th>Username</th>
        <th>Userid</th>
        <th>Epoints</th>
        <th>Date</th>
      </tr>
    </thead>
    <tbody>
      @php
      $total = 0;
      $username = 0;
      @endphp
      @foreach($epoints as $epoints)
      <tr>
        @php
        $username = Member::where('id',$epoints->receiver_id)->first();
        @endphp
        <td>{{$username->username}}</td>
        <td>SR1{{sprintf("%05d", $username->user_id)}}</td>
        <td>{{$epoints->no_of_epoint}}</td>
        <td>{{$epoints->created_at}}</td>
      </tr>
      @php
        $total = $epoints->no_of_epoint+$total;
      @endphp
      @endforeach
    </tbody>
    <tr>
        <td><STRONG>Total</STRONG></td>
        <td></td>
        <td></td>
        <td><strong>@if (isset($epoints->no_of_epoint)) {{$total}} @else {{ 0 }} @endif</strong></td>
      </tr>
  </table>
</div>
    </div>
   </div>
</main>
@stop
