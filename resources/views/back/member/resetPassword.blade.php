<?php 
use App\Models\MemberDetail;
use App\Models\Member;
use App\User;


?>


@extends('back.app')

@section('title')
Update Member Account | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">Member Detail</li>
</ul>
@stop

@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container" >
    @include('back.include.header')

    <style type="text/css">
      #DataTables_Table_0_wrapper .row{width: 100% !important}
    </style>
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
     @if(!empty($message))
     <p class="alert-success" style="padding: 20px">{{$message}}</p>
     @endif

      <section style="padding:  1% 5%">
          <div class="page-header">
            <h1><i class="md md-settings"></i> @lang('settings.title1')</h1>
            <p class="lead">@lang('settings.subTitle1')</p>
          </div>
          @if($errors->any())
    <div class="alert alert-danger">
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </div>
  @endif

  @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
  @endif
          <div class="row m-b-40">
            <div class="col-md-6">
              <div class="well white">
                <form method="POST" action="/admin/password/{{$user->id}}">
                  <fieldset>

                    <div class="form-group">
                      <label class="control-label" for="new_password">Enter New Password</label>
                      <input type="password" name="new_password" class="form-control" id="new_password" required="" value="">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="confirm_password">Confirm Password</label>
                      <input type="password" name="confirm_password" class="form-control" id="confirm_password" required="" value="">
                    </div>
                    

                    
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">
                        <span class="btn-preloader">
                          <i class="md md-cached md-spin"></i>
                        </span>
                        <span>@lang('common.submit')</span>
                      </button>
                      <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</main>
@stop
