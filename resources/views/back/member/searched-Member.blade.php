<?php 
use App\Models\MemberDetail;
?>


@extends('back.app')

@section('title')
Update Member Account | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">Member List</li>
</ul>
@stop

@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container" >
    @include('back.include.header')

    <style type="text/css">
      #DataTables_Table_0_wrapper .row{width: 100% !important}
    </style>
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section class="tables-data" style="padding: 5px 40px">
        <div class="page-header">
          <h1><i class="md md-group-add"></i> Member List</h1>
        </div>
        <div style="text-align: right;">
            <form method="get" action="/admin/searched-members">
              <input type="search" class="form-group" name="search_username" style="padding: 5px" placeholder="Enter username here" />
              <button type="submit" class="btn btn-primary">Search</button>
            </form>

        </div>
        
        <div class="card">
          <div>
            <div class="container">

              <table  class="table table-full-small " style="text-align: center;">
                <thead>
                  <tr>
                   
                    <th>Username</th>
                    <th>Phone</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  
                  @if(!empty($member))
                  <?php
                  $mbr = MemberDetail::where('member_id',$member->id)->first();
                  ?>
                  <tr>
                    
                    <td>{{$member->username}}</td>
                    <td>{{$mbr->mobile_phone}}</td>
                    <td>
                      <a href="/admin/personal-info/{{$member->id}}" class="btn btn-primary">Personal Info</a>
                      <a href="/admin/bank/{{$member->id}}" class="btn btn-primary">Bank Detail</a>
                      <a href="/admin/password/{{$member->id}}" class="btn btn-primary">Password</a>
                    </td>
                  </tr>
                  @else

                  <div style="text-align: center;padding: 20px ">
                    <p>Member Not Found</p>
                  </div>
                  @endif
                </tbody>
              </table>
            </div>
          </div>

        </div>
      </section>
    </div>
  </div>
</main>
@stop
