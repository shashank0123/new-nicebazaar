@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<style>
	.logo-img{ width: 100px; height: auto; }
	#getSize input { margin-left: 20px }
</style>

<?php
use App\MainCategory;
$maincat = MainCategory::where('category_id',0)->count();

?>

@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">


	<a href="/admin/product"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="name">Product</label><br>
							<input type="text" class="form-control" id="name" name="name" value="{{ $product->name }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="email">Category</label><br>
							<select name="category_id" class="form-control">
								<option value=""></option>
								@foreach($categoryarray as $data)
								<option value="{{ $data->id}}" {{ $product->category_id==$data->id? 'selected': null }}>{{ $data->Mcategory_name  }}</option>
								@endforeach
								
							</select>

						</div>
					</div>
				</div>

				{{-- <div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<div class="form-control" id="getSize" style="margin-left: 10% ; width: 90%">

						<label for="text">Weight</label><br>
								<?php								
								$productArr = explode(",",$product->product_size); 
								?>

							<input type="checkbox" <?php if(in_array('100gm',$productArr)){ echo 'checked'; }?> name="product_size[]" value="100gm">100gm
							<input type="checkbox" <?php if(in_array('200gm',$productArr)){ echo 'checked'; }?> name="product_size[]" value="200gm">200gm
							<input type="checkbox" <?php if(in_array('500gm',$productArr)){ echo 'checked'; }?> name="product_size[]" value="500gm">500gm
							<input type="checkbox" <?php if(in_array('1kg',$productArr  )){ echo 'checked'; }?> name="product_size[]" value="1kg">1kg
						</div>
					</div>
				</div> --}}
				<div class="form-group row">
				<div class="col-md-6">
					<div class="form-material floating">
						<label for="text">Color</label><br>
						<input type="text" class="form-control" id="product_color" name="product_color" value="{{ $product->product_color }}">
					</div>
				</div>
			{{-- </div>
			<div class="form-group row"> --}}
					{{-- <div class="col-md-6">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-material floating">
									<label for="text">Select PV Category</label><br>
									<select  class="form-control" name="pv_category">
										<option></option>
										<option value="general" {{$product->pv_category=='general'?'selected':null }} >General</option>
										<option value="weight" {{ $product->pv_category=='weight'?'selected':null }}>Kg/Ltr</option>
									</select>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-material floating">
									<label for="text">PV</label><br>
									<input type="number" class="form-control" id="pv" name="pv" value="{{$product->pv}}">
								</div>
							</div>
						</div>						
					</div> --}}
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Brand Name</label><br>
							<input type="text" class="form-control" id="brand" name="brand" value="{{ $product->brand}}">
						</div>
					</div>
				</div>

			{{-- Row 2 --}}
			<div class="form-group row">
				<div class="col-md-6">
					<div class="form-material floating">
						<label for="text">Short Description</label><br>
						<input type="text" class="form-control" id="short_descriptions" name="short_descriptions" value="{{ $product->short_descriptions }}">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-material floating">
						<label for="text">MRP</label><br>
						<input type="number" class="form-control" id="mrp" name="mrp" value="{{ $product->mrp }}">
					</div>
				</div>
			</div>
			{{-- Row 3 --}}
			<div class="form-group row">					
				<div class="col-md-6">
					<div class="form-material floating">
						<label for="text">Selling Price</label><br>
						<input type="number" class="form-control" id="sell_price" name="sell_price" value="{{ $product->sell_price }}">
					</div>
				</div>
				<div class="col-md-6 from-inline">
					<div class="row">
						<div class="col-sm-8">
							<div class="form-material floating">

								<label for="text">Image 1</label><br>
								<input type="file" class="form-control" id="image1" name="image1" style="margin-left: 25% ; width: 75%">

							</div>
						</div>
						<div class="col-sm-4">
							<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }} " class="logo-img">
						</div>
					</div>
				</div>
			</div>
			{{-- Row 4 --}}
			<div class="form-group row">					
				<div class="col-md-6">
					<div class="row">
						<div class="col-sm-8">
							<div class="form-material floating">

								<label for="text">Image 2</label><br>
								<input type="file" class="form-control" id="image2" name="image2" style="margin-left: 25% ; width: 75%">

							</div>
						</div>
						<div class="col-sm-4">
							<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image2 }} " class="logo-img">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-sm-8">
							<div class="form-material floating">

								<label for="text">Image 3</label><br>
								<input type="file" class="form-control" id="image3" name="image3" style="margin-left: 25% ; width: 75%">

							</div>
						</div>
						<div class="col-sm-4">
							<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image3 }} " class="logo-img">
						</div>
					</div>
				</div>
			</div>
			{{-- Row 5 --}}
			<div class="form-group row">				
				<div class="col-md-6">
					<div class="form-material floating">
						<label for="text">Featured</label><br>
						<select name="trending" class="form-control">
							<option value="yes" {{ $product->trending=='yes'? 'selected': null }}>yes</option>
							<option value="no" {{ $product->trending=='no'? 'selected': null }}>no</option>
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-material floating">
						<label for="text">Page Keywords</label><br>
						<input type="text" class="form-control" id="page_keywords" name="page_keywords" value="{{ $product->page_keywords }}">
					</div>
				</div>
			</div>
			{{-- Row 7 --}}
			<div class="form-group row">
				<div class="col-md-12">
					<div class="form-material floating">
						<div class="col-md-4">
							<label for="text">Long Description</label>
						</div>
						<div class="col-md-12">
							<textarea name="long_descriptions" class="form-control" id="long_descriptions">{{$product->long_descriptions}}</textarea><br>  
							<script>
								CKEDITOR.replace( 'long_descriptions' );
							</script>
						</div>								
					</div>					
				</div>
			</div>

			{{-- Row 7 --}}
			<div class="form-group row">
				<div class="col-md-12">
					<div class="form-material floating">
						<div class="col-md-4">
							<label for="text">Page Description</label>
						</div>
						<div class="col-md-12">
							<textarea name="page_description" class="form-control" id="page_description" >{{$product->page_description}}</textarea><br>  
							<script>
								CKEDITOR.replace( 'page_description' );
							</script>
						</div>								
					</div>					
				</div>
			</div>

			<div class="form-group row">
				<div class="col-md-6">
					<div class="form-material floating">
						<label for="text">Page Title</label><br>
						<input type="text" class="form-control" id="page_title" name="page_title" value="{{ $product->page_title }}">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-material floating">
						<label for="availability">Availability</label><br>
						<select name="availability" class="form-control">
							<option value="no" {{ $product->availability=='no'? 'selected': null }}>Out of Stock</option>
							<option value="yes" {{ $product->availability=='yes'? 'selected': null }}>In Stock</option>
						</select>
					</div>
				</div>
			</div>


			<div class="form-group row">				
				<div class="col-md-6">
					<div class="form-material floating">
						<label for="mobile">Status</label><br>
						<select name="status" class="form-control">
							<option value="Deactive" {{ $product->status=='Deactive'? 'selected': null }}>Deactive</option>
							<option value="Active" {{ $product->status=='Active'? 'selected': null }}>Active</option>
						</select>
					</div>
				</div>
			</div>

                   
                        	<button type="submit" class="btn btn-alt-primary">Submit</button>
                        
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
    @endsection