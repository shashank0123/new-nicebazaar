@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')


@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">

	<a href="offer"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="add-offer" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}
				
				
				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<label for="offer_title">Title</label><br>
							<input type="text" class="form-control" id="offer_title" name="offer_title">
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-material floating">
							<label for="offer_sub_title">Sub Title</label><br>
							<input type="text" class="form-control" id="offer_sub_title" name="offer_sub_title">
						</div>
					</div>
				</div>



				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<label for="offer_short_description">Short Descripton</label><br>
							<input type="text" class="form-control" id="offer_short_description" name="offer_short_description">
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-material floating">
							<div class="col-md-4">
								<label for="offer_long_description">Long Description</label>
							</div>
							<div class="col-md-12">
								<textarea name="offer_long_description" class="form-control" id="offer_long_description"></textarea><br>  
								<script>
									CKEDITOR.replace( 'offer_long_description' );
								</script>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">

							<label for="image_url">Image</label><br>
							<input type="file" class="form-control" id="image_url" name="image_url" style="margin-left: 15%; width: 85%">
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-material floating">
							<label for="status">Status</label><br>
							<select name="status" class="form-control">
								<option value=""></option>
								<option value="Active">Active</option>
								<option value="Deactive">Deactive</option>
							</select>
						</div>
					</div>
				</div>
				
				<button type="submit" class="btn btn-alt-primary">Submit</button>

			</div>
		</form>
	</div>
</div>
</div>
<!-- END Page Content -->
@endsection

