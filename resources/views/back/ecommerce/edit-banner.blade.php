@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<style>
	.logo-img{ width: 200px; height: 200px; }
</style>

<?php
use App\MainCategory;
$maincat = MainCategory::where('category_id',0)->count();

?>

@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">

	<a href="/admin/banner"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br>
	<br>
	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}
				
				
				<div class="form-group row">					
					
					<div class="col-md-6 from-inline">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-material floating">
									<label for="image_url">Image 1</label><br>
									<input type="file" class="form-control" id="image_url" name="image_url">
								</div>
							</div>							
						</div><br>

						<div class="row">
							<div class="col-sm-12">
								<div class="form-material floating">
									<label for="keyword">Keyword For Banner Image</label><br>
									<input type="text" class="form-control" id="keyword" name="keyword" value="{{$banner->keyword}}">
								</div>
							</div>							
						</div><br>


						<div class="row">
							<div class="col-sm-12">
								<div class="form-material floating">
									<label for="image_type">Image Type</label><br>
									<select name="image_type" class="form-control">
										<option value="banner" {{ $banner->image_type=='banner'? 'selected': null }}>Full Banner</option>
										<option value="small" {{ $banner->image_type=='small'? 'selected': null }}>Small Banner</option>				
										<option value="brand" {{ $banner->image_type=='brand'? 'selected': null }}>Brand Logo</option>				
									</select>
								</div>
							</div>							
						</div><br>

						<div class="row">
							<div class="col-sm-12">
								<div class="form-material floating">
									<label for="status">Status</label><br>
									<select name="status" class="form-control">
										<option value="Deactive" {{ $banner->status=='Deactive'? 'selected': null }}>Deactive</option>
										<option value="Active" {{ $banner->status=='Active'? 'selected': null }}>Active</option>
									</select>
								</div>
							</div>							
						</div>

						<div class="row">
							
						</div>

					</div>
					<div class="col-sm-6  from-inline" style="text-align: center;">
						<br><br>
						<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/banner/{{ $banner->image_url }} " class="logo-img">
					</div>
				</div>
				
				
				
                    {{-- <div class="form-group row">
	                    <div class="col-md-6">
	                        <div class="form-material floating">
	                            <input type="text" class="form-control" id="sponsorid" name="sponsorid">
	                            <label for="sponsorid">SponsorID</label>
	                        </div>
	                    </div> --}}
	                    

	                    
	                    
                    {{-- </div>
                    <div class="form-group row">
                        <div class="col-md-9">
                        	<a href="/member/checkid/" class="btn btn-alt-success">Verify SponsorID</a> --}}
                        	<button type="submit" class="btn btn-alt-primary">Submit</button>
                        {{-- </div> --}}
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
    @endsection

