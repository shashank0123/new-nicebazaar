@extends('back.app')

@section('content')


@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">

	<a href="main-category"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="add-brands" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}
			{{--	<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="text" class="form-control" id="name" name="Mcategory_name">
							<label for="name">Category</label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="text" class="form-control" id="Mcategory_description" name="Mcategory_description">
							<label for="email">Description</label>
						</div>
					</div>
				</div> --}}
				{{--  <input type="hidden" name="pool" value="@if (isset($_GET['pool'])){{ $_GET['pool'] }} @else {{ 1 }} @endif"> --}}
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="mobile">Parent Category</label><br>
							<select name="parent_id" class="form-control">
								<option value=""></option>
								@foreach($maincategory as $row)
								<option value="{{$row->id}}">{{$row->Mcategory_name}}</option>

								@endforeach
								</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Brand Name</label><br>
							<input type="text" class="form-control" id="brand_name	" name="brand_name">
						</div>
					</div>
				</div>
				<div class="form-group row">					
					{{--<div class="col-md-6">
						<div class="form-material floating">
							<input type="file" class="form-control" id="image1" name="image1">
							<label for="text">Image 1</label>
						</div>
					</div> --}}
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="mobile">Status</label><br>
							<select name="status" class="form-control">
								<option value=""></option>
								<option value="Active">Active</option>
								<option value="Deactive">Deactive</option>
							</select>
						</div>
					</div>
				</div>
                    {{-- <div class="form-group row">
	                    <div class="col-md-6">
	                        <div class="form-material floating">
	                            <input type="text" class="form-control" id="sponsorid" name="sponsorid">
	                            <label for="sponsorid">SponsorID</label>
	                        </div>
	                    </div> --}}
	                    

	                    
	                    
                    {{-- </div>
                    <div class="form-group row">
                        <div class="col-md-9">
                        	<a href="/member/checkid/" class="btn btn-alt-success">Verify SponsorID</a> --}}
                        	<button type="submit" class="btn btn-alt-primary">Submit</button>
                        {{-- </div> --}}
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
    @endsection

