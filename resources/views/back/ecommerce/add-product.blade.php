@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<?php
use App\MainCategory;
$maincat = MainCategory::where('category_id',0)->count();
?>
<style>
	#groceryDivW, #groceryDivS{ display: none; }
</style>
@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->

<div class="content" style="background: white">

	<a href="product"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="add-product" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}

				{{-- Row 1 --}}
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="name">Product</label><br>
							<input type="text" class="form-control" id="name" name="name">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="email">Category</label><br>
							<select name="category_id" class="form-control">
								<option value=""></option>
								@foreach($categoryarray as $row)
								<option value="{{$row->id}}">{{$row->Mcategory_name}}</option>
								@endforeach								
							</select>
						</div>
					</div>
				</div>
				
				{{-- Row 3 --}}
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">MRP</label><br>
							<input type="number" class="form-control" id="mrp" name="mrp">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Selling Price</label><br>
							<input type="number" class="form-control" id="sell_price" name="sell_price">
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-material floating">
									<label for="text">Select PV Category</label><br>
									<select  class="form-control" id="pv_category" name="pv_category" onchange="displayDiv()">
										<option></option>
										<option value="general">General</option>
										<option value="weight">Kg/Ltr</option>
									</select>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-material floating">
									<label for="text" onclick="hello()">PV<span id="perWeight"></span></label><br>
									<input type="number" class="form-control" id="pv" name="pv">
								</div>
							</div>
						</div>						
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Brand Name</label><br>
							<input type="text" class="form-control" id="brand" name="brand">
						</div>
					</div>
				</div>
				<!-- //Weight & size -->
				<div class="form-group row">
					<div class="col-md-6" id="groceryDivW">
						<div class="form-material floating">
							<label for="text">Select Measure Unit</label><br>
							<select  class="form-control" id="weight_unit" name="weight_unit">
								<option></option>
								<option value="kilogram">Kilogram</option>
								<option value="litre">Litre</option>
							</select>
						</div>
					</div>
					<div class="col-md-6" id="groceryDivS">
						<div class="form-material floating">
							<label for="text">Enter Weight</label>
							<input type="text" class="form-control" id="1" name="size[]">
							<input type="text" class="form-control" id="2" name="size[]">
							<input type="text" class="form-control" id="3" name="size[]">
							<input type="text" class="form-control" id="4" name="size[]">
							<input type="text" class="form-control" id="5" name="size[]">
							<input type="text" class="form-control" id="6" name="size[]">
							<input type="text" class="form-control" id="7" name="size[]">
							<input type="text" class="form-control" id="8" name="size[]">
							<input type="text" class="form-control" id="9" name="size[]">
							<input type="text" class="form-control" id="10" name="size[]">							
						</div>
					</div>
				</div>

				{{-- Row 4 --}}
				<div class="form-group row">					
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Image 1</label><br>
							<input type="file" class="form-control" id="image1" name="image1">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Image 2</label><br>
							<input type="file" class="form-control" id="image2" name="image2">
						</div>
					</div>
				</div>
				{{-- Row 5 --}}
				<div class="form-group row">					
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Image 3</label><br>
							<input type="file" class="form-control" id="image3" name="image3">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Featured</label><br>
							<select name="trending" class="form-control">
								<option value=""></option>
								<option value="yes">yes</option>
								<option value="no">no</option>
							</select>
						</div>
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Short Description</label><br>
							<input type="text" class="form-control" id="short_descriptions" name="short_descriptions">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Page Keywords</label><br>
							<input type="text" class="form-control" id="page_keywords" name="page_keywords">
						</div>
					</div>
				</div>

				

				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<div class="col-md-4">
								<label for="text">Long Description</label>
							</div>
							<div class="col-md-12">
								<textarea name="long_descriptions" class="form-control" id="long_descriptions"></textarea><br>  
								<script>
									CKEDITOR.replace( 'long_descriptions' );
								</script>
							</div>								
						</div>					
					</div>
				</div>

				{{-- Row 7 --}}
				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<div class="col-md-4">
								<label for="text">Page Description</label>
							</div>
							<div class="col-md-12">
								<textarea name="page_description" class="form-control" id="page_description"></textarea><br>  
								<script>
									CKEDITOR.replace( 'page_description' );
								</script>
							</div>								
						</div>					
					</div>
				</div>


				<div class="col-md-6">
					<div class="form-material floating">
						<div class="form-control" id="getSize" style="margin-left: 10% ; width: 90%">
							<label for="text">Size</label><br>
							<input type="checkbox" name="product_size[]" value="100gm">100gm
							<input type="checkbox" name="product_size[]" value="200gm">200gm
							<input type="checkbox" name="product_size[]" value="500gm">500gm
							<input type="checkbox" name="product_size[]" value="1kg">1kg
						</div>
													
						</div>
					</div>

					{{-- Row 6 --}}
					<div class="form-group row">					
						<div class="col-md-6">
							<div class="form-material floating">
								<label for="text">Page Title</label><br>
								<input type="text" class="form-control" id="page_title" name="page_title">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-material floating">
								<label for="mobile">Status</label><br>
								<select name="status" class="form-control">
									<option value=""></option>
									<option value="Active">Active</option>
									<option value="Deactive">Deactive</option>
								</select>
							</div>
						</div>

					</div>



                    {{-- <div class="form-group row">
	                    <div class="col-md-6">
	                        <div class="form-material floating">
	                            <input type="text" class="form-control" id="sponsorid" name="sponsorid">
	                            <label for="sponsorid">SponsorID</label>
	                        </div>
	                    </div> --}}
	                    

	                    
	                    
                    {{-- </div>
                    <div class="form-group row">
                        <div class="col-md-9">
                        	<a href="/member/checkid/" class="btn btn-alt-success">Verify SponsorID</a> --}}
                        	<button type="submit" class="btn btn-alt-primary">Submit</button>
                        {{-- </div> --}}
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection

    @section('js_after')

    <script>
    	function hello(){
    		// alert('hello');
    	}

    	function displayDiv(){
    		var value = $('#pv_category').val();
    		// alert(value);
    		if(value == 'weight'){
    			$('#groceryDivW').show();
    			$('#perWeight').text('(Per Kg/Per Litre)')
    		}
    	}

    	function displayUnit(){
    		var value = $('#weight_unit').val();
    		// alert(value);
    		}
    	
    </script>>
    <!-- END Page Content -->
    @endsection

