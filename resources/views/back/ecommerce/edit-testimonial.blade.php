@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<style>
	.logo-img{ width: 100px; height: auto; }
</style>

@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">

	<a href="/admin/testimonial"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}
						
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="client_name">Client Name</label><br>
							<input type="text" class="form-control" id="client_name" name="client_name" value="{{ $testimonial->client_name }}">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-material floating">
							<label for="city">City</label><br>
							<input type="text" class="form-control" id="city" name="city" value="{{ $testimonial->city }}">
						</div>
					</div>
				</div>


				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="rating">Rating</label><br>
							<input type="number" class="form-control" id="rating" name="rating" value="{{ $testimonial->rating }}">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-material floating">
							<label for="review">Review</label><br>
							<input type="text" class="form-control" id="review" name="review" value="{{ $testimonial->review }}">
						</div>
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-sm-8">
								<div class="form-material floating">
									<label for="image_url">Image 3</label><br>
									<input type="file" class="form-control" id="image_url" name="image_url">
								</div>
							</div>
							<div class="col-sm-4">
								<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $testimonial->image_url }} " class="logo-img">
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-material floating">
							<label for="status">Status</label><br>
							<select name="status" class="form-control">
								<option value="Deactive" {{ $testimonial->status=='Deactive'? 'selected': null }}>Deactive</option>
								<option value="Active" {{ $testimonial->status=='Active'? 'selected': null }}>Active</option>
							</select>
						</div>
					</div>				
				</div>
				
				

				

                   
                        	<button type="submit" class="btn btn-alt-primary">Submit</button>
                       </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
    @endsection

