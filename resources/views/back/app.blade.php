<!doctype html>
<html lang="{{ config('app.locale') }}" class="no-focus">
    <head>

    <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>@yield('title', config('app.name'))</title>

        <meta name="description" content="Feelgrowth">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcun icon" href="{{ asset('assets/img/logo2.jpg') }}" type="image/x-icon" />
    <link href="{{ \URL::current() }}" rel="canonical">
    <meta name="_t" content="{{ csrf_token() }}" />
    <link rel="alternate" href="{{ \URL::current() }}" hreflang="x-default" />
    @yield('css_before')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
    <link rel="stylesheet" id="css-main" href="{{ url('/css/codebase.css') }}">
    @yield('css_after')

    {{-- <link rel="stylesheet" type="text/css" media="screen" title="style (screen)" href="{{ asset('assets/css/back/plugins.css') }}" /> --}}
    {{-- <link rel="stylesheet" type="text/css" media="screen" title="style (screen)" href="{{ asset('assets/css/back/theme.css') }}" /> --}}
    {{-- <title>@yield('title', config('app.name'))</title> --}}
    <script>
      (function(e,t){typeof module!="undefined"&&module.exports?module.exports=t():typeof define=="function"&&define.amd?define(t):this[e]=t()})("$script",function(){function p(e,t){for(var n=0,i=e.length;n<i;++n)if(!t(e[n]))return r;return 1}function d(e,t){p(e,function(e){return!t(e)})}function v(e,t,n){function g(e){return e.call?e():u[e]}function y(){if(!--h){u[o]=1,s&&s();for(var e in f)p(e.split("|"),g)&&!d(f[e],g)&&(f[e]=[])}}e=e[i]?e:[e];var r=t&&t.call,s=r?t:n,o=r?e.join(""):t,h=e.length;return setTimeout(function(){d(e,function t(e,n){if(e===null)return y();e=!n&&e.indexOf(".js")===-1&&!/^https?:\/\//.test(e)&&c?c+e+".js":e;if(l[e])return o&&(a[o]=1),l[e]==2?y():setTimeout(function(){t(e,!0)},0);l[e]=1,o&&(a[o]=1),m(e,y)})},0),v}function m(n,r){var i=e.createElement("script"),u;i.onload=i.onerror=i[o]=function(){if(i[s]&&!/^c|loade/.test(i[s])||u)return;i.onload=i[o]=null,u=1,l[n]=2,r()},i.async=1,i.defer=1,i.src=h?n+(n.indexOf("?")===-1?"?":"&")+h:n,t.insertBefore(i,t.lastChild)}var e=document,t=e.getElementsByTagName("head")[0],n="string",r=!1,i="push",s="readyState",o="onreadystatechange",u={},a={},f={},l={},c,h;return v.get=m,v.order=function(e,t,n){(function r(i){i=e.shift(),e.length?v(i,r):v(i,t,n)})()},v.path=function(e){c=e},v.urlArgs=function(e){h=e},v.ready=function(e,t,n){e=e[i]?e:[e];var r=[];return!d(e,function(e){u[e]||r[i](e)})&&p(e,function(e){return u[e]})?t():!function(e){f[e]=f[e]||[],f[e][i](t),n&&n(r)}(e.join("|")),v},v.done=function(e){v([null],e)},v})
    </script>
    <!--googleoff: index-->
    <script type="text/javascript">
      var App = {};
      window._root = '{{ URL::to("/") }}';
      window._adminUrl = '{{ config('app.adminUrl') }}';
      App.Scripts = {
        core: [
          window._root + '/lib/jquery.js'
        ],
        bundle_dep: [
          window._root + '/lib/jquery-migrate.js',
          window._root + '/lib/modernizr.js',
          window._root + '/lib/bootstrap.js',
          window._root + '/assets/js/back/theme.js',
        ],
        bundle: [
          window._root + '/lib/cache2.js',
          window._root + '/assets/js/back/index.js'
        ]
      };

      $script(App.Scripts.core, "core");

      $script.ready(["core"], function() {
        $script(App.Scripts.bundle_dep, "bundle_dep");
      });

      $script.ready(["core", "bundle_dep"], function() {
        $script(App.Scripts.bundle, "bundle");
      });
    </script>
    <!--googleon: index-->
    <style>
       .main-content { width: 94%; margin: 1% 3%; }
    </style>
  </head>

  <?php $route = \Route::currentRouteName(); ?>
  <body @if ($route == 'admin.login' || $route == 'admin.logout') class="page-login" init-ripples="" @else scroll-spy="" id="top" class=" theme-template-dark theme-pink alert-open alert-with-mat-grow-top-right" @endif>

        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-modern main-content-boxed sidebar-inverse">
            <!-- Side Overlay-->
            <aside id="side-overlay">
@if (session()->has('flashMessage'))
      <?php $msg = session('flashMessage'); ?>
      @if (isset($msg['class']) && isset($msg['message']))
      <div class="alert alert-fixed alert-{{ $msg['class'] }}">
        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </a>
        {{ $msg['message'] }}
      </div>
      @endif
    @endif                <!-- Side Header -->
                <div class="content-header content-header-fullrow">
                    <div class="content-header-section align-parent">
                        <!-- Close Side Overlay -->
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                        <button type="button" class="btn btn-circle btn-dual-secondary align-v-r" data-toggle="layout" data-action="side_overlay_close">
                            <i class="fa fa-times text-danger"></i>
                        </button>
                        <!-- END Close Side Overlay -->

                        <!-- User Info -->
                        <div class="content-header-item">
                            <a class="img-link mr-5" href="javascript:void(0)">
                                <img class="img-avatar img-avatar32" src="{{ asset('media/avatars/avatar15.jpg') }}" alt="">
                            </a>
                            <a class="align-middle link-effect text-primary-dark font-w600" href="javascript:void(0)">{{ 'Auth::user()->name' }}</a>
                        </div>
                        <!-- END User Info -->
                    </div>
                </div>
                <!-- END Side Header -->

                <!-- Side Content -->
                <div class="content-side">
                    <p>
                        Content..
                    </p>
                </div>
                <!-- END Side Content -->
            </aside>
            

            <!-- Main Container -->
            <main id="main-container">
    @yield('content')
</main>
            <!-- END Main Container -->

            <!-- Footer -->
            <footer id="page-footer" class="opacity-0">
                <div class="content py-20 font-size-xs clearfix">
                    <div class="float-right">
                        Crafted with <i class="fa fa-heart text-pulse"></i> by <a class="font-w600" href="https://backstagesupporters.com" target="_blank">NiceBazaar</a>
                    </div>
                    <div class="float-left">
                        <a class="font-w600" href="https://backstagesupporters.com" target="_blank">NiceBazaar</a> &copy; <span class="js-year-copy"></span>
                    </div>
                </div>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->
        <script src="{{ url('js/codebase.app.js') }}"></script>

        <!-- Laravel Scaffolding JS -->
        <script src="{{ url('js/laravel.app.js') }}"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
      <script type="text/javascript">
    $(document).ready(function(){
        $("#myModal").modal('show');
        $('#packageselect').on('change', function() {               
            if ($("#packageselect").val() != "") {                                
                $.ajax({
                    url: '/members/setpackage?id='+$(this).val(),
                    type: 'GET',
                    success: function (response){                      
                        // window.location();
                        location.reload();
                    },
                    error: function (xhr) {
                        alert("Something went wrong, please try again");
                    }
                });
            }

        });
    });
</script>


    <!--[if IE]>
      <script type="text/javascript" src="{{ asset('lib/html5shiv.js') }}"></script>
      <script type="text/javascript" src="{{ asset('lib/respond.js') }}"></script>
    <![endif]-->
        @yield('js_after')
  </body>
</html>
