<?php
   use App\Product;
   use App\MainCategory;
   use App\Testimonial;
   use App\Review;
   use App\Wishlist;
   
   $count=0;
   
   $total=0;
   $i=0;
   ?>
<!doctype html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>@yield('title', config('app.name'))</title>
      <meta name="description" content="Shop powered by PrestaShop">
      <meta name="keywords" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/vnd.microsoft.icon" href="/prestashop/INNO02/INNO1005_Gomart/IT05/img/favicon.ico?1557119198">
      <link rel="stylesheet" type="text/css" href="/nice-css/niceb.css">
      <link rel="shortcut icon" type="image/x-icon" href="/prestashop/INNO02/INNO1005_Gomart/IT05/img/favicon.ico?1557119198">
      <link rel="stylesheet" href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/themes/IT1005/assets/css/theme.css" type="text/css" media="all">
      <link rel="stylesheet" href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itproductnextprev/views/css/itproductnextprev.css" type="text/css" media="all">
      <link rel="stylesheet" href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itmegamenu/views/css/front.css" type="text/css" media="all">
      <link rel="stylesheet" href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/productcomments/productcomments.css" type="text/css" media="all">
      <link rel="stylesheet" href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/blockwishlist/blockwishlist.css" type="text/css" media="all">
      <link rel="stylesheet" href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itpopupnewsletter/css/styles.css" type="text/css" media="all">
      <link rel="stylesheet" href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itvegamenu/views/css/itvegamenu.css" type="text/css" media="all">
      <link rel="stylesheet" href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itcustomhtml/views/css/itcustomhtml.css" type="text/css" media="all">
      <link rel="stylesheet" href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itblog//views/css/itblog.css" type="text/css" media="all">
      <link rel="stylesheet" href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itcategoryfeature//views/css/itcategoryfeature.css" type="text/css" media="all">
      <link rel="stylesheet" href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/js/jquery/ui/themes/base/minified/jquery-ui.min.css" type="text/css" media="all">
      <link rel="stylesheet" href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/js/jquery/ui/themes/base/minified/jquery.ui.theme.min.css" type="text/css" media="all">
      <link rel="stylesheet" href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/js/jquery/plugins/fancybox/jquery.fancybox.css" type="text/css" media="all">
      <link rel="stylesheet" href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itimageslider/css/nivo-slider.css" type="text/css" media="all">
      <link rel="stylesheet" href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/themes/IT1005/assets/css/custom.css" type="text/css" media="all">
      <link rel="stylesheet" href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/themes/IT1005/assets/css/inno-theme-option.css" type="text/css" media="all">
      <script type="text/javascript" src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/themes/IT1005/assets/js/jquery-1.7.1.min.js"></script>
      <script type="text/javascript"> 
         var baseDir = "https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/"; 
          
         var prestashop = {"cart":{"products":[],"totals":{"total":{"type":"total","label":"Total","amount":0,"value":"$0.00"},"total_including_tax":{"type":"total","label":"Total (tax incl.)","amount":0,"value":"$0.00"},"total_excluding_tax":{"type":"total","label":"Total (tax excl.)","amount":0,"value":"$0.00"}},"subtotals":{"products":{"type":"products","label":"Subtotal","amount":0,"value":"$0.00"},"discounts":null,"shipping":{"type":"shipping","label":"Shipping","amount":0,"value":"Free"},"tax":{"type":"tax","label":"Taxes","amount":0,"value":"$0.00"}},"products_count":0,"summary_string":"0 items","vouchers":{"allowed":0,"added":[]},"discounts":[],"minimalPurchase":0,"minimalPurchaseRequired":""},"currency":{"name":"US Dollar","iso_code":"USD","iso_code_num":"840","sign":"$"},"customer":{"lastname":null,"firstname":null,"email":null,"birthday":null,"newsletter":null,"newsletter_date_add":null,"optin":null,"website":null,"company":null,"siret":null,"ape":null,"is_logged":false,"gender":{"type":null,"name":null},"addresses":[]},"language":{"name":"English (English)","iso_code":"en","locale":"en-US","language_code":"en-us","is_rtl":"0","date_format_lite":"m\/d\/Y","date_format_full":"m\/d\/Y H:i:s","id":1},"page":{"title":"","canonical":null,"meta":{"title":"Demo Shop","description":"Shop powered by PrestaShop","keywords":"","robots":"index"},"page_name":"index","body_classes":{"lang-en":true,"lang-rtl":false,"country-US":true,"currency-USD":true,"layout-full-width":true,"page-index":true,"tax-display-disabled":true},"admin_notifications":[]},"shop":{"name":"Demo Shop","logo":"\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/demo-shop-logo-1557119198.jpg","stores_icon":"\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/logo_stores.png","favicon":"\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/favicon.ico"},"urls":{"base_url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/","current_url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/","shop_domain_url":"https:\/\/innovatorythemes.com","img_ps_url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/","img_cat_url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/c\/","img_lang_url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/l\/","img_prod_url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/p\/","img_manu_url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/m\/","img_sup_url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/su\/","img_ship_url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/s\/","img_store_url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/st\/","img_col_url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/co\/","img_url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/themes\/IT1005\/assets\/img\/","css_url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/themes\/IT1005\/assets\/css\/","js_url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/themes\/IT1005\/assets\/js\/","pic_url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/upload\/","pages":{"address":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/address","addresses":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/addresses","authentication":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/login","cart":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/cart","category":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/index.php?controller=category","cms":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/index.php?controller=cms","contact":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/contact-us","discount":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/discount","guest_tracking":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/guest-tracking","history":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/order-history","identity":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/identity","index":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/","my_account":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/my-account","order_confirmation":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/order-confirmation","order_detail":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/index.php?controller=order-detail","order_follow":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/order-follow","order":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/order","order_return":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/index.php?controller=order-return","order_slip":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/credit-slip","pagenotfound":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/page-not-found","password":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/password-recovery","pdf_invoice":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/index.php?controller=pdf-invoice","pdf_order_return":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/index.php?controller=pdf-order-return","pdf_order_slip":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/index.php?controller=pdf-order-slip","prices_drop":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/prices-drop","product":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/index.php?controller=product","search":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/search","sitemap":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/sitemap","stores":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/stores","supplier":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/supplier","register":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/login?create_account=1","order_login":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/order?login=1"},"alternative_langs":{"en-us":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/","de-de":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/de\/","fr-fr":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/fr\/","es-es":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/es\/","it-it":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/it\/"},"theme_assets":"\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/themes\/IT1005\/assets\/","actions":{"logout":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/?mylogout="},"no_picture_image":{"bySize":{"small_default":{"url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/p\/en-default-small_default.jpg","width":98,"height":98},"cart_default":{"url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/p\/en-default-cart_default.jpg","width":125,"height":125},"medium_default":{"url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/p\/en-default-medium_default.jpg","width":300,"height":300},"home_default":{"url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/p\/en-default-home_default.jpg","width":300,"height":300},"large_default":{"url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/p\/en-default-large_default.jpg","width":700,"height":700}},"small":{"url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/p\/en-default-small_default.jpg","width":98,"height":98},"medium":{"url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/p\/en-default-medium_default.jpg","width":300,"height":300},"large":{"url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/img\/p\/en-default-large_default.jpg","width":700,"height":700},"legend":""}},"configuration":{"display_taxes_label":false,"is_catalog":false,"show_prices":true,"opt_in":{"partner":true},"quantity_discount":{"type":"discount","label":"Discount"},"voucher_enabled":0,"return_enabled":0},"field_required":[],"breadcrumb":{"links":[{"title":"Home","url":"https:\/\/innovatorythemes.com\/prestashop\/INNO02\/INNO1005_Gomart\/IT05\/en\/"}],"count":1},"link":{"protocol_link":"https:\/\/","protocol_content":"https:\/\/"},"time":1561696159,"static_token":"f5413003fb36bd93bab467ca1b34a0ea","token":"10fcc22cd065f30b0f47e0eaa0fe058f"}; 
      </script>
       <style type="text/css">
         :root {
         --main-color: #222;
         --background-color: #FF0000 !important;
         --border-color: #e2e2e2;
         --button-hover-color: #FF0000 !important;
         --text-hover-color: #FF0000 !important;
         --secondary-color: #f9c938;
         }
         :root {
         --main-color: #222;
         --background-color: #FF0000 !important;
         --border-color: #e2e2e2;
         --button-hover-color: #FF0000 !important;
         --text-hover-color: #FF0000 !important;
         --secondary-color: #f9c938;
         }

         #menudiv{ display: none; }

         .menu-vertical { display: none; }

@media (max-width: 1529px) and (min-width: 1360px){
.it-menu-vertical .menu-dropdown .it-menu-row {
     display: block;!important; 
    align-items: center;
}




      </style>
   </head>
   <body itemscope itemtype="http://schema.org/WebPage" id="index" class=" lang-en country-us currency-usd layout-full-width page-index tax-display-disabled">
      <!--<div class="se-pre-con"></div>--><!-- Preloader -->
      <div class="preloader"><img src="/nice-css/preloader.gif" class="preloader-img" alt="" width="auto" height="auto" /></div>
      <main>
         <header id="header">
            <div class="header-banner"></div>
            <nav class="header-nav">
               <div class="container hidden-md-down">
                  <div class="row">
                     <div class="Nav1 col-md-6 col-sm-6">
                        <!-- Static Block module -->
                        <p class="header-title" style="text-align: left; font-weight: bold;">Welcome to Nice-Bazaar Big Store</p>
                        <!-- /Static block module -->
                     </div>
                     <div class="Nav2 col-md-6 col-sm-6">
                        <div id="_desktop_currency_selector">
                           <div class="currency-selector-wrapper">
                              <div class="language-selector">
                                 <div class="language-selector dropdown js-dropdown">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <?php
                        if(!empty($member->id) )
                           $sel_wishlist=WishList::where('user_id',$member->id)->count();
                        elseif(!empty($user->id))
                        $sel_wishlist=WishList::where('user_id',$user->id)->count();
                        else
                             $sel_wishlist="0";
                        ?>
                        <?php
                        if(!empty($member->id) || !empty($user->id))
                        {
                        ?>
                        <div id="_desktop_wishtlistTop">
                            <div class="innovatorywishtlistTop">
                              <a class="wishtlist_top" href="/wishlist">
                                 <span class="icon" style=" font-weight: bold;">
                                    <span class="text">Wishlist </span>
                                    <span class="cart-wishlist-number" id="ajaxdata">({{$sel_wishlist}})</span>
                                 </span>
                              </a>
                           </div>
                        </div>
                        <?php
                        }else{
                        ?>
                        <div id="_desktop_wishtlistTop">
                            <div class="innovatorywishtlistTop">
                              <a class="wishtlist_top" href="" onclick="alert('please login ')">
                                 <span class="icon" style=" font-weight: bold;">
                                    <span class="text">Wishlist </span>
                                    <span class="cart-wishlist-number" id="ajaxdata">({{$sel_wishlist}})</span>
                                 </span>
                              </a>
                           </div>
                        </div>
                        <?php
                     }
                        ?>
                     </div>
                     <div class="clearfix"></div>
                  </div>
               </div>
               <div class="mobile-menu-meta">
                  <div class="container">
                     <div class="row">
                        <div class="mobile-logo-bar">
                           <div id="menu-icon"><i class="ti-menu aria-hidden="true"></i></div>
                           <div class="top-logo" id="_mobile_logo"></div>
                           <div id="_mobile_wishtlistTop"></div>
                           <div id="_mobile_cart"></div>
                        </div>
                     </div>
                  </div>
                  <div class="mobile-search-bar">
                     <div class="container">
                        <div id="_mobile_search" class="innovatoryDisplaySearch"></div>
                     </div>
                  </div>
               </div>
            </nav>
            <!--mobile-sidebar-->
            <div class="sidebar-overlay"></div>
            <div id="mobile_top_menu_wrapper" class="row hidden-lg-up">
               <a class="close-sidebar pull-right"><i class="ti-close"></i></a>
               <div id="_mobile_user_info"></div>
               <div class="js-top-menu-bottom">
                  <div id="_mobile_megamenu" class="it-menu-horizontal"></div>
                  <div id="_mobile_vegamenu"></div>
                  <div class="slidetoggle mobile-sidebar-meta mb-30">
                     <h4 class="menu-tit hidden-lg-up slidetoggle-init"><i class="ti-settings"></i> Settings</h4>
                     <div class="slidetoggle-menu">
                        <div id="_mobile_language_selector" class="col-xs-6"></div>
                        <div id="_mobile_currency_selector" class="col-xs-6"></div>
                        <div id="_mobile_wishtlistTop" class="col-xs-6"></div>
                        <div class="clearfix"></div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="header-top">
               <div class="container">
                  <div class="row">
                     <div class="valign-middle">
                        <div class="hidden-md-down innovatoryLogo col-md-3 col-lg-3" id="_desktop_logo"><a href="/"><img class="logo img-responsive" src="/nice-css/logo.png" width="300" height="100" alt="Demo Shop"></a></div>
                        <div class="hidden-md-down search-wrap col-xs-6">
                           <div id="_desktop_search" class="innovatoryDisplaySearch hidden-md-down">
                              <!-- Block search module TOP -->
                              <div id="search_widget" class="pull-right search-widget page-search" data-search-controller-url="">
                                 <div class="innovatory-search">
                                    <form method="get" action="/searchproduct" id="searchbox"><input type="hidden" name="controller" value="search">
                                       <input id="search_query_top" type="text" name="product_keyword" value="" placeholder="Search our catalog"><button type="submit"><i class="ti-search"></i></button></form>
                                 </div>
                              </div>
                              <!-- /Block search module TOP -->
                           </div>
                        </div>
                         @if(!empty($member->id) )
                        <div class="col-md-3 nav-right hidden-md-down">
                           <div id="_desktop_user_info">
                              <div class="user-info language-selector innovatory-user-info">
                                 <div class="user-info-wrap hidden-lg-up">
                                    <i class="fa fa-user-circle user-icon" aria-hidden="true"></i>
                                    <div class="user-info-btn">
                                       <a href="/en/member" title="Log in to your customer account" rel="nofollow">My account</a>
                                       <a class="register" href="/en/logout">logout</a>
                                    </div>
                                 </div>
                                 <div class="hidden-md-down dropdown js-dropdown">
                                    <div class="icon-wrap-circle" data-toggle="dropdown">
                                       <span class="icon"></span>
                                       <div class="user-content-right"><span class="icon-wrap-tit">My Account</span></div>
                                    </div>
                                    <div class="dropdown-menu">
                                       <ul>
                                          <li><a href="/en/member" title="Log in to your customer account" rel="nofollow">My account</a></li>
                                          <li><a class="register" href="/en/logout">logout</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        @else
                        <div class="col-md-3 nav-right hidden-md-down">
                           <div id="_desktop_user_info">
                              <div class="user-info language-selector innovatory-user-info">
                                 <div class="user-info-wrap hidden-lg-up">
                                    <i class="fa fa-user-circle user-icon" aria-hidden="true"></i>
                                    <div class="user-info-btn">
                                       <a href="/en/login" title="Log in to your customer account" rel="nofollow">Login</a>
                                       <a class="register" href="/register">Register</a>
                                    </div>
                                 </div>
                                 <div class="hidden-md-down dropdown js-dropdown">
                                    <div class="icon-wrap-circle" data-toggle="dropdown">
                                       <span class="icon"></span>
                                       <div class="user-content-right"><span class="icon-wrap-tit">Register | Login</span></div>
                                    </div>
                                    <div class="dropdown-menu">
                                       <ul>
                                          <li><a href="/en/login" title="Log in to your customer account" rel="nofollow">Sign In</a></li>
                                          <li><a class="register" href="/register">Register</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        @endif
                        <div class="clearfix"></div>
                     </div>
                  </div>
               </div>
            </div>
           <div class="container-fluid full-nav hidden-md-down" >
               <div class="container">
                  <div class="col-lg-3 padd-0">
                     <div id="_desktop_vegamenu" >
                        <!-- Module Megamenu-->
                        <div class="container_it_vegamenu mb-30">
                           <div class="it-menu-vertical clearfix" onclick="showmenu()">
                              <div class="title-menu" ><i class="ti-menu"></i> <span class="hidden-md-down text-uppercase" >Top Categories</span><span class="hidden-lg-up" >Categories</span></div>
                              <div class="menu-vertical">
                                 <ul class="menu-content">
                                    @foreach($maincategory as $row)
                                     <?php $count_cat=0;?>
                                    <li class="level-1  parent">
                                       <a href="/products/{{$row->id}}">{{$row->Mcategory_name}}</a>
                                     <!-- <img src="/nice-css/right.png" height="5px" width="5px"> -->
                                       <div class="it-sub-menu menu-dropdown col-xs-12 column-3  it-sub-center">
                                          <div class="it-menu-row row ">
                                             <?php
                                                $category = MainCategory::where('category_id',$row->id)->where('status','Active')->limit(4)->get();
                                                ?>
                                             @foreach($category as $rows)
                                             <div class="it-menu-col col-xs-12 col-sm-3  CAT">
                                                <?php $count_cat++;?>
                                                <ul class="ul-column">
                                                   <li class="menu-item item-header">
                                                      <a href="/products/{{$rows->id}}">{{$rows->Mcategory_name}}</a>
                                                   </li>
                                                   <?php
                                                      $subcategory = MainCategory::where('category_id',$rows->id)->where('status','Active')->limit(4)->get();
                                                      ?>
                                                   @foreach($subcategory as $col)
                                                   <li><a href="/products/{{$col->id}}">{{$col->Mcategory_name}}</a>
                                                   </li>
                                                   @endforeach 
                                                </ul>
                                             </div>
                                             @endforeach
                                              @if($count_cat==4)
                                             <div style="position: absolute;bottom: 10px; right: 10px"> 
                                                <a href="/products/{{$row->id}}">view more</a>
                                             </div>
                                              @endif
                                          </div>

                                       </div>

                                    </li>

                                    @endforeach
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <!-- /Module Megamenu -->
                     </div>
                  </div>
                  <div class="col-lg-9 full-nav-menu"">
                     <!-- Module Megamenu-->
                     <div id="_desktop_megamenu" class="it-menu-horizontal">
                        <h4 class="menu-tit hidden-lg-up"><i class="ti-menu"></i> Menu</h4>
                        <ul class="menu-content">
                           @foreach($maincategory as $row)
                           <li class="level-1 ">
                              <a href="/products/{{$row->id}}">{{$row->Mcategory_name}}</a>
                              <span class="icon-drop-mobile"></span>
                           </li>
                           @endforeach
                        </ul>
                     </div>
                     <div id="_desktop_cart">
                        <div class="blockcart innovatory-cart" data-refresh-url="">
                           <?php
                              $a=0;
                              $show =0;
                              if (!empty(session()->get('cart'))){
                                 $show = session()->get('count');
                              }      
                           ?>
                           
                           <a class="cart" rel="nofollow"  onclick="showCart()">
                              <span class="icon cart_icon"></span>
                              <div class="cart-content-right"><span class="cart-products-text hidden-md-down">Cart</span>
                                 @if(session()->get('count') != null)
                                 <span class="cart-products-count hidden-md-down show-total">(<span>{{$show}}</span>)</span>
                                
                                 <span class="cart-products-count hidden-lg-up show-total">(<span>{{$show}}</span>)</span>
                                  @else
                                  <span class="cart-products-count hidden-md-down show-total">(0)</span>
                                
                                 <span class="cart-products-count hidden-lg-up show-total">(0)</span>
                                 @endif
                              </div>
                           </a>
                           <div class="cart_block block exclusive" id="showminicart">
                              <div class="cart-product-wrap">
                                 @if($cartproducts != null)
                                 <?php $i=0; $final_rate = 0; ?>
                                 @foreach($cartproducts as $product)
                                 <div class="products" id="delc{{$product->id}}">
                                    <div class="img">
                                       <a href="" class="thumbnail product-thumbnail"><img src = "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" alt = "" data-full-size-image-url = 
                                          "{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" /></a>
                                    </div>
                                    <div class="cart-info">
                                       <h2 class="h2 productName" itemprop="name"><a href="">{{$product->name}}</a></h2>
                                       <div class="innovatoryPrice"><span class="quantity">{{$quantity[$i]}}X</span><span class="price">Rs. {{$product->sell_price}}</span></div>
                                    </div>
                                    <div class="rate">
                                       <?php
                                          $total  =$total + $product->sell_price * $quantity[$i];                   
                                          ?>
                                    </div>
                                    <p class="remove_link"><a title="Remove this item" class="mini-cart-remove" onclick="deleteProduct({{$product->id}})"><span class="fa fa-times"></span></a></p>
                                 </div>
                                 @endforeach 
                              </div>
                              @endif      
                              <div class="cart-total-div">
                                 <div class="cart-prices">
                                    <div class="price total">
                                       <span class="label">Total</span><span class="value pull-right"id="bill"></span>
                                    </div>
                                 </div>
                                 <div class="cart-buttons"><a rel="nofollow" href="<?php if(empty($member->id)){echo "/en/login";}else{echo "/cart";}?>">Check out <i class="ti-angle-right"></i></a></div>
                              </div>
                           </div>
                           <script> if (window.jQuery) { $(document).ready(function(){ accordionCart();  }); } </script>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </header>
         @yield('content')
         <footer id="footer">
            <div class="innovatoryFooter-top">
               <div class="container">
                  <div class="block_newsletter">
                     <div class="news-blocks">
                        <div class="title-block col-lg-6">
                           <div class="news-icon"><i class="fa fa-envelope-o"></i></div>
                           <div class="title-text">
                              <p class="h3">SIGN UP FOR NEWSLETTER</p>
                              <p> To subscribe Our Newsletter &amp; Get coupons.</p>
                           </div>
                        </div>
                        <div class="innovatoryForm col-lg-6">
                           <form action=" method="post">
                              <div class="input-wrapper"><input name="email" type="text" value="" placeholder="Your email address"></div>
                              <input class="btn btn-primary hidden-xs-down" name="submitNewsletter" type="submit" value="Subscribe"><input class="btn btn-primary hidden-sm-up" name="submitNewsletter" type="submit" value="OK"><input type="hidden" name="action" value="0">
                           </form>
                           <div class="innovatoryConditions"></div>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="innovatoryFooter-center footer">
               <div class="container">
                  <!-- Static Block module -->
                  <div class="service-sec col-md-12">
                     <div class="col-md-6 col-lg-3 service-box">
                        <div class="service">
                           <i class="icon-image icon_1"></i>
                           <div class="service-desc">
                              <h2>Best Prices & Offers</h2>
                              <ul>
                                 <li><i class="fa fa-check"></i> Get 20% extra</li>
                                 <li><i class="fa fa-check"></i> Exclusive Deal</li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-lg-3 service-box">
                        <div class="service">
                           <i class="icon-image icon_2"></i>
                           <div class="service-desc">
                              <h2>Wide Assortment</h2>
                              <ul>
                                 <li><i class="fa fa-check"></i>Free Delivery</li>
                                 <li><i class="fa fa-check"></i> Premium Licenses</li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-lg-3  service-box">
                        <div class="service">
                           <i class="icon-image icon_3"></i>
                           <div class="service-desc">
                              <h2>Easy Returns Policy</h2>
                              <ul>
                                 <li><i class="fa fa-check"></i>Return 30Days</li>
                                 <li><i class="fa fa-check"></i>No Policy & Terms</li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-lg-3 service-box">
                        <div class="service">
                           <i class="icon-image icon_4"></i>
                           <div class="service-desc">
                              <h2>secure payments</h2>
                              <ul>
                                 <li><i class="fa fa-check"></i>100% security</li>
                                 <li><i class="fa fa-check"></i>Confirmation system</li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </div>
                  <div class="clearfix"></div>
                  <!-- /Static block module -->
                  <div id="itfooterlogo" class="footer-about col-md-3 col-xs-12">
                     <div class="footer-logo"><a href="/" title="Demo Shop"> <img src="/nice-css/logo.png" alt="Demo Shop" width="300px" height="100px"/></a></div>
                  </div>
                  <div class="innovatorySocial col-md-3 col-xs-12">
                     <ul class="innovatory-media-body">
                        <li class="pull-left innovatory-facebook"><a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li class="pull-left innovatory-twitter"><a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li class="pull-left innovatory-youtube"><a href="#" title="YouTube" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                        <li class="pull-left innovatory-googleplus"><a href="#" title="Google +" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                        <li class="pull-left innovatory-instagram"><a href="#" title="Instagram" target="_blank"><i class="fa fa-instagram"></i></a></li>
                     </ul>
                  </div>
                  <div id="block_myaccount_infos" class="col-md-2 link-wrap wrapper">
                     <h3 class="h3 myaccount-title hidden-md-down">
                        <!-- <a class="text-uppercase" href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/my-account" rel="nofollow"> -->My account<!--  </a> -->
                     </h3>
                     <div class="title clearfix hidden-lg-up collapsed" data-target="#footer_account_list" data-toggle="collapse"><span class="h3">My account</span><span class="pull-xs-right"><span class="navbar-toggler collapse-icons"><i class="fa fa-plus-square-o" aria-hidden="true"></i><i class="fa fa-minus-square-o" aria-hidden="true"></i></span></span></div>
                     <ul class="account-list collapse" id="footer_account_list">
                        <li><a href="" title="Personal info" rel="nofollow"><i class="fa fa-angle-right"></i> Personal info</a></li>
                        <li><a href="" title="Orders" rel="nofollow"><i class="fa fa-angle-right"></i> Orders</a></li>
                        <li><a href="" title="Credit slips" rel="nofollow"><i class="fa fa-angle-right"></i> Credit slips</a></li>
                        <li><a href="" title="Addresses" rel="nofollow"><i class="fa fa-angle-right"></i> Addresses</a></li>
                        <li>
                           <!-- MODULE WishList --><a class="lnk_wishlist col-lg-4 col-md-6 col-sm-6 col-xs-12" href=""><i class="fa fa-heart"></i>My wishlists</span></a><!-- END : MODULE WishList -->
                        </li>
                     </ul>
                  </div>
                  <div class="col-lg-2 col-md-2 wrapper link-wrap">
                     <h3 class="h3 hidden-md-down">PRODUCTS</h3>
                     <div class="title clearfix hidden-lg-up collapsed" data-target="#footer_sub_menu_96872" data-toggle="collapse"><span class="h3">PRODUCTS</span><span class="pull-xs-right"><span class="navbar-toggler collapse-icons"><i class="fa fa-plus-square-o" aria-hidden="true"></i><i class="fa fa-minus-square-o" aria-hidden="true"></i></span></span></div>
                     <ul id="footer_sub_menu_96872" class="collapse">
                        <li><a id="link-cms-page-4-1" class="cms-page-link" href=""  title="Learn more about us"><i class="fa fa-angle-right"></i>About us</a></li>
                        <li><a id="link-product-page-prices-drop-1" class="cms-page-link" href="" title="Our special products"><i class="fa fa-angle-right"></i>Prices drop</a></li>
                        <li><a id="link-product-page-new-products-1" class="cms-page-link" href=""  title="Our new products"><i class="fa fa-angle-right"></i>New products</a></li>
                        <li><a id="link-product-page-best-sales-1" class="cms-page-link" href=""   title="Our best sales"><i class="fa fa-angle-right"></i>Best sales</a></li>
                        <li><a id="link-static-page-contact-1" class="cms-page-link" href="" title="Use our form to contact us"><i class="fa fa-angle-right"></i>Contact us</a></li>
                     </ul>
                  </div>
                  <div class="col-lg-2 col-md-2 wrapper link-wrap">
                     <h3 class="h3 hidden-md-down">OUR COMPANY</h3>
                     <div class="title clearfix hidden-lg-up collapsed" data-target="#footer_sub_menu_17057" data-toggle="collapse"><span class="h3">OUR COMPANY</span><span class="pull-xs-right"><span class="navbar-toggler collapse-icons"><i class="fa fa-plus-square-o" aria-hidden="true"></i><i class="fa fa-minus-square-o" aria-hidden="true"></i></span></span></div>
                     <ul id="footer_sub_menu_17057" class="collapse">
                        <li><a id="link-cms-page-1-2" class="cms-page-link" href="#"  title="Our terms and conditions of delivery"><i class="fa fa-angle-right"></i>Delivery</a></li>
                        <li><a id="link-cms-page-2-2" class="cms-page-link" href="#" title="Legal notice"><i class="fa fa-angle-right"></i>Legal Notice</a></li>
                        <li><a id="link-cms-page-5-2" class="cms-page-link" href="#"  title="Our secure payment method"><i class="fa fa-angle-right"></i>Secure payment</a></li>
                        <li><a id="link-static-page-sitemap-2" class="cms-page-link" href="#" title="Lost ? Find what your are looking for"><i class="fa fa-angle-right"></i>Sitemap</a></li>
                        <li><a id="link-static-page-stores-2" class="cms-page-link" href="#"   title=""><i class="fa fa-angle-right"></i>Stores</a></li>
                     </ul>
                  </div>
                  <div class="innovatory-contact col-lg-3 col-md-3 links wrapper">
                     <h3 class="h3 hidden-sm-down block-contact-title text-uppercase">Contact Us</h3>
                     <div class="title clearfix hidden-lg-up collapsed" data-target="#innovatoryContact" data-toggle="collapse"><span class="h3 text-uppercase">Contact Us</span><span class="pull-xs-right"><span class="navbar-toggler collapse-icons"><i class="fa fa-plus-square-o" aria-hidden="true"></i><i class="fa fa-minus-square-o" aria-hidden="true"></i></span></span></div>
                     <div id="innovatoryContact" class="collapse">
                        <p class="item innovatoryAddress"><i class="ti-location-pin"></i>Jivan park uttam nagar east</p>
                        <p class="item innovatoryPhone"><i class="ti-mobile"></i>(+91) 9876543210</p>
                        <p class="item innovatoryFax"><i class="ti-email"></i>backstagesupporters.com</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="innovatoryFooter-bottom">
               <div class="container">
                  <div class="copyright col-lg-6"><a class="_blank" href="http://www.backstagesupporters.com" target="_blank">© 2019 - Ecommerce software by backstagesupporters</a></div>
                  <!-- Static Block module -->
                  <!-- /Static block module -->
                  <div id="paiement_logos" class="payment_logos_images col-lg-6 payment">
                     <p class="payment-p">Payment acceptable on</p>
                     <a href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/en/content/1-delivery"><img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itpaymentlogo/views/img/visa.png" alt="Visa" width="40" height="25" /><img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itpaymentlogo/views/img/visa.png" alt="Mastercard" width="40" height="25" /><img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itpaymentlogo/views/img/visa.png" alt="Paypal" width="40" height="25" /><img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itpaymentlogo/views/img/visa.png" alt="American Express" width="40" height="25" /><img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itpaymentlogo/views/img/visa.png" alt="Discover" width="40" height="25" /><img src="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itpaymentlogo/views/img/visa.png" alt="Diners" width="40" height="25" /></a>
                  </div>
               </div>
            </div>
         </footer>
      </main>
      <!--   <div id="innovatoryPopupnewsletter" class="modal fade" tabindex="-1" role="dialog">
         <div class="innovatoryPopupnewsletter-i" role="document" style="max-width:700px;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ti-close" aria-hidden="true"></i></button>
            <div class="itpopupnewsletter" style="height:460px;background-image: url(/prestashop/INNO01/INNO1026_Gomart/IT26/modules/itpopupnewsletter/img/newslater-bg.jpg);">
               <div id="newsletter_block_popup" class="block">
                  <div class="block_content">
                     <form method="post">
                        <div class="newsletter_title">
                           <h3 class="h3">Newsletter</h3>
                        </div>
                        <div class="innovatoryContent">                 Sign up here to get 20% off on your next purchase, special offers and other discount information.              </div>
                        <div class="form-wrap">
                           <input class="inputNew" id="itnewsletter-input" type="text" name="email" placeholder="Enter your mail..."/> 
                           <div class="send-reqest btn btn-primary">Subscribe!</div>
                           <div class="itAlert"></div>
                        </div>
                     </form>
                  </div>
                  <!-- <div class="newsletter_block_popup-bottom check-fancy">           <input id="innovatory_newsletter_dont_show_again" type="checkbox" class="hidden">            <label class="innovatory_newsletter_dont_show_again" for="innovatory_newsletter_dont_show_again">Do not show this popup again</label>        </div> -->
      </div>
      </div>
      </div>
      <!-- /.modal-dialog -->
      <!--  </div> --> 
      <!-- /.modal --><script type="text/javascript">    var placeholder2 = "Enter your mail...";     $(document).ready(function() {            $('#itnewsletter-input').on({                focus: function() {                    if ($(this).val() == placeholder2) {                        $(this).val('');                    }                },                blur: function() {                    if ($(this).val() == '') {                        $(this).val(placeholder2);                    }                }            });        });    </script><script type="text/javascript">var field_width=700;var field_height=460;var field_newsletter=1;var field_path='https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/modules/itpopupnewsletter/ajax.php';</script>
      <!--scroll up--><a href="javascript:void(0)" class="mypresta_scrollup hidden-phone open"><i class="ti-arrow-up"></i></a>
      <script type="text/javascript" src="/nice-js/core.js" ></script>
      <script type="text/javascript" src="/nice-js/theme.js" ></script>
      <script type="text/javascript" src="/nice-js/front.js" ></script>
      <script type="text/javascript" src="/nice-js/jquery.rating.pack.js" ></script>
      <script type="text/javascript" src="/nice-js/jquery.textareaCounter.plugin.js" ></script>
      <script type="text/javascript" src="/nice-js/productcomments.js" ></script>
      <script type="text/javascript" src="/nice-js/itleftnewproducts.js" ></script>
      <script type="text/javascript" src="/nice-js/itleftspecialproducts.js" ></script>
      <script type="text/javascript" src="/nice-js/ajax-wishlist.js" ></script>
      <script type="text/javascript" src="/nice-js/init.js" ></script>
      <script type="text/javascript" src="/nice-js/front.js" ></script>
      <script type="text/javascript" src="/nice-js/jquery-ui.min.js" ></script>
      <script type="text/javascript" src="/nice-js/jquery.fancybox.js" ></script>
      <script type="text/javascript" src="/nice-js/ps_searchbar.js" ></script>
      <script type="text/javascript" src="/nice-js/ps_shoppingcart.js" ></script>
      <script type="text/javascript" src="/nice-js/itblog.js" ></script>
      <script type="text/javascript" src="/nice-js/jquery.nivo.slider.pack.js" ></script>
      <script type="text/javascript" src="/nice-js/itimageslider.js" ></script>
      <script type="text/javascript" src="/nice-js/custom.js" ></script>
      <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"></script>
       <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

       <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.css"></script>

       <script>       
         function showCart(){

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
               /* the route pointing to the post function */
               url: '/cart/show',
               type: 'POST',
               /* send the csrf-token and the input to the controller */               
               data: {_token: CSRF_TOKEN},
               success: function (data) {      
                  $('#showminicart').html(data);
                  $("#bill").text(data.bill);         
               }
            }); 
         }
         
         // var count_product=<?php echo session()->get('count'); ?>;
         function deleteProduct(id){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');       
            // var std = $("#show-total").html();
            // count_product--;        
            $('#listhide'+id).hide();
            $('#delc'+id).hide();
            $.ajax({
               /* the route pointing to the post function */
               url: '/cart/delete-product/id',
               type: 'POST',
               /* send the csrf-token and the input to the controller */            
               data: {_token: CSRF_TOKEN, id: id},
               success: function (data) { 
                  $(".show-total").html(data.showcount);       
                  $("#bill").html(data.bill);       
               }
            }); 
         }

         function showmenu(){
            $('.menu-vertical').toggle();
         }


         function getCart(){
            checkId = $('#hid_user').val();
            // alert(checkId);
            if(checkId){
                window.location.href = "/cart";
            }
            else{
               Swal('Please first Login');
            }
           
         }
          
      </script>
      @yield('script')
   </body>
</html>