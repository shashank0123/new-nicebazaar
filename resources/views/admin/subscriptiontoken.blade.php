@extends('layouts.admin')

@section('content')


<section style="margin-top: 50px;">
<div class="container">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>S.N</th>
        <th>Token</th>
        <th>Subscriptions</th>
        <th>User ID</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
      <?php 
      $sn = 1;
      ?>
      @foreach($subtok as $st)
      <tr>
        <td>{{$sn}}</td>
        <td>{{$st->token}}</td>
        <td>{{$st->subscription}}</td>       
        <td>@if ($st->user_id != null){{'SS00'.$st->user_id}}@endif</td>       
        <td>{{$st->used}}</td>       
      </tr>
      <?php $sn ++;?>
      @endforeach
    </tbody>
  </table>
</div>
</section>


@endsection
