@extends('front.app')
<style>
  body{
    background:url(../assets/img/login.jpg) no-repeat center center fixed !important;
  }
  .pink-text {  font-size: 24px;  }

  .card{margin-top:0% auto; max-width:40%;margin-left: 28%;margin-top: 10%; background-color: #fff;border-radius: 4%;}


  .pull-right button { margin-bottom: 20px !important }
  @media screen and (max-width: 991px)
  {
    .card { max-width:70%;margin-left: 15%;margin-top: 20% !important }
  }

  @media screen and (max-width: 580px)
  {
    .card { max-width:90%;margin-left: 5%; }
  }

  #login-head { width: 100% !important; background-color: orange }
  #otpcontainer { display: none; }
</style>



@section('title')
Register - {{ config('app.name') }}
@stop

@section('content')
<div class="clearfix"></div>
<div class="center">
  <div  class="col-lg-12 col-md-12 col-sm-12" style="background: orange; position: absolute; top: 0;">
                        <div style="background-color: orange;padding-left: 10px; padding-right: 10px; padding-top: 5px;
    padding-bottom: 0;" class="pull-right" >
                            <ul>
                                <li style="list-style: none">
                                    <a href="#" style="color: #fff; font-size: 14px">Help Line:  1800-419-8447</a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>



  <div class="card bordered z-depth-2" style="margin-top: 70px">
    <div class="card-header">
      <div class="brand-logo" style="text-align: center;">
        <img src="{{ asset('asset/images/menu/logo/mbiz.png') }}" width="100">
      </div>
    </div>
    <div id="formcontainer" class="container m-b-30" style="max-width: 100%;">
      <form class="form-floating action-form" id="register" http-type="post" data-url="{{ route('registerforuse', ['lang' => \App::getLocale()]) }}">
        <div class="card-content">
          <div class="m-b-30">
            <div class="card-title strong pink-text">Registeration Form - Become a member</div>
          </div>
          <div class="form-group">
            <label for="username">Name</label>
            <input type="text" name="username" class="form-control" id="username" required="required">
          </div>

          <div class="form-group">
            <label for="username">Email</label>
            <input type="text" name="email" class="form-control" id="email" required="required">
          </div>

          <div class="form-group">
            <label for="username">Mobile</label>
            <input type="text" name="mobile" class="form-control" id="phone" required="required">
          </div>

          <div class="form-group">
            <label for="username">Adhaar</label>
            <input type="text" name="adhaar" class="form-control" id="username" required="required">
          </div>
        </div>
        
        <div class="card-action clearfix">
          <div class="pull-left">
            <a href="/en/login" class="btn btn-link black-text">              
              <span style="color:red">Login</span>
            </a>
          </div>

          <div class="pull-right">
            <span id="submit" onclick="showDiv()" class="btn btn-link black-text">
              <span class="btn-preloader">
                <i class="md md-cached md-spin"></i>
              </span>
              <span>@lang('login.savetitle')</span>
            </span>
          </div>
        </div>
      </form>
      <br>
    </div>

    <div id="otpcontainer" class="container m-b-30" style="max-width: 100%;">
      <form id="otpform" class="form-floating action-form" http-type="post" data-url="{{ route('verifyotp', ['lang' => \App::getLocale()]) }}">
        <div class="card-content">
          <div class="m-b-30">
            <div class="card-title strong pink-text">Verify Your OTP</div>
          </div>
          <input type="hidden" name="email" id="emailvalue">

          <div class="form-group">
            <label for="username">Enter OTP</label>
            <input type="text" name="otp" class="form-control" id="otp" required="">
          </div>
        </div>
        <input type="hidden" name="verify_otp" class="form-control" id="verify_otp" required="">

        <div class="card-action clearfix"> 
          <div class="pull-right" >
            <span id="verify" class="btn btn-link black-text">
              <span class="btn-preloader">
                <i class="md md-cached md-spin"></i>
              </span>
              <span>@lang('login.savetitle')</span>
            </span>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

  @stop

  <script>


    $('#otpform').submit(function() {

        $.ajax({
            type: "POST",
            url: 'verifyotp',
            data: {
                otp: $("#otp").val(),
            },
            success: function(data)
            {
                if (data === 'Correct') {
                    window.location.replace('admin/admin.php');
                }
                else {
                    alert(data);
                }
            }
        });
        //this is mandatory other wise your from will be submitted.
        return false; 
    });

    function showDiv(){    
    // event.preventDefault();

    var request = $.ajax({
      type: "POST",
      url: "/register",
      data: $('#register').serialize(),
      
    });
    request.done(function(msg) {
      if (msg.message == 'Data Inserted Successfully'){

        $('#formcontainer').hide();
        $('#emailvalue').val($('#email').val())
        console.log(msg);
        $("#otpcontainer").show();
      }
      else
        Swal(msg.message);
                  // $("div").html(result);
                  
                });

    request.fail(function(msg) {
      Swal(msg.message);
    });
    

  }  
  
</script>








{{-- //Controller --}}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controller;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Validator;
use App\Leads;
use StdClass;

class NewRegController extends BaseController
{
    public function Register(Request $request)
    {
        $response = new StdClass;
        $response->status = 200;
        $response->message = 'Something went wrong';
        $user = new Leads;
        $checkemail = Leads::where('email',$request->email)->first();

        if($checkemail)
        { 
            $response->message = "Already Registered"; 
            return response()->json($response);
            exit();             
        }

        else
        {
            if($request->username == null  ||  $request->email == null  ||  $request->mobile == null  ||  $request->adhaar == null)
            {
                $response->message = 'Please fill All the Fields';
                $response->username = $request->username;
                $response->email = $request->email;
                $response->mobile = $request->mobile;
                $response->adhaar = $request->adhaar;
                return response()->json($response);
                exit();
            }

            else{

                $user->username = $request->username;
                $user->phone    = $request->mobile;
                $user->email    = $request->email;
                $user->adhaar   = $request->adhaar;
                $user->otp      = rand(100000, 999999);
                $user->otp_time = date('Y-m-d H:m:s') ;

                $user->save();
            }

            $mobile = $user->phone;
            $message = "Your%20OTP%20for%20NiceBazaar%20is%20$user->otp";
            // $url = "http://103.247.98.91/API/SendMsg.aspx?uname=20190770&pass=d797g9t4&send=flgrth&dest=$mobile&msg=$message";  
            $url = "";
            $c = curl_init();
            curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($c,CURLOPT_HTTPGET ,1);

            curl_setopt($c, CURLOPT_URL, $url);
            $contents = curl_exec($c);
            if (curl_errno($c)) {
                echo 'Curl error: ' . curl_error($c);
            }
            else{
                curl_close($c);
            }
            if($user){
                $response->message ="Data Inserted Successfully";
                return response()->json($response);
            }

            else{
                echo " Something Went Wrong";
            }
        }        
    }

    public function verifyotp(Request $request)
    {
        $data = $request->data;
        $findUser = Leads::where('email',$data['email'])->first();

        if($findUser->verify_otp != 'verified')
        {
            if($findUser->otp == $data['otp']) {
                $findUser->verify_otp ='verified';
                $findUser->update();
                echo "Number Verified. ";
                return "Registered Successfully";
            }
            else {                
                return "Wrong OTP";
            }
        }
        else
        {
            echo "Already Verified";
        }        
    }
}
