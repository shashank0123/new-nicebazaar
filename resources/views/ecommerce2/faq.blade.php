@extends('layouts/ecommerce2')

@section('content')

<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
</head>
<style>
  body {
    font-family: "helvetica neue", helvetica, sans-serif;
    background-color: #ffffff !important;
    overflow-y:auto;
  }

  .faq_section{
    margin:40px auto;
  }
  .FaQ_Each{
    padding-bottom: 10px;
  }
  .box  {
    /*background: rgba(246, 246, 246, 1);*/
    color: #666666;
    padding-top: 15px;
    padding-bottom:15px;
    padding-left: 20px;
    font-size: 13px;
    text-transform: uppercase;
    background-color: #f4f4f4;
    cursor:pointer;
    border: 1px solid #d9d9d9;
  }

  .box span:hover { color: #37bc9b; }
  .draw {
    display: none;
    background: #ffffff;
    padding: 20px;
    border-bottom: 1px solid #d9d9d9;
    border-left: 1px solid #d9d9d9;
    border-right: 1px solid #d9d9d9;
    color: #000000;
    padding-left:30px;
  }
  .pull-right { margin-right: 20px }
  .faq_section{ text-align: center; }
  #other{
    display: none;
  }

  .frequently-desc { padding: 50px 50px 10px; text-align: center; line-height: 1.5; font-size: 16px }
</style>

<div class="container-fluid faq">
  <div class="frequently-content">
    <div class="frequently-desc">
      <h4>Below are frequently asked questions, you may find the answer for yourself</h4>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id erat sagittis, faucibus
        metus malesuada, eleifend turpis. Mauris semper augue id nisl aliquet, a porta lectus
        mattis. Nulla at tortor augue. In eget enim diam. Donec gravida tortor sem, ac fermentum
        nibh rutrum sit amet. Nulla convallis mauris vitae congue consequat. Donec interdum nunc
      purus, vitae vulputate arcu fringilla quis. Vivamus iaculis euismod dui.</p>
    </div>
  </div>

  <div class="container faq_section">

    {{-- Row1 --}}
    <div class="FaQ_Each">
      <section class="box">

        <span>Mauris congue euismod purus at semper. Morbi et vulputate massa?</span>
        <span class="pull-right">
          <i class="fa fa-plus" aria-hidden="true"></i>
          <i class="fa fa-minus" id="other" aria-hidden="true"></i>
        </span>
      </section>
      <section class="draw">
       Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
       richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
       brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
       aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
       Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
       ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
       farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them
       accusamus labore sustainable VHS.
     </section>
   </div>

   {{-- Row 2 --}}

   <div class="FaQ_Each">
    <section class="box">

      <span>Donec mattis finibus elit ut tristique?</span>
      <span class="pull-right">
        <i class="fa fa-plus" aria-hidden="true"></i>
        <i class="fa fa-minus" id="other" aria-hidden="true"></i>
      </span>
    </section>
    <section class="draw">
      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
      richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
      brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
      aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
      Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
      ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
      farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them
      accusamus labore sustainable VHS.
    </section>
  </div>


  {{-- Row 3 --}}
  <div class="FaQ_Each">
    <section class="box">

      <span>Vestibulum a lorem placerat, porttitor urna vel?</span>
      <span class="pull-right">
        <i class="fa fa-plus" aria-hidden="true"></i>
        <i class="fa fa-minus" id="other" aria-hidden="true"></i>
      </span>
    </section>
    <section class="draw">
      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
      richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
      brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
      aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
      Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
      ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
      farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them
      accusamus labore sustainable VHS.
    </section>
  </div>


  {{-- Row 4 --}}


  <div class="FaQ_Each">
    <section class="box">

      <span>Aenean elit orci, efficitur quis nisl at, accumsan?</span>
      <span class="pull-right">
        <i class="fa fa-plus" aria-hidden="true"></i>
        <i class="fa fa-minus" id="other" aria-hidden="true"></i>
      </span>
    </section>
    <section class="draw">
     Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
     richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
     brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
     aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
     Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
     ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
     farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them
     accusamus labore sustainable VHS.
   </section>
 </div>





 <div class="FaQ_Each">
  <section class="box">

    <span>Pellentesque habitant morbi tristique senectus et netus?</span>

    <span class="pull-right">
      <i class="fa fa-plus" aria-hidden="true"></i>
      <i class="fa fa-minus" id="other" aria-hidden="true"></i>
    </span>
  </section>
  <section class="draw">
    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
    richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
    brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
    aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
    Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
    ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
    farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them
    accusamus labore sustainable VHS.
  </section>
</div>
<div class="FaQ_Each">
  <section class="box">

    <span>Nam pellentesque aliquam metus?</span>

    <span class="pull-right">
      <i class="fa fa-plus" aria-hidden="true"></i>
      <i class="fa fa-minus" id="other" aria-hidden="true"></i>
    </span>
  </section>
  <section class="draw">
    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
    richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
    brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
    aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
    Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
    ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
    farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them
    accusamus labore sustainable VHS.
  </section>
</div>
<div class="FaQ_Each">
  <section class="box">

    <span>Aenean elit orci, efficitur quis nisl at?</span>

    <span class="pull-right">
      <i class="fa fa-plus" aria-hidden="true"></i>
      <i class="fa fa-minus" id="other" aria-hidden="true"></i>
    </span>
  </section>
  <section class="draw">
   Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
   richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
   brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
   aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
   Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
   ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
   farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them
   accusamus labore sustainable VHS.
 </section>
</div>
<div class="FaQ_Each">
  <section class="box">

    <span>Morbi gravida, nisi id fringilla ultricies, elit lorem?</span>

    <span class="pull-right">
      <i class="fa fa-plus" aria-hidden="true"></i>
      <i class="fa fa-minus" id="other" aria-hidden="true"></i>
    </span>
  </section>
  <section class="draw">
   Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
   richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
   brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
   aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
   Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
   ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
   farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them
   accusamus labore sustainable VHS.
 </section>
</div>
</div>
</div>

<script>
  $(document).ready(function(){
    $(".box").click(function(){
      $(this).next().slideToggle("fast");
      $(this).find('i').toggle();
    });  

  });
</script>

@endsection