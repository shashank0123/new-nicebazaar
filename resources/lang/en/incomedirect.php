<?php

return [
'title'	=>	'Direct Income',
'subtitle' => 'Your direct members registration history',
'username' => 'Username',
'password' => 'Password',
'remember' => 'Remember Me'
];