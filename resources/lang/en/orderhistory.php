<?php

return [
	'title' => 'Orders History',
	'subTitle' => 'Your all orders list.',
	'serial' => 'S.No.',
	'products' => 'Order Items',
	'payment' => 'Payment Mode',
	'transactionId' => 'Transaction ID',
	'payableAmount' => 'Total Payable Amount',
	'status' => 'Status',
	'action' => 'Action',
	'date' => 'Date'
];