/*
 * Custom code goes here.
 * A template should always ship with an empty custom.js
 */
$(function(){
    $('.innovatoryProd-cate .innovatory-Tab1 .nav-item').click(function(){
        $('.innovatoryProd-cate .innovatory-Tab1 .nav-item').removeClass('innovatory-active');
        $(this).addClass('innovatory-active');
    });
});

/*=============Loading Function============*/
$(document).ready(function (){
    loadding();
});
function loadding() {
    $(window).load(function() {
        // Animate loader off screen
        $(".preloader").fadeOut("slow");;
    });
}

/*=============Grid List Active Class============*/
$(document).ready(function () {
    $("#grid a").click(function(e) {
        $("#products").removeClass("active_list");
        $("#products").addClass("active_grid");
        setCookie('status_list_product','active_grid',1);
    });
    $("#list a").click(function(e) {
        $("#products").removeClass("active_grid");
        $("#products").addClass("active_list");
        setCookie('status_list_product','active_list',1);
    });
    if(getCookie('status_list_product')!=="" && getCookie('status_list_product')!=="active_grid"){
        $("#products").removeClass("active_grid");
        $("#products").addClass("active_list");
    }
});

/*=============Cookie popup============*/  
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

/*=============Scroll to Top============*/ 
$(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.mypresta_scrollup').addClass('open');
        } else {
            $('.mypresta_scrollup').removeClass('open');
        }
    });
    // scroll body to 0px on click
    $('.mypresta_scrollup').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
});

/*=============Sticky header============*/
 function fixedheader(){
    $(window).scroll(function () {    
        var sc = $(window).scrollTop();
        if (sc > 300) {
            $("#header").addClass("sticky");        
        } else {
            $("#header").removeClass("sticky");
        }
    });
}
fixedheader();

/*=============Megamenu Responsive============*/
$(document).ready(function(){
    if ($(document).width() >= 1200){
      var number_blocks =5;
      var count_block = $('.full-nav .it-menu-horizontal .menu-content > .level-1');
      var moremenu = count_block.slice(number_blocks, count_block.length);
      moremenu.wrapAll('<li class="view_menu level-1 parent"><div class="more-menu menu-dropdown cat-drop-menu it-sub-auto">');
      $('.view_menu').prepend('<a href="#" class="level-top"><span>More</span></a>');
    }
    if (($(document).width() >= 1051) && ($(document).width() <= 1199)){
      var number_blocks = 3;
      var count_block = $('.full-nav .it-menu-horizontal .menu-content > .level-1');
      var moremenu = count_block.slice(number_blocks, count_block.length);
      moremenu.wrapAll('<li class="view_menu level-1 parent"><div class="more-menu menu-dropdown cat-drop-menu it-sub-auto">');
      $('.view_menu').prepend('<a href="#" class="level-top"><span>More</span></a>');
    };
});

/*=============Mobile header============*/
$(document).ready(function(){
    $(".close-sidebar").click(function() {
        $("html").removeClass('sidebar-open');
        $('#header').removeClass('toggle');
    });
    setTimeout(function(){  
        $(".sidebar-overlay").click(function() {
            $("html").removeClass('sidebar-open');
            $('#header').removeClass('toggle');
        });   
    }, 300);
});

/*=============Sidebar left column position============*/
function responsivecolumn(){
    if($(document).width()<=991){
        $('body:not(#contact) .container #left-column').insertAfter('#content-wrapper');
        $('#search_filters_wrapper').insertBefore('.pos-top');
    }else if($(document).width()>=992){
        $('body:not(#contact) .container #left-column').insertBefore('#content-wrapper')
    }
}

/*=============Mobile Vegamenu Position============*/
function vegamenuposition(){
    if($(document).width()<=1050){
        $("#mobile_top_menu_wrapper .container_it_vegamenu .title-menu").click(function(){
            $("#mobile_top_menu_wrapper .it-menu-horizontal .menu-content").slideUp();
            $(this).next().slideDown();
        });
        $("#mobile_top_menu_wrapper .it-menu-horizontal .menu-tit").click(function(){
            $(this).next().slideDown();
            $("#mobile_top_menu_wrapper .container_it_vegamenu .menu-vertical").slideUp();
        });
    }
}

$(document).ready(function(){
    fixedheader();
    responsivecolumn();
    vegamenuposition();
});
$(window).resize(function(){
    fixedheader();
    responsivecolumn();
});   

/*=============Init Accordian============*/
var responsiveflag = false;

$(document).ready(function () {
    responsiveResize();
    $(window).resize(responsiveResize);
});
function responsiveResize()
{
    if ($(window).width() <= 991 && responsiveflag == false)
    {
        accordion('enable');
        responsiveflag = true;
    }
    else if ($(window).width() >= 992)
    {
        accordion('disable');
        responsiveflag = false;
    }
}
function accordion(status)
{
    if(status == 'enable')
    {
        var accordion_selector = '#left-column .category-top-menu .title, #left-column .sidebar-head,#left-column .title_block,.product-column-style .itProductList .title_block';

        $(accordion_selector).on('click', function(e){
            $(this).toggleClass('active').next().stop().slideToggle('medium');
            e.preventDefault();
        });
        $(accordion_selector).next().slideUp('fast');
    }
    else
    {
        $('#left-column .category-top-menu .title, #left-column .sidebar-head,#left-column .title_block,.product-column-style .itProductFilter').removeClass('active').off().next().removeAttr('style').slideDown('fast');
    }
}

/*=============Cart-toggle============*/
function accordionCart()
{
    $('#header .blockcart').on('click', function(e){
        e.stopPropagation();
        $('#header .header-nav [data-toggle=dropdown],#_desktop_user_info [data-toggle=dropdown]').next('.dropdown-menu').slideUp();
        $('#header .header-nav [data-toggle=dropdown],#_desktop_user_info [data-toggle=dropdown]').parent().removeClass('open');
        $(this).toggleClass('active').parent().find('.cart_block').stop().slideToggle('medium');
        e.preventDefault();
    });
    $('.blockcart').find('.cart_block').slideUp('medium');

    $('.dropdown').on('show.bs.dropdown', function () {
      $('.blockcart').find('.cart_block').slideUp('medium');
    });
}
$(document).on('click', function(e){
    e.stopPropagation();
    $('.blockcart').find('.cart_block').slideUp('medium');
    $('#header .blockcart .cart').removeClass('active');
});

/*=============fancybox============*/
$(document).ready(function() {      
    $("a.grouped_elements").fancybox({
        'speedIn'       :   600, 
        'speedOut'      :   200, 
        'overlayShow'   :   false
    });
});

/*=============Contact map Position============*/
$('#map-style').insertAfter('#wrapper > .container');

/*=============Itmanufacture============*/
var manufacturer = $(".list_manufacturer");
manufacturer.owlCarousel({
    dots:false,
    rewind:true,
    autoplayHoverPause:true,
    autoplay:true,
    nav:false,
    responsive:{
        0:{
            items:2
        },
        480:{
            items:3
        },
        992:{
            items:4
        },
        1200:{
            items:5
        },
        1500:{
            items:5
        }
    }
});

/*=============itCategoryFeature============*/
var owl = $(".itCategoryFeature");
owl.owlCarousel({
    rewind:true,nav:true,autoplay:false,autoplayHoverPause:true,dots:false,
    navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
    responsive:{
        0:{
            items:1
        },
        480:{
            items:2
        },
        992:{
            items:3
        },
        1200:{
            items:4
        },
        1500:{
            items:4
        }
    }
});
/*============= Slide Toggle ============*/
$(".slidetoggle-init").click(function(){
  $(this).parent().find(".slidetoggle-menu").slideToggle();
});

/*=============Vegamenu menu more link============*/
if(jQuery(window).width() >= 1340){
    var max_elem = 10;   
}else if(jQuery(window).width() >= 1200){
    var max_elem = 7;   
}else if(jQuery(window).width() >= 1051){
    var max_elem = 7;   
}else{
      
}
var menu = $('.container_it_vegamenu .menu-content > li');
if (menu.length > max_elem) {
    $('.container_it_vegamenu .menu-content').append('<li class="more level-1"><a class="more-menu"><span class="categories">More Categories<i class="material-icons">&#xE145;</i></span></a></li>');
}
$('.container_it_vegamenu .menu-content > li .more-menu').click(function() {
    if ($(this).hasClass('active')) {
        menu.each(function(j) {
            if (j >= max_elem) {
                $(this).slideUp(500);
            }
        });
        $(this).removeClass('active');
      
        $('.container_it_vegamenu .menu-content > li .more-menu').html('<span class="categories">More Categories<i class="material-icons">&#xE145;</i></span>');
    } else {
        menu.each(function(j) {
            if (j >= max_elem) {
                $(this).slideDown(500);
            }
        });
        $(this).addClass('active');
        $('.container_it_vegamenu .menu-content > li .more-menu').html('<span class="categories">Less Categories<i class="material-icons">&#xE15B;</i></span>');
    }
});

menu.each(function(j) {
    if (j >= max_elem) {
        $(this).css('display', 'none');
    }
});