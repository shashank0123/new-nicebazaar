/**
* 2015-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  jQuery v1.7.1 jquery.com | jquery.org/license 
*  @copyright 2015-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/

$(document).ready(function() {
	var $newSlideConf = $('.it_new_product');
	var items       = parseInt($newSlideConf.attr('data-items'));
	var speed     	= parseInt($newSlideConf.attr('data-speed'));
	var autoPlay    = parseInt($newSlideConf.attr('data-autoplay'));
	var time    	= parseInt($newSlideConf.attr('data-time'));
	var arrow       = parseInt($newSlideConf.attr('data-arrow'));
	var pagination  = parseInt($newSlideConf.attr('data-pagination'));
	var move        = parseInt($newSlideConf.attr('data-move'));
	var pausehover  = parseInt($newSlideConf.attr('data-pausehover'));
	var lg          = parseInt($newSlideConf.attr('data-lg'));
	var md          = parseInt($newSlideConf.attr('data-md'));
	var sm          = parseInt($newSlideConf.attr('data-sm'));
	var xs          = parseInt($newSlideConf.attr('data-xs'));
	var xxs         = parseInt($newSlideConf.attr('data-xxs'));
	
	if(autoPlay==1) {
		if(time){
			autoPlay = time; 
		}else{
			autoPlay = '3000';
		}
	}else{
		autoPlay = false;
	}
	if(pausehover){pausehover = true}else{pausehover=false}
	if(move){move = false}else{move=true}
	if(arrow){arrow =true}else{arrow=false}
	if(pagination==1){pagination = true}else{pagination=false}

	var newSlide = $(".it_new_product .newSlide");
	newSlide.owlCarousel({
		autoPlay : autoPlay ,
		smartSpeed: speed,
		autoplayHoverPause: pausehover,
		scrollPerPage: move,
		nav: arrow,
		dots : pagination,	
		responsive:{
			0:{
				items:xxs,
			},
			480:{
				items:xs,
			},
			768:{
				items:sm,
				nav:false,
			},
			992:{
				items:md,
			},
			1200:{
				items:lg,
			},
			1920:{
				items:items,
			}
		}
	});
	checkClasses();
    newSlide.on('translated.owl.carousel', function(event) {
        checkClasses();
    });

    function checkClasses(){
		var total = $('.it_new_product .newSlide .owl-stage .owl-item.active').length;
        $('.it_new_product ').each(function(){
			$(this).find('.owl-item').removeClass('firstActiveItem');
			$(this).find('.owl-item').removeClass('lastActiveItem');
			$(this).find('.owl-item.active').each(function(index){
				if (index === 0) { $(this).addClass('firstActiveItem'); }
				if (index === total - 1 && total>1) {
					$(this).addClass('lastActiveItem');
				}
			})  
        });
    }
});

