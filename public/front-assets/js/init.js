/**
*  @author    Innovatory Themes
*  @copyright 2015-2018 Innovatory Themes. All Rights Reserved.
*  @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/
$(window).load(function() {
    if ($.cookie("itpopupnewsletter") != "true" && $('body').outerWidth() > 319) {

		setTimeout(function(){
	        $("#innovatoryPopupnewsletter").modal({
	            show: true
	        });
    	}, 5000);        
        

        $(".send-reqest").click(function() {
            var email = $("#itnewsletter-input").val();
            $.ajax({
                type: "POST",
                headers: {
                    "cache-control": "no-cache"
                },
                async: false,
                url: field_path,
                data: "name=marek&email=" + email,
                success: function(data) {
                    if (data) $(".itAlert").text(data);
                }
            });
        });
        $('#itnewsletter-input').keypress(function(event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                var email = $("#itnewsletter-input").val();
                $.ajax({
                    type: "POST",
                    headers: {
                        "cache-control": "no-cache"
                    },
                    async: false,
                    url: field_path,
                    data: "name=marek&email=" + email,
                    success: function(data) {
                        if (data) $(".itAlert").text(data);
                    }
                });
                event.preventDefault();
            }
        });
        $("#innovatory_newsletter_dont_show_again").prop("checked") == false;
    }
    $('#innovatory_newsletter_dont_show_again').change(function() {
        if ($(this).is(':checked')) {
            $.cookie("itpopupnewsletter", "true");
        } else {
            $.cookie("itpopupnewsletter", "false");
        }
    });
});