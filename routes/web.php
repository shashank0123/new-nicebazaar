<?php
\Cache::flush();
// use App\Models\Member;
// use App\Repositories\SharesRepository;
// use App\Repositories\MemberRepository;
// use App\Repositories\BonusRepository;
/*
|--------------------------------------------------------------------------
| Web Routes

|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
| 
*/

// Route::get('test', function () {
//     $member = \App\Models\Member::where('id', 3)->first();
//     $wallet = $member->wallet;
//     $repo = new SharesRepository;
//     $repo->repurchasePackage($member, $wallet->purchase_point, $wallet);
// });

Route::post('/update-order-status', 'Admin\SiteController@updateOrderStatus');

Route::post('/user-availability','SiteController@checkAvailability');

Route::post('/user-name', 'Admin\MemberController@getMemberDetail');
Route::post('/update-order-status', 'Admin\SiteController@updateOrderStatus');

Route::get('test', 'SiteController@fixNetwork');

// Route::get('/', function() {
//     //return redirect()->route('/', ['lang' => App::getLocale()]);
//     return  view('nicebazaar.index');
// });

Route::get('/register', function() {
  return  view('register');
});
Route::get('/about', function() {
  return view('about');
});
Route::get('/contact', function() {
  return view('contact');
});
Route::get('/login', function() {
    return redirect()->route('home', ['lang' => App::getLocale()]);
});
Route::post('register', 'NewRegController@Register')->name('registerforuse');
Route::post('verifyotp', 'NewRegController@verifyotp')->name('verifyotp');

Route::get('/register/membership-fee/{id}','NewRegController@checkoutForm');
Route::post('/register/membership-fee/{id}','NewRegController@checkoutForm1');

Route::post('/success','NewRegController@getSuccess');


/**
 * Language specific routes
 */
Route::group(['prefix' => '{lang?}', 'where' => ['lang' => '(en|chs|cht)'], 'middleware' => 'locale'], function () {
    Route::get('login', ['as' => 'login', 'uses' => 'SiteController@getLogin']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'SiteController@getLogout']);

    Route::get('/', ['as' => 'home', 'uses' => 'SiteController@getHome']);
    Route::get('/', ['as' => 'home', 'uses' => 'NiceBazaarController@index']);
    Route::get('/member', ['as' => 'memberhome', 'uses' => 'SiteController@getHome']);

    Route::get('settings/account', ['as' => 'settings.account', 'uses' => 'SiteController@getSettingsAccount']);
    Route::get('settings/bank', ['as' => 'settings.bank', 'uses' => 'SiteController@getSettingsBank']);
    Route::get('settings/address', ['as' => 'settings.address', 'uses' => 'SiteController@getSettingsAddress']);

    Route::get('member/register', ['as' => 'member.register', 'uses' => 'SiteController@getMemberRegister']);
    Route::get('member/register-success', ['as' => 'member.registerSuccess', 'uses' => 'SiteController@getMemberRegisterSuccess']);
    Route::get('member/register-history', ['as' => 'member.registerHistory', 'uses' => 'SiteController@getMemberRegisterHistory']);
    Route::get('member/upgrade', ['as' => 'member.upgrade', 'uses' => 'SiteController@getMemberUpgrade']);

    Route::get('network/binary', ['as' => 'network.binary', 'uses' => 'SiteController@getNetworkBinary']);
    Route::get('network/unilevel', ['as' => 'network.unilevel', 'uses' => 'SiteController@getNetworkUnilevel']);

    Route::get('shares/market', ['as' => 'shares.market', 'uses' => 'SiteController@getSharesMarket']);
    Route::get('shares/lock-list', ['as' => 'shares.lock', 'uses' => 'SiteController@getSharesLock']);
    Route::get('shares/statement', ['as' => 'shares.statement', 'uses' => 'SiteController@getSharesStatement']);

    Route::get('transaction/transfer', ['as' => 'transaction.transfer', 'uses' => 'SiteController@getTransfer']);
    Route::get('transaction/withdraw', ['as' => 'transaction.withdraw', 'uses' => 'SiteController@getWithdraw']);
    Route::get('transaction/statement', ['as' => 'transaction.statement', 'uses' => 'SiteController@getTransactionStatement']);

    Route::get('income-direct', ['as' => 'income.direct', 'uses' => 'SiteController@getDirectPage']);
    Route::get('bonus-statement', ['as' => 'bonus.statement', 'uses' => 'SiteController@getBonusStatement']);
    Route::get('summary-statement', ['as' => 'summary.statement', 'uses' => 'SiteController@getSummaryStatement']);

    Route::get('terms', ['as' => 'terms', 'uses' => 'SiteController@getTerms']);
    Route::get('member/show-modal', ['as' => 'member.showModal', 'uses' => 'MemberController@getMemberRegisterModal']);

    Route::get('member/get-binary', ['as' => 'member.getBinary', 'uses' => 'MemberController@getBinary']);
    Route::get('member/binary-back', ['as' => 'member.getBinaryTop', 'uses' => 'MemberController@getBinaryTop']);
    Route::get('member/binary-modal', ['as' => 'member.binary.modal', 'uses' => 'MemberController@getBinaryModal']);

    Route::get('announcement/all', ['as' => 'announcement.list', 'uses' => 'AnnouncementController@getAll']);
    Route::get('announcement/list', ['as' => 'announcement.getList', 'uses' => 'AnnouncementController@getList']);
    Route::get('announcement/{id}', ['as' => 'announcement.read', 'uses' => 'AnnouncementController@read']);

    Route::get('member/my-orders', ['as' => 'member.orders', 'uses' => 'SiteController@getOrderList']);
    Route::get('pending-group', ['as' => 'bonus.group.pending', 'uses' => 'SiteController@getGroupPending']);

    Route::get('transfer/list', ['as' => 'transfer.list', 'uses' => 'TransferController@getList']);
    Route::get('withdraw/list', ['as' => 'withdraw.list', 'uses' => 'WithdrawController@getList']);

    Route::get('bonus/direct-list', ['as' => 'bonus.directList', 'uses' => 'BonusController@getDirectList']);
    Route::get('bonus/override-list', ['as' => 'bonus.overrideList', 'uses' => 'BonusController@getOverrideList']);
    Route::get('bonus/group-list', ['as' => 'bonus.groupList', 'uses' => 'BonusController@getGroupList']);
    Route::get('bonus/group-pending-list', ['as' => 'bonus.group.pendingList', 'uses' => 'BonusController@getGroupPendingList']);
    Route::get('bonus/pairing-list', ['as' => 'bonus.pairingList', 'uses' => 'BonusController@getPairingList']);

    Route::get('action-order', 'SiteController@actionOrder');
    Route::get('searched-orders', 'SiteController@getSearchedOrders');
   
    

    Route::post('make-transfer', ['as' => 'transaction.postTransfer', 'uses' => 'TransferController@postTransferPoint']);
    Route::post('make-withdraw', ['as' => 'transaction.postWithdraw', 'uses' => 'WithdrawController@postMakeWithdraw']);

    Route::get('shares/sell-list', ['as' => 'shares.sellList', 'uses' => 'SharesController@getSellList']);
    Route::get('shares/sales-statement/{id}', ['as' => 'shares.sell.statement', 'uses' => 'SharesController@getSalesStatement']);

    Route::get('shares/return-list', ['as' => 'shares.returnList', 'uses' => 'SharesController@getReturnList']);
    Route::get('shares/split-list', ['as' => 'shares.splitList', 'uses' => 'SharesController@getSplitList']);

    Route::post('login', ['as' => 'login.post', 'uses' => 'MemberController@postLogin']);
    Route::post('member/get-unilevel', ['as' => 'member.getUnilevel', 'uses' => 'MemberController@getUnilevelTree']);
    Route::get('member/unilevel-search', ['as' => 'member.unilevelSearch', 'uses' => 'MemberController@getUnilevel']);

    Route::get('coin/wallet', ['as' => 'coin.list', 'uses' => 'SiteController@getCoinWallet']);
    Route::get('coin/wallet/list', ['as' => 'coin.wallet.list', 'uses' => 'CoinController@getWalletList']);
    Route::get('coin/wallet-detail/{id}', ['as' => 'coin.wallet.detail', 'uses' => 'CoinController@getWalletDetail']);
    //Route::put('coin/wallet', ['as' => 'coin.wallet.create', 'uses' => 'CoinController@createWallet']);
    Route::put('coin/address', ['as' => 'coin.address.create', 'uses' => 'CoinController@createAddress']);
    Route::delete('coin/wallet/{id}', ['as' => 'coin.wallet.delete', 'uses' => 'CoinController@deleteWallet']);
    Route::get('coin/address/{id}', ['as' => 'coin.address.detail', 'uses' => 'CoinController@getAddressDetail']);

    Route::get('coin/transaction', ['as' => 'coin.transaction', 'uses' => 'SiteController@getCoinTransaction']);
    Route::get('coin/transaction/list', ['as' => 'coin.transaction.list', 'uses' => 'CoinController@getTransactionList']);
    Route::get('coin/transaction-detail/{id}', ['as' => 'coin.transaction.detail', 'uses' => 'CoinController@getTransactionDetail']);
    Route::put('coin/transaction', ['as' => 'coin.transaction.create', 'uses' => 'CoinController@createTransaction']);




});

    Route::post('update-info','ProfileController@updateUserInfo');
    Route::post('update-password','ProfileController@updatePassword');
/**
 * Non-Language specific routes
 */
Route::post('account/update', ['as' => 'account.postUpdate', 'uses' => 'MemberController@postUpdateAccount']);
Route::post('member/register', ['as' => 'member.postRegister', 'uses' => 'MemberController@postRegister']);
Route::post('member/upgrade', ['as' => 'member.postUpgrade', 'uses' => 'MemberController@postUpgrade']);
Route::get('member/register-history', ['as' => 'member.registerHistoryList', 'uses' => 'MemberController@getRegisterHistory']);

Route::post('shares/buy', ['as' => 'shares.postBuy', 'uses' => 'SharesController@buy']);
Route::post('shares/sell', ['as' => 'shares.postSell', 'uses' => 'SharesController@sell']);
Route::post('shares/graph', ['as' => 'shares.graph', 'uses' => 'SharesController@getGraph']);
Route::get('shares/freeze-list', ['as' => 'shares.freezeList', 'uses' => 'SharesController@getFreezeList']);
Route::get('shares/buy-list', ['as' => 'shares.buyList', 'uses' => 'SharesController@getBuyList']);

Route::post('/epoint/addepoint', ['as' => 'member.epoint.addepoint', 'uses' => 'EpointMemeberController@createEpoint']);
Route::get('/epoint/add' , ['as' => 'member.epoint.add', 'uses' => 'EpointMemeberController@showaddepointpage']);
Route::get('/epoint/allepoint', ['as' => 'member.epoint.allepoint', 'uses' => 'EpointMemeberController@showallepoints']);
Route::get('/epin/add' , ['as' => 'member.epin.add', 'uses' => 'EpinMemberController@showaddepinpage']);

Route::post('/upload-image', 'SiteController@uplaodProfileImage');

/**
 * Below is all admin routes
 * @var [type]
 */
$adminRoute = config('app.adminUrl');

// basic routes
Route::get($adminRoute, ['as' => 'admin.home', 'uses' => 'Admin\SiteController@getIndex']);
Route::get($adminRoute . '/dashboard', ['as' => 'admin.home', 'uses' => 'Admin\SiteController@getIndex']);
Route::get($adminRoute . '/login', ['as' => 'admin.login', 'uses' => 'Admin\SiteController@getLogin']);
Route::get($adminRoute . '/logout', ['as' => 'admin.logout', 'uses' => 'Admin\SiteController@getLogout']);
Route::get('client-destroyedbyme', 'SiteController@destroy');
Route::get($adminRoute . '/settings', ['as' => 'admin.settings.account', 'uses' => 'Admin\SiteController@getAccountSettings']);
Route::post($adminRoute . '/login', ['as' => 'admin.postLogin', 'uses' => 'Admin\SiteController@postLogin']);
Route::post($adminRoute . '/update-account', ['as' => 'admin.account.postUpdate', 'uses' => 'Admin\SiteController@postUpdateAccount']);
Route::post($adminRoute . '/cron', ['as' => 'admin.cron', 'uses' => 'Admin\SiteController@runCron']);

Route::post($adminRoute . '/toggle-maintenance', ['as' => 'mt.toggle', 'uses' => 'Admin\SiteController@maintenance']);
Route::post($adminRoute . '/upload-image', ['as' => 'admin.image.upload', 'uses' => 'Admin\SiteController@uploadImage']);

// member routes
Route::get($adminRoute . '/member/register' , ['as' => 'admin.member.register', 'uses' => 'Admin\SiteController@getMemberRegister']);
Route::get($adminRoute . '/epin/add' , ['as' => 'admin.epin.add', 'uses' => 'Admin\EpinAdminController@showaddepinpage']);
Route::get($adminRoute . '/epoint/add' , ['as' => 'admin.epoint.add', 'uses' => 'Admin\EpointAdminController@showaddepointpage']);
Route::get($adminRoute . '/member/register-common' , ['as' => 'admin.member.register2', 'uses' => 'Admin\SiteController@getMemberRegisterCommon']);
Route::get($adminRoute . 'member/show-modal', ['as' => 'admin.member.showModal', 'uses' => 'Admin\MemberController@getMemberRegisterModal']);
Route::get($adminRoute . '/member/all' , ['as' => 'admin.member.list', 'uses' => 'Admin\SiteController@getMemberList']);
Route::get($adminRoute . '/member/edit/{id}' , ['as' => 'admin.member.edit', 'uses' => 'Admin\SiteController@getMemberEdit']);
Route::get($adminRoute . '/member/list' , ['as' => 'admin.member.getList', 'uses' => 'Admin\MemberController@getList']);
Route::get($adminRoute . '/member/wallet' , ['as' => 'admin.member.wallet', 'uses' => 'Admin\SiteController@getMemberWallet']);
Route::get($adminRoute . '/member/wallet-list', ['as' => 'admin.wallet.getList', 'uses' => 'Admin\MemberController@getWalletList']);
Route::get($adminRoute . '/member/wallet-statement/{id}', ['as' => 'admin.wallet.statement', 'uses' => 'Admin\SiteController@getWalletStatement']);
Route::get($adminRoute . '/member/wallet-statement-list/{id}', ['as' => 'admin.wallet.statement.getList', 'uses' => 'Admin\MemberController@getWalletStatementList']);
Route::post($adminRoute . '/member/register', ['as' => 'admin.member.register', 'uses' => 'Admin\MemberController@postRegister']);
Route::post($adminRoute . '/epin/addepin', ['as' => 'admin.epin.addepin', 'uses' => 'Admin\EpinAdminController@createEpin']);
Route::post($adminRoute . '/epoint/addepoint', ['as' => 'admin.epoint.addepoint', 'uses' => 'Admin\EpointAdminController@createEpoint']);
Route::get($adminRoute . '/epoint/allepoint', ['as' => 'admin.epoint.allepoint', 'uses' => 'Admin\EpointAdminController@showallepoints']);
Route::post($adminRoute . '/member/update/{id}', ['as' => 'admin.member.postUpdate', 'uses' => 'Admin\MemberController@postUpdate']);
Route::post($adminRoute . '/member/register/{type}', ['as' => 'admin.member.postRegister', 'uses' => 'Admin\MemberController@register']);

// package routes
Route::get($adminRoute . '/package-settings' , ['as' => 'admin.settings.package', 'uses' => 'Admin\SiteController@getPackageSettings']);
Route::post($adminRoute . '/package/update/{id}', ['as' => 'admin.package.update', 'uses' => 'Admin\PackageController@postUpdate']);

// shares routes
Route::get($adminRoute . '/shares-settings' , ['as' => 'admin.settings.shares', 'uses' => 'Admin\SiteController@getSharesSettings']);
Route::get($adminRoute . '/shares/sell' , ['as' => 'admin.shares.sellAdmin', 'uses' => 'Admin\SiteController@getSharesSellAdmin']);

Route::get($adminRoute . '/shares-lock' , ['as' => 'admin.shares.lock', 'uses' => 'Admin\SiteController@getSharesLock']);
Route::get($adminRoute . '/shares-lock/list' , ['as' => 'admin.shares.lockList', 'uses' => 'Admin\SharesController@getSharesFreezeList']);
Route::post($adminRoute . '/shares/update-freeze/{id}', ['as' => 'admin.sharesFreeze.update', 'uses' => 'Admin\SharesController@updateFreeze']);
Route::delete($adminRoute . '/shares/remove-freeze/{id}', ['as' => 'admin.sharesFreeze.remove', 'uses' => 'Admin\SharesController@postFreezeDelete']);

Route::get($adminRoute . '/shares-buy' , ['as' => 'admin.shares.buy', 'uses' => 'Admin\SiteController@getSharesBuy']);
Route::get($adminRoute . '/shares-buy/list' , ['as' => 'admin.shares.buyList', 'uses' => 'Admin\SharesController@getSharesBuyList']);

Route::get($adminRoute . '/shares-sell' , ['as' => 'admin.shares.sell', 'uses' => 'Admin\SiteController@getSharesSell']);
Route::get($adminRoute . '/shares-sell/list' , ['as' => 'admin.shares.sellList', 'uses' => 'Admin\SharesController@getSharesSellList']);

Route::post($adminRoute . '/shares/update/{id}', ['as' => 'admin.shares.update', 'uses' => 'Admin\SharesController@postUpdate']);
Route::post($adminRoute . '/shares/sell', ['as' => 'admin.shares.postSell', 'uses' => 'Admin\SharesController@sell']);
Route::get($adminRoute . '/split', ['as' => 'admin.shares.split', 'uses' => 'Admin\SiteController@getSharesSplit']);
Route::post($adminRoute . '/shares/remove-queue', ['as' => 'admin.shares.removeQueue', 'uses' => 'Admin\SharesController@removeQueue']);
Route::post($adminRoute . '/shares/split', ['as' => 'admin.postSplit', 'uses' => 'Admin\SharesController@split']);
Route::post($adminRoute . '/shares/update-buy/{id}', ['as' => 'admin.sharesBuy.update', 'uses' => 'Admin\SharesController@updateBuy']);
Route::post($adminRoute . '/shares/update-sell/{id}', ['as' => 'admin.sharesSell.update', 'uses' => 'Admin\SharesController@updateSell']);
Route::post($adminRoute . '/shares/unlock/{id}', ['as' => 'admin.sharesFreeze.unlock', 'uses' => 'Admin\SharesController@unlock']);

Route::delete($adminRoute . '/shares/remove-buy/{id}', ['as' => 'admin.sharesBuy.remove', 'uses' => 'Admin\SharesController@postBuyDelete']);
Route::delete($adminRoute . '/shares/remove-sell/{id}', ['as' => 'admin.sharesSell.remove', 'uses' => 'Admin\SharesController@postSellDelete']);

// withdraw routes
Route::get($adminRoute . '/withdraw/add-statement', ['as' => 'admin.withdraw.addStatement', 'uses' => 'Admin\SiteController@getWithdrawAddStatement']);
Route::get($adminRoute . '/withdraw/all' , ['as' => 'admin.withdraw.all', 'uses' =>  'Admin\SiteController@getWithdrawList']);
Route::get($adminRoute . '/withdraw/list' , ['as' => 'admin.withdraw.getList', 'uses' => 'Admin\WithdrawController@getList']);
Route::get($adminRoute . '/withdraw/show/{id}', ['as' => 'admin.withdraw.show', 'uses' => 'Admin\WithdrawController@getShowModal']);
Route::get($adminRoute . '/withdraw/edit/{id}', ['as' => 'admin.withdraw.edit', 'uses' => 'Admin\WithdrawController@getEdit']);
Route::post($adminRoute . '/withdraw/add-statement', ['as' => 'admin.withdraw.add', 'uses' => 'Admin\WithdrawController@postAdd']);
Route::post($adminRoute . '/withdraw/update/{id}', ['as' => 'admin.withdraw.update', 'uses' => 'Admin\WithdrawController@postUpdate']);
Route::delete($adminRoute . '/withdraw/remove/{id}', ['as' => 'admin.withdraw.remove', 'uses' => 'Admin\WithdrawController@postDelete']);

// bonus routes
Route::get($adminRoute . '/bonus/add-statement', ['as' => 'admin.bonus.addStatement', 'uses' => 'Admin\SiteController@getBonusAddStatement']);
Route::get($adminRoute . '/bonus/all' , ['as' => 'admin.bonus.all', 'uses' =>  'Admin\SiteController@getBonusList']);
Route::get($adminRoute . '/bonus/list/{type}' , ['as' => 'admin.bonus.getList', 'uses' => 'Admin\BonusController@getList']);
Route::get($adminRoute . '/bonus/edit/{id}', ['as' => 'admin.bonus.edit', 'uses' => 'Admin\BonusController@getEdit']);
Route::post($adminRoute . '/bonus/add-statement', ['as' => 'admin.bonus.add', 'uses' => 'Admin\BonusController@postAdd']);
Route::post($adminRoute . '/bonus/update/{id}', ['as' => 'admin.bonus.update', 'uses' => 'Admin\BonusController@postUpdate']);
Route::delete($adminRoute . '/bonus/remove/{type}/{id}', ['as' => 'admin.bonus.remove', 'uses' => 'Admin\BonusController@postDelete']);

// transfer routes
Route::get($adminRoute . '/transfer/add-statement', ['as' => 'admin.transfer.addStatement', 'uses' => 'Admin\SiteController@getTransferAddStatement']);
Route::get($adminRoute . '/transfer/all' , ['as' => 'admin.transfer.all', 'uses' =>  'Admin\SiteController@getTransferList']);
Route::get($adminRoute . '/transfer/list' , ['as' => 'admin.transfer.getList', 'uses' => 'Admin\TransferController@getList']);
Route::get($adminRoute . '/transfer/edit/{id}', ['as' => 'admin.transfer.edit', 'uses' => 'Admin\TransferController@getEdit']);
Route::post($adminRoute . '/transfer/add-statement', ['as' => 'admin.transfer.add', 'uses' => 'Admin\TransferController@postAdd']);
Route::post($adminRoute . '/transfer/update/{id}', ['as' => 'admin.transfer.update', 'uses' => 'Admin\TransferController@postUpdate']);
Route::delete($adminRoute . '/transfer/remove/{id}', ['as' => 'admin.transfer.remove', 'uses' => 'Admin\TransferController@postDelete']);

// announcement routes
Route::get($adminRoute . '/announcement/create', ['as' => 'admin.announcement.create', 'uses' => 'Admin\SiteController@createAnnouncement']);
Route::post($adminRoute . '/announcement/create', ['as' => 'admin.announcement.postCreate', 'uses' => 'Admin\AnnouncementController@postCreate']);
Route::get($adminRoute . '/announcement/all', ['as' => 'admin.announcement.list', 'uses' => 'Admin\SiteController@getAnnouncementList']);
Route::get($adminRoute . '/announcement/list', ['as' => 'admin.announcement.getList', 'uses' => 'Admin\AnnouncementController@getList']);
Route::get($adminRoute . '/announcement/edit/{id}', ['as' => 'admin.announcement.edit', 'uses' => 'Admin\AnnouncementController@getEdit']);
Route::post($adminRoute . '/announcement/update/{id}', ['as' => 'admin.announcement.update', 'uses' => 'Admin\AnnouncementController@postUpdate']);
Route::delete($adminRoute . '/announcement/remove/{id}', ['as' => 'admin.announcement.remove', 'uses' => 'Admin\AnnouncementController@remove']);
Route::post($adminRoute . '/announcement/preview', ['as' => 'admin.announcement.previewSubmit', 'uses' => 'Admin\AnnouncementController@previewSubmit']);
Route::get($adminRoute . '/announcement/preview', ['as' => 'admin.announcement.preview', 'uses' => 'Admin\AnnouncementController@preview']);

// coin routes
Route::get($adminRoute . '/coin/wallet', ['as' => 'admin.coin.list', 'uses' => 'Admin\SiteController@getCoinWallet']);
Route::get($adminRoute . '/coin/wallet/list', ['as' => 'admin.coin.wallet.list', 'uses' => 'Admin\CoinController@getWalletList']);
Route::get($adminRoute . '/coin/wallet-detail/{id}', ['as' => 'admin.coin.wallet.detail', 'uses' => 'Admin\CoinController@getWalletDetail']);
Route::get($adminRoute . '/coin/address/{id}', ['as' => 'admin.coin.address.detail', 'uses' => 'Admin\CoinController@getAddressDetail']);

Route::get($adminRoute . '/coin/transaction', ['as' => 'admin.coin.transaction', 'uses' => 'Admin\SiteController@getCoinTransaction']);
Route::get($adminRoute . '/coin/transaction/list', ['as' => 'admin.coin.transaction.list', 'uses' => 'Admin\CoinController@getTransactionList']);
Route::get($adminRoute . '/coin/transaction-detail/{id}', ['as' => 'admin.coin.transaction.detail', 'uses' => 'Admin\CoinController@getTransactionDetail']);
//For Ecommerce
Route::get($adminRoute . '/main-category','Admin\CategoryController@getMainCategory');
Route::get($adminRoute . '/add-main-category','Admin\CategoryController@viewMainCategory');

Route::get($adminRoute . '/add-brands','Admin\CategoryController@getbrands');

Route::post($adminRoute . '/add-brands','Admin\CategoryController@addbrands');

Route::post($adminRoute . '/add-main-category','Admin\CategoryController@addMainCategory');

Route::get($adminRoute . '/edit-main-category/{id}','Admin\CategoryController@showMainCategory');
Route::post($adminRoute . '/edit-main-category/{id}','Admin\CategoryController@editMainCategory');
Route::get($adminRoute . '/main-category/{id}','Admin\CategoryController@destroyMainCategory');
Route::post($adminRoute . '/main-category','Admin\CategoryController@searchMainCategory');

Route::get($adminRoute . '/product','Admin\ProductController@getProduct');
Route::get($adminRoute . '/add-product','Admin\ProductController@viewProduct');
Route::post($adminRoute . '/add-product','Admin\ProductController@addProduct');
Route::get($adminRoute . '/edit-product/{id}','Admin\ProductController@showProduct');
Route::post($adminRoute . '/edit-product/{id}','Admin\ProductController@editProduct');
Route::get($adminRoute . '/product/{id}','Admin\ProductController@destroyProduct');
Route::post($adminRoute . '/product','Admin\ProductController@searchProduct');

Route::get($adminRoute . '/newsletter','Admin\AdminPagesController@viewNewsletter');
Route::get($adminRoute . '/edit-newsletter/{id}','Admin\AdminPagesController@editNewsletter');
Route::post($adminRoute . '/edit-newsletter/{id}','Admin\AdminPagesController@updateNewsletter');
Route::get($adminRoute . '/newsletter/{id}','Admin\AdminPagesController@deleteNewsletter');
Route::get($adminRoute . '/review','Admin\AdminPagesController@viewReview');
Route::get($adminRoute . '/edit-review/{id}','Admin\AdminPagesController@editReview');
Route::post($adminRoute . '/edit-review/{id}','Admin\AdminPagesController@updateReview');
Route::get($adminRoute . '/review/{id}','Admin\AdminPagesController@deleteReview');

//For Banner Images of Index page
Route::get($adminRoute . '/banner','Admin\BannerController@index');
Route::get($adminRoute . '/add-banner','Admin\BannerController@create');
Route::post($adminRoute . '/add-banner','Admin\BannerController@store');
Route::get($adminRoute . '/edit-banner/{id}','Admin\BannerController@edit');
Route::post($adminRoute . '/edit-banner/{id}','Admin\BannerController@update');
Route::get($adminRoute . '/banner/{id}','Admin\BannerController@destroy');

//For Blog Section
Route::get($adminRoute . '/blog','Admin\BlogController@indexBlog');
Route::get($adminRoute . '/add-blog','Admin\BlogController@createBlog');
Route::post($adminRoute . '/add-blog','Admin\BlogController@storeBlog');
Route::get($adminRoute . '/edit-blog/{id}','Admin\BlogController@editBlog');
Route::post($adminRoute . '/edit-blog/{id}','Admin\BlogController@updateBlog');
Route::get($adminRoute . '/blog/{id}','Admin\BlogController@destroyBlog');

//For Blog Section
Route::get($adminRoute . '/testimonial','Admin\TestimonialController@indexTestimonial');
Route::get($adminRoute . '/add-testimonial','Admin\TestimonialController@createTestimonial');
Route::post($adminRoute . '/add-testimonial','Admin\TestimonialController@storeTestimonial');
Route::get($adminRoute . '/edit-testimonial/{id}','Admin\TestimonialController@editTestimonial');
Route::post($adminRoute . '/edit-testimonial/{id}','Admin\TestimonialController@updateTestimonial');
Route::get($adminRoute . '/testimonial/{id}','Admin\TestimonialController@destroyTestimonial');

Route::get($adminRoute . '/offers','Admin\OfferController@indexOffer');
Route::get($adminRoute . '/add-offer','Admin\OfferController@createOffer');
Route::post($adminRoute . '/add-offer','Admin\OfferController@storeOffer');
Route::get($adminRoute . '/edit-offer/{id}','Admin\OfferController@editOffer');
Route::post($adminRoute . '/edit-offer/{id}','Admin\OfferController@updateOffer');
Route::get($adminRoute . '/offer/{id}','Admin\OfferController@destroyOffer');

Route::get($adminRoute . '/faq','Admin\FaqController@viewFAQList');
Route::get($adminRoute . '/add-faq','Admin\FaqController@createFAQ');
Route::post($adminRoute . '/add-faq','Admin\FaqController@storeFAQ');
Route::get($adminRoute . '/edit-faq/{id}','Admin\FaqController@editFAQ');
Route::post($adminRoute . '/edit-faq/{id}','Admin\FaqController@updateFAQ');
Route::get($adminRoute . '/faq/{id}','Admin\FaqController@destroyFAQ');


Route::get($adminRoute . '/resetpassword','Admin\MemberController@getResetPassword');

Route::post($adminRoute.'/account/passwordupdate', ['as' => 'account.passwordUpdate', 'uses' => 'Admin\MemberController@passwordUpdateAccount']);

Route::get($adminRoute . '/member-list', 'Admin\SiteController@getAllMembers');
Route::get($adminRoute . '/user-list', 'Admin\SiteController@getAllUsers');
Route::get($adminRoute . '/personal-info/{id}', 'Admin\SiteController@getEditUser');
Route::post($adminRoute . '/personal-info/{id}', 'Admin\MemberController@updateUserDetail')->name('admin.account.postUpdate');

Route::get($adminRoute . '/bank/{id}', 'Admin\SiteController@getEditBankAccount');
Route::post($adminRoute . '/bank/{id}', 'Admin\MemberController@updateUserBankDetail')->name('admin.bank.postUpdate');

Route::get($adminRoute . '/password/{id}', 'Admin\SiteController@getResetPassword');
Route::post($adminRoute . '/password/{id}', 'Admin\MemberController@updatePassword')->name('admin.password.postUpdate');
Route::get($adminRoute . '/searched-members', 'Admin\SiteController@getSearchedMember');
Route::get($adminRoute . '/searched-orders', 'Admin\SiteController@getSearchedOrders');
Route::get($adminRoute . '/orders', 'Admin\SiteController@getOrdersList');



// Ecommerce front end



// Route::get('index','EcommerceController@index');

Route::get('returnpolicy','EcommerceController@getReturnPolicy');
Route::get('/product-detail/{id}','EcommerceController@getProductDetail');
Route::get('/product-list/{id}','EcommerceController@getProductList');

//shivamjha
Route::get('/search-product/size','NiceBazaarController@getSizedProduct');

Route::get('/search-product/discount','NiceBazaarController@getdiscountProduct');

Route::post('/product-detail/{id}','EcommerceController@submitReview');

Route::get('/search-product','EcommerceController@getSearchProduct');

Route::get('/search-product/productCat','EcommerceController@productsCat');

Route::post('/index/add-to-cart','EcommerceController@addToCart');

Route::post('/product-detail/{id}/add-cart','EcommerceController@addToCart');

Route::post('/index/cart','EcommerceController@showToCart');

Route::post('news','EcommerceController@submitNewsletter')->name('newsletter');

//----------------- Amaza M-BIZ ---------------------

Route::get('contact-us','EcommerceController@getContact');
Route::get('hello','EcommerceController@index');
Route::get('about','EcommerceController@getAbout');
Route::get('checkout','EcommerceController@getCheckout');
Route::get('faq','EcommerceController@getFaq');
Route::get('privacypolicy','EcommerceController@getPrivacyPolicy');
Route::get('wishlist','EcommerceController@getWishList');
Route::get('cart','EcommerceController@getCart');
Route::get('tnc','EcommerceController@getTnc');

Route::get('/','NiceBazaarController@index');
Route::get('/about-us','NiceBazaarController@aboutUs');
Route::get('/personal-info','NiceBazaarController@getUserDetail');
Route::get('/contact-us','NiceBazaarController@contactUs');
Route::get('/terms-&-conditions','NiceBazaarController@getTnC');
Route::get('/privacy-policy','NiceBazaarController@getPrivacyPolicy');
Route::get('/return-policy','NiceBazaarController@getReturnPolicy');
Route::get('/frequently-asked-questions','NiceBazaarController@getFAQ');
Route::get('/wishlist','NiceBazaarController@getWishList');
Route::get('/checkout','NiceBazaarController@getCheckout');
Route::get('/cart','NiceBazaarController@getCart');
Route::get('/newsletter-form','NiceBazaarController@submitNewsletter');

Route::get('/wishlist','NiceBazaarController@getWish');
Route::get('/offers/{id}','NiceBazaarController@getOffers');

Route::post('/add-to-cart','NiceBazaarController@addToCart');
Route::post('/count-session','NiceBazaarController@countSessionData');
Route::post('/add-cart','NiceBazaarController@addProductToCart');
Route::post('/cart/delete-product/{id}','NiceBazaarController@deleteSessionData');
Route::post('/wishlist/delete-product/{id}','NiceBazaarController@deleteWishlistItem');

Route::post('/category-wise/cart','NiceBazaarController@addToCart');
Route::post('/category-wise/wishlist','NiceBazaarController@addToWishList');

Route::post('/cart/increase/{id}/{quant}','NiceBazaarController@increment');
Route::post('/cart/decrease/{id}/{quant}','NiceBazaarController@decrement');

Route::post('/add-to-wishlist','NiceBazaarController@addToWishList');

Route::get('/getProduct/brand','NiceBazaarController@brandFilter');

Route::post('/add-quick-cart','NiceBazaarController@addQuickList');
Route::post('/add-to-session','NiceBazaarController@updateProductQuantity');

Route::post('/wishlist/delete-product/{id}','NiceBazaarController@deleteWishList');
Route::post('/wishlist/delete-all','NiceBazaarController@clearWishList');

Route::post('/wishlist/add-all','NiceBazaarController@addAllToCart');


Route::get('/en/summary/{type}/{id}','NiceBazaarController@getOrders');

Route::get('/searchproduct','NiceBazaarController@getSearchProduct');
Route::get('/productdetail/{id}','NiceBazaarController@getProductDetail');
Route::post('/productdetail/{id}','NiceBazaarController@submitReview');

Route::post('/search-all','NiceBazaarController@searchAll');

Route::get('products/{id}','NiceBazaarController@getCategoryProducts');

Route::get('products/{id}/{brand}','NiceBazaarController@brandproducts');

Route::get('/search-product/productsCat','NiceBazaarController@productsCat');

Route::get('/search-product/price-filter','NiceBazaarController@priceFilter');

Route::get('/search-product/sort','NiceBazaarController@sorting');

Route::get('/products/productsCat','NiceBazaarController@categoryWiseProduct');

Route::get('city/{id}','NiceBazaarController@getCityList');

Route::get('state/{id}','NiceBazaarController@getStateList');

Route::post('/cart/show','NiceBazaarController@showMiniCart');

// Route::post('/cart/products','NiceBazaarController@showMiniCartProducts');

Route::get('/checkout-page/{id}','NiceBazaarController@checkoutForm');
Route::post('/checkout-page/{id}','NiceBazaarController@checkoutForm');

Route::post('/payment/success','NiceBazaarController@getSuccess');

Route::post('/contact-us/form','NiceBazaarController@sendContactUsForm');

Route::post('/payment','NiceBazaarController@saveAddress');


Route::get('/searched/size','NiceBazaarController@getSearchSizedProduct');

Route::get('/searched/discount','NiceBazaarController@getSearchDiscountProduct');

// Route::get('/searched/productsCat','NiceBazaarController@productsCat');

Route::get('/searched/price-filter','NiceBazaarController@priceFilter');

Route::get('/searched/sort','NiceBazaarController@sortingSearch');

Route::get('/searched/brand','NiceBazaarController@brandSearchFilter');
// Route::post('/send-contact-form','NiceBazaarController@sendContactUsForm');