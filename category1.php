<?php
   include("library/raso_function.php");

   $_SESSION['user_id'];
   
   $category_id=$_REQUEST['id'];
   
   $sel_product1=exeQuery("select * from ".TABLE_CATEGORY." where id='".$category_id."' ");
   $sel_cat_name=fetchAssoc($sel_product1);
   
   $count=exeQuery("select count(id) from ".TABLE_PRODUCT." where category_id='".$category_id."' ");
   $res_count1=fetchAssoc($count);
   
   
   ?>
<!doctype html>
<html lang="en">
   <meta http-equiv="content-type" content="text/html;charset=utf-8" />
   <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title><?php echo SITENAME;?></title>
      <meta name="description" content="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard.">
      <meta name="keywords" content="">
      <link rel="canonical" href="11-fruit-veges.php">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/vnd.microsoft.icon" href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/img/favicon.ico?1557119198">
      <link rel="shortcut icon" type="image/x-icon" href="https://innovatorythemes.com/prestashop/INNO02/INNO1005_Gomart/IT05/img/favicon.ico?1557119198">
      <link rel="stylesheet" href="themes/IT1005/assets/css/theme.css" type="text/css" media="all">
      <link rel="stylesheet" href="modules/itproductnextprev/views/css/itproductnextprev.css" type="text/css" media="all">
      <link rel="stylesheet" href="modules/itmegamenu/views/css/front.css" type="text/css" media="all">
      <link rel="stylesheet" href="modules/productcomments/productcomments.css" type="text/css" media="all">
      <link rel="stylesheet" href="modules/blockwishlist/blockwishlist.css" type="text/css" media="all">
      <link rel="stylesheet" href="modules/itvegamenu/views/css/itvegamenu.css" type="text/css" media="all">
      <link rel="stylesheet" href="modules/itcustomhtml/views/css/itcustomhtml.css" type="text/css" media="all">
      <link rel="stylesheet" href="modules/itblog/views/css/itblog.css" type="text/css" media="all">
      <link rel="stylesheet" href="modules/itcategoryfeature/views/css/itcategoryfeature.css" type="text/css" media="all">
      <link rel="stylesheet" href="js/jquery/ui/themes/base/minified/jquery-ui.min.css" type="text/css" media="all">
      <link rel="stylesheet" href="js/jquery/ui/themes/base/minified/jquery.ui.theme.min.css" type="text/css" media="all">
      <link rel="stylesheet" href="js/jquery/plugins/fancybox/jquery.fancybox.css" type="text/css" media="all">
      <link rel="stylesheet" href="modules/itimageslider/css/nivo-slider.css" type="text/css" media="all">
      <link rel="stylesheet" href="themes/IT1005/assets/css/custom.css" type="text/css" media="all">
      <link rel="stylesheet" href="themes/IT1005/assets/css/inno-theme-option.css" type="text/css" media="all">
      <script type="text/javascript" src="themes/IT1005/assets/js/jquery-1.7.1.min.js"></script>
   </head>
   <body itemscope itemtype="http://schema.org/WebPage" id="category" class="subpage lang-en country-us currency-eur layout-left-column page-category tax-display-disabled category-id-11 category-fruit-veges category-id-parent-2 category-depth-level-2">
      <!--<div class="se-pre-con"></div>--><!-- Preloader -->
      <div class="preloader"><img src="themes/IT1005/assets/img/preloader.gif" class="preloader-img" alt="" width="auto" height="auto" /></div>
      <main>
         <?php include("include/header1.php");?>
         <div class="innovatoryBreadcrumb">
            <div class="container">
               <nav data-depth="2" class="breadcrumb hidden-sm-down">
                  <ol itemscope itemtype="http://schema.org/BreadcrumbList">
                     <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <a itemprop="item" href="index.php">
                        <span itemprop="name">Home</span>
                        </a>
                        <meta itemprop="position" content="1">
                     </li>
                     <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <a itemprop="item" href="category.php?><?php echo $sel_cat_name['id'];?>">
                        <span itemprop="name"><?php echo $sel_cat_name['category_name'];?></span>
                        </a>
                        <meta itemprop="position" content="2">
                     </li>
                  </ol>
               </nav>
            </div>
         </div>
         <section id="wrapper">
            <aside id="notifications">
               <div class="container">
               </div>
            </aside>
            <div class="container">
               <div class="row">
                  <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
                     <div id="search_filters_wrapper" class="hidden-sm-down">
                        <div id="search_filter_controls" class="hidden-md-up">
                           <span id="_mobile_search_filters_clear_all"></span>
                           <button class="btn btn-secondary ok">
                           <i class="material-icons">&#xE876;</i>
                           OK
                           </button>
                        </div>
                        <div id="search_filters">
                           <section class="facet clearfix">
                              <h1 class="h6 facet-title hidden-sm-down">Categories</h1>
                              <div class="title hidden-md-up" data-target="#facet_55687" data-toggle="collapse">
                                 <h1 class="h6 facet-title">Categories</h1>
                                 <span class="pull-xs-right">
                                 <span class="navbar-toggler collapse-icons">
                                 <i class="material-icons add">&#xE313;</i>
                                 <i class="material-icons remove">&#xE316;</i>
                                 </span>
                                 </span>
                              </div>
                              <ul id="facet_55687" class="collapse">
                                 <?php
                                    $sel_cat=exeQuery("select * from ".TABLE_CATEGORY." where status=1 and category_id=0 ");
                                    while($res_cat=fetchAssoc($sel_cat)){
                                    
                                     $count=exeQuery("select count(id) from ".TABLE_PRODUCT." where category_id='".$res_cat['id']."' ");
                                    
                                     $res_count=fetchAssoc($count);
                                     ?>
                                 <li>
                                    <label class="facet-label">
                                    <a
                                       href="category.php?id=<?php echo $res_cat['id'];?>"
                                       class="_gray-darker search-link js-search-link"
                                       rel="nofollow"
                                       onclick="filter('<?php echo $res_cat['id']; ?>');">
                                    <?php echo $res_cat['category_name']; ?>
                                    <span class="magnitude">(<?php echo $res_count['count(id)'];?>)</span>
                                    </a>
                                    </label>
                                 </li>
                                 <?php
                                    }
                                    ?>
                              </ul>
                           </section>
                        </div>
                     </div>
                     <div class="it_new_product product-column-style" data-items="1" data-speed="1000" data-autoplay="0"  data-time="3000"  data-arrow="0" data-pagination="0" data-move="1" data-pausehover="0"  data-lg="1" data-md="1" data-sm="1" data-xs="1" data-xxs="1">
                        <div class="itProductList itcolumn">
                           <div class="title_block">
                              <h3><a href="#">New products</a></h3>
                           </div>
                           <div class="block-content">
                              <div class="itProductFilter row">
                                 <div class="special-item owl-carousel">
                                    <div class="item-product">
                                       <?php
                                          $sel_product_new=exeQuery("select * from ".TABLE_PRODUCT." where product_type='new' limit 4 ");

                                          echo "select * from ".TABLE_PRODUCT." where product_type='new' limit 4 ";
                                          
                                          while($res_product_new=fetchAssoc($sel_product_new)){
                                          
                                            $sel_product_img=exeQuery("select * from ".TABLE_PRO_IMG." where p_id='".$res_product_new['id']."'  ");
                                            $res_product_img=fetchAssoc($sel_product_img);
                                          
                                            ?>
                                       <div class="item">
                                          <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="46" data-id-product-attribute="289">
                                             <div class="innovatory-thumbnail-container">
                                                <div class="row no-margin">
                                                   <div class="pull-left product_img">
                                                      <a href="single.php?id=<?php echo $res_product_new['id']; ?>" class="thumbnail product-thumbnail">
                                                      <img src = "<?php echo "admin/".$res_product_img['image'];?>" alt = "" data-full-size-image-url = "<?php echo "admin/".$res_product_img['image'];?>" width="auto" height="auto">
                                                      </a>
                                                   </div>
                                                   <div class="innovatoryMedia-body">
                                                      <div class="innovatory-product-description">
                                                         <div class="comments_note">
                                                            <div class="star_content">
                                                               <div class="star star_on"><i class="fa fa-star"></i></div>
                                                               <div class="star star_on"><i class="fa fa-star"></i></div>
                                                               <div class="star"><i class="fa fa-star"></i></div>
                                                               <div class="star"><i class="fa fa-star"></i></div>
                                                               <div class="star"><i class="fa fa-star"></i></div>
                                                            </div>
                                                            <span class="laberCountReview pull-left">Review</span>
                                                         </div>
                                                         <h2 class="h2 productName" itemprop="name"><a href="home/46-289-router-and-bits.html#/5-color-grey/32-kg-20_kg"><?php echo $res_product_new['product_name']; ?></a></h2>
                                                         <span style="text-align: right;">PV    :    <?php echo $res_product_new['pv'];?></span>
                                                         <div class="innovatory-product-price-and-shipping">
                                                            <span itemprop="price" class="price"><?php echo $res_product_new['price']; ?></span>
                                                            <span class="reduction_percent_display">              
                                                            -12%                
                                                            </span>
                                                            <span class="regular-price"><?php echo $res_product_new['dp']; ?></span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <?php
                                          }
                                          ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     
                     <div id="itleftbanners" class="block hidden-md-down">
                        <ul class="banner-col">,
                           <li class="itleftbanners-container">
                              <a href="#" title="Left Banner 1">
                              <img src="modules/itleftbanners/images/left-banner-1.jpg" alt="Left Banner 1" width="auto" height="auto"/>
                              </a>
                           </li>
                        </ul>
                     </div>
                     <div class="it-special-products product-column-style" data-items="1" data-speed="1000" data-autoplay="0" data-time="3000"  data-arrow="0" data-pagination="0" data-move="1" data-pausehover="0"  data-md="1" data-sm="1" data-xs="1" data-xxs="1">
                        <div class="itProductList itcolumn">
                           <div class="title_block">
                              <h3>Special Product</span></h3>
                           </div>
                           <div class="block-content">
                              <div class="itProductFilter row">
                                 <div class="special-item owl-carousel">
                                    <div class="item-product">
                                       <?php
                                          $sel_product_special=exeQuery("select * from ".TABLE_PRODUCT." where product_type='featured' limit 4 ");
                                          
                                          while($res_product_special=fetchAssoc($sel_product_special)){
                                          
                                            $sel_product_img=exeQuery("select * from ".TABLE_PRO_IMG." where p_id='".$res_product_special['id']."'  ");
                                            $res_product_img=fetchAssoc($sel_product_img);
                                          
                                            ?>
                                       <div class="item">
                                          <div class="innovatoryProductsList product-miniature js-product-miniature" data-id-product="46" data-id-product-attribute="289">
                                             <div class="innovatory-thumbnail-container">
                                                <div class="row no-margin">
                                                   <div class="pull-left product_img">
                                                      <a href="single.php?id=<?php echo $res_product_special['id']; ?>" class="thumbnail product-thumbnail">
                                                      <img src = "<?php echo "admin/".$res_product_img['image'];?>" alt = "" data-full-size-image-url = "<?php echo "admin/".$res_product_img['image'];?>" width="auto" height="auto">
                                                      </a>
                                                   </div>
                                                   <div class="innovatoryMedia-body">
                                                      <div class="innovatory-product-description">
                                                         <div class="comments_note">
                                                            <div class="star_content">
                                                               <div class="star star_on"><i class="fa fa-star"></i></div>
                                                               <div class="star star_on"><i class="fa fa-star"></i></div>
                                                               <div class="star"><i class="fa fa-star"></i></div>
                                                               <div class="star"><i class="fa fa-star"></i></div>
                                                               <div class="star"><i class="fa fa-star"></i></div>
                                                            </div>
                                                            <span class="laberCountReview pull-left">Review</span>
                                                         </div>
                                                         <h2 class="h2 productName" itemprop="name"><a href="home/46-289-router-and-bits.html#/5-color-grey/32-kg-20_kg"><?php echo $res_product_special['product_name']; ?></a></h2>
                                                         <span style="text-align: right;">PV    :    <?php echo $res_product_special['pv'];?></span>
                                                         <div class="innovatory-product-price-and-shipping">
                                                            <span itemprop="price" class="price"><?php echo $res_product_special['price']; ?></span>
                                                            <span class="reduction_percent_display">              
                                                            <?php $total=$res_product_special['dp']-$res_product_special['price'];
                                                               $save=$total/$res_product_special['dp']*100;
                                                               echo round($save)."%";
                                                               ?>            
                                                            </span>
                                                            <span class="regular-price"><?php echo $res_product_special['dp']; ?></span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <?php
                                          }
                                          ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
                     <section id="main">
                        <div class="block-category card card-block">
                           <h1 class="h1"><?php echo $sel_cat_name['category_name'];?></h1>
                           <div class="category-cover">
                              <img src="c/11-category_default/fruit-veges.jpg" alt="">
                           </div>
                        </div>
                        <section id="products" class="active_grid">
                           <div id="">
                              <div id="js-product-list-top" class="products-selection row">
                                 <div class="col-lg-5 total-products">
                                    <ul class="innovatoryGridList">
                                       <li id="grid" class="pull-left"><a rel="nofollow" href="javascript:void(0)" title="Grid"></a></li>
                                    </ul>
                                    <p>There are <?php echo $res_count1['count(id)'];?> products.</p>
                                 </div>
                                 <div class="col-lg-7">
                                    <div class="products-sort-order dropdown pull-right">
                                       <form action="" method="POST"  onchange="submitform('<?php echo $category_id;?>')" >
                                          <select class="form-control" id="checkid">
                                             <option value="" >Relevance</option>
                                             <option value="1" >low to high</option>
                                             <option value="2" > high to low </option>
                                          </select>
                                       </form>
                                       <!-- </div> -->
                                    </div>
                                    <span class=" hidden-sm-down sort-by pull-right">Sort by:</span>      
                                    <div class="col-sm-3 col-xs-4 hidden-md-up filter-button">
                                       <button id="search_filter_toggler" class="btn btn-secondary">
                                       Filter
                                       </button>
                                    </div>
                                    <div class="clearfix"></div>
                                 </div>
                                 <div class="clearfix"></div>
                                 <!-- <div class="col-sm-12 hidden-md-up text-xs-center showing">
                                    Showing 1-12 of 35 item(s)
                                 </div> -->
                              </div>
                           </div>
                           <div id="" class="hidden-sm-down">
                              <section id="js-active-search-filters" class="hide">
                                 <h6 class="h6 active-filter-title">Active filters</h6>
                              </section>
                           </div>
                           <div id="">
                              <div id="js-product-list">
                                 <div  class="innovatoryProductGrid innovatoryProducts">
                                    <div class="row" id="rollid">
                                       <?php 
                                          $sel_product=exeQuery("select * from ".TABLE_PRODUCT." where category_id='".$category_id."' ");
                                          while($res=fetchAssoc($sel_product)){
                                          
                                             $sel_product_img=exeQuery("select * from ".TABLE_PRO_IMG." where p_id='".$res['id']."' ");
                                             $res_product_img=fetchAssoc($sel_product_img);
                                          
                                          
                                             ?>
                                       <div class="item-inner col-lg-4 col-xl-4 col-md-4 col-sm-6 col-xs-6  last-in-line   last-item-of-tablet-line    last-item-of-mobile-line " >
                                          <article class="product-miniature js-product-miniature" data-id-product="28" data-id-product-attribute="418" itemscope itemtype="http://schema.org/Product">
                                             <div class="innovatoryProduct-container item">
                                                <div class="innovatoryProduct-image">
                                                   <a href="single.php?id=<?php echo $res['id'];?>" class="thumbnail product-thumbnail">
                                                   <span class="cover_image">
                                                   <img src = "<?php echo "admin/".$res_product_img['image'];?>" data-full-size-image-url = "<?php echo "admin/".$res_product_img['image'];?>" alt="" width="180px" height="150px" />
                                                   </span>
                                                   <span class="hover_image">
                                                   <img src = "<?php echo "admin/".$res_product_img['image'];?>" data-full-size-image-url = "<?php echo "admin/".$res_product_img['image'];?>" alt="" width="auto" height="auto" /> 
                                                   </span>
                                                   </a>
                                                   <span class="innovatoryNew-label">New</span>
                                                   <div class="innovatoryActions hidden-lg-up">
                                                      <div class="innovatoryActions-i">
                                                         <div class="innovatoryCart innovatoryItem">
                                                           <?php
                                                            if($_SESSION['user_id']!=""){
                                                            ?>
                                                            <form action="" method="post">
                                                               <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                               <input type="hidden" value="28" name="id_product">
                                                               <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart" onclick="submit('<?php echo $res['id']; ?>','<?php echo $_SESSION['user_id'];?>')">
                                                               <i class="ti-shopping-cart"></i>
                                                               </a>
                                                            </form>
                                                            <?php
                                                         }else{
                                                            ?>
                                                             <form action="" method="post">
                                                               <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                               <input type="hidden" value="28" name="id_product">
                                                               <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart" onclick="login()">
                                                               <i class="ti-shopping-cart"></i>
                                                               </a>
                                                            </form>
                                                         <?php  
                                                         }
                                                         ?>
                                                         </div>
                                                         <div class="innovatoryQuick innovatoryItem">
                                                            <a  onclick='$("#myemd").attr("src","<?php echo "admin/".$res_product_img['image'];?>");' class="quick-view" data-link-action="quickview" title="Quickview" data-toggle="modal" data-target="#myModal">
                                                            <i class="ti-eye"></i>
                                                            </a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="innovatory-product-description">
                                                   <h2 class="h2 productName" itemprop="name"><a href="home/single.php"><?php echo $res['product_name'];?></a></h2>
                                                   <span style="text-align: right;">PV    :    <?php echo $res['pv'];?></span>
                                                   <div class="product-detail">
                                                      <div class="innovatory-product-price-and-shipping">
                                                         <span itemprop="price" class="price"><?php echo $res['price'];?></span>
                                                      </div>
                                                      <div class="comments_note">
                                                         <div class="star_content">
                                                            <div class="star star_on"><i class="fa fa-star"></i></div>
                                                            <div class="star star_on"><i class="fa fa-star"></i></div>
                                                            <div class="star star_on"><i class="fa fa-star"></i></div>
                                                            <div class="star star_on"><i class="fa fa-star"></i></div>
                                                            <div class="star star_on"><i class="fa fa-star"></i></div>
                                                         </div>
                                                         <span class="laberCountReview pull-left">Review</span>
                                                      </div>
                                                   </div>
                                                   <div class="innovatoryActions hidden-md-down">
                                                      <div class="innovatoryActions-i">
                                                         <div class="innovatoryCart innovatoryItem">
                                                            <?php
                                                            if($_SESSION['user_id']!=""){
                                                            ?>
                                                            <form action="" method="post">
                                                               <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                               <input type="hidden" value="28" name="id_product">
                                                               <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart" onclick="submit('<?php echo $res['id']; ?>','<?php echo $_SESSION['user_id'];?>')">
                                                               <i class="ti-shopping-cart"></i>
                                                               </a>
                                                            </form>
                                                            <?php
                                                         }else{
                                                            ?>
                                                             <form action="" method="post">
                                                               <input type="hidden" name="token" value="f5413003fb36bd93bab467ca1b34a0ea">
                                                               <input type="hidden" value="28" name="id_product">
                                                               <a data-button-action="add-to-cart" class="cart-btn" title="Add To Cart" onclick="login()">
                                                               <i class="ti-shopping-cart"></i>
                                                               </a>
                                                            </form>
                                                         <?php  
                                                         }
                                                         ?>
                                                         </div>
                                                         <div class="innovatoryQuick innovatoryItem">
                                                            <a  onclick='$("#myemd").attr("src","<?php echo "admin/".$res_product_img['image'];?>");' class="quick-view" data-link-action="quickview" title="Quickview" data-toggle="modal" data-target="#myModal">
                                                            <i class="ti-eye"></i>
                                                            </a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </article>
                                       </div>
                                       <?php
                                          }
                                          ?>      
                                    </div>
                                 </div>
                                 <div class="row no-margin">
                                 </div>
                              </div>
                              <div class="hidden-md-up text-xs-right up">
                                 <a href="#header" class="btn btn-secondary">
                                 Back to top
                                 <i class="material-icons">&#xE316;</i>
                                 </a>
                              </div>
                           </div>
                  </div>
                  <div id="js-product-list-bottom">
                  <div id="js-product-list-bottom"></div>
                  </div>
                  </section>
                  </section>
               </div>
            </div>
            </div>
            <div class="displayPosition displayPosition6">
               <!-- Static Block module -->
               <!-- /Static block module -->
            </div>
         </section>
         <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
               <!-- Modal content-->
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title"></h4>
                  </div>
                  <div class="modal-body">
                     <embed id="myemd" src=""
                        frameborder="0" width="100%" height="450px">
                     <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php include("include/footer.php");?>
      </main>
      <!--scroll up--><a href="javascript:void(0)" class="mypresta_scrollup hidden-phone open"><i class="ti-arrow-up"></i></a>
      <script type="text/javascript" src="themes/core.js" ></script>
      <script type="text/javascript" src="themes/IT1005/assets/js/theme.js" ></script>
      <script type="text/javascript" src="modules/itmegamenu/views/js/front.js" ></script>
      <script type="text/javascript" src="modules/productcomments/js/jquery.rating.pack.js" ></script>
      <script type="text/javascript" src="modules/productcomments/js/jquery.textareaCounter.plugin.js" ></script>
      <script type="text/javascript" src="modules/productcomments/js/productcomments.js" ></script>
      <script type="text/javascript" src="modules/itleftnewproducts/js/itleftnewproducts.js" ></script>
      <script type="text/javascript" src="modules/itleftspecialproducts/js/itleftspecialproducts.js" ></script>
      <script type="text/javascript" src="modules/blockwishlist/js/ajax-wishlist.js" ></script>
      <script type="text/javascript" src="modules/itvegamenu/views/js/front.js" ></script>
      <script type="text/javascript" src="js/jquery/ui/jquery-ui.min.js" ></script>
      <script type="text/javascript" src="js/jquery/plugins/fancybox/jquery.fancybox.js" ></script>
      <script type="text/javascript" src="themes/IT1005/modules/ps_searchbar/ps_searchbar.js" ></script>
      <script type="text/javascript" src="modules/ps_shoppingcart/ps_shoppingcart.js" ></script>
      <script type="text/javascript" src="modules/itblog/views/js/itblog.js" ></script>
      <script type="text/javascript" src="modules/itimageslider/js/jquery.nivo.slider.pack.js" ></script>
      <script type="text/javascript" src="modules/itimageslider/js/itimageslider.js" ></script>
      <script type="text/javascript" src="themes/IT1005/assets/js/custom.js" ></script>
      <script type="text/javascript">
         function filter(id){
            $('#id').val(id);
            window.location.href = "category.php?id="+id;
         }
      </script>
      <script type="text/javascript">
         function submit(id,user_id){
           $.ajax({
              url:"add_product.php",  
              type:"POST",  
              data:{id:id,user_id:user_id,},  
              success:function(data){ 
                $(".iamuniq").empty();
              $(".iamuniq").append(data);
              window.location.href = "checkout.php";  
            }  
         
         });
         
         }
         
         function submitform(category_id){
           var id=$('#checkid').val();
            $.ajax({
              url:"price_filter.php",  
              type:"POST",  
              data:{id:id,category_id:category_id,},  
              success:function(data){ 
               $("#rollid").empty();
               $("#rollid").fadeIn(500).html(data);  
            }  
         
         });
         
         }
         
         function login(){
                alert("please login first");
                 window.location.href = "login.php";
              }
      </script>
            <script type="text/javascript">
   function deletefile(product_id,user_id)
   {  
       $("#cart"+product_id).hide();
      $.ajax({
            url:"delete_cart.php",  
                      type:"POST",  
                      data:{product_id:product_id,user_id:user_id,},  
                      success:function(data){
                         $(".iamuniq").empty();
                          $(".iamuniq").append(data); 
                      }  
         
          });
   }
</script>
   </body>
</html>