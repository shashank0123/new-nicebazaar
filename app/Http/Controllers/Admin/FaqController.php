<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Faq;

class FaqController extends Controller
{
    public function viewFAQList(){
    	$faq = Faq::orderBy('created_at','DESC')->get();
    	return view('back.ecommerce.faq',compact('faq'));
    }

    public function createFAQ()
    {
        return view('back.ecommerce.add-faq');
    }

    public function storeFAQ(Request $request){
    	$faq = new Faq;
    	$faq->questions = $request->question;
    	$faq->answers = $request->answer;
    	$faq->status = $request->status;
    	$faq->save();
    	return redirect()->back()->with('message','Data Inserted Successfully');
    }

    public function editFAQ($id)
    {
        $faq = Faq::find($id);
        return view('back.ecommerce.edit-faq',compact('faq'));
    }

    public function updateFAQ(Request $request, $id)
    {
        $faq = Faq::find($id);        
        $faq->questions = $request->question;
        $faq->answers = $request->answer;
        $faq->status = $request->status;
        $faq->update();
        return redirect()->back()->with('message','Data Successfully Updated');
    }

    public function destroyFAQ($id)
    {
        $faq = Faq::find($id);
        $faq->delete();
        return redirect()->back()->with('message','Data Successfully Deleted');
    }

}
