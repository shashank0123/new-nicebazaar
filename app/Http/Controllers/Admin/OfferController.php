<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Offer;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexOffer()
    {
        $offers = Offer::all();
        return view('back.ecommerce.offer',compact('offers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createOffer()
    {
        return view('back.ecommerce.add-offer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeOffer(Request $request)
    {
        $offer = new Offer;

        // for image_url
        if (Input::hasfile('image_url')) {
           $file=Input::file('image_url');
           $file->move(public_path(). '/offers/', time().$file->getClientOriginalName());
           $offer->image_url=time().$file->getClientOriginalName();           
        }
        $offer->offer_title = $request->offer_title;
        $offer->offer_sub_title = $request->offer_sub_title;
        $offer->offer_short_description = $request->offer_short_description;
        $offer->offer_long_description = $request->offer_long_description;
        $offer->status = $request->status;
        $offer->save();

        return redirect()->back()->with('message','Data Successfully Inserted');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editOffer($id)
    {
        $offer = Offer::find($id);
        return view('back.ecommerce.edit-offer',compact('offer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateOffer(Request $request, $id)
    {
        $offer = Offer::find($id);
         // for image_url
        if (Input::hasfile('image_url')) {
           $file=Input::file('image_url');
           $file->move(public_path(). '/offers/', time().$file->getClientOriginalName());
           $offer->image_url=time().$file->getClientOriginalName();           
        }
        $offer->offer_title = $request->offer_title;
        $offer->offer_sub_title = $request->offer_sub_title;
        $offer->offer_short_description = $request->offer_short_description;
        $offer->offer_long_description = $request->offer_long_description;
        $offer->status = $request->status;
        $offer->update();

        return redirect()->back()->with('message','Data Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyOffer($id)
    {
        $offer = Offer::find($id);
        $offer->delete();

        return redirect()->back()->with('message','Offer Successfully Deleted');
    }
}
