<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Blog;


class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexBlog()
    {
        $blog = Blog::all();
        return view('back.ecommerce.blog',compact('blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createBlog()
    {
        return view('back.ecommerce.add-blog');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeBlog(Request $request)
    {
        $blog = new blog;

        // for image_url
        if (Input::hasfile('image_url')) {
           $file=Input::file('image_url');
           $file->move(public_path(). '/assetsss/images/AdminProduct/banner', time().$file->getClientOriginalName());
           $blog->image_url=time().$file->getClientOriginalName();           
        }
        $blog->description = $request->description;
        $blog->heading = $request->heading;
        $blog->status = $request->status;
        $blog->save();

        return redirect()->back()->with('message','Data Successfully Inserted');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editBlog($id)
    {
        $blog = Blog::find($id);
        return view('back.ecommerce.edit-blog',compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateBlog(Request $request, $id)
    {
        $blog = Blog::find($id);
         // for image_url
        if (Input::hasfile('image_url')) {
           $file=Input::file('image_url');
           $file->move(public_path(). '/assetsss/images/AdminProduct/banner', time().$file->getClientOriginalName());
           $blog->image_url=time().$file->getClientOriginalName();           
        }
        $blog->status = $request->status;
        $blog->heading = $request->heading;
        $blog->description = $request->description;
        $blog->update();

        return redirect()->back()->with('message','Data Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyBlog($id)
    {
        $blog = Blog::find($id);
        $blog->delete();

        return redirect()->back()->with('message','Blog Successfully Deleted');
    }
}
