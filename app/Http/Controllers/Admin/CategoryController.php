<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\MainCategory;
use App\Brand;

class CategoryController extends Controller
{
    
    // Display a listing of the Main Category on the Navbar.
    
    public function getMainCategory()
    {
        $maincategory = MainCategory::all();
        return view('back.ecommerce.main-category',compact('maincategory'));
    }

    public function getbrands()
    {
        $maincategory = MainCategory::all();
        return view('back.ecommerce.add-brands',compact('maincategory'));
    }
   
    // Show the form for creating a new Category Record
    public function viewMainCategory()
    {
        $maincategory = MainCategory::all();
        // echo $maincategory;
        // exit();
        return view('back.ecommerce.add-main-category',compact('maincategory'));   
    }
   
    // Store a newly created resource in storage.
    public function addMainCategory(Request $request)
    {
        // $validatedData = $request->validate([
        //     'Mcategory_name'     => 'required'      
        // ]);
        $maincategory = new MainCategory;

        if (Input::hasfile('image1')) {
           $file=Input::file('image1');
           $file->move(public_path(). '/assetsss/images/AdminProduct/', time().$file->getClientOriginalName());
           $maincategory->image1=time().$file->getClientOriginalName();           
        }
       
        $maincategory->Mcategory_name        = $request->Mcategory_name;
        $maincategory->Mcategory_description       = $request->Mcategory_description;
        $maincategory->featured       = $request->featured;
        $maincategory->slug       = strtolower(str_replace(' ', '-', $request->Mcategory_name));
        $maincategory->category_id       = $request->category_id;
        $maincategory->status           = $request->status;

        $maincategory->save();

        return redirect()->back()->with('message','Data Successfully Inserted');
    }
    
    // Display the specified Category.   
    public function showMainCategory($id)
    {
        $maincategory = MainCategory::find($id);
        return view('back.ecommerce.edit-main-category',compact('maincategory'));
    }
   
    // Show the form for editing the specified Category.    
    public function editMainCategory($id,Request $request)
    {
        $maincategory = MainCategory::find($id);

         if (Input::hasfile('image1')) {
           $file=Input::file('image1');
           $file->move(public_path(). '/assetsss/images/AdminProduct/', time().$file->getClientOriginalName());
           $maincategory->image1=time().$file->getClientOriginalName();           
        }

        $maincategory->Mcategory_name        = $request->Mcategory_name;
        $maincategory->Mcategory_description       = $request->Mcategory_description;
        $maincategory->featured       = $request->featured;
        $maincategory->slug       = strtolower(str_replace(' ', '-', $request->Mcategory_name));
        $maincategory->category_id       = $request->category_id;
        $maincategory->status  = $request->status;
        $maincategory->update();

        return redirect()->back()->with('message','Data Successfully Updated');

    }

  // Remove Main Category from the db 
    public function destroyMainCategory($id)
    {
        $maincategory = MainCategory::find($id);       
        $maincategory->delete();
        return redirect()->back()->with('message','Data Successfully Deleted');
    }

// add brands
 public function addbrands(Request $request)
    {
        
        $brands = new Brand;
       
        $brands->parent_id        = $request->parent_id;
        $brands->brand_name       = $request->brand_name   ;
        $brands->status           = $request->status;

        $brands->save();

        return redirect()->back()->with('message','Data Successfully Inserted');
    }

    // Seacrh Main Category
    public function searchMainCategory(Request $request)
    {
        $keyword = $request->product_keyword;
        $maincategory = MainCategory::where('Mcategory_name','LIKE','%'.$keyword.'%')->orWhere('Mcategory_description','LIKE','%'.$keyword.'%')->orWhere('featured','LIKE','%'.$keyword.'%')->orWhere('slug_name','LIKE','%'.$keyword.'%')->get();
        return view('back.ecommerce.searched-main-cat',compact('maincategory'));
    }

}
