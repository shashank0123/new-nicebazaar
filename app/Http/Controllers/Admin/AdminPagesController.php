<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Newsletter;
use App\Review;

class AdminPagesController extends Controller
{

	// ***************************For Newsletters****************************

	// To view all Newsletters Email
    public function viewNewsletter()
    {
    	$newsletter = Newsletter::all();
    	return view('back.ecommerce.newsletter',compact('newsletter'));
    }

    // To Edit Newsletter
    public function editNewsletter($id)
    {
    	$newsletter = Newsletter::find($id);
    	return view('back.ecommerce.edit-newsletter',compact('newsletter'));
    }

    // To Edit Newsletter
    public function updateNewsletter($id, Request $request)
    {
    	$newsletter = Newsletter::find($id);
    	$newsletter->status = $request->status;
    	$newsletter->update();
    	return redirect()->back()->with('message','Data Successfully Updated');
    }

    // To Delete Newsletter
    public function deleteNewsletter($id)
    {
    	$newsletter = Newsletter::find($id);
    	$newsletter->delete();
    	return redirect()->back()->with('message','Record Successfully Deleted');
    }


    // *************************For Reviews***************************

    // To view all Newsletters Email
    public function viewReview()
    {
    	$review = Review::all();        
    	return view('back.ecommerce.review',compact('review'));
    }

    // To Edit Review
    public function editReview($id)
    {
    	$review = Review::find($id);
    	return view('back.ecommerce.edit-review',compact('review'));
    }

    // To Edit Review
    public function updateReview($id, Request $request)
    {
    	$review = Review::find($id);
    	$review->rating = $request->rating;
        $review->review = $request->review;
        $review->status = $request->status;
    	$review->update();
    	return redirect()->back()->with('message','Data Successfully Updated');
    }

    // To Delete Review
    public function deleteReview($id)
    {
    	$review = Review::find($id);
    	$review->delete();
    	return redirect()->back()->with('message','Record Successfully Deleted');
    }
}
