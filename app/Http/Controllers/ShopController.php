<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\NetworkAfterRegisterJob;
use App\Jobs\SharesAfterRegisterJob;
use App\Repositories\MemberRepository;
use App\Repositories\SharesRepository;
use DB;


class ShopController extends AutoController
{
    public function postLogin () {
        $data = \Input::get('data');    
           
        try {            
                $user = \Sentinel::authenticate([
                'username'  =>  $data['username'],
                'password'  =>  $data['password'],
            ], (isset($data['remember'])));
             
            if (!$user) {
                throw new \Exception(\Lang::get('error.loginError'), 1);
                return false;
            }           

            if ($user->is_ban ) {
                throw new \Exception(\Lang::get('error.userBan'), 1);
                return false;
            }
        } catch (\Exception $e) {
            \Sentinel::logout();
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  $e->getMessage()
            ]);
        }

        return \Response::json([
            'type'      =>  'success',
            'message'   =>  \Lang::get('message.loginSuccess'),
            'redirect'  =>  route('home', ['lang' => \App::getLocale()])
        ]);
    }

}
