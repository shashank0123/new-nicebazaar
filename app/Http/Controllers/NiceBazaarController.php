<?php
namespace App\Http\Controllers;
use App\MainCategory;
use App\Product;
use App\Blog;
use App\Review;
use App\Testimonial;
use App\Models\Member;
use App\Cart;
use App\City;
use App\Newsletter;
use App\Banner;
use App\Offer;
use App\State;
use App\Country;
use App\Address;
use App\Order;
use App\Wishlist;
use App\OrderItem;
use App\User;
use App\Contact;
use App\CustomerPv;
use App\Faq;
use StdClass;
use Mail;
use Session;

use Illuminate\Http\Request;

class NiceBazaarController extends Controller
{
	public function index()
	{
		$cartproducts = array();
		$quantity = array();
		$i = 0;
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$banners = Banner::where('image_type','banner')->where('status','active')->get(); 
		
		$products = Product::where('status','Active')->orderBy('created_at','desc')->limit(4)->get();
		$blogs = Blog::where('status','active')->get();    	
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$i] = Product::where('id',$cart->product_id)->first();
				$quantity[$i] = $cart->quantity;
				$i++;
			}
		}
		$bills=session::get('bill');	
		$data=array('bills' =>$bills);
		return view('nicebazaar.index',compact('maincategory','products','blogs','cartproducts','quantity','banners'))->with($data);
	}
	
    // For Search Page
	public function getSearchProduct(Request $request)
	{
		$keyword = $request->product_keyword;
		$max_price = Product::where('status','Active')->max("sell_price");

		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}

		$lastproduct = Product::where('status','Active')->orderBy('created_at','desc')->first();
		$totals = array();
		$i=0;
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();

		foreach($maincategory as $row){
			$count=0;
			$count=Product::where('category_id',$row->id)->where('status','Active')->count();        	
			$category = MainCategory::where('category_id',$row->id)->where('status','Active')->get();
			if($category){
				foreach($category as $col){
					$count+=Product::where('category_id',$col->id)->where('status','Active')->count();        		
					$subcategory = MainCategory::where('category_id',$col->id)->where('status','Active')->get();
					if($subcategory){
						foreach($subcategory as $set){
							$count+=Product::where('category_id',$set->id)->where('status','Active')->count();       			
						}
					}
				}
				if($count==0)
					$totals[$i++] = 0;
				else					
					$totals[$i++] = $count;   
			}
		}
		$searched_products = Product::where('status','Active')->where('name','LIKE','%'.$keyword.'%')->get();				
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$message = "No products available";
		$sel_brands=Product::select('brand')->where('status','Active')->distinct()->get();
		
		return view('nicebazaar.search-product',compact('maincategory','searched_products','totals','lastproduct','max_price','cartproducts','quantity','message','sel_brands','keyword'));
	}

    // For Product Detail
	public function getProductDetail($id)
	{
		$bill = 0;
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$getproduct = Product::where('id',$id)->where('status','Active')->first();

		$products = Product::where('category_id',$getproduct->category_id)->where('status','Active')->get();

		$product_category = MainCategory::where('id',$getproduct->category_id)->where('status','Active')->first();
		$count_review = Review::where('product_id',$id)->count();
		$reviews = Review::where('product_id',$id)->orderBy('created_at','desc')->get();
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $pro){
				$product = Product::where('id',$pro->product_id)->first();
				$price = $product->sell_price;
				$bill = $bill + $pro->quantity*$price;
			}
		}		
		
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}

		return view('nicebazaar.product-detail
			',compact('maincategory','getproduct','product_category','count_review','reviews','products','bill','id','cartproducts','quantity'));
	}

	public function submitReview($id,Request $request)
	{
		$review = new Review;
		$review->name = $request->name;
		$review->email = $request->email;
		$review->rating = $request->rating;
		$review->review = $request->review;
		$review->product_id = $id;        
		$review->save();

		return redirect()->back()->with('message','Thank You For Your Review');
	}

	public function aboutUs()
	{
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$testimonials = Testimonial::all();
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}

		return view('nicebazaar.about-us',compact('maincategory','testimonials','cartproducts','quantity'));
	}

	public function getFAQ()
	{
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$testimonials = Testimonial::all();
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}

		$faqs = Faq::where('status','Active')->get();

		return view('nicebazaar.faq',compact('maincategory','testimonials','cartproducts','quantity','faqs'));
	}	

	public function getUserDetail()
	{
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$testimonials = Testimonial::all();
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		
		$countries = Country::all();
		$states = State::all();
// 		$cities = City::all();
		

		
		return view('nicebazaar.personal-info',compact('maincategory','testimonials','cartproducts','quantity','countries','states','cities'));
	}	

	public function contactUs()
	{
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$testimonials = Testimonial::all();
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}

		return view('nicebazaar.contact-us',compact('maincategory','testimonials','cartproducts','quantity'));
	}


	public function getReturnPolicy()
	{
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$testimonials = Testimonial::all();
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}

		return view('nicebazaar.return-policy',compact('maincategory','testimonials','cartproducts','quantity'));
	}

	public function getPrivacyPolicy()
	{
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$testimonials = Testimonial::all();
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}

		return view('nicebazaar.privacy-policy',compact('maincategory','testimonials','cartproducts','quantity'));
	}


	public function getTnC()
	{
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$testimonials = Testimonial::all();
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}

		return view('nicebazaar.terms',compact('maincategory','testimonials','cartproducts','quantity'));
	}













	public function getWishList()
	{
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}

		return view('nicebazaar.wishlist',compact('maincategory','cartproducts','quantity'));
	}	

	public function getCheckout()
	{
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$countries = Country::all();
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}
		return view('nicebazaar.checkout',compact('maincategory','countries','cartproducts','quantity'));
	}

	 //Dependent DropDown Of Countries , States & Cities
	public function getStateList(Request $request)
	{
		$request->country_id;
		$states = State::where("country_id",$request->country_id)->get();
		return response()->json($states);
	}

	public function getCityList(Request $request)
	{
		$request->state_id;
		$cities = City::where("state_id",$request->state_id)->get();

		return response()->json($cities);
	}

	// To return cart view
	public function getCart(Request $request)
	{
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();	
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		$pv=0;

		$allproducts = array();
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$pro_pv = $cartproducts[$cnt]->pv;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}

		return view('nicebazaar.cart',compact('maincategory','cartproducts','quantity','pv'));
	}


	public function getWish(Request $request)
	{
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();	
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		$pv=0;

		$allproducts = array();
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				// $pro_pv = $cartproducts[$cnt]->pv;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}

		return view('nicebazaar.wishlist',compact('maincategory','cartproducts','quantity','pv'));
	}

	//Category Wise Products Selection
	public function getCategoryProducts(Request $request,$id){
		$subcat = $request->subcategory;
		$keyword = $request->product_keyword;
		$max_price = Product::where('status','Active')->max("sell_price");
		$brands = Product::select('brand')->where('status','Active')->distinct()->get();

		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}

		$lastproduct = Product::where('status','Active')->orderBy('created_at','desc')->first();
		$totals = array();
		$i=0;
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();

		foreach($maincategory as $row){
			$count=0;
			$count=Product::where('category_id',$row->id)->where('status','Active')->count();        	
			$category = MainCategory::where('category_id',$row->id)->where('status','Active')->get();
			if($category){
				foreach($category as $col){
					$count+=Product::where('category_id',$col->id)->where('status','Active')->count();        		
					$subcategory = MainCategory::where('category_id',$col->id)->where('status','Active')->get();
					if($subcategory){
						foreach($subcategory as $set){
							$count+=Product::where('category_id',$set->id)->where('status','Active')->count();       			
						}
					}
				}
				if($count==0)
					$totals[$i++] = 0;
				else					
					$totals[$i++] = $count;   
			}
		}		

		$searched_products = array();
		$cat_ids = array();
		$cat_ids[0] = MainCategory::where('id',$id)->where('status','Active')->first();

		$cid = 1;
		$cnt_pro = 0;
		$idss = MainCategory::where('category_id',$id)->where('status','Active')->get();
		if($idss != null){
			foreach($idss as $ids){
				$cat_ids[$cid++] = $ids;
				$idsss = MainCategory::where('category_id',$ids->id)->where('status','Active')->get();
				if($idsss != null){
					foreach($idsss as $ds){
						$cat_ids[$cid++] = $ds;						
					}
				}
			}
		}
		
		for($j=0 ; $j<$cid ; $j++){
			$products = Product::where('category_id',$cat_ids[$j]->id)->where('status','Active')->get();
			if($products != null){
				foreach($products as $pro){
					$searched_products[$cnt_pro++] = $pro;
				}
			}			
		}
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$subcategory = MainCategory::where('category_id',$id)->where('status','Active')->get();
		$maincategory1 = MainCategory::where('id',$id)->where('status','Active')->select('Mcategory_name')->first();
		$sel_brands=Product::select('brand')->where('status','Active')->distinct()->get();

		return view('nicebazaar.categorywise-products',compact('maincategory','searched_products','totals','lastproduct','max_price','cartproducts','quantity','id','brands','maincategory','subcategory','maincategory1','sel_brands'));
	}

	public function brandproducts(Request $request,$id,$brand){
		$subcat = $request->subcategory;
		$keyword = $request->product_keyword;
		$max_price = Product::where('status','Active')->max("sell_price");
		$brands = Product::select('brand')->where('status','Active')->distinct()->get();

		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}

		$lastproduct = Product::where('status','Active')->orderBy('created_at','desc')->first();
		$totals = array();
		$i=0;
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();

		foreach($maincategory as $row){
			$count=0;
			$count=Product::where('category_id',$row->id)->where('status','Active')->count();  	
			$category = MainCategory::where('category_id',$row->id)->where('status','Active')->get();
			if($category){
				foreach($category as $col){
					$count+=Product::where('category_id',$col->id)->where('status','Active')->count();        		
					$subcategory = MainCategory::where('category_id',$col->id)->where('status','Active')->get();
					if($subcategory){
						foreach($subcategory as $set){
							$count+=Product::where('category_id',$set->id)->where('status','Active')->count();       			
						}
					}
				}
				if($count==0)
					$totals[$i++] = 0;
				else					
					$totals[$i++] = $count;   
			}
		}

		$searched_products = array();
		$cat_ids = array();
		$cat_ids[0] = MainCategory::where('id',$id)->where('status','Active')->first();
		$cid = 1;
		$cnt_pro = 0;
		$idss = MainCategory::where('category_id',$id)->where('status','Active')->get();
		if($idss != null){
			foreach($idss as $ids){
				$cat_ids[$cid++] = $ids;
				$idsss = MainCategory::where('category_id',$ids->id)->where('status','Active')->get();
				if($idsss != null){
					foreach($idsss as $ds){
						$cat_ids[$cid++] = $ds;						
					}
				}
			}
		}
		
		for($j=0 ; $j<$cid ; $j++){
			$products = Product::where('category_id',$cat_ids[$j]->id)->where('status','Active')->get();
			if($products != null){
				foreach($products as $pro){
					$searched_products[$cnt_pro++] = $pro;
				}
			}			
		}
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$subcategory = MainCategory::where('category_id',$id)->where('status','Active')->get();
		$maincategory1 = MainCategory::where('id',$id)->select('Mcategory_name')->first();
		$sel_brands=Product::select('brand')->where('status','Active')->distinct()->get();
		$searched_products  = Product::where('brand', $brand)->where('status','Active')->get();
		
		return view('nicebazaar.categorywise-products',compact('maincategory','searched_products','totals','lastproduct','max_price','cartproducts','quantity','id','brands','maincategory','subcategory','maincategory1','sel_brands'));
	}

	// For Ajax Call (Adding product to cart)
	public function addToCart(Request $request)
	{
		$count = new StdClass;
		$cart = new Cart;
		$product_id = $request->id;
		if (session()->get('cart') != null && session()->get('count') != null){
			$cart = session()->get('cart');
			$count = session()->get('count');			
		}
		else {
			$cart = array();
			$count=0;
		}

		if (isset($cart[$product_id]->quantity)){
			$cart[$product_id]->quantity = $cart[$product_id]->quantity + 1;
		}
		else{
			$item = new StdClass;
			$item->product_id = $product_id;
			$item->quantity = 1;
			$cart[$product_id] = $item;
			$count++;
		}
		$request->session()->put('cart',$cart);
		$request->session()->put('count',$count);		

		if(!$cart){
			echo "Something Went Worng";        
		}
		return response()->json(['status' => 200, 'message' => 'data retrieved','count' => $count]); 
	}

    // For Ajax Call (Adding product to cart)
	public function deleteSessionData(Request $request,$id)
	{	
		$flag = 1 ;
		$count = session()->get('count');
		$count--;
		$bill = 0;
		$pv = 0;
		$showcount = $count;
		$cart = session()->get('cart');
		$cart[$request->id] = null;
		$cart = array_filter($cart);
		$request->session()->put('cart',$cart);
		$request->session()->put('count',$count);
		foreach(session()->get('cart') as $pro){
			$product = Product::where('id',$pro->product_id)->first();
			$price = $product->sell_price;
			$product_pv = $product->pv;
			$bill = $bill + $pro->quantity*$price;
			$pv = $pv + $pro->quantity*$product_pv;
		}
		if($flag == 1)		
			return response()->json(['status' => 200, 'message' => 'Data Deleted Successfully', 'showcount' => $showcount,'bill' => $bill ,'pv'=>$pv]);
		else		
			return response()->json(['status' => 201, 'message' => 'Something Went Wrong']);	
	}

	public function increment(Request $request,$id,$quant)
	{
		$product = Product::where('id',$request->id)->first();
		$bill = 0;
		$pv = 0;
		$cart = session()->get('cart');
		$ids = $request->id ;		
		if (array_key_exists($ids,$cart)) {
			$cart[$ids]->quantity = $cart[$ids]->quantity + 1;
		}                    
		else {
			$cart[$ids]->quantity = 1;            
		}  
		$quant = $cart[$ids]->quantity;
		$sub = $quant*$product->sell_price;

		$request->session()->put('cart', $cart);

		foreach(session()->get('cart') as $pro){
			$product = Product::where('id',$pro->product_id)->first();
			$pv = $pv+($product->pv*$pro->quantity);
			$price = $product->sell_price;
			$bill = $bill + $pro->quantity*$price;
		}
		return response()->json(['status' => 200, 'message' => 'Data Successfully Updated','sub' => $sub , 'quant' => $quant , 'bill' => $bill, 'pv' => $pv]);
	}

	public function decrement(Request $request,$id,$quant)
	{
		$product = Product::where('id',$request->id)->first();
		$bill = 0;
		$pv = 0;
		$cart = session()->get('cart');
		$ids = $request->id ;		
		if ($cart[$ids]->quantity > 0) {
			$cart[$ids]->quantity = $cart[$ids]->quantity - 1;			
		}                    
		else {			
			$cart[$ids]->quantity = 0;            
		}  
		$quant = $cart[$ids]->quantity;
		$sub = $quant*$product->sell_price;

		$request->session()->put('cart', $cart);

		foreach(session()->get('cart') as $pro){
			$product = Product::where('id',$pro->product_id)->first();
			$pv = $pv+($product->pv*$pro->quantity);
			$price = $product->sell_price;
			$bill = $bill + $pro->quantity*$price;
		}

		return response()->json(['status' => 200, 'message' => 'Data Successfully Updated', 'sub' => $sub , 'quant' => $quant , 'bill' => $bill , 'pv' => $pv] );
	}

	// For Ajax Call (Adding product to cart)
	public function addToWishList(Request $request)
	{
		$wishlistItem = new Wishlist;
		$product_id = $request->product_id;
		$user_id = $request->user_id;
		$check = Wishlist::where('user_id',$user_id)->where('product_id',$product_id)->first();

		if(empty($check)){
			$wishlistItem->product_id = $product_id;
			$wishlistItem->user_id = $user_id;		
			$wishlistItem->save();
		}
		else{
		}
		$sel_wishlist=Wishlist::where('user_id',$user_id)->count();
		
		return response()->json(['status' => 200, 'message' => 'data retrieved','sel_wishlist' => $sel_wishlist]); 
	}

    // For Ajax Call (Adding product to wishlist)
	public function deleteWishList(Request $request)
	{			
		$pid = $request->pid;
		$uid = $request->uid;
		$wishlist = Wishlist::where('product_id',$pid)->where('user_id',$uid)->first();
		$wishlist->delete();

		return response()->json(['status' => 200, 'message' => 'Data Deleted Successfully']);	
	}

	//Delete All Items From WishList
	public function clearWishList()
	{	
		$wishlist = session()->forget('wishlist');
		if($wishlist == null)
			return response()->json(['status' => 200, 'message' => 'Data Deleted Successfully']);
		else		
			return response()->json(['status' => 201, 'message' => 'Something Went Wrong']);	
	}

	//Search All
	public function searchAll(){
		$product = Product::where('status','Active')->get();
		return response()->json(['status' => 200, 'message' => 'Data Retrieved']);
	}

	// Add All To Cart
	public function addAllToCart(Request $request)
	{
		$wishlist = new Cart;
		if(session()->get('wishlist') != null){
			$wishlist = session()->get('wishlist');			
			foreach($wishlist as $list){
				$cart = new Cart;
				$product_id = $list->product_id;
				if (session()->get('cart') != null){
					$cart = session()->get('cart');
				}
				else 
					$cart = array();
				if (isset($cart[$product_id]->quantity)){
					$cart[$product_id]->quantity = $list->quantity ;
				}
				else{
					$item = new StdClass;
					$item->product_id = $product_id;
					$item->quantity =$list[$product_id]->quantity;
					$cart[$product_id] = $item;
				}
				$request->session()->put('cart',$cart);
				if(!$cart)
				{
					$message = "Something Went Worng";        
				}
				else{
					$message = "Data Added To Cart";
				}
			}
		}
		session()->flush('wishlist');
		return response()->json(['status' => 200, 'message' => $message]);
	}

	public function productsCat(Request $request){
		$searched_products = array();
		$count = 0 ;
		$id = array();
		$i=1;
		$id[0] = $request->cat_id;
		if($id[0] == 0)
		{
			$products = Product::where('status','Active')->get();
			foreach($products as $pro){
				$searched_products[$count++] = $pro;
			}
		}
		else{
			$cat_id = MainCategory::where('category_id',$id[0])->where('status','Active')->get();
			if($cat_id != null){
				foreach($cat_id as $cat){
					$id[$i] = $cat->id;
					$sub_id =MainCategory::where('category_id',$id[$i])->where('status','Active')->get();
					$i++;
					if($sub_id != null){
						foreach($sub_id as $sub){
							$id[$i] = $sub->id;
							$i++;
						}
					}
				}
			}
			for($j=0 ; $j<$i ; $j++){
				$products= Product::where('category_id',$id[$j])->where('status','Active')->get();
				foreach($products as $pro){
					$searched_products[$count++] = $pro;
				}
			}	
		}		
		return view('nicebazaar.product_cat',compact('searched_products'));
	}

	//Filter products on the basis of there prices
	public function priceFilter(Request $request){
		$max = $request->max;
		$min = $request->min;
		$send_id = $request->cat_id;
		$brand = $request->brand;
		$weight = $request->weight;
		$discount = $request->discount;

		if($discount != null){
			$disc = explode(',',$discount);
		}

		$category_ids = array();
		$category_ids[0] = $send_id;
		$j = 1;

		$searched_products = array();
		$i=0;	

		$cat = MainCategory::where('category_id',$send_id)->get();
		if($cat != null){
			foreach($cat as $cid){
				$category_ids[$j++] = $cid->id;
				$subcat = MainCategory::where('category_id',$cid->id)->get();
				if($subcat != null){
					foreach($subcat as $sid){
						$category_ids[$j++] = $sid->id;
					}
				}
			}
		}

		if($brand=="" && $weight=="" && $discount==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('status','Active')->get();
				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}
		else if($brand=="" && $weight==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('status','Active')->get();
				foreach($products as $pro){
					$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;

					if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
						$searched_products[$i++] = $pro;
					} 
				}
			}
		}
		else if($brand=="" && $discount==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('status','Active')->get();

				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}
		else if($weight=="" && $discount==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('brand',$brand)->where('status','Active')->get();

				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}
		else if($weight==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('brand',$brand)->where('status','Active')->get();

				foreach($products as $pro){
					$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;

					if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
						$searched_products[$i++] = $pro;
					} 
				}
			}
		}
		else if($brand==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('status','Active')->get();

				foreach($products as $pro){
					$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;

					if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
						$searched_products[$i++] = $pro;
					} 
				}
			}
		}
		else if($discount==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('brand',$brand)->where('status','Active')->get();

				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}
		else{
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('brand',$brand)->where('status','Active')->get();

				foreach($products as $pro){
					$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;

					if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
						$searched_products[$i++] = $pro;
					} 
				}
			}
		}
		return view('nicebazaar.product_cat',compact('searched_products'));
	}

	//For Category Wise Product Filter
	public function categoryWiseProduct(Request $request){
		$searched_products = array();
		$count = 0 ;
		$id = array();
		$i=1;
		$id[0] = $request->cat_id;
		if($id[0] == 0)
		{
			$products = Product::where('status','Active')->get();
			foreach($products as $pro){
				$searched_products[$count++] = $pro;
			}
		}
		else{
			$cat_id = MainCategory::where('category_id',$id[0])->where('status','Active')->get();
			if($cat_id != null){
				foreach($cat_id as $cat){
					$id[$i] = $cat->id;
					$sub_id =MainCategory::where('category_id',$id[$i])->where('status','Active')->get();
					$i++;
					if($sub_id != null){
						foreach($sub_id as $sub){
							$id[$i] = $sub->id;
							$i++;
						}
					}
				}
			}
			for($j=0 ; $j<$i ; $j++){
				$products= Product::where('category_id',$id[$j])->where('status','Active')->get();
				foreach($products as $pro){
					$searched_products[$count++] = $pro;
				}
			}	
		}

		return view('nicebazaar.cat-wise-product',compact('searched_products'));
	}

	public function saveAddress(Request $request){
		$key = $request->shipping;
		$id = $request->uid;
		$bill = $request->total_bill;
		$pv = $request->total_pv;
		
		$phoneno = 0;
		
		$address = new Address;
		$address2 = new Address;
		$cart = new Cart;
		$order = new Order;
		$order_item = new OrderItem;

		$checkSessionID = Cart::where('session_id',session()->getId())->first();

		if($checkSessionID != null){
		}
		else{
			$cart->session_id = session()->getId();
			$cart->user_id = $id;
			$cart->cart = json_encode(session()->get('cart'));
			$cart->date = date('Y-m-d H:m:s');
			$cart->save();
		}

		$check = Address::where('user_id',$id)->first();

		if(!empty($check)){
			$check->address = $request->address1;
			$check->country_id = $request->country_id1;
			$check->state_id = $request->state_id1;
			$check->city_id = $request->city_id1;
			$check->pin_code = $request->pin_code1;
			$check->phone = $request->phone1;
			$check->address_category = $request->address_category1;
			$check->update();
			$address = $check;
		$phoneno = $request->phone1;
		}
		else{
			$address->address = $request->address1;
			$address->user_id = $id;
			$address->country_id = $request->country_id1;
			$address->state_id = $request->state_id1;
			$address->city_id = $request->city_id1;
			$address->pin_code = $request->pin_code1;
			$address->phone = $request->phone1;
			$address->address_category = $request->address_category1;
			$address->save();
		$phoneno = $request->phone1;
		}

		if($key == 'different'){

			$check2 = Address::where('address',$request->address2)->where('user_id',$id)->first();
			$final = "";
			if(!empty($check2)){
				$check2->user_id = $id;
			$check2->address = $request->address2;
			$check2->country_id = $request->country_id2;
			$check2->state_id = $request->state_id2;
			$check2->city_id = $request->city_id2;
			$check2->pin_code = $request->pin_code2;
			$check2->phone = $request->phone2;
			$check2->address_category = $request->address_category2;
			$check2->update();
			$final = $check2;
			}
			else{
				$address2->user_id = $id;
			$address2->address = $request->address2;
			$address2->country_id = $request->country_id2;
			$address2->state_id = $request->state_id2;
			$address2->city_id = $request->city_id2;
			$address2->pin_code = $request->pin_code2;
			$address2->phone = $request->phone2;
			$address2->address_category = $request->address_category2;
			$address2->save();
			$final = $address2;
			}
			
		$phoneno = $request->phone1;

		$order->user_id = $id;
		$order->billing_address =  $address->id;
		$order->shipping_address =  $final->id;
		$order->price =  $bill;
		$order->payment_method =  'payumoney';
		$order->date = date('Y-m-d H:m:s');
		$order->save();

		}

		else{ 

			$order->user_id = $id;
		$order->billing_address =  $address->id;
		$order->shipping_address =  $address->id;
		$order->price =  $bill;
		$order->payment_method =  'payumoney';
		$order->date = date('Y-m-d H:m:s');
		$order->save();

		}

		$order_id = Order::where('user_id',$id)->where('status',NULL)->orderBy('created_at','DESC')->first();
		$i=0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$order_item = new OrderItem;
				$order_item->order_id = $order_id->id;
				$order_item->item_id = $cart->product_id;
				$price = Product::where('id',$cart->product_id)->first();
				$order_item->quantity	= $cart->quantity;
				$order_item->price = $cart->quantity * $price->sell_price;
				$order_item->save();
				$i++;
			}						
		}



		$user_info = User::where('id',$id)->first();

		$check_pv = CustomerPv::where('order_id',$order_id->id)->first();

		if(!empty($check_pv)){
			$check_pv->pv = $pv;
			$check_pv->username = $user_info->username;
			$check_pv->order_id = $order_id->id;
			$check_pv->update();
		}
		else{
			$cpv = new CustomerPv;

			$cpv->pv = $pv;
			$cpv->username = $user_info->username;
			$cpv->order_id = $order_id->id;
			$cpv->save();
		}

		$checkorder = Order::where('user_id',$id)->orderBy('created_at','DESC')->first();
		return view('nicebazaar.payment',compact('user_info','checkorder','phoneno'));


	}

	// public function checkoutForm(Request $request,$id){
		
	// 	$cart = new Cart;
	// 	$cpv = new CustomerPv;
	// 	$order = new Order;
	// 	// $address = new Address(); 
	// 	$checkSessionID = Cart::where('session_id',session()->getId())->first();
	// 	$id = $request->id;
	// 	$phoneno = $request->phone1;
	// 	die;
	// 	$bill = $request->bill;
	// 	$ship = $request->ship;
	// 	$id = $request->uid;

	// 	if($checkSessionID != null){
	// 	}
	// 	else{
	// 		$cart->session_id = session()->getId();
	// 		$cart->user_id = $id;
	// 		$cart->cart = json_encode(session()->get('cart'));
	// 		$cart->date = date('Y-m-d H:m:s');
	// 		$cart->save();
	// 	}		

	// 	$check = Address::where('user_id',$id)->first();
	// 	if($check != null){
	// 		$check->user_id 		= $id;
	// 		$check->address 		= $request->address;
	// 		$check->country_id 	= $request->country_id;
	// 		$check->state_id 		= $request->state_id;
	// 		$check->city_id 		= $request->city_id;
	// 		$check->pin_code 		= $request->pin_code;
	// 		$check->phone 		= $request->phone;
	// 		$check->update();
	// 	}
	// 	else{
	// 		$address->user_id 		= $id;
	// 		$address->address 		= $request->address;
	// 		$address->country_id 	= $request->country_id;
	// 		$address->state_id 		= $request->state_id;
	// 		$address->city_id 		= $request->city_id;
	// 		$address->pin_code 		= $request->pin_code;
	// 		$address->phone 		= $request->phone;
	// 		$address->save();
	// 	}		
		
	// 	$order->user_id = $id;
	// 	$order->billing_address =  $address_id->bill;
	// 	$order->shipping_address =  $address_id->ship;
	// 	$order->price =  $request->total_bill;
	// 	$order->payment_method =  'payumoney';
	// 	$order->date = date('Y-m-d H:m:s');
	// 	$order->save();

	// 	$order_id = Order::where('user_id',$id)->where('status',NULL)->orderBy('created_at','DESC')->first();
	// 	$i=0;
	// 	if(session()->get('cart') != null){
	// 		foreach(session()->get('cart') as $cart){
	// 			$order_item = new OrderItem;
	// 			$order_item->order_id = $order_id->id;
	// 			$order_item->item_id = $cart->product_id;
	// 			$price = Product::where('id',$cart->product_id)->first();
	// 			$order_item->quantity	= $cart->quantity;
	// 			$order_item->price = $cart->quantity * $price->sell_price;
	// 			$order_item->save();
	// 			$i++;
	// 		}						
	// 	}
	// 	$user_info = User::where('id',$id)->first();

	// 	$check_pv = CustomerPv::where('order_id',$order_id->id)->first();

	// 	if(!empty($check_pv)){
	// 		$cpv->pv = $request->total_pv;
	// 		$cpv->username = $user_info->username;
	// 		$cpv->order_id = $order_id->id;
	// 		$cpv->update();
	// 	}
	// 	else{

	// 		$cpv->pv = $request->total_pv;
	// 		$cpv->username = $user_info->username;
	// 		$cpv->order_id = $order_id->id;
	// 		$cpv->save();
	// 	}

	// 	$checkorder = Order::where('user_id',$id)->orderBy('created_at','DESC')->first();
	// 	return view('nicebazaar.payment',compact('user_info','checkorder','phoneno'));
	// }

	public function checkoutForm1(Request $request,$id){
		$success = "";
		if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0){
			$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
			if(strcasecmp($contentType, 'application/json') == 0){
				$data = json_decode(file_get_contents('php://input'));
				$hash=hash('sha512', $data->key.'|'.$data->txnid.'|'.$data->amount.'|'.$data->pinfo.'|'.$data->fname.'|'.$data->email.'|||||'.$data->udf5.'||||||'.$data->salt);
				$json=array();			
				$json['success'] = $hash;
				echo json_encode($json);		
			}
			exit(0);
		}
	}  

	public function getSuccess(Request $request){
		$postdata = $_POST;
			// var_dump($postdata);
			// die;
		$user = User::where('email',$postdata['email'])->first();
			// echo $user;
			// die;
			// $member = Member::where('user_id',$user->id)->first();	

		$updateorder = new Order;
		$updateorder = Order::where('user_id',$user->id)->orderBy('created_at','DESC')->first();
		$updateorder->transaction_id = $postdata['txnid'];
		$updateorder->encrypted_payment_id = $postdata['encryptedPaymentId'];
		$updateorder->status = $postdata['txnStatus'];
		$updateorder->update();

		$transaction_details = Order::where('user_id',$updateorder->user_id)->whereNotNull('transaction_id')->orderBy('updated_at','DESC')->get();

		session()->forget('cart');
		return view('nicebazaar.result',compact('transaction_details'));
	}

	//To show session data on Mini Cart
	public function showMiniCart(){
		$cartproducts = array();
		$quantity = array();
		$i = 0;
		$bill = 0;
		$pv=0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$i] = Product::where('id',$cart->product_id)->first();
				$quantity[$i] = $cart->quantity;
				$i++;
			}
			$j=0;
			foreach(session()->get('cart') as $cart){
				$quantity[$j];
				$j++;
			}
			foreach(session()->get('cart') as $pro){
				$product = Product::where('id',$pro->product_id)->first();
				$price = $product->sell_price;
				$bill = $bill + $pro->quantity*$price;
				$pv = $pv + $product->pv*$pro->quantity;
			}
			session(['bill'=>$bill]);
		}	

		return view('nicebazaar.minicart',compact('cartproducts','quantity','bill','pv'))->render();
	}

	//Show Sorted Products on Search Category Page
	public function sorting(Request $request){
		$searched_products = array();
		$i=0;
		$sort_type = $request->sort_type;
		$category_id = $request->category_id;
		$min = $request->min;
		$max = $request->max;
		$brand = $request->brand;
		$weight = $request->weight;
		$discount = $request->disc;
		$disc = explode(',',$discount);

		$category_ids = array();
		$category_ids[0] = $category_id;
		$j = 1;

		$searched_products = array();
		$i=0;	

		$cat = MainCategory::where('category_id',$category_id)->get();
		if($cat != null){
			foreach($cat as $cid){
				$category_ids[$j++] = $cid->id;
				$subcat = MainCategory::where('category_id',$cid->id)->get();
				if($subcat != null){
					foreach($subcat as $sid){
						$category_ids[$j++] = $sid->id;
					}
				}
			}
		}

		if($brand=="" && $weight=="" && $discount==""){
			for($k=0 ; $k<$j ; $k++){				
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('status','Active')->get();

				foreach($products as $pro){										
					$searched_products[$i++] = $pro;					
				}
			}
		}
		else if($brand=="" && $weight==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('status','Active')->get();

				foreach($products as $pro){
					$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;

					if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
						$searched_products[$i++] = $pro;
					} 
				}
			}
		}
		else if($weight=="" && $discount==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('brand',$brand)->where('status','Active')->get();

				foreach($products as $pro){

					$searched_products[$i++] = $pro;
				}				
			}
		}
		else if($brand=="" && $discount==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('brand',$brand)->where('status','Active')->get();

				foreach($products as $pro){
					$searched_products[$i++] = $pro;
				} 			
			}
		}
		else if($brand==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('status','Active')->get();

				foreach($products as $pro){
					$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;
					if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
						$searched_products[$i++] = $pro;
					} 
				}
			}
		}
		else if($weight==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('brand',$brand)->where('status','Active')->get();

				foreach($products as $pro){
					$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;
					if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
						$searched_products[$i++] = $pro;
					} 
				}
			}
		}
		else if($discount==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('brand',$brand)->where('status','Active')->get();

				foreach($products as $pro){
					$searched_products[$i++] = $pro;
				}
			}
		}
		else{
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('brand',$brand)->where('status','Active')->get();
				foreach($products as $pro){
					$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;
					if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
						$searched_products[$i++] = $pro;
					} 
				}
			}
		}

		if($sort_type == 'name'){
			$name = array_column($searched_products, 'name');
			array_multisort($name, SORT_ASC, $searched_products);
		}		
		else if($sort_type == 'low-high'){
			$sell_price = array_column($searched_products, 'sell_price');
			array_multisort($sell_price, SORT_ASC, $searched_products);
		}
		else if($sort_type == 'high-low'){
			$sell_price = array_column($searched_products, 'sell_price');
			array_multisort($sell_price, SORT_DESC, $searched_products);
		}

		return view('nicebazaar.product_cat',compact('searched_products'));
	}

	public function addProductToCart(Request $request)
	{
		$count = new StdClass;
		$cart = new Cart;
		$product_id = $request->id;
		$user_id = $request->uid;
		$product_quantity = $request->quantity;
		$product = Product::where('id',$product_id)->first();

		if (session()->get('cart') != null && session()->get('count') != null){
			$cart = session()->get('cart');
			$count = session()->get('count');			
		}
		else {
			$cart = array();
			$count=0;
		}

		if (isset($cart[$product_id]->quantity)){
			$cart[$product_id]->quantity = $cart[$product_id]->quantity + 1;
		}
		else{
			$item = new StdClass;
			$item->product_id = $product_id;
			$item->quantity = $product_quantity;
			$item->pv = $product->pv;
			$cart[$product_id] = $item;
			$count++;
		}

		$request->session()->put('cart',$cart);
		$request->session()->put('count',$count);	

		$search =Wishlist::where('user_id',$user_id)->where('product_id',$product_id)->first();
		if(!empty($search)){
			$search->delete();
		}	
		if(!$cart)
		{
			echo "Something Went Worng";        
		}
		return response()->json(['status' => 200, 'message' => 'data retrieved','count' => $count]); 
	}

	public function sendContactUsForm(Request $request)
	{
		$response = new StdClass;
		$response->status = 200;
		$response->msg = 'Something went wrong';
		$contact_user = new Contact;

		$name = $request->name;
		$email = $request->email;
		$phone = $request->phone;
		$subject = $request->subject;
		$content = $request->message;

		$contact_user->name = $name;
		$contact_user->email = $email;
		$contact_user->phone = $phone;
		$contact_user->subject = $subject;
		$contact_user->message_content = $content;
		$contact_user->response_status = 'Pending';

		$contact_user->save();
		if($contact_user){
			// $data = array('Name'=>$contact_user->name,'Email'=>$contact_user->email, 'Subject'=>$contact_user->subject, 'Message'=>$contact_user->message_content);
			// Mail::send('nicebazaar.contact-form', $data, function($message) use ( $name,$email,$subject,$content)
			// {   
			// 	$message->from('no-reply@m-biz.in', 'Mailer');
			// 	$message->to('Admin@m-biz.in', 'Admin')->subject('Query Mail');
			// });		
			$response->msg ="Thank you .We will reply you soon";
			return response()->json($response);
		}
		else{
			echo " Something Went Wrong";
			$response->msg =" Something Went Wrong";
		}

		return response()->json($response);	    
	}

	public function updateProductQuantity(Request $request){
		$ids=$request->id;
		$quant=$request->quantity;
		$cart = session()->get('cart');

		if (array_key_exists($ids,$cart)) {
			$cart[$ids]->quantity = $quant;
			echo "data updated ".$cart[$ids]->quantity;
		}                    
		else {
			echo "New inserted quantity"; 
			$item = new StdClass;
			$item->product_id = $ids;
			$item->quantity = $quant; 
			$cart[$ids] = $item;
		}  
		$request->session()->put('cart', $cart);
	}

	public function addWishList(Request $request)
	{
		$wishlist = new WishList;

		$product_id = $request->product_id;
		$user_id = $request->user_id;
		$check = Wishlist::where('user_id',$user_id)->where('product_id',$product_id)->first();

		if(empty($check)){
			$wishlistItem->product_id = $product_id;
			$wishlistItem->user_id = $user_id;		
			$wishlistItem->save();
		}
		else{
		}

		return response()->json('Data Successfully Inserted');
	}

	public function getSizedProduct(Request $request){
		$size = $request->size;
		$color = $request->color;
		$send_id = $request->cat_id;
		$min = $request->min_cost;
		$max = $request->max_cost;

		$category_ids = array();
		$category_ids[0] = $request->cat_id;
		$j = 1;
		$searched_products = array();
		$i=0;	

		$cat = MainCategory::where('category_id',$request->cat_id)->where('status','Active')->get();
		if($cat != null){
			foreach($cat as $cid){
				$category_ids[$j++] = $cid->id;
				$subcat = MainCategory::where('category_id',$cid->id)->where('status','Active')->get();
				if($subcat != null){
					foreach($subcat as $sid){
						$category_ids[$j++] = $sid->id;
					}
				}
			}
		}
		for($k=0 ; $k<$j ; $k++){
			$category_ids[$k]."   ";
			if($color != "Choose Color"){
				$products = Product::where('category_id',$category_ids[$k])->where('product_size',$size)->where('status','Active')->get();
			}
			else{
				$products = Product::where('category_id',$category_ids[$k])->where('product_size',$size)->where('status','Active')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->get();
			}
			if($products != null){
				foreach($products as $product){
					$searched_products[$i++] = $product;
				}				
			} 
		}
		shuffle($searched_products);
		return view('ecommerce2.product_cat',compact('searched_products','send_id'));
	}

	public function countSessionData(){
		$count = count(session()->get('cart'));
		$countwishlist = Wishlist::where('user_id',$member->id)->count();

		return response()->json($count,$countwishlist);
	}

	public function getdiscountProduct(Request $request){
		$discount = $request->discount;
		$send_id = $request->cat_id;
		$min = $request->min;
		$max = $request->max;
		$brand = $request->brand;
		$weight = $request->weight;

		$disc=explode(",", $discount);

		$category_ids = array();
		$category_ids[0] = $request->cat_id;
		$j = 1;

		$searched_products = array();
		$get_products = array();
		$i=0;	
		$cat = MainCategory::where('category_id',$request->cat_id)->where('status','Active')->get();
		if($cat != null){
			foreach($cat as $cid){
				$category_ids[$j++] = $cid->id;
				$subcat = MainCategory::where('category_id',$cid->id)->where('status','Active')->get();
				if($subcat != null){
					foreach($subcat as $sid){
						$category_ids[$j++] = $sid->id;
					}
				}
			}
		}

		if($weight=="" && $brand==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('status','Active')->get();

				foreach($products as $pro){
					$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;
					if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
						$searched_products[$i++] = $pro;
					} 
				}
			}
		}
		else if($weight==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('brand',$brand)->where('status','Active')->get();

				foreach($products as $pro){
					$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;
					if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
						$searched_products[$i++] = $pro;
					} 
				}
			}
		}

		else if($brand==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('status','Active')->get();

				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}
		else{
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('brand',$brand)->where('status','Active')->get();

				foreach($products as $pro){
					$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;
					if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
						$searched_products[$i++] = $pro;
					} 
				}
			}
		}

		shuffle($searched_products);
		return view('nicebazaar.product_cat',compact('searched_products','send_id'));
	}

	public function brandFilter(Request $request)	{
		$max = $request->max;
		$min = $request->min;
		$send_id = $request->cat_id;
		$brand = $request->brand;
		$weight = $request->weight;
		$discount = $request->discount;

		if($discount != null){
			$disc = explode(',',$discount);
		}

		$category_ids = array();
		$category_ids[0] = $send_id;
		$j = 1;

		$searched_products = array();
		$i=0;	

		$cat = MainCategory::where('category_id',$send_id)->get();
		if($cat != null){
			foreach($cat as $cid){
				$category_ids[$j++] = $cid->id;
				$subcat = MainCategory::where('category_id',$cid->id)->get();
				if($subcat != null){
					foreach($subcat as $sid){
						$category_ids[$j++] = $sid->id;
					}
				}
			}
		}

		if($weight=="" && $discount==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('brand',$brand)->where('status','Active')->get();
				$products;

				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}			
		}
		else if($weight==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('brand',$brand)->where('status','Active')->get();
				$products;

				foreach($products as $pro){
					$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;

					if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
						$searched_products[$i++] = $pro;
					} 
				}
			}			
		}

		else if($discount==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('brand',$brand)->where('status','Active')->get();
				$products;

				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}			
		}
		else{
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('brand',$brand)->where('status','Active')->get();
				$products;

				foreach($products as $pro){
					$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;
					if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
						$searched_products[$i++] = $pro;
					} 
				}
			}			
		}

		return view('nicebazaar.product_cat',compact('searched_products'));
	}

	public function getSearchSizedProduct(Request $request){
		$max = $request->max;
		$min = $request->min;
		$keyword = $request->cat_id;
		$brand = $request->brand;
		$weight = $request->weight;
		$discount = $request->discount;

		if($discount != null){
			$disc = explode(',',$discount);
		}

		$searched_products = array();
		$i=0;

		if($brand=="" && $weight=="" && $discount==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('name','LIKE',$keyword)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('status','Active')->get();
				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}
		else if($brand=="" && $weight==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('name','LIKE',$keyword)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('status','Active')->get();
				foreach($products as $pro){
					$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;

					if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
						$searched_products[$i++] = $pro;
					} 
				}
			}
		}
		else if($brand=="" && $discount==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('name','LIKE',$keyword)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('status','Active')->get();

				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}
		else if($weight=="" && $discount==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('name','LIKE',$keyword)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('brand',$brand)->where('status','Active')->get();

				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}
		else if($weight==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('name','LIKE',$keyword)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('brand',$brand)->where('status','Active')->get();

				foreach($products as $pro){
					$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;
					if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
						$searched_products[$i++] = $pro;
					} 
				}
			}
		}
		else if($brand==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('name','LIKE',$keyword)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('status','Active')->get();

				foreach($products as $pro){
					$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;
					if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
						$searched_products[$i++] = $pro;
					} 
				}
			}
		}
		else if($discount==""){
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('name','LIKE',$keyword)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('brand',$brand)->where('status','Active')->get();

				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}
		else{
			for($k=0 ; $k<$j ; $k++){
				$products = Product::where('name','LIKE',$keyword)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('brand',$brand)->where('status','Active')->get();

				foreach($products as $pro){
					$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;
					if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
						$searched_products[$i++] = $pro;
					} 
				}
			}
		}

		return view('nicebazaar.product_cat',compact('searched_products'));
	}

	public function getSearchDiscountProduct(Request $request){
		$discount = $request->discount;
		$keyword = $request->cat_id;
		$min = $request->min;
		$max = $request->max;
		$brand = $request->brand;
		$weight = $request->weight;
		$disc=explode(",", $discount);		

		$searched_products = array();
		$get_products = array();
		$i=0;	

		if($weight=="" && $brand==""){
			$products = Product::where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('status','Active')->get();
			foreach($products as $pro){
				$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;
				if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
					$searched_products[$i++] = $pro;
				} 
			}
		}
		else if($weight==""){
			$products = Product::where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('brand',$brand)->where('status','Active')->get();

			foreach($products as $pro){
				$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;
				if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
					$searched_products[$i++] = $pro;
				} 
			}
		}		
		else if($brand==""){
			$products = Product::where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('status','Active')->get();

			if($products != null){
				foreach($products as $product){
					$searched_products[$i++] = $product;
				}				
			}
		}
		else{
			$products = Product::where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('brand',$brand)->where('status','Active')->get();

			foreach($products as $pro){
				$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;
				if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
					$searched_products[$i++] = $pro;
				} 
			}
		}

		shuffle($searched_products);
		return view('nicebazaar.product_cat',compact('searched_products','send_id'));
	}

	//Show Sorted Products on Search Category Page
	public function sortingSearch(Request $request){
		$searched_products = array();
		$i=0;
		$sort_type = $request->sort_type;
		$keyword = $request->category_id;
		$min = $request->min;
		$max = $request->max;
		$brand = $request->brand;
		$weight = $request->weight;
		$discount = $request->disc;
		$disc = explode(',',$discount);

		$searched_products = array();
		$i=0;	

		if($brand=="" && $weight=="" && $discount==""){				
			$products = Product::where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('status','Active')->get();

			foreach($products as $pro){										
				$searched_products[$i++] = $pro;					
			}			
		}
		else if($brand=="" && $weight==""){				
			$products = Product::where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('status','Active')->get();

			foreach($products as $pro){
				$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;
				if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
					$searched_products[$i++] = $pro;
				} 
			}			
		}
		else if($weight=="" && $discount==""){			
			$products = Product::where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('brand',$brand)->where('status','Active')->get();

			foreach($products as $pro){
				$searched_products[$i++] = $pro;
			}			
		}
		else if($brand=="" && $discount==""){
			$products = Product::where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('brand',$brand)->where('status','Active')->get();

			foreach($products as $pro){
				$searched_products[$i++] = $pro;
			} 			
		}
		else if($brand==""){
			$products = Product::where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('status','Active')->get();

			foreach($products as $pro){
				$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;
				if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
					$searched_products[$i++] = $pro;
				} 
			}			
		}
		else if($weight==""){
			$products = Product::where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('brand',$brand)->where('status','Active')->get();

			foreach($products as $pro){
				$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;
				if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
					$searched_products[$i++] = $pro;
				} 
			}
		}
		else if($discount==""){
			$products = Product::where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('brand',$brand)->where('status','Active')->get();

			foreach($products as $pro){
				$searched_products[$i++] = $pro;
			}
		}
		else{
			$products = Product::where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('brand',$brand)->where('status','Active')->get();

			foreach($products as $pro){
				$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;
				if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
					$searched_products[$i++] = $pro;
				} 
			}
		}

		if($sort_type == 'name'){
			$name = array_column($searched_products, 'name');
			array_multisort($name, SORT_ASC, $searched_products);
		}		
		else if($sort_type == 'low-high'){
			$sell_price = array_column($searched_products, 'sell_price');
			array_multisort($sell_price, SORT_ASC, $searched_products);
		}
		else if($sort_type == 'high-low'){
			$sell_price = array_column($searched_products, 'sell_price');
			array_multisort($sell_price, SORT_DESC, $searched_products);
		}

		return view('nicebazaar.product_cat',compact('searched_products'));
	}

	public function brandSearchFilter(Request $request)	{
		$max = $request->max;
		$min = $request->min;
		$keyword = $request->cat_id;
		$brand = $request->brand;
		$weight = $request->weight;
		$discount = $request->discount;

		if($discount != null){
			$disc = explode(',',$discount);
		}
		$searched_products = array();
		$i=0;	

		if($weight=="" && $discount==""){
			$products = Product::where('name','LIKE','%'.$keyword.'%' )->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('brand',$brand)->where('status','Active')->get();
			$products;

			if($products != null){
				foreach($products as $product){
					$searched_products[$i++] = $product;
				}				
			}
		}
		else if($weight==""){
			$products = Product::where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('brand',$brand)->where('status','Active')->get();
			$products;

			foreach($products as $pro){
				$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;
				if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
					$searched_products[$i++] = $pro;
				} 
			}
		}		
		else if($discount==""){
			$products = Product::where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('brand',$brand)->where('status','Active')->get();
			$products;

			if($products != null){
				foreach($products as $product){
					$searched_products[$i++] = $product;
				}				
			}
		}
		else{
			$products = Product::where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$weight.',%')->where('brand',$brand)->where('status','Active')->get();
			$products;

			foreach($products as $pro){
				$dcnt = (($pro->mrp - $pro->sell_price)*100)/$pro->mrp;
				if($dcnt>=$disc[0] && $dcnt<=$disc[1]){						
					$searched_products[$i++] = $pro;
				} 
			}
		}

		return view('nicebazaar.product_cat',compact('searched_products'));
	}

	public function submitNewsletter(Request $request){
		$response = new StdClass;
		$response->status = 200;
		$response->message = 'Something went wrong';

		$news = new Newsletter;
		$news->email = $request->email;
		$news->save();
		if($news){
			$response->message = "Thank you. We will reply you soon.";
		}

		return response()->json($response);

	}

	public function getOffers($id){
		$offer = Offer::find($id);
		// echo $offer;
		// die;
		$offers = Offer::where('id','!=',$id)->get();
		// echo $offers;
		// die;
		$cartproducts = array();
		$quantity = array();
		$i = 0;
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$banners = Banner::where('image_type','banner')->where('status','active')->get(); 

		$products = Product::where('status','Active')->orderBy('created_at','desc')->limit(4)->get();
		$blogs = Blog::where('status','active')->get();    	
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$i] = Product::where('id',$cart->product_id)->first();
				$quantity[$i] = $cart->quantity;
				$i++;
			}
		}
		$bills=session::get('bill');	
		$data=array('bills' =>$bills);

		return view('nicebazaar.offers',compact('maincategory','products','blogs','cartproducts','quantity','banners','offer','offers'))->with($data);
	}

	public function getOrders($type,$id){
			// echo $type." / ".$id;
		if($type == 'user'){}
			else{
				$member = Member::where('id',$id)->first();
				$id = $member->user_id;
			}

			$transaction_details = Order::where('user_id',$id)->whereNotNull('transaction_id')->orderBy('updated_at','DESC')->get();
			// echo $transaction_details;

			return view('nicebazaar.summary',compact('transaction_details')); 
		}

		}