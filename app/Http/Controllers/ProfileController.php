<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Address;
use Hash;

class ProfileController extends Controller
{
     public function updateUserInfo(Request $request){
     	$message = 'Something went wrong';
     	$user = User::where('email',$request->email)->first();
        $address = Address::where('user_id',$user->id)->first();
        if(!empty($address)){
        	$address->address = $request->address;
        	$address->country_id = $request->country_id;
        	$address->state_id = $request->state_id;
        	$address->city_id = $request->city_id;
        	$address->pin_code = $request->pin_code;
        	$address->phone = $request->phone;
        	$address->update();
        	$message = 'Information updated successfully';
        }else{
        	$address->user_id = $user->id;
        	$address->address = $request->address;
        	$address->country_id = $request->country_id;
        	$address->state_id = $request->state_id;
        	$address->city_id = $request->city_id;
        	$address->pin_code = $request->pin_code;
        	$address->phone = $request->phone;
        	$address->save();
        	$message = 'Information updated successfully';
        }

        $user->first_name = $request->username;
        $user->update();
        return response()->json(['message' => $message]);
    }

    public function updatePassword(Request $request){
    	$message = 'Something went wrong';
    	$user = User::where('id',$request->user_id)->first();
    	
    	if($request->new_password == $request->re_password){
    		$user->password = Hash::make($request->new_password);
    		$user->update();
    		$message = "Password updated successfully";
    	}
    	else{
    		$message = "Password did not match";
    	}
        return response()->json(['message' => $message]);


    }
}
