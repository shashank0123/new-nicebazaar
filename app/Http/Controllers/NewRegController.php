<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
// use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User;
use App\Leads;
use App\MainCategory;
use Mail;
use Log;
use StdClass;
// use PHPMailer\PHPMailer\PHPMailer;
// use PHPMailer\PHPMailer\Exception;

class NewRegController extends Controller
{
    public function Register(Request $request)
    {
        $response = new StdClass;
        $response->status = '200';
        $response->message = 'Something went wrong';
        $member = new User;
        $checkemail = User::where('email',$request->email)->where('username',$request->username)->first();

        Log::info($request);
        
        if($checkemail)
        { 
            $response->message = "Already Registered"; 
        }
        else{

           $this->validate($request,[                
                'username' => 'required|string|max:30',
                'email' => 'required|string|email',
                'mobile' => ['required','regex:/[0-9]{10}/'],
                'password' => 'required|min:6',
            ]);



            if($request->username == null  ||  $request->email == null  ||  $request->mobile == null || $request->password == null || $request->repass == null)
            {
                $response->message = 'Please fill All the Fields';
                $response->username = $request->username;
                $response->email = $request->email;
                $response->mobile = $request->mobile;
                $response->name = $request->name;
                $response->password = $request->password;
                return response()->json($response);
                exit();
            }
            else{

                $pass= $request->password;
                $repass= $request->repass;
                if($pass == $repass){

                    $member = \Sentinel::registerAndActivate([
                    'email'   => $request->email,
                    'username'  =>  $request->username,
                    'first_name'  =>  $request->name,
                    'password'  =>   $request->password,
                    'phone'  =>  $request->mobile,
                    'permissions' =>  [
                        'user' => true,
                    ]
                ]);


                    // $member->username = $request->username;
                    // $member->phone    = $request->mobile;
                    // $member->email    = $request->email;
                    // $member->password    = Hash::make($pass);
               // $member->type = 'direct';
               // $member->is_ban = '1';
               // $member->permissions = '{"member":true}';

                    $member->save();
                    $response->message = "Registered Successfully"; 
                }
                else{
                    $response->message = "Password Did Not Match";
                }               
            }        
            // else{
            //     $mobile = $checkemail->phone;
            //     $message = "Your%20OTP%20for%20MBiz%20is%20$checkemail->otp";
            //     $url = "http://smpp1.webtechsolution.co/http-tokenkeyapi.php?authentic-key=39316261636b73746167653032333637331559059857&senderid=MBIZTL&route=1&number=$mobile&message=$message"; 
            // // die;
            // // $url ="http://103.247.98.91/API/SendMsg.aspx?uname=backstage023&pass=backstage023&send=MBIZTL&dest=$mobile&msg=$message";  
            // // $url = "";
            //     $c = curl_init();
            //     curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
            //     curl_setopt($c,CURLOPT_HTTPGET ,1);

            //     curl_setopt($c, CURLOPT_URL, $url);
            //     $contents = curl_exec($c);
            //     if (curl_errno($c)) { 
            //         echo 'Curl error: ' . curl_error($c);
            //     }
            //     else{
            //         curl_close($c);
            //     }
            //     if($user){
            //         $response->message ="Data Inserted Successfully";
            //         return response()->json($response);
            //     }

            //     else{
            //         echo " Something Went Wrong";
            //     }
            // }  

            return response()->json($response);
            exit();             
        }
    }

        // else
        // {
        //     if($request->username == null  ||  $request->email == null  ||  $request->mobile == null )
        //     {
        //         $response->message = 'Please fill All the Fields';
        //         $response->username = $request->username;
        //         $response->email = $request->email;
        //         $response->mobile = $request->mobile;
        //         return response()->json($response);
        //         exit();
        //     }

            // else{

            //     $user->username = $request->username;
            //     $user->phone    = $request->mobile;
            //     $user->email    = $request->email;
            //     $user->otp      = rand(100000, 999999);
            //     $user->otp_time = date('Y-m-d H:m:s') ;

            //     $user->save();
            // }

            // $mobile = $user->phone;
            // $message = "Your%20OTP%20for%20MBiz%20is%20$user->otp";
            // $url = "http://smpp1.webtechsolution.co/http-tokenkeyapi.php?authentic-key=39316261636b73746167653032333637331559059857&senderid=MBIZTL&route=1&number=$mobile&message=$message"; 
            // // die;
            // // $url ="http://103.247.98.91/API/SendMsg.aspx?uname=backstage023&pass=backstage023&send=MBIZTL&dest=$mobile&msg=$message";  
            // // $url = "";
            // $c = curl_init();
            // curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
            // curl_setopt($c,CURLOPT_HTTPGET ,1);

            // curl_setopt($c, CURLOPT_URL, $url);
            // $contents = curl_exec($c);
            // if (curl_errno($c)) {
            //     echo 'Curl error: ' . curl_error($c);
            // }
            // else{
            //     curl_close($c);
            // }
    //         if($user){
    //             $response->message ="Data Inserted Successfully";
    //             return response()->json($response);
    //         }

    //         else{
    //             echo " Something Went Wrong";
    //         }
    //     }        
    // }

    // public function verifyotp(Request $request)
    // {
    //     $response = new StdClass;
    //     $response->status = 200;
    //     $response->message = 'Something went wrong';

    //     $findUser = Leads::where('email',$request->email)->first();

    //     if($findUser->verify_otp != 'verified')
    //     {
    //         if($findUser->otp == $request->otp) {                
    //             $response->message = 'Registered Successfully. Please Wait we will call you shortly.';
    //             $response->user_id = $findUser->id;
    //             $findUser->verify_otp ='verified';
    //             $findUser->update();
    //             return response()->json($response);

    //         }
    //         else {                

    //             $response->message = "Wrong OTP";
    //             return response()->json($response);
    //         }
    //     }
    //     else
    //     {
    //         $response->message = "Already Verified";
    //         return response()->json($response);
    //     }        
     // }

    public function checkoutForm(Request $request,$id)
    {      
        $lead = Leads::where('id',$id)->first();
        return view('membership-payment',compact('lead'));
    }

    public function checkoutForm1(Request $request,$id){
        $success = "";
        if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0){
    //Request hash
            $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';   
            if(strcasecmp($contentType, 'application/json') == 0){
                $data = json_decode(file_get_contents('php://input'));
                $hash=hash('sha512', $data->key.'|'.$data->txnid.'|'.$data->amount.'|'.$data->pinfo.'|'.$data->fname.'|'.$data->email.'|||||'.$data->udf5.'||||||'.$data->salt);
                $json=array();          
                $json['success'] = $hash;
                echo json_encode($json);        
            }
            exit(0);
        }

    }

    public function getSuccess(Request $request){

        $lead = new Leads;

        $post_data = $_POST;  


        $user_email = $post_data['email'];
        $status = $post_data['txnStatus'];
        $txnid = $post_data['txnid'];

        if($status == 'SUCCESS'){
            $lead = Leads::where('email',$user_email)->first();
            $lead->payment_status = 'success';
            $lead->update();
        }



        $data = array('Email'=>$user_email, 'TransactionID'=>$txnid, 'Payment_Status'=>$status);
        Mail::send('mail', $data, function($message) use ($user_email, $txnid, $status)
        {   
            $message->from('no-reply@m-biz.in', 'Mailer');
            $message->to('Admin@m-biz.in', 'Admin')->subject('Successfull Registration');
        });



        return view('register');
    }


//     public function getSuccesssssss(Request $request){    

//         $message = "Something went wrong";
//         $lead = new Leads;

//         $post_data = $_POST;       

//         $user_email = $post_data['email'];
//         $status = $post_data['txnStatus'];
//         $txnid = $post_data['txnid'];

//         if($status = 'SUCCESS'){
//             $lead = Leads::where('email',$user_email)->first();
//             $lead->payment_status = 'success';
//             $lead->update();
//         }
//         $mail = new PHPMailer(true);

//         try {
//             //Server settings
//             $mail->SMTPDebug = 2;                                       // Enable verbose debug output
//             $mail->isSMTP();                                            // Set mailer to use SMTP
//             $mail->Host       = 'mail.m-biz.in:587';  // Specify main and backup SMTP servers
//             $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
//             // $mail->Username   = 'Admin@m-biz.in';                     // SMTP username
//             $mail->Username   = 'no-reply@m-biz.in';                     // SMTP username
//             $mail->Password   = 'qwerty123456789';                               // SMTP password
//             $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
//             // $mail->Port       = 587;                                    // TCP port to connect to

//             //Recipients
//             $mail->setFrom('no-reply@m-biz.in', 'Mailer');
//             $mail->addAddress('Admin@m-biz.in', 'Admin');     // Add a recipient
//             // $mail->addAddress('ellen@example.com');               // Name is optional
//             // $mail->addReplyTo('info@example.com', 'Information');
//             // $mail->addCC('shashank@backstagesupporters.com');
//             // $mail->addBCC('shashank@backstagesupporters.com');

//             // Attachments
//             // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//             // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

//             // Content
//             $mail->isHTML(true);                                  // Set email format to HTML
//             $mail->Subject = 'Memeber Registration';
//             $mail->Body    = "New User Registered \n Email : ".$user_email."\n Transaction Id : ".$txnid."\n Payment Status : ".$status;
//             $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

//             $mail->send();
//             // $message = 'Message has been sent';
//         }
//         catch (Exception $e) {
//            // $message = "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
//         }
// // 
//         // echo $message;
//         // die();
//         return view('register');
//     }
}